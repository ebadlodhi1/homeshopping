package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.Cart.CouponExample;
import com.homeshopping.admin.homeshoping10.POJO.PlaceOrderPOJO.PlaceOrderExample;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface PlaceOrderAPi {
    @POST("checkout/commitOrder")
    Call<PlaceOrderExample> getDetails(@Header("data") String data, @Body RequestBody body);
}

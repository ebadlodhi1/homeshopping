package com.homeshopping.admin.homeshoping10.POJO.PlaceOrderPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderId {@SerializedName("orderId")
@Expose
private String orderId;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}

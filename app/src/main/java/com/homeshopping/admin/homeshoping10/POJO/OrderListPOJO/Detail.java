package com.homeshopping.admin.homeshoping10.POJO.OrderListPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {
    @SerializedName("orderId")
    @Expose
    private Integer orderId;
    @SerializedName("orderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("orderTotal")
    @Expose
    private String orderTotal;
    @SerializedName("orderDate")
    @Expose
    private Integer orderDate;
    @SerializedName("productImage")
    @Expose
    private String productImage;
    @SerializedName("productQuantity")
    @Expose
    private Integer productQuantity;
    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public Integer getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Integer orderDate) {
        this.orderDate = orderDate;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }
}

package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Interfaces.addnewshipaddressAPI;
import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddress;
import com.homeshopping.admin.homeshoping10.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.LoginActivity.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class AddShippingAddressActivity extends AppCompatActivity {
    EditText shipaddressfnfull_name,shipaddressfnlast_name,shipaddressfncompany,shipaddressfnaddress
            ,shipaddressfncity,shipaddressfnphone;
    TextView title;
    Button add;
    String acname = "a";
    String firstname= "",adress= "",city= "",pno = "";
    Boolean flag =true;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_shipping_address);
        add  = findViewById(R.id.button16);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Add Shipping Adresses");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Add shipping Address Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        shipaddressfnfull_name = findViewById(R.id.shipaddressfnfull_name);
        shipaddressfnlast_name = findViewById(R.id.shipaddressfnlast_name);
        shipaddressfncompany = findViewById(R.id.shipaddressfncompany);
        shipaddressfnaddress =findViewById(R.id.shipaddressfnaddress);
        shipaddressfncity = findViewById(R.id.shipaddressfncity);
        shipaddressfnphone = findViewById(R.id.shipaddressfnphone);
        title = findViewById(R.id.textView40);
        title.setText("Add Shipping Details");
        add.setText("Add");
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = getIntent().getExtras();
                acname = bundle.getString("activityname", "a");
                firstname = shipaddressfnfull_name.getText().toString().trim();
                adress = shipaddressfnaddress.getText().toString().trim();
                city = shipaddressfncity.getText().toString().trim();
                pno = shipaddressfnphone.getText().toString().trim();
                flag = true;
                if (firstname.isEmpty()||firstname.matches("")) {
                    shipaddressfnfull_name.setError("Enter first name");
                    shipaddressfnfull_name.requestFocus();
                    flag = false;
                }

                if (adress.isEmpty()||adress.matches("")) {
                    shipaddressfnaddress.setError("Enter address");
                    shipaddressfnaddress.requestFocus();
                    flag = false;
                }
                if (city.isEmpty()||city.matches("")) {
                    shipaddressfncity.setError("Enter City");
                    shipaddressfncity.requestFocus();
                    flag = false;
                }
                if (pno.isEmpty()||pno.matches("")) {
                    shipaddressfnphone.setError("Enter Phone no");
                    shipaddressfnphone.requestFocus();
                    flag = false;
                }
                if (flag) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    addnewshipaddressAPI api = retrofit.create(addnewshipaddressAPI.class);
                    Call<shipaddress> call = api.getDetails(MainActivity.Tokendata,shipaddressfnfull_name.getText().toString(),
                            shipaddressfnlast_name.getText().toString(),
                            shipaddressfncompany.getText().toString(),
                            shipaddressfnaddress.getText().toString(),
                            shipaddressfncity.getText().toString(),
                            shipaddressfnphone.getText().toString(), Dashboard.prefConfig.readID());
                    call.enqueue(new Callback<shipaddress>() {
                        @Override
                        public void onResponse(Call<shipaddress> call, Response<shipaddress> response) {
                            if (response.isSuccessful())
                            {
                            String res = response.body().getStatus().toString();
                            if (res.equals("1")) {
//                                Toast.makeText(getApplicationContext(), "Added", Toast.LENGTH_SHORT).show();
                                if (acname.equals("checkout")) {
                                    Intent intent = new Intent(getApplicationContext(), CheckOutActivity.class);
                                    startActivity(intent);
                                    finish();


                                } else {
                                    Intent intent = new Intent(getApplicationContext(), ShipingAddressActivity.class);
                                    startActivity(intent);
                                    finish();

                                }
                            }
                            }
                            else
                            {
                            Toast.makeText(getApplicationContext(), ""+response.message(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<shipaddress> call, Throwable t) {

                        }
                    });
                }
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);
        finish();

    }
}

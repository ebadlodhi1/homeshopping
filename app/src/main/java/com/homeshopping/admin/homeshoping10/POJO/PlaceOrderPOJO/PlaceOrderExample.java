package com.homeshopping.admin.homeshoping10.POJO.PlaceOrderPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlaceOrderExample {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("orderId")
    @Expose
    private List<OrderId> orderId = null;
    @SerializedName("productId")
    @Expose
    private String productId;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public List<OrderId> getOrderId() {
        return orderId;
    }

    public void setOrderId(List<OrderId> orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}

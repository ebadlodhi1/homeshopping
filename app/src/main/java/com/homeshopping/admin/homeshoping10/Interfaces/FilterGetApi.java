package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.FilterList.Filterlist;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface FilterGetApi {
              @GET("products/getProductFilters")
              Call<Filterlist> getDetails(@Header("data") String data, @Query("categoryId") String categoryId);
}

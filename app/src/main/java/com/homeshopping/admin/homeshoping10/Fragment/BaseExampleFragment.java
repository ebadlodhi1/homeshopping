package com.homeshopping.admin.homeshoping10.Fragment;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;

import com.arlib.floatingsearchview.FloatingSearchView;

public abstract class BaseExampleFragment extends Fragment
{
        private BaseExampleFragmentCallbacks mCallbacks;

public interface BaseExampleFragmentCallbacks{

    void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset);

    void attachSearchViewActivityDrawer(FloatingSearchView searchView);

    boolean onActivityBackPress();

    void onAttachSearchViewToDrawer(FloatingSearchView searchView);
}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseExampleFragmentCallbacks) {
            mCallbacks = (BaseExampleFragmentCallbacks) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement BaseExampleFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    protected void attachSearchViewActivityDrawer(FloatingSearchView searchView){
        if(mCallbacks != null){
            mCallbacks.onAttachSearchViewToDrawer(searchView);
        }
    }

    public abstract boolean onActivityBackPress();
}

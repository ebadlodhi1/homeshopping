package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface AddcartAPI {

    @FormUrlEncoded
    @POST("/cart/addCart")
    Call<generalstatusmessagePOJO> getDetails(@Header ("data") String data, @Field("productId") String productId, @Field("quantity") String quantity, @Field("customerId") String userId, @Field("combinaitonId") String combinaitonId, @Field("fieldId") String fieldId, @Field("fieldOption") String fieldOption, @Field("bankId") String bankId);

}

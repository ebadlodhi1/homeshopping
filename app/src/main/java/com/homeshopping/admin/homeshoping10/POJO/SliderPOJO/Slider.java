package com.homeshopping.admin.homeshoping10.POJO.SliderPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Slider {


    @SerializedName("imagePath")
    @Expose
    private String imagePath;
    @SerializedName("pageId")
    @Expose
    private Integer pageId;
    @SerializedName("pageType")
    @Expose
    private String pageType;
    @SerializedName("title")
    @Expose
    private String title;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Interfaces.RegisterApi;
import com.homeshopping.admin.homeshoping10.POJO.RegisterPOJO;
import com.homeshopping.admin.homeshoping10.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;


public class SignupActivity extends AppCompatActivity {
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(),Dashboard.class);
        startActivity(intent);
        finish();
    }
            Spinner spinnermobcode;
    String[] mobcode = {"300","301","302","303","304","305","306","307","308","309",
            "310","311","312","313","314","315","316","317","318","319","320","321","322",
            "323","324","325","326","327","328","329","330","331","332","333","334","335","336","337","338"
            ,"339","340","341","342", "343","344","345","346","347","348","349","350","355","356","357","358"
            ,"359","360"};

 String[] countryname = {
         "Select city..","Abbotabad","Abdul Hakim", "Adda Bun Bosan", "Adda Lar", "Adda Zakheera", "Ahmed Pur East",
         "Ahmed Pur Lamma", "Ahmed Pur Sial", "Al Ain", "Ali Pur Chatta", "Alipur", "Aminpur Banglow", "Arif Wala",
         "Attock", "Awaran","Badin", "Bagh", "Bahawalnagar", "Bahawalpur", "Bajwar", "Banglow Gogera", "Bannu","Bara Kahu","Barkhan",
         "Barnala", "Basir Pur", "Batkhela", "Battgram", "Bela", "Bhai Pharu", "Bhakkar", "Bhalwal", "Bhawana", "Bhera", "Bhimber",
         "Bhiria City", "Bhiria Road", "Bhit Shah", "Buchiana Mandi", "Budhla Sant", "Bunair", "Burewala","Chachro", "Chak Jhumra",
         "Chak Sawari", "Chakwal", "Chaman", "Charsadda", "Chashma", "Chawinda", "Chenab Nagar", "Chichawatni", "Chiniot", "Chishtian",
         "Chitral", "Choa Syden Shah", "Chor Cantt", "Chowk Azam", "Chowk Sarwer Shaheed", "Chunian", "Dadu", "Daharki", "Dahranwala", "Dakota",
         "Dalbandin", "Dara Adm Khel", "Dargai", "Dary Khan", "Daska", "Daud Khel", "Daulatpur", "Depal Pur", "Dera Allahyar", "Dera Ghazi Khan",
         "Dera Ismail Khan", "Deramurad Jamal", "Dhanot", "Dhodhak", "Digri", "Dijkot", "Dina", "Dinga", "Dokri", "Dolat Nagar", "Dour", "Dukki",
         "Dulle Wala", "Dunya Pur", "Ellah Abad", "Faisalabad", "Farroqabad", "Fateh Jang", "Fatehpur", "Fazil Pur", "Feroz Watowan", "Feroza", "Forte Abbas",
         "Gadoon Amazai", "Gaggo Mandi", "Gambat", "Garh Mor", "Gari Khairo", "Gari Yasin", "Gawadar", "Ghakar", "Gharo", "Ghazi Abad", "Ghotki", "Ghous Pur",
         "Gilgit", "Gojra", "Golarchi", "Guddu", "Gujarkhan", "Gujranwala", "Gujrat","Hazari",  "Hafizabad", "Hajira", "Hala", "Hangu", "Haripur", "Harnouli",
         "Haroonabad", "Hasilpur", "Hassan Abdal", "Hatter", "Havellian", "Hazro", "Hub", "Hujra Shamuqeem", "Humak", "Hunza", "Hyderabad", "Iqbal Nagar", "Iskandarabad",
         "Islamabad", "Islamkot", "Issa Khel", "Jabelali", "Jacobabad", "Jalal Pur Bhattian", "Jalalpur Jattan", "Jalalpurpirwala", "Jamaldin Wali", "Jampur", "Jamshoro",
         "Jand Wala", "Jaranwala", "Jarwar", "Jatoi", "Jawarian", "Jhang", "Jhangira", "Jhanian", "Jhat Pat", "Jhelum", "Jouharabad", "Kabir Wala", "Kacha Kho", "Kahota",
         "Kala Bagh", "Kala Shah Kaku", "Kalar Kahar", "Kalat,571", "Kaloor Kot", "Kamalia", "Kambar Ali Khan", "Kamer Moshani", "Kamoke", "Kana Nau", "Kandh Kot", "Kandiari",
         "Kandyaro", "Karachi", "Karak", "Karor Lalesan", "Karor Pakka", "Kashmore", "Kasowal", "Kasur", "Katlang", "Khaipur Tamewal","Khairpur", "Khairpur Nathan", "Khan Bela",
         "Khanewal", "Khanpur", "Khanqa Sharif", "Khaplu", "Kharan", "Kharian", "Kharian Cantt", "Khazakhela", "Khewra Dandot", "Khidder Wala", "Khipro", "Khushab", "Khuzdar", "Kohat",
         "Kot Abdul Malik", "Kot Addu", "Kot Chutta",    "Kot Ghulam Muhd", "Kot Mitthan", "Kot Momin", "Kot Radha Kisha", "Kot Samabah", "Kotla", "Kotli-A.Kashmir", "Kotri", "Kundian",
         "Kunri", "Lahore", "Laki Marwat","Lalamusa", "Lalian", "Landikotal", "Larkana", "Layyah", "Liaquatpur", "Lodhran", "Lora Lai", "Machi Goth", "Mailsi", "Makhdoom Aali", "Malak Wal",
         "Mamun Kanjan", "Mandi Bahauddin", "Manga Mandi", "Mangla", "Mangowal", "Mankera", "Manshera", "Mardan", "Mastung", "Matiari", "Matli", "Mehar", "Mehmoodkot", "Mehrab Pur", "Mian Chanoo",
         "Mianwali", "Minchanabad", "Minchanabad", "Mingora (Swat)", "Mirpur A.K", "Mirpur Khas", "Mirpur Mathelo", "Mirpur Sakro", "Mirwah Gorchani", "Mithi", "Mityari", "Moro", "Mubarak Pur", "Much",
         "Multan", "Murid Wala", "Muridkey", "Murree", "Muslim Bagh", "Muzaffarabad Ak", "Muzaffargarh", "Nankana Sahib", "Narowal", "Narwala Bangla", "Nasirabad", "Nauabad", "Naudero", "Naushera", "Nawabshah",
         "New Jatoi", "New Saeedabad", "Noorpur", "Noshero Feroz", "Noshki", "Nowshera", "Nowshera Virka", "Nurpur Thal", "Oghi", "Okara", "Okara Cantt", "Ole Khi", "Ole Lhe", "Ole Rwp", "Pabbi", "Pahar Pur", "Painsra",
         "Pak Pattan Shar", "Palandri", "Panjgoor", "Pannu Akil", "Panu Aqil Cantt", "Pasni", "Pasroor", "Patoki (Lahore)", "Peshawar", "Petaro", "Phalia", "Pind Dadan Khan", "Pindi Bhatian", "Pindi Gheb", "Piplan", "Piryalo",
         "Pishin", "Qaboola", "Qadirpur Rawan", "Qalandrabad", "Qazi Ahmed", "Qila Dedar Sing", "Quaidabad", "Quetta", "Radhan Station", "Rahimyarkhan", "Raiwand", "Rajana", "Rajanpur", "Ranipur", "Ras Al Khaimah", "Ratto Dero",
         "Rawalakot", "Rawalpindi", "Rawat", "Renala Khurd", "Rohri", "Sadiqabad", "Sahiwal", "Saidu Sharif", "Sakardu", "Sakrand", "Samandri", "Samaro", "Sanawan", "Sanghar", "Sangla Hill", "Sargodah", "Sarwar Shaheed", "Satiana Bangla",
         "Sehwan", "Serai Naurang", "Shabqadar", "Shahdad Kot", "Shahdadpur", "Shahdara", "Shahkot", "Shakar Garh", "Sharaqpur", "Sharjah", "Sheikhupura", "Shikarpur", "Shinkiari", "Shorkot", "Shujaabad", "Sialkot", "Sibi", "Silanwali", "Sinjhoro",
         "Sita Road", "Srai Alamgeer", "Sui", "Sujawal", "Sukkur", "Sumbrial", "Sundar Adda", "Swabi", "Swat", "Takhat Bai", "Talagang", "Tall", "Tandlianwala", "Tando Adam", "Tando Allayar", "Tando Bagho", "Tando Jam", "Tando Mohd Khan", "Tank", "Tarbela",
         "Taunsa Sharif", "Taxila", "Temargarah", "Ternol", "Tharo Shah", "Thatta", "Thull", "Tibba Sultan", "Tobatek-Singh", "Topi", "Turbat", "Ubaro", "Uch Sharif", "Umerkot", "Umm Al Quwain", "Upper Dir", "Usta Mohammad", "Uthal", "Vari Dir", "Vehari", "Wah",
         "Wan Bachran", "Warah", "Wazirabad","Winder", "Yazman Mandi", "Zafarwal", "Zahirpeer","Zhob"};
    TextView signuptext;
    Button createaccount;
    EditText full_name,fulllastname,full_emailemail,full_emailpass,full_emailaddress; AutoCompleteTextView spinnercity;
    String name = "",lastname = "",email = "",password = "",address = "",phoneno = "",city = "";
    boolean flag=true;
ProgressBar progressBar11;
    EditText pno;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Sign Up");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Sign up Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());

//        spinnermobcode = findViewById(R.id.spinnermobcode);
//        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,mobcode);
//        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        //Setting the ArrayAdapter data on the Spinner
//        spinnermobcode.setAdapter(aa);
        progressBar11 = findViewById(R.id.progressBar11);
        progressBar11.setVisibility(View.GONE);


        spinnercity = findViewById(R.id.spinnercity);
        ArrayAdapter aaa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,countryname);
        aaa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spinnercity.setAdapter(aaa);
        signuptext = (TextView) findViewById(R.id.signuptext);
        Spannable word = new SpannableString(" Login");
        word.setSpan(new ForegroundColorSpan(Color.rgb(171,208,55)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        signuptext.append(word);

        createaccount = findViewById(R.id.button6);

//        full_name,fulllastname,full_emailemail,full_emailpass,full_emailaddress;
        full_name = findViewById(R.id.full_name);
        fulllastname = findViewById(R.id.fulllastname);
        full_emailemail = findViewById(R.id.full_emailemail);
        full_emailpass = findViewById(R.id.full_emailpass);
        full_emailaddress = findViewById(R.id.full_emailaddress);
        pno = findViewById(R.id.pno);



        createaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//String name = "",lastname = "",email = "",password = "",address = "",phoneno = "",city = "";

        name = full_name.getText().toString();
        lastname = fulllastname.getText().toString();
        email = full_emailemail.getText().toString();
        password = full_emailpass.getText().toString();
        address = full_emailaddress.getText().toString();
        city = spinnercity.getText().toString();
        phoneno  = "0"+pno.getText().toString();
//                Toast.makeText(SignupActivity.this, ""+name+"\n"+lastname+"\n"+
//                        email+"\n"+password+"\n"+address+"\n"+city+"\n"+phoneno, Toast.LENGTH_SHORT).show();
            String pnno = pno.getText().toString();
              flag = true;
              if(name.isEmpty())
              {
                  full_name.setError("Please enter first name");
                  full_name.requestFocus();
                  flag = false;
              }
                if(lastname.isEmpty())
                {
                    fulllastname.setError("Please enter last name");
                    fulllastname.requestFocus();
                    flag = false;
                }
                if(email.isEmpty())
                {
                    full_emailemail.setError("Please enter email");
                    full_emailemail.requestFocus();
                    flag = false;
                }
                else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                {
                    full_emailemail.setError("Invalid email");
                    full_emailemail.requestFocus();
                    flag=false;

                }
                if(password.isEmpty())
                {
                    full_emailpass.setError("Please enter password");
                    full_emailpass.requestFocus();
                    flag = false;
                }
                if(address.isEmpty())
                {
                    full_emailaddress.setError("Please enter address");
                    full_emailaddress.requestFocus();
                    flag = false;
                }
                if(city.isEmpty())
                {
                    spinnercity.setError("Please Enter City");
                    spinnercity.requestFocus();

                    flag = false;
                }
                if(pnno.isEmpty())
                {
                    pno.requestFocus();
                    pno.setError("Please enter phone number");
                    flag = false;
                }
                if(pnno.length()>10)
                {
                    pno.requestFocus();
                    pno.setError("Please enter correct phone number");
                    flag = false;
                }
                if(pnno.length()<10 )
                {
                    pno.requestFocus();
                    pno.setError("Please enter correct phone number");
                    flag = false;
                }

                if(flag)
                {
                    progressBar11.setVisibility(View.VISIBLE);

//                    Toast.makeText(SignupActivity.this, ""+name+"\n"+lastname+"\n"+
//                        email+"\n"+password+"\n"+address+"\n"+city+"\n"+phoneno, Toast.LENGTH_SHORT).show();

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    RegisterApi api = retrofit.create(RegisterApi.class);
                    Call<RegisterPOJO> call = api.getDetails(MainActivity.Tokendata,email,password,address,
                            name,lastname,city,"Pakistan",phoneno,"2" );
                call.enqueue(new Callback<RegisterPOJO>() {
                    @Override
                    public void onResponse(Call<RegisterPOJO> call, Response<RegisterPOJO> response) {
                        if(response.isSuccessful()) {
                            String ret = response.body().toString();
                            String success = response.body().getSuccess().toString();
                            String message = response.body().getMessage().toString();
                            //                        Toast.makeText(SignupActivity.this, ""+mes, Toast.LENGTH_SHORT).show();

                            if (success.equals("false")) {
                                progressBar11.setVisibility(View.GONE);


                                if (message.equals("Email Address already exits")) {
                                    Toast.makeText(SignupActivity.this, "Email Already Register", Toast.LENGTH_SHORT).show();
                                } else if (message.equals("Phone Number already exits")) {
                                    Toast.makeText(SignupActivity.this, "Phone number Already Register", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                progressBar11.setVisibility(View.GONE);

                                Toast.makeText(SignupActivity.this, "Congractulations! you are register to HSN", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intent);
                                finish();

                            }
                        }
                        else
                        {
                            Toast.makeText(SignupActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<RegisterPOJO> call, Throwable t) {

                        Toast.makeText(SignupActivity.this, "Server not responding.", Toast.LENGTH_SHORT).show();
                    }
                });
                }


            }
        });
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    public void signup(View view) {
        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
        intent.putExtra("comingcart","0");
        startActivity(intent);
        finish();
    }
}

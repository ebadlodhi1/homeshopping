package com.homeshopping.admin.homeshoping10.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.homeshopping.admin.homeshoping10.Activity.AddShippingAddressActivity;
import com.homeshopping.admin.homeshoping10.Activity.CartActivity;
import com.homeshopping.admin.homeshoping10.Activity.CheckOutActivity;
import com.homeshopping.admin.homeshoping10.Activity.Dashboard;
import com.homeshopping.admin.homeshoping10.Activity.EditShippingAddressActivity;
import com.homeshopping.admin.homeshoping10.Activity.ShipingAddressActivity;
import com.homeshopping.admin.homeshoping10.Interfaces.IOnBackPressed;
import com.homeshopping.admin.homeshoping10.Interfaces.removeaddresslistAPI;
import com.homeshopping.admin.homeshoping10.POJO.Cart.Cart;
import com.homeshopping.admin.homeshoping10.POJO.City.cityDetails;
import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddress;
import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddresslist;
import com.homeshopping.admin.homeshoping10.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class CheckoutFragmentPersonaldetails extends Fragment implements IOnBackPressed {

    Button proceedtopayment;
    EditText fullname,mobilenumber,otherinstruction,redeemcode;
    AutoCompleteTextView checkoutspinnercity;
    TextView checkoutadddress;
    ConstraintLayout checkoutaddressconstraint;
    List<cityDetails> city = new ArrayList<>();
    public  String[] cityname,cityid;
    ArrayAdapter aa;
    int a = 0 ;
     ArrayList<String> ShipAddressId  ;
     ArrayList<String> ShipAddressFirstName  ;
      ArrayList<String> ShipAddressLastName  ;
      ArrayList<String> ShipAddressCompany  ;
      String[] ShipAddressAddress  ;
      ArrayList<String> ShipAddressCity  ;
      ArrayList<String> ShipAddressPhone ;
    List<shipaddresslist> shiplist = new ArrayList<>();
    String fulname = " ",mobile = " ";
    Boolean flag = true;
    private String[] myImageNameList = new String[]{"Benz", "Bike",
            "Car","Carrera"
            ,"Ferrari","Harly",
            "Lamborghini","Silver"};
    public CheckoutFragmentPersonaldetails() {
       shiplist = CheckOutActivity.shiplist;
       ShipAddressAddress = new String[shiplist.size()];
       for (int i = 0 ; i < shiplist.size();i++)
       {
           ShipAddressAddress[i] = shiplist.get(i).getShipAddressAddress();

       }



    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chekoutpersonaldetailfragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        proceedtopayment = view.findViewById(R.id.proceedtopayment);
        fullname = view.findViewById(R.id.check_fullname);
        mobilenumber = view.findViewById(R.id.check_mobilenumber);
        otherinstruction  = view.findViewById(R.id.check_otherinstruction);
        checkoutspinnercity = view.findViewById(R.id.checkoutspinnercity);


        try {
            checkoutspinnercity.setText(Dashboard.prefConfig.readcity());
            aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, CartActivity.cityname);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            checkoutspinnercity.setAdapter(aa);


        }
        catch (Exception e)
        {
            Log.d("city exception ",""+e);
        }

        checkoutadddress = view.findViewById(R.id.textView31);
        checkoutadddress.setText(ShipAddressAddress[0]);
        checkoutaddressconstraint = view.findViewById(R.id.constraintLayout98);

        fullname.setText(Dashboard.prefConfig.readName()+" "+Dashboard.prefConfig.readlastname());
        mobilenumber.setText(Dashboard.prefConfig.readpno());


        proceedtopayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fulname = fullname.getText().toString().trim();
                mobile = mobilenumber.getText().toString().trim();
                flag = true;
                if(fulname.isEmpty()||fulname.matches(""))
                {
                    fullname.setError("Kindly Enter Fullname");
                    fullname.requestFocus();
                    flag = false;
                }

                if(mobile.isEmpty()||mobile.matches(""))
                {
                    mobilenumber.setError("Kindly Enter Mobile no");
                    mobilenumber.requestFocus();
                    flag = false;
                }
                if(flag)
                {
                    a =  CartActivity.cityname.indexOf(checkoutspinnercity.getText().toString().trim());
                    if(a==-1)
                    {
                        checkoutspinnercity.setError("Enter correct city name");
                        checkoutspinnercity.requestFocus();
                    }
                    else {
                        String cty = CartActivity.cityid.get(a);
                        String ctyname = CartActivity.cityname.get(a);
//                Toast.makeText(getActivity(), "city id = "+cty, Toast.LENGTH_SHORT).show();
                        CartActivity.cityidd = cty;
                        CartActivity.citynamee = ctyname;
                        CheckOutActivity.finalfirstName = fullname.getText().toString();
                        CheckOutActivity.finallastName = Dashboard.prefConfig.readlastname();
                        CheckOutActivity.finaladdress = "" + checkoutadddress.getText().toString();
                        CheckOutActivity.finalstate_id = cty;
                        CheckOutActivity.finalstate = ctyname;
                        CheckOutActivity.finalphone = "" + mobilenumber.getText().toString();
                        CheckOutActivity.finalcustomerId = Dashboard.prefConfig.readID();
                        CheckOutActivity.finalremarks = "" + otherinstruction.getText().toString();
                        showFragment(new CheckoutFragmentpaymentdetails());
                    }
                    }



            }
        });
        checkoutaddressconstraint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogAdress();
            }
        });

    }
    private void showFragment(Fragment fragment) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.checkout_fragmentlayout, fragment).addToBackStack("paymentdetail").commitAllowingStateLoss();


    }



    public void showdialogAdress()
    {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.fragcustomaddress, null);
        dialogBuilder.setView(dialogView);
        View view = null;
        dialogBuilder.setTitle("Select Address");
        final  AlertDialog b = dialogBuilder.create();
        ListView addresslistview =dialogView.findViewById(R.id.fradaddlistview);
//        ProgressBar progressBar2 = dialogView.findViewById(R.id.progressBar2);
//        progressBar2.setVisibility(View.GONE);
        Button add = dialogView.findViewById(R.id.button21);
        ShipAddressId = new ArrayList<>();;
        ShipAddressFirstName = new ArrayList<>();;
        ShipAddressLastName = new ArrayList<>();;
        ShipAddressCompany = new ArrayList<>();;
        ShipAddressAddress = new String[shiplist.size()];
        ShipAddressCity = new ArrayList<>();;
        ShipAddressPhone = new ArrayList<>();;
//            Toast.makeText(ShipingAddressActivity.this, "list size "+shiplist.size(), Toast.LENGTH_SHORT).show();
        for(int i = 0 ; i<shiplist.size();i++)
        {
            ShipAddressId.add(shiplist.get(i).getShipAddressId().toString());
            ShipAddressFirstName.add(shiplist.get(i).getShipAddressFirstName());
            ShipAddressLastName.add(shiplist.get(i).getShipAddressLastName());
            ShipAddressCompany.add(shiplist.get(i).getShipAddressCompany());
            ShipAddressAddress[i]  =shiplist.get(i).getShipAddressAddress();
            ShipAddressCity.add(shiplist.get(i).getShipAddressCity());
            ShipAddressPhone.add(shiplist.get(i).getShipAddressPhone());
//            Toast.makeText(getActivity(), ""+ShipAddressAddress[i], Toast.LENGTH_SHORT).show();
        }


        getshiplistAdapter getshiplistAdapter = new getshiplistAdapter(getActivity(), shiplist,ShipAddressId
        ,ShipAddressFirstName,ShipAddressLastName,ShipAddressCompany,ShipAddressAddress,ShipAddressCity,ShipAddressPhone);

        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(),R.layout.list_item, R.id.tv, ShipAddressAddress);
        addresslistview.setAdapter(getshiplistAdapter);

        addresslistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                checkoutadddress.setText(ShipAddressAddress[position]);
                b.dismiss();
            }
        });

        dialogBuilder.setCancelable(true);
        final View finalView = view;



        b.show();
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddShippingAddressActivity.class);
                intent.putExtra("activityname","checkout");
                startActivity(intent);
                b.dismiss();
                getActivity().finish();
            }
        });


    }

    @Override
    public boolean onBackPressed() {
        getActivity().finish();
        return false;
    }

    public class getshiplistAdapter extends ArrayAdapter<String> {
        ArrayList<String> ShipAddressId  = new ArrayList<>() ;
        ArrayList<String> ShipAddressFirstName  = new ArrayList<>() ;
        ArrayList<String> ShipAddressLastName   = new ArrayList<>();
        ArrayList<String> ShipAddressCompany   = new ArrayList<>();
        String[] ShipAddressAddress ;
        ArrayList<String> ShipAddressCity   = new ArrayList<>();
        ArrayList<String> ShipAddressPhone  = new ArrayList<>();


        public getshiplistAdapter(Context context, List<shipaddresslist> list,ArrayList<String> ShipAddressId,ArrayList<String> ShipAddressFirstName,ArrayList<String> ShipAddressLastName,ArrayList<String> ShipAddressCompany,String[] ShipAddressAddress,ArrayList<String> ShipAddressCity,ArrayList<String> ShipAddressPhone ) {
            super(context, R.layout.customshipaddressitem, ShipAddressId);
            this.ShipAddressId = ShipAddressId;
            this.ShipAddressFirstName = ShipAddressFirstName;
            this.ShipAddressLastName = ShipAddressLastName;
            this.ShipAddressCompany = ShipAddressCompany;
            this.ShipAddressAddress = ShipAddressAddress;
            this.ShipAddressCity = ShipAddressCity;
            this.ShipAddressPhone = ShipAddressPhone;




        }

        public View getView(final int position, View view, ViewGroup parent) {
          ViewHolder2 vh;
            View v = view;
            if(v == null) {
                vh = new ViewHolder2();
                LayoutInflater li = LayoutInflater.from(getActivity());
                v = li.inflate(R.layout.customshipaddressitem,null);
//            Example app = items.get(position);
//            Log.d("position",Integer.toString(position));
//            if(app != null) {
//                Toast.makeText(getActivity(), "shiplist size "+ShipAddressAddress[position], Toast.LENGTH_SHORT).show();

                vh.shipaddfname = (TextView) v.findViewById(R.id.shipaddfname);
                vh. shipadd = (TextView) v.findViewById(R.id.shipadd);
                vh. shipaddcity = (TextView) v.findViewById(R.id.shipaddcity);
                vh. shipaddpno = (TextView) v.findViewById(R.id.shipaddpno);
                vh.shipaddedit = v.findViewById(R.id.constraintLayout73);
                vh.shipadddelete = v.findViewById(R.id.constraintLayout74);


                v.setTag(vh);
            }
            else{
                vh = (ViewHolder2) view.getTag();
            }



// give a timezone reference for formatting (see comment at the bottom)

            vh.shipaddedit.setVisibility(View.GONE);
            vh.shipadddelete.setVisibility(View.GONE);
            vh.shipaddfname.setText(ShipAddressFirstName.get(position)+" "+ShipAddressLastName.get(position));
            vh.shipadd.setText(ShipAddressAddress[position]);
            vh.shipaddcity.setText(ShipAddressCompany.get(position)+" "+ShipAddressCity.get(position));
            vh.shipaddpno.setText("Phone no: "+ShipAddressPhone.get(position));




            return v;

        };
    }
    static class ViewHolder2
    {
        // ImageView imageView;
        TextView shipaddfname,shipadd,shipaddcity,shipaddpno;
        ConstraintLayout shipaddedit,shipadddelete;



    }




}

package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddress;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.HTTP;
import retrofit2.http.Header;

public interface UpdateshipaddressAPI {

    @FormUrlEncoded
    @HTTP(method = "PUT", path = "/shipAddress/updateShipAddress", hasBody = true)
    Call<shipaddress> getDetails(@Header("data") String data, @Field("shipAddressFirstName") String shipAddressFirstName
            , @Field("shipAddressLastName") String shipAddressLastName
            , @Field("shipAddressCompany") String shipAddressCompany
            , @Field("shipAddressAddress") String shipAddressAddress
            , @Field("shipAddressCity") String shipAddressCity
            , @Field("shipAddressPhone") String shipAddressPhone
            , @Field("shipCustomerId") String shipCustomerId
            , @Field("shipAddressId") String shipAddressId);

}

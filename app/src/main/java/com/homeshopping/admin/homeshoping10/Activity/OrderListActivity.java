package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Interfaces.OrderdetailsAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.OrderlistAPI;
import com.homeshopping.admin.homeshoping10.POJO.OderListPOJO;
import com.homeshopping.admin.homeshoping10.POJO.OrderDetailPOJO.OrderDetail;
import com.homeshopping.admin.homeshoping10.POJO.OrderDetailPOJO.OrderdetailExample;
import com.homeshopping.admin.homeshoping10.POJO.OrderListPOJO.Detail;
import com.homeshopping.admin.homeshoping10.POJO.OrderListPOJO.OrderlistExample;

import com.homeshopping.admin.homeshoping10.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.constraintlayout.widget.ConstraintLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class OrderListActivity extends AppCompatActivity {
ListView orderlistview,orderdetailprodlist;
    List<Detail> list1 = new ArrayList<Detail>();
    String[] orderid,orderstatus,ordertotal,orderdate,Image;
    int[]  productQuantity;
    String[] itemprodid,itemprodtitle,itemprodqty,itemprodprice,itemprodcon,itemprodimageurl;
    String formattedDateyear,formattedDatemonth,formattedDateday,formatedproductprice;
    ProgressBar progressBar5;
    List<OrderDetail> listord = new ArrayList<>();
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("My Order List");
        orderlistview = findViewById(R.id.orderlistview);
        progressBar5 = findViewById(R.id.progressBar5);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Account Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        OrderlistAPI api = retrofit.create(OrderlistAPI.class);
         Call<OrderlistExample> listCall = api.getDetails(MainActivity.Tokendata, prefConfig.readID());

         listCall.enqueue(new Callback<OrderlistExample>() {
             @Override
             public void onResponse(Call<OrderlistExample> call, Response<OrderlistExample> response) {
               if(response.isSuccessful()) {
                   int status = response.body().getStatus();
                   if (status == 1) {
                       list1 = response.body().getDetail();
                       orderid = new String[list1.size()];
                       orderstatus = new String[list1.size()];
                       ordertotal = new String[list1.size()];
                       orderdate = new String[list1.size()];
                       Image = new String[list1.size()];
                       productQuantity = new int[list1.size()];

                       for (int i = 0; i < list1.size(); i++) {
                           orderid[i] = list1.get(i).getOrderId().toString();
                           orderstatus[i] = list1.get(i).getOrderStatus().toString();
                           ordertotal[i] = list1.get(i).getOrderTotal().toString();
                           orderdate[i] = list1.get(i).getOrderDate().toString();
                           Image[i] = list1.get(i).getProductImage().toString();
                           productQuantity[i] = list1.get(i).getProductQuantity();
//                    Toast.makeText(OrderListActivity.this, ""+orderid[i], Toast.LENGTH_SHORT).show();
                       }
                       MyListAdapter1 adapter = new MyListAdapter1(getApplicationContext(), list1);
                       orderlistview.setAdapter(adapter);
                       progressBar5.setVisibility(View.GONE);
                   } else {
                       Toast.makeText(OrderListActivity.this, "No order placed yet by you.", Toast.LENGTH_SHORT).show();
                   }
               }
               else
               {
                   Toast.makeText(OrderListActivity.this, "Server not responding.", Toast.LENGTH_SHORT).show();
               }
             }

             @Override
             public void onFailure(Call<OrderlistExample> call, Throwable t) {

             }
         });



    }


    public class MyListAdapter1 extends ArrayAdapter<String> {



        public MyListAdapter1(Context context, List<Detail> list ) {
            super(context, R.layout.orderlistcustomitem, orderid);




        }

        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder vh;
            View v = view;
            if(v == null) {
                vh = new ViewHolder();
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                v = li.inflate(R.layout.orderlistcustomitem,null);
//            Example app = items.get(position);
//            Log.d("position",Integer.toString(position));
//            if(app != null) {

                vh.year = (TextView) v.findViewById(R.id.year);

                vh.totalproduct = (TextView) v.findViewById(R.id.totalproduct);
                vh. ordernum = (TextView) v.findViewById(R.id.ordernum);
                vh. totalamount = (TextView) v.findViewById(R.id.totalamount);
                vh. status = (TextView) v.findViewById(R.id.status);
                vh.orderitemconstraint = v.findViewById(R.id.orderitemconstraint);
                vh.imageView = v.findViewById(R.id.imageView66);
                v.setTag(vh);
            }
            else{
                vh = (ViewHolder) view.getTag();
            }



            String dateunix = orderdate[position];

            long unixSeconds = Long.parseLong(dateunix);
// convert seconds to milliseconds
            Date dateun = new java.util.Date(unixSeconds*1000L);
// the format of your date
            SimpleDateFormat sdfyear = new java.text.SimpleDateFormat("dd-MM-YYYY", Locale.US);
            SimpleDateFormat sdfmonth = new java.text.SimpleDateFormat("MMM", Locale.US);
            SimpleDateFormat sdfday = new java.text.SimpleDateFormat("dd", Locale.US);
// give a timezone reference for formatting (see comment at the bottom)

             formattedDateyear = sdfyear.format(dateun);
             formattedDatemonth = sdfmonth.format(dateun);
             formattedDateday = sdfday.format(dateun);

            vh.year.setText(formattedDateyear);
//            vh.date.setText(formattedDateday);
//            vh.month.setText(formattedDatemonth);
            vh.ordernum.setText("Order ID: "+orderid[position]);
            DecimalFormat formatter = new DecimalFormat("#,###,###");
             formatedproductprice = formatter.format(Math.round(Float.parseFloat(ordertotal[position])));
            vh.totalamount.setText("Rs "+formatedproductprice);
            vh.status.setText(orderstatus[position]);
            if(productQuantity[position] >1)
            vh.totalproduct.setText(productQuantity[position]+" Item (s)");
            else
                vh.totalproduct.setText(productQuantity[position]+" Item");

            Glide.with(getApplicationContext()).load(Image[position])
                    .into(vh.imageView);
            vh.orderitemconstraint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(OrderListActivity.this, ""+orderid[position], Toast.LENGTH_SHORT).show();
//                    showDialog2(orderid[position],orderstatus[position],orderdate[position],ordertotal[position]);
                    Intent intent = new Intent(getApplicationContext(),OrderDetailActivity.class);
                    intent.putExtra("orderid",orderid[position]);
                    intent.putExtra("orderstatus",orderstatus[position]);
                    intent.putExtra("orderdate",orderdate[position]);
                        intent.putExtra("ordertotal",ordertotal[position]);
                    startActivity(intent);

                }
            });



            return v;

        };
    }
    static class ViewHolder
    {
        // ImageView imageView;
        TextView year,date,month,totalproduct,ordernum,totalamount,status;
        ConstraintLayout orderitemconstraint;
        ImageView imageView;



    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);
        finish();

    }

    public void showDialog2(String ordid,String ordstatus,String orddate,String ordamount)
    {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        formatedproductprice = formatter.format(Math.round(Float.parseFloat(ordamount)));
        String dateunix =orddate ;

        long unixSeconds = Long.parseLong(dateunix);
// convert seconds to milliseconds
        Date dateun = new java.util.Date(unixSeconds*1000L);
// the format of your date
        SimpleDateFormat sdfyear = new java.text.SimpleDateFormat("yyyy", Locale.US);
        SimpleDateFormat sdfmonth = new java.text.SimpleDateFormat("MMM", Locale.US);
        SimpleDateFormat sdfday = new java.text.SimpleDateFormat("dd", Locale.US);
// give a timezone reference for formatting (see comment at the bottom)

        formattedDateyear = sdfyear.format(dateun);
        formattedDatemonth = sdfmonth.format(dateun);
        formattedDateday = sdfday.format(dateun);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.orderdetailcustomlayout, null);
        dialogBuilder.setView(dialogView);
        View view = null;
        dialogBuilder.setTitle("Order Details ");
        dialogBuilder.setCancelable(true);

        final  AlertDialog b = dialogBuilder.create();
       TextView orderid = dialogView.findViewById(R.id.textView43);
        TextView orderstatus = dialogView.findViewById(R.id.textView54);

        TextView orderdate = dialogView.findViewById(R.id.textView58);

        TextView ordertotal = dialogView.findViewById(R.id.textView60);
        orderdetailprodlist = dialogView.findViewById(R.id.orderdetailprodlist);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        OrderdetailsAPI orderdetailsAPI = retrofit.create(OrderdetailsAPI.class);
//        Toast.makeText(this, "dd "+ordid, Toast.LENGTH_SHORT).show();
        Call<OrderdetailExample> orderInfoCall = orderdetailsAPI.getDetails(MainActivity.Tokendata,ordid);
        orderInfoCall.enqueue(new Callback<OrderdetailExample>() {
            @Override
            public void onResponse(Call<OrderdetailExample> call, Response<OrderdetailExample> response) {

                if (response.isSuccessful()) {
                    listord =  response.body().getOrderDetail();

                    Toast.makeText(OrderListActivity.this, "" + listord.size(), Toast.LENGTH_SHORT).show();

                    itemprodid = new String[listord.size()];
                    itemprodtitle = new String[listord.size()];
                    itemprodqty = new String[listord.size()];
                    itemprodprice = new String[listord.size()];
                    itemprodcon = new String[listord.size()];
                    itemprodimageurl = new String[listord.size()];
                    for (int i = 0; i < listord.size(); i++) {
                        itemprodid[i] = listord.get(i).getProductId().toString();
                        itemprodtitle[i] = listord.get(i).getProductTitle().toString();
                        itemprodqty[i] = listord.get(i).getProductQuantity().toString();
                        itemprodprice[i] = listord.get(i).getProductPrice().toString();
                        itemprodcon[i] = listord.get(i).getProductCondition().toString();
                        itemprodimageurl[i] = listord.get(i).getProductImageUrl().toString();

                    }

                    MyListAdapter2 adapter = new MyListAdapter2(getApplicationContext());
                    orderdetailprodlist.setAdapter(adapter);
                }
                }

            @Override
            public void onFailure(Call<OrderdetailExample> call, Throwable t) {
                Toast.makeText(OrderListActivity.this, ""+t, Toast.LENGTH_SHORT).show();
                Log.e("Error",""+t);
            }
        });


        orderid.setText("#"+ordid);
        orderstatus.setText(ordstatus);
        orderdate.setText(formattedDateday+"-"+formattedDatemonth+"-"+formattedDateyear);
        ordertotal.setText("RS "+formatedproductprice);
        b.show();

    }


    public class MyListAdapter2 extends ArrayAdapter<String> {



        public MyListAdapter2(Context context ) {
            super(context, R.layout.customorderdetaillistitem, itemprodid);




        }

        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder2 vh;
            View v = view;
            if(v == null) {
                vh = new ViewHolder2();
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                v = li.inflate(R.layout.customorderdetaillistitem,null);
//            Example app = items.get(position);
//            Log.d("position",Integer.toString(position));
//            if(app != null) {

                vh.itemname = (TextView) v.findViewById(R.id.textView62);
                vh. itemqty = (TextView) v.findViewById(R.id.textView63);
                vh. itemcondition = (TextView) v.findViewById(R.id.textView65);
                vh.itemamount = (TextView) v.findViewById(R.id.textView67);

                vh.itemimage = v.findViewById(R.id.imageView17);
                v.setTag(vh);
            }
            else{
                vh = (ViewHolder2) view.getTag();
            }



// give a timezone reference for formatting (see comment at the bottom)


            vh.itemname.setText(itemprodtitle[position]);
            vh.itemqty.setText("Qty: "+itemprodqty[position]);
            vh.itemcondition.setText(itemprodcon[position]);

            DecimalFormat formatter = new DecimalFormat("#,###,###");
            formatedproductprice = formatter.format(Math.round(Float.parseFloat(itemprodprice[position])));
            vh.itemamount.setText("Rs "+formatedproductprice);
            Glide.with(getApplicationContext())
                    .load(itemprodimageurl[position])// image url
                    // any placeholder to load at start
                    .into( vh.itemimage);



            return v;

        };
    }
    static class ViewHolder2
    {
        // ImageView imageView;
        TextView itemname,itemqty,itemcondition,itemamount;
        ImageView itemimage;



    }


}

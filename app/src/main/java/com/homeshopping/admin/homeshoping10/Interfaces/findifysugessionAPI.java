package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.findifysugestionPOJO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface findifysugessionAPI {
    @GET("products/suggestfindifytext")
    Call<List<findifysugestionPOJO>> getDetails(@Header("data") String data, @Query("keyword") String keyword);

}

package com.homeshopping.admin.homeshoping10.Class;

import android.content.Context;
import android.content.SharedPreferences;

import com.homeshopping.admin.homeshoping10.R;

public class PrefConfig {
    private SharedPreferences sharedPreferences;
    private Context context;
    public PrefConfig(Context context)
    {
        this.context=context;
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.pref_file),Context.MODE_PRIVATE);
    }

    public void writeLoginStatus(boolean status)
    {
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putBoolean(context.getString(R.string.pref_login_status),status);
        editor.commit();
    }
    public boolean readLoginStatus()
    {
        return sharedPreferences.getBoolean(context.getString(R.string.pref_login_status),false);
    }

    public void writeName(String CustomerID,String First_name,String Last_name,String Email,String pno,String city)
    {
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putString("CustomerID",CustomerID);
        editor.putString("FirstName",First_name);
        editor.putString("LastName",Last_name);
        editor.putString("Email",Email);
        editor.putString("Pno",pno);
        editor.putString("city",city);
        editor.commit();
    }

    public  void Logout()
    {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
        editor.apply();

    }
    public String readID()
    {
        return sharedPreferences.getString("CustomerID","0");
    }
    public String readName()
    {
        return sharedPreferences.getString("FirstName","User");
    }
    public String readlastname()
    {
        return sharedPreferences.getString("LastName","User");
    }
    public String reademail()
    {
        return sharedPreferences.getString("Email","User");
    }

    public String readpno()
    {
        return sharedPreferences.getString("Pno","pno");
    }
    public String readcity()
    {
        return sharedPreferences.getString("city","city");
    }

}

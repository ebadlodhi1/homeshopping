package com.homeshopping.admin.homeshoping10.POJO.whishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class whishlistdetails {
    @SerializedName("wishlistId")
    @Expose
    private Integer wishlistId;



    @SerializedName("brandName")
    @Expose
    private String brandName;
    @SerializedName("productTitle")
    @Expose
    private String productTitle;
    @SerializedName("productTotalPrice")
    @Expose
    private String productTotalPrice;
    @SerializedName("productSalePrice")
    @Expose
    private String productSalePrice;
    @SerializedName("productImageUrl")
    @Expose
    private String productImageUrl;
    @SerializedName("productId")
    @Expose
    private Integer productId;

    @SerializedName("isExistInCart")
    @Expose
    private Integer isExistInCart;

    public Integer getWishlistId() {
        return wishlistId;
    }

    public void setWishlistId(Integer wishlistId) {
        this.wishlistId = wishlistId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getProductTotalPrice() {
        return productTotalPrice;
    }

    public void setProductTotalPrice(String productTotalPrice) {
        this.productTotalPrice = productTotalPrice;
    }

    public String getProductSalePrice() {
        return productSalePrice;
    }

    public void setProductSalePrice(String productSalePrice) {
        this.productSalePrice = productSalePrice;
    }

    public String getProductImageUrl() {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) {
        this.productImageUrl = productImageUrl;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }
    public Integer getIsExistInCart() {
        return isExistInCart;
    }

    public void setIsExistInCart(Integer isExistInCart) {
        this.isExistInCart = isExistInCart;
    }
}

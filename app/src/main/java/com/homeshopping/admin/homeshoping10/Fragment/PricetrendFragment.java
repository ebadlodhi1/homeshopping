package com.homeshopping.admin.homeshoping10.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.homeshopping.admin.homeshoping10.Activity.MainActivity;
import com.homeshopping.admin.homeshoping10.Interfaces.PricetrendAPI;
import com.homeshopping.admin.homeshoping10.POJO.PricetrendPOJO.Detail;
import com.homeshopping.admin.homeshoping10.POJO.PricetrendPOJO.PriceTrendExample;
import com.homeshopping.admin.homeshoping10.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class PricetrendFragment extends Fragment {
    public PricetrendFragment() {
    }
    List<Detail> detailsPrice = new ArrayList<>();
    private LineChart mChart;
    ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
    ArrayList<String> xVals = setXAxisValues();

    ArrayList<Entry> yVals = setYAxisValues();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.newlaytest,container,false);
    }
    private ArrayList<String> setXAxisValues(){
        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add("10");
        xVals.add("20");
        xVals.add("30");
        xVals.add("30.5");
        xVals.add("40");

        return xVals;
    }
    private ArrayList<Entry> setYAxisValues(){
        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PricetrendAPI pricetrendAPI = retrofit2.create(PricetrendAPI.class);
        Call<PriceTrendExample> reviewExampleCall = pricetrendAPI.getDetails(MainActivity.Tokendata,"81387");
        reviewExampleCall.enqueue(new Callback<PriceTrendExample>() {
            @Override
            public void onResponse(Call<PriceTrendExample> call, Response<PriceTrendExample> response) {
                int status = response.body().getStatus();
                int a = 0;
                int value = 0;
                if(status == 1)
                {
                    detailsPrice = response.body().getDetails();
                    ArrayList<Entry> yVals = new ArrayList<Entry>();
                    for (int i = 0 ; i <detailsPrice.size();i++)
                    {
                        Toast.makeText(getActivity(), ""+detailsPrice.get(i).getDate(), Toast.LENGTH_SHORT).show();
                        yVals.add(new Entry(Float.parseFloat(detailsPrice.get(i).getDate()),Float.parseFloat(detailsPrice.get(i).getPrice())));
                    }
                }
            }

            @Override
            public void onFailure(Call<PriceTrendExample> call, Throwable t) {

            }
        });

        return yVals;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//
        mChart = (LineChart) view.findViewById(R.id.linechart);
//        mChart.setOnChartGestureListener((OnChartGestureListener) this);
//        mChart.setOnChartValueSelectedListener((OnChartValueSelectedListener) this);
//
        setData();
//
        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(Legend.LegendForm.LINE);
    }
    private void setData() {


        LineDataSet set1;

        // create a dataset and give it a type
        set1 = new LineDataSet(yVals, "DataSet 1");
        set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        // set1.enableDashedLine(10f, 5f, 0f);
        // set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.BLACK);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(true);


        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(dataSets);

        // set data
        mChart.setData(data);

    }
}

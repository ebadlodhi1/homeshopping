package com.homeshopping.admin.homeshoping10.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShipmentratePOJO {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("shippment_cost")
    @Expose
    private String shippmentCost;
    @SerializedName("shippment_cost_with_qty")
    @Expose
    private String shippmentCostWithQty;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getShippmentCost() {
        return shippmentCost;
    }

    public void setShippmentCost(String shippmentCost) {
        this.shippmentCost = shippmentCost;
    }

    public String getShippmentCostWithQty() {
        return shippmentCostWithQty;
    }

    public void setShippmentCostWithQty(String shippmentCostWithQty) {
        this.shippmentCostWithQty = shippmentCostWithQty;
    }
}

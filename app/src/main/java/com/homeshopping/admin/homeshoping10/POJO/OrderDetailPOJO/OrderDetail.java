package com.homeshopping.admin.homeshoping10.POJO.OrderDetailPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderDetail {
    @SerializedName("productId")
    @Expose
    private Integer productId;
    @SerializedName("productTitle")
    @Expose
    private String productTitle;
    @SerializedName("productType")
    @Expose
    private String productType;
    @SerializedName("productQuantity")
    @Expose
    private Integer productQuantity;
    @SerializedName("productPrice")
    @Expose
    private String productPrice;
    @SerializedName("productCondition")
    @Expose
    private String productCondition;
    @SerializedName("productImageUrl")
    @Expose
    private String productImageUrl;
    @SerializedName("brandDisplayName")
    @Expose
    private String brandDisplayName;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Integer getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(Integer productQuantity) {
        this.productQuantity = productQuantity;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductCondition() {
        return productCondition;
    }

    public void setProductCondition(String productCondition) {
        this.productCondition = productCondition;
    }

    public String getProductImageUrl() {
        return productImageUrl;
    }

    public void setProductImageUrl(String productImageUrl) {
        this.productImageUrl = productImageUrl;
    }

    public String getBrandDisplayName() {
        return brandDisplayName;
    }

    public void setBrandDisplayName(String brandDisplayName) {
        this.brandDisplayName = brandDisplayName;
    }
}

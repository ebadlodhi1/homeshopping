package com.homeshopping.admin.homeshoping10.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Class.NetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Class.NotLoginDataNetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Fragment.DiscriptionFragment;
import com.homeshopping.admin.homeshoping10.Fragment.PricetrendFragment;
import com.homeshopping.admin.homeshoping10.Fragment.ReviewFragment;
import com.homeshopping.admin.homeshoping10.Interfaces.AddorRemoveCallbacks;
import com.homeshopping.admin.homeshoping10.Interfaces.AddtowishlistAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.CategoryProductApi;
import com.homeshopping.admin.homeshoping10.Interfaces.GetProductDetailAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.GetconfigrableAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.GetvariationsAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.ProductimageApi;
import com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO.ConfFeildFieldOption;
import com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO.ConfFieldDetail;
import com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO.ConfFieldExample;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;
import com.homeshopping.admin.homeshoping10.POJO.CategoryProductPOJO.ProductExample;
import com.homeshopping.admin.homeshoping10.POJO.GetProductImages.ProductImagesExample;
import com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO.ConfigurableField;
import com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO.PriceTrend;
import com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO.ProductDetailExample;
import com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO.ProductImage;
import com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO.Review;
import com.homeshopping.admin.homeshoping10.POJO.Variation.Detail;
import com.homeshopping.admin.homeshoping10.POJO.Variation.Variation;
import com.homeshopping.admin.homeshoping10.POJO.Variation.VariationExample;
import com.homeshopping.admin.homeshoping10.R;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;


public class ProductDetailActivity extends AppCompatActivity implements OnChartGestureListener,
        OnChartValueSelectedListener, AddorRemoveCallbacks {
    static String[] productnamee = {"Iphone X","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8"} ;
    static String[] orignalpricetvv = {"113000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000"} ;
    static  String[] offpercenttvv  = {"10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%"} ;;
    static  String[]discountedpricetvv = {"113000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000"} ;
    static int[] Productimagee = {R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex} ;
    private TabLayout tabLayout;
    public static ViewPager viewPager;
    SliderLayout productsliderLayout;
    TextView qtyespinner;
   public static String distext;
    TextView sellername;
    ListView variationlistview;
    ListView conflistview;
List<Detail> detailListvariation  = new ArrayList<>();
List<ConfFieldDetail> confFieldDetails = new ArrayList<>();
List<Variation>  variationoptionlist = new ArrayList<>();
ArrayList<String>  variationId,variationName,configrableName;
ArrayList<Integer> configrableRequired,configrablefeildID;
int configrablefeildIDforcart=0;
String configrablefeildNameforcart = "";
List<Variation> variations = new ArrayList<>();

     TextView variationnametxt, variname;

    String strvariname,strvaricombid,strvarifunc,strvariprice;

    String[] qty = {"1","2","3","4","5","6","7","8","9","10"};

    LinearLayoutManager layoutManagerviewmost;
    RecyclerView recyclerViewmostitem;

    ArrayList<Integer> mImageUrls = new ArrayList<>() ;


    List<com.homeshopping.admin.homeshoping10.POJO.GetProductImages.Detail> listImages = new ArrayList<>();
    String[] imagelist;
   static android.support.constraint.ConstraintLayout constraintLayout84,constraintlayoutconfigrable;
TextView productname,price,probrand,Rating,discountprice,discount,orignalprice;
String ProdURL,ProductName,ProductDescription="",ProductPrice,ProductSalePrice,BrandDisplayName, VendorName, productCondition,productDelivery;

    ArrayList<Integer> recomendProductID = new ArrayList<>() ;
    ArrayList<String> recomendProductName = new ArrayList<>() ;
    ArrayList<String> recomendProductUrl  = new ArrayList<>();
    ArrayList<String> recomendProductDescription = new ArrayList<>();
    ArrayList<String> recomendProductPrice  = new ArrayList<>();
    ArrayList<String> recomendProductSalePrice = new ArrayList<>();
    ArrayList<String> recomendBrandDisplayName  = new ArrayList<>();
    ArrayList<Integer> recomendproductwishlist  = new ArrayList<>();
    ArrayList<Integer> recomendproductisExistInCart  = new ArrayList<>();


    ArrayList<String> recomendproductImages = new ArrayList<>();
int productRating,productAvailability,productcategoryID,isExistInCart =0;
String BaseRootUrl = "https://dev-api.homeshopping.pk/products/";
HashMap<String , List<Variation>> variationoptionlistful;
HashMap<String,List<ConfFeildFieldOption>>   configrableoptionlistful ;
    RadioGroup rg;
     RadioButton[] rb;
    // This is used to store x-axis values


    public DatabaseHelper db;
    float pricefloat,salepricefloat;
    int SalePrice = 1 ;
    int  Productprice = 1;
    int discoutnamount = 0 ;
    List<com.homeshopping.admin.homeshoping10.POJO.CategoryProductPOJO.Detail> list = new ArrayList<com.homeshopping.admin.homeshoping10.POJO.CategoryProductPOJO.Detail>();
    Button subqty,addqty,buynow;
    int addcartstatus  = 0 ;
    private Tracker mTracker;
    public  String BankId = "0";
    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intent = new Intent(getApplicationContext(),Dashboard.class);
//        startActivity(intent);
        finish();
    }
    public  static String pid = "" ;
    String title,pricee = "";
    int wishliststatus = 0;

    List<ProductImage> getProductImage = new ArrayList<>();
    List<ConfigurableField> getconfigurableFields = new ArrayList<>();
    List<com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO.Variation> getvariations = new ArrayList<>();
    List<Review> getreviews = new ArrayList<>();
    List<PriceTrend> getpriceTrends = new ArrayList<>();

    ImageView Socialshare,ratingimage,wishlist;
     Bundle bundle,bundlediscription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        constraintLayout84 = findViewById(R.id.constraintLayout84);
        constraintlayoutconfigrable = findViewById(R.id.constraintlayoutconfigrable);
        discount = findViewById(R.id.discount);
        orignalprice = findViewById(R.id.orignalprice);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        subqty = findViewById(R.id.button24);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Product Detail Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            subqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.LightGrey));


        }
        addqty = findViewById(R.id.button25);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            addqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.DarkGray));

        }
        wishlist = findViewById(R.id.imageView51);
        buynow = findViewById(R.id.button5);



        addqty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(qtyespinner.getText().toString());
                if(qty > 0)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        subqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.DarkGray));
                        addqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.DarkGray));

                    }
                    qty = qty+1;
                    qtyespinner.setText(""+qty);
                    buynow.setText("Add to Cart");

                }
                else
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        subqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.LightGrey));
                        addqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.DarkGray
                        ));

                    }
                }

            }
        });
        subqty.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(qtyespinner.getText().toString());
                if(qty==1)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        subqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.LightGrey));
                        addqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.DarkGray));
                    }
                }
                if(qty > 1)
                {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        subqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.DarkGray));
                        addqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.DarkGray));
                    }
                    qty = qty-1;
                    qtyespinner.setText(""+qty);
                }
                else
                {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        subqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.LightGrey));
                        addqty.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(),R.color.DarkGray));
                    }
                }

            }
        });
        Socialshare = findViewById(R.id.imageView42);
            Intent intent = getIntent();
            Uri data = intent.getData();

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        db = new DatabaseHelper(getApplicationContext());
         bundle = new Bundle(getIntent().getExtras());
        bundlediscription = new Bundle();
        if(data!=null)
        pid = data.getQueryParameter("pid");
        else
         pid = bundle.getString("productid");
        BankId  = bundle.getString("BankID","0");

        wishliststatus = bundle.getInt("prodwishlist");

        try {
            Boolean res2 = db.getproductforwishlist(Integer.parseInt(pid));

            if (res2 == false) {
                wishlist.setBackgroundResource(R.drawable.heartgrey);
            } else {
                wishlist.setBackgroundResource(R.drawable.heartfilled);

            }
        }
        catch (Exception e)
        {
            Log.d("Exception",""+e);
        }
        wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Dashboard.prefConfig.readLoginStatus()) {


                    if (!db.getproductforwishlist(Integer.parseInt(pid))) {
                        db.addRecordwishlist(String.valueOf(pid),Dashboard.prefConfig.readID());
                        Toast.makeText(ProductDetailActivity.this, "Please wait adding item to wishlist.", Toast.LENGTH_SHORT).show();
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
                        Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata, String.valueOf(pid), Dashboard.prefConfig.readID());
                        //   Toast.makeText(mContext, " id = "+pid[position], Toast.LENGTH_SHORT).show();
                        call.enqueue(new Callback<generalstatusmessagePOJO>() {
                            @Override
                            public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                                if (response.isSuccessful()) {
                                    int status = response.body().getStatus();
                                    String message = response.body().getMessage().toString();
                                    if (status == 1) {
                                        wishlist.setBackgroundResource(R.drawable.heartfilled);
//                                View parentLayout = findViewById(android.R.id.content);
//                                Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
//                                View sbView = snackbar.getView();
//                                sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                                snackbar.show();
                                        Toast.makeText(getApplicationContext(), "" + message, Toast.LENGTH_SHORT).show();

                                    }
                                    else {
                                        wishlist.setBackgroundResource(R.drawable.heartgrey);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                            }
                        });
                    } else {
                        Toast.makeText(ProductDetailActivity.this, "Item already added to wishlist.", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Kindly login first to make wishlist", Toast.LENGTH_SHORT).show();

                }
            }
        });
int addcartstatus = bundle.getInt("addcartstatus",0);
if(addcartstatus == 1)
{
buynow.setText("Added to Cart");
}
else
    buynow.setText("Add to Cart");


        variationoptionlistful = new HashMap<>();
        configrableoptionlistful = new HashMap<>();
//        Toast.makeText(this, ""+pid, Toast.LENGTH_SHORT).show();
//        sellername = findViewById(R.id.textView80);
//        sellername.setPaintFlags(sellername.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
try {
    Retrofit retrofit2 = new Retrofit.Builder()
            .baseUrl(ROOT_URL) //Setting the Root URL
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    GetProductDetailAPI getProductDetailAPI = retrofit2.create(GetProductDetailAPI.class);
    Call<ProductDetailExample> productDetailExampleCall = getProductDetailAPI.getDetails(MainActivity.Tokendata, pid, Dashboard.prefConfig.readID(),BankId);
    productDetailExampleCall.enqueue(new Callback<ProductDetailExample>() {
        @Override
        public void onResponse(Call<ProductDetailExample> call, Response<ProductDetailExample> response) {
            if (response.isSuccessful()) {
                int status = response.body().getStatus();
                Log.d("status", "" + status);
                if (status == 1) {
                    ProdURL = response.body().getDetails().getProduct().getProductUrl() + "?pid=" + pid;
                    ProductName = response.body().getDetails().getProduct().getProductName();
                    ProductDescription = response.body().getDetails().getProduct().getProductDescription();
                    ProductPrice = response.body().getDetails().getProduct().getProductPrice();
                    if(BankId.equals("0"))
                        ProductSalePrice = response.body().getDetails().getProduct().getProductSalePrice();
                    else
                        ProductSalePrice = response.body().getDetails().getProduct().getProductBankPrice();

                    BrandDisplayName = response.body().getDetails().getProduct().getBrandDisplayName();
                    productRating = response.body().getDetails().getProduct().getProductRating();
                    VendorName = response.body().getDetails().getProduct().getVendorName();
                    productCondition = response.body().getDetails().getProduct().getProductCondition();
                    productDelivery = response.body().getDetails().getProduct().getProductDelivery();
                    productAvailability = response.body().getDetails().getProduct().getProductAvailability();
                    productcategoryID = Integer.parseInt(response.body().getDetails().getProduct().getProductCategoryId());
                    isExistInCart = response.body().getDetails().getProduct().getIsExistInCart();
                    if (isExistInCart == 1) {
                        buynow.setText("Added to cart");
                    }
//                        else
//                        {
//                            buynow.setText("Add to cart");
//                        }
//                      Toast.makeText(ProductDetailActivity.this, "pcatid res = "+productcategoryID, Toast.LENGTH_SHORT).show();
                    productname = findViewById(R.id.textView8);
                    productname.setText(ProductName);
                    price = findViewById(R.id.textView9);
                    pricefloat = Float.parseFloat(ProductPrice);
                    salepricefloat = Float.parseFloat(ProductSalePrice);
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    int pricee = Math.round(pricefloat);
                    int saleprice = Math.round(salepricefloat);
                    Log.d("saleprice", "" + saleprice);
                    probrand = findViewById(R.id.probrand);
                    if (saleprice == pricee) {
                        discount.setVisibility(View.GONE);
                        orignalprice.setVisibility(View.GONE);
                        price.setText("PKR " + formatter.format(pricee));

                    } else if (saleprice < pricee) {
                        int orprice = Math.round(pricefloat);
                        int salprice = Math.round(salepricefloat);
                        int discountpr = orprice - salprice;
                        int percentage = ((discountpr * 100) / orprice);

                        discount.setVisibility(View.VISIBLE);
                        orignalprice.setVisibility(View.VISIBLE);
                        price.setText("PKR " + formatter.format(saleprice));
                        orignalprice.setText("PKR " + formatter.format(pricee));
                        discount.setText(percentage + "% Off");
                        orignalprice.setPaintFlags(orignalprice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                    }


                    Log.e("discription ", ProductDescription);
                    distext = ProductDescription;
                    bundlediscription.putString("discriptionnew", "" + ProductDescription);

                    probrand.setText(BrandDisplayName);
                    Rating = findViewById(R.id.textView74);
                    Rating.setText("" + productRating);
                    ratingimage = findViewById(R.id.imageView4);
                    int rate = productRating;
                    if (rate == 0) {
                        ratingimage.setBackgroundResource(R.drawable.star0);
                    } else if (rate == 1) {
                        ratingimage.setBackgroundResource(R.drawable.star1);
                    } else if (rate == 2) {
                        ratingimage.setBackgroundResource(R.drawable.star2);
                    } else if (rate == 3) {
                        ratingimage.setBackgroundResource(R.drawable.star3);
                    } else if (rate == 4) {
                        ratingimage.setBackgroundResource(R.drawable.star4);
                    } else if (rate == 5) {
                        ratingimage.setBackgroundResource(R.drawable.star5);
                    }
                    discountprice = findViewById(R.id.discountprice);
                    Loadproducts("newest", productcategoryID);

                }
            } else {
                Toast.makeText(ProductDetailActivity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFailure(Call<ProductDetailExample> call, Throwable t) {
            Log.d("fail res", "" + t);
        }
    });
}
catch (Exception e)
{

}
        addTabs(viewPager);
        this.setTitle("Details");

        qtyespinner = findViewById(R.id.qtyespinner);
//        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,qty);
//        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        //Setting the ArrayAdapter data on the Spinner
//        qtyespinner.setAdapter(aa);
       title = bundle.getString("title","a");


//        Toast.makeText(this, "ss "+distext, Toast.LENGTH_SHORT).show();
         pricee = bundle.getString("price","123");

        productsliderLayout = findViewById(R.id.productimageSlider);
        productsliderLayout.setIndicatorAnimation(IndicatorAnimations.DROP);
        productsliderLayout.setAutoScrolling(false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ProductimageApi productimageApi = retrofit.create(ProductimageApi.class);
        Call<ProductImagesExample> call = productimageApi.getDetails(MainActivity.Tokendata,Integer.parseInt(pid));

        call.enqueue(new Callback<ProductImagesExample>() {
            @Override
            public void onResponse(Call<ProductImagesExample> call, Response<ProductImagesExample> response) {
                if (response.isSuccessful()) {
                    int status = response.body().getStatus();
                    if (status == 1) {
                        listImages = response.body().getDetails();
                        imagelist = new String[listImages.size()];
                        for (int i = 0; i < listImages.size(); i++) {
                            imagelist[i] = listImages.get(i).getImageFile();
//            Toast.makeText(ProductDetailActivity.this, ""+imagelist[i], Toast.LENGTH_SHORT).show();
                        }

                        setSliderViews(imagelist);
                    }
                }
            }


            @Override
            public void onFailure(Call<ProductImagesExample> call, Throwable t) {

            }
        });




// set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
//        productsliderLayout.setScrollTimeInSec(3); //set scroll delay in seconds :
        String prodbrand = bundle.getString("prodbrand","abc");




//        wv1.loadUrl("http://al-kauther.com/ebad.php");

        recyclerViewmostitem = findViewById(R.id.recyclerViewmostitem);
        layoutManagerviewmost = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mImageUrls.add(R.drawable.iphonex);
        mImageUrls.add(R.drawable.samsungnote);
        mImageUrls.add(R.drawable.iphonex);
        recyclerViewmostitem.setLayoutManager(layoutManagerviewmost);
        recyclerViewmostitem.setHasFixedSize(true);




        variationlistview = findViewById(R.id.variationlistview);
        GetvariationsAPI getvariationsAPI =retrofit.create(GetvariationsAPI.class);
        Call<VariationExample> variationExampleCall = getvariationsAPI.getDetails(MainActivity.Tokendata,pid);
        variationExampleCall.enqueue(new Callback<VariationExample>() {
            @Override
            public void onResponse(Call<VariationExample> call, Response<VariationExample> response) {
                if(response.isSuccessful()) {
                    int status = response.body().getStatus();
                    variationName = new ArrayList<>();
                    constraintLayout84.setVisibility(View.GONE);
                    if (status == 1) {
                        constraintLayout84.setVisibility(View.VISIBLE);
                        detailListvariation = response.body().getDetails();
//                    Toast.makeText(ProductDetailActivity.this, "aa\n"+response.body().getDetails().size(), Toast.LENGTH_LONG).show();

                        for (int i = 0; i < detailListvariation.size(); i++) {
//                        variationId.add(detailListvariation.get(i).getVariationId().toString());
                            variationName.add(detailListvariation.get(i).getVariationName());
                            variationoptionlistful.put(variationName.get(i), detailListvariation.get(i).getVariations());


                        }

                        MyVariationListAdapter myVariationListAdapter = new MyVariationListAdapter(getApplicationContext(), variationoptionlistful);
                        variationlistview.setAdapter(myVariationListAdapter);
                        justifyListViewHeightBasedOnChildren(variationlistview);
                        variationlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                variationnametxt = (TextView) view.findViewById(R.id.textView76);
                                variname = view.findViewById(R.id.textView103);
                                ArrayList<String> variopt = new ArrayList<>();
                                ArrayList<String> varioptcombinationid = new ArrayList<>();
                                ArrayList<String> variationFunction = new ArrayList<>();
                                ArrayList<String> variationPrice = new ArrayList<>();

                                for (int i = 0; i < variationoptionlistful.get(variationName.get(position)).size(); i++) {
                                    variopt.add(variationoptionlistful.get(variationName.get(position)).get(i).getVariationName());
                                    varioptcombinationid.add(variationoptionlistful.get(variationName.get(position)).get(i).getCombinationId());
                                    variationFunction.add(variationoptionlistful.get(variationName.get(position)).get(i).getVariationFunction());
                                    variationPrice.add(variationoptionlistful.get(variationName.get(position)).get(i).getVariationPrice());
                                }

//                            Toast.makeText(getApplicationContext(), variationName.get(position)+":"+variationoptionlistful.get(variationName.get(position)).size(), Toast.LENGTH_SHORT).show();
                                showdialogvariation(variationoptionlistful.get(variationName.get(position)).size(), variopt, varioptcombinationid, variationFunction, variationPrice);

                            }
                        });
                    }
                }
                else
                {

                }
            }

            @Override
            public void onFailure(Call<VariationExample> call, Throwable t) {

            }
        });


        conflistview = findViewById(R.id.conflistview);
        GetconfigrableAPI getconfigrableAPI = retrofit.create(GetconfigrableAPI.class);
        Call<ConfFieldExample> ConfFieldExample = getconfigrableAPI.getDetails(MainActivity.Tokendata,pid,BankId);
        ConfFieldExample.enqueue(new Callback<com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO.ConfFieldExample>() {
            @Override
            public void onResponse(Call<com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO.ConfFieldExample> call, Response<com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO.ConfFieldExample> response) {
                if(response.isSuccessful()) {
                    int status = response.body().getStatus();
                    configrableName = new ArrayList<>();
                    configrableRequired = new ArrayList<>();
                    configrablefeildID = new ArrayList<>();
                    constraintlayoutconfigrable.setVisibility(View.GONE);
                if (status == 1)
                {
                    constraintlayoutconfigrable.setVisibility(View.VISIBLE);
                    confFieldDetails = response.body().getDetails();
                    for (int i = 0 ;i<confFieldDetails.size();i++)
                    {

                        configrableName.add(confFieldDetails.get(i).getFieldName());
                        configrableRequired.add(confFieldDetails.get(i).getFieldRequired());
                        configrablefeildID.add(confFieldDetails.get(i).getFieldId());
                        Log.d("confidfromretrofit ",""+confFieldDetails.get(i).getFieldId());

                        configrableoptionlistful.put(configrableName.get(i),confFieldDetails.get(i).getFieldOptions());

                    }
                    MyConfigrableListAdapter myConfigrableListAdapter = new MyConfigrableListAdapter(getApplicationContext(), configrableoptionlistful);
                    conflistview.setAdapter(myConfigrableListAdapter);
                    justifyConfigrableListViewHeightBasedOnChildren(conflistview);
                    conflistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            variationnametxt = (TextView) view.findViewById(R.id.textView76);
                            variname = view.findViewById(R.id.textView103);
                            ArrayList<String> confiopt = new ArrayList<>();
                            for (int i = 0; i < configrableoptionlistful.get(configrableName.get(position)).size(); i++) {
                                confiopt.add(configrableoptionlistful.get(configrableName.get(position)).get(i).getFieldOptions());

                            }
                            configrablefeildIDforcart  = configrablefeildID.get(position);
                            Log.d("conffeildaddingstate",""+configrablefeildID.get(position));
                            showdialogconfigrable(configrableoptionlistful.get(configrableName.get(position)).size(),confiopt);
                        }
                    });

                }
                }
            }

            @Override
            public void onFailure(Call<com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO.ConfFieldExample> call, Throwable t) {

            }
        });

//        android.support.constraint.ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) constraintLayout84.getLayoutParams();
////        lp.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//        lp.horizontalBias = Float.parseFloat(Integer.toString(1000));
//        constraintLayout84.setLayoutParams(lp);
//        constraintLayout84.requestLayout();


//        ListAdapter listadp = variationlistview.getAdapter();
//        if (listadp != null) {
//            int totalHeight = 0;
//            for (int i = 0; i < listadp.getCount(); i++) {
//                View listItem = listadp.getView(i, null, variationlistview);
//                listItem.measure(0, 0);
//                totalHeight += listItem.getMeasuredHeight();
//            }
//            ViewGroup.LayoutParams params = variationlistview.getLayoutParams();
//            params.height = totalHeight + (variationlistview.getDividerHeight() * (listadp.getCount() - 1));
//            variationlistview.setLayoutParams(params);
//            variationlistview.requestLayout();
//        }
        Socialshare.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        String title = Dashboard.prefConfig.readName()+" shared Homeshopping.pk product with you";
        String link = "Follow this link to see the product. " +
                ProdURL;
        intent.putExtra(Intent.EXTRA_SUBJECT,title);
        intent.putExtra(Intent.EXTRA_TEXT,link);
        startActivity(Intent.createChooser(intent,"Share Homeshoppin.pk product using")) ;
    }
});
    }

    public String getMyData() {
        return ProductDescription;
    }

    public class MyVariationListAdapter extends ArrayAdapter<String> {

        private final Context context;
        private final ArrayList variationname;
        private   HashMap<String,List<Variation>> variationlistoptionful ;

        public MyVariationListAdapter(Context context, HashMap<String,List<Variation>> variationlistoptionful ) {
            super(context, R.layout.variationcustom, variationName);


            this.context=context;
            this.variationname=variationName;
            this.variationlistoptionful = variationlistoptionful;

        }

        public View getView(final int position, View view, ViewGroup parent) {
            LayoutInflater inflater=LayoutInflater.from(getApplicationContext());
            View rowView=inflater.inflate(R.layout.variationcustom, null,true);

            variationnametxt = (TextView) rowView.findViewById(R.id.textView76);
             variname = rowView.findViewById(R.id.textView103);
            ConstraintLayout variitrmconstraint = rowView.findViewById(R.id.variitrmconstraint);
            variname.setText(variationName.get(position));
//            variationnametxt.setText(variationlistoptionful.get(variationName.get(position)).get(position).getVariationName());

//            variitrmconstraint.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });
//





            return rowView;

        };
    }


    public class MyConfigrableListAdapter extends ArrayAdapter<String> {

        private final Context context;
        private final ArrayList configrablename;
        private   HashMap<String,List<ConfFeildFieldOption>> configrablelistoptionful ;

        public MyConfigrableListAdapter(Context context, HashMap<String,List<ConfFeildFieldOption>> configrablelistoptionful ) {
            super(context, R.layout.variationcustom, configrableName);


            this.context=context;
            this.configrablename=configrableName;
            this.configrablelistoptionful = configrablelistoptionful;

        }

        public View getView(final int position, View view, ViewGroup parent) {
            LayoutInflater inflater=LayoutInflater.from(getApplicationContext());
            View rowView=inflater.inflate(R.layout.variationcustom, null,true);

            variationnametxt = (TextView) rowView.findViewById(R.id.textView76);
            variname = rowView.findViewById(R.id.textView103);
            ConstraintLayout variitrmconstraint = rowView.findViewById(R.id.variitrmconstraint);
            variname.setText(configrableName.get(position));
//            variationnametxt.setText(variationlistoptionful.get(variationName.get(position)).get(position).getVariationName());

//            variitrmconstraint.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                }
//            });
//





            return rowView;

        };
    }

    public String showdialogvariation(int varicount,ArrayList<String>  variationname,ArrayList<String> variationcombinationID,ArrayList<String> variationfunction, ArrayList<String> variationprice )
    {
        final String[] selectedvariation = {""};
        final String[] selectedvariationcombinationID = {""};
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.customvariationlayout, null);
        dialogBuilder.setView(dialogView);
        View view = null;
        dialogBuilder.setTitle("Variation ");
         rg = (RadioGroup) dialogView.findViewById(R.id.variradiogroup);
        rg.setOrientation(RadioGroup.VERTICAL);
         rb = new RadioButton[varicount];
        for (int i = 0 ; i < varicount ; i++)
        {
            rb[i]  = new RadioButton(this);
            rg.addView(rb[i]); //the RadioButtons are added to the radioGroup instead of the layout
            rb[i].setText(variationname.get(i));
            selectedvariationcombinationID[0] = variationcombinationID.get(i);
        }

        dialogBuilder.setCancelable(true);
        final View finalView = view;

        final  AlertDialog b = dialogBuilder.create();
        Button apply = dialogView.findViewById(R.id.button17);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               try {
                   int selectedvalue = rg.getCheckedRadioButtonId();
                   rb[0] = dialogView.findViewById(selectedvalue);
                   selectedvariation[0] = rb[0].getText().toString();

//                Toast.makeText(CategoryActivity.this, " a "+Sortvalue, Toast.LENGTH_SHORT).show();
                   variationnametxt.setText(selectedvariation[0]);

                   int posi = -1;
                   posi = variationname.indexOf(selectedvariation[0]);
                   if(posi == -1)
                   {
                       Toast.makeText(ProductDetailActivity.this, "No value found", Toast.LENGTH_SHORT).show();
                   }
                   else
                   {

                       strvariname = variationname.get(posi);
                       strvaricombid = variationcombinationID.get(posi);
                       strvarifunc = variationfunction.get(posi);
                       strvariprice = variationprice.get(posi);
//                       Toast.makeText(ProductDetailActivity.this, "variname ="+strvariname+"\nvaricombid "+strvaricombid
//                               +"\nvarifunc "+strvarifunc+"\nvariprice "+strvariprice, Toast.LENGTH_SHORT).show();
                        pricefloat = Float.parseFloat(ProductPrice);
                       float varipricefloat = Float.parseFloat(strvariprice);
                       pricefloat = pricefloat+varipricefloat;
                       DecimalFormat formatter = new DecimalFormat("#,###,###");
                       String pricee = formatter.format(Math.round(pricefloat));
                       price.setText("Rs "+ pricee);

                   }

               }
               catch (Exception e)
               {

               }


                b.dismiss();
            }
        });
        b.show();
return selectedvariation[0];
    }


    public String showdialogconfigrable(int conficount,ArrayList<String>  configrableNamename )
    {
        final String[] selectedconfigrable = {""};

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.customvariationlayout, null);
        dialogBuilder.setView(dialogView);
        View view = null;
        dialogBuilder.setTitle("Variation ");
        rg = (RadioGroup) dialogView.findViewById(R.id.variradiogroup);
        rg.setOrientation(RadioGroup.VERTICAL);
        rb = new RadioButton[conficount];
        for (int i = 0 ; i < conficount ; i++)
        {
            rb[i]  = new RadioButton(this);
            rg.addView(rb[i]); //the RadioButtons are added to the radioGroup instead of the layout
            rb[i].setText(configrableNamename.get(i));
//            selectedvariationcombinationID[0] = variationcombinationID.get(i);
        }

        dialogBuilder.setCancelable(true);
        final View finalView = view;

        final  AlertDialog b = dialogBuilder.create();
        Button apply = dialogView.findViewById(R.id.button17);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int selectedvalue = rg.getCheckedRadioButtonId();
                    rb[0] = dialogView.findViewById(selectedvalue);
                    selectedconfigrable[0] = rb[0].getText().toString();
                    configrablefeildNameforcart =selectedconfigrable[0];


//                Toast.makeText(CategoryActivity.this, " a "+Sortvalue, Toast.LENGTH_SHORT).show();
                    variationnametxt.setText(selectedconfigrable[0]);

                    int posi = -1;
                    posi = configrableNamename.indexOf(selectedconfigrable[0]);
//                    if(posi == -1)
//                    {
//                        Toast.makeText(ProductDetailActivity.this, "No value found", Toast.LENGTH_SHORT).show();
//                    }
//                    else
//                    {
//
//                        strvariname = variationname.get(posi);
//                        strvaricombid = variationcombinationID.get(posi);
//                        strvarifunc = variationfunction.get(posi);
//                        strvariprice = variationprice.get(posi);
////                       Toast.makeText(ProductDetailActivity.this, "variname ="+strvariname+"\nvaricombid "+strvaricombid
////                               +"\nvarifunc "+strvarifunc+"\nvariprice "+strvariprice, Toast.LENGTH_SHORT).show();
//                        pricefloat = Float.parseFloat(ProductPrice);
//                        float varipricefloat = Float.parseFloat(strvariprice);
//                        pricefloat = pricefloat+varipricefloat;
//                        DecimalFormat formatter = new DecimalFormat("#,###,###");
//                        String pricee = formatter.format(Math.round(pricefloat));
//                        price.setText("Rs "+ pricee);
//
//                    }

                }
                catch (Exception e)
                {

                }


                b.dismiss();
            }
        });
        b.show();
        return selectedconfigrable[0];
    }



    public static void justifyListViewHeightBasedOnChildren (ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) constraintLayout84.getLayoutParams();
////        lp.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//lp.horizontalBias = Float.parseFloat(Integer.toString(totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1))));
//        constraintLayout84.setLayoutParams(lp);
//        constraintLayout84.requestLayout();
        listView.setLayoutParams(par);
        listView.requestLayout();
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(constraintLayout84);
        constraintSet.constrainHeight(R.id.constraintLayout84,par.height);
//        constraintSet.setVerticalBias(R.id.constraintLayout84,7000.0f);
        constraintSet.applyTo(constraintLayout84);
    }


    public static void justifyConfigrableListViewHeightBasedOnChildren (ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) constraintLayout84.getLayoutParams();
////        lp.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//lp.horizontalBias = Float.parseFloat(Integer.toString(totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1))));
//        constraintLayout84.setLayoutParams(lp);
//        constraintLayout84.requestLayout();
        listView.setLayoutParams(par);
        listView.requestLayout();
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(constraintlayoutconfigrable);
        constraintSet.constrainHeight(R.id.constraintlayoutconfigrable,par.height);
//        constraintSet.setVerticalBias(R.id.constraintLayout84,7000.0f);
        constraintSet.applyTo(constraintlayoutconfigrable);
    }



    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

    private void addTabs(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(new DiscriptionFragment(), "Description");
        adapter.addFrag(new ReviewFragment(), "Reviews");
//        adapter.addFrag(new PricetrendFragment(),"Price Trend");
        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);
        finish();

    }

    private void setSliderViews(String[] imagelist) {

        for (int i = 0; i < imagelist.length; i++) {

          SliderView sliderView = new SliderView(this);

//            switch (i) {
//                case 0:
//                    sliderView.setImageUrl("https://cdn.homeshopping.pk/product_images/v/289/Oppo_A3s_Dual_Sim_%284G__2GB_RAM__16GB_ROM__Red%29_Official_Warranty__86301_zoom.jpg");
//                    break;
//                case 1:
//                    sliderView.setImageUrl("https://cdn.homeshopping.pk/product_images/n/676/oppo_a3s_16gb_dual_sim_red_1__06717_zoom.jpg");
//                    break;
//                case 2:
//                    sliderView.setImageUrl("https://cdn.homeshopping.pk/product_images/c/293/item_XXL_37627014_146475835__47498_zoom.jpg");
//                    break;
//                case 3:
//                    sliderView.setImageUrl("https://cdn.homeshopping.pk/product_images/b/670/item_XXL_37627014_146475837__24220_zoom.jpg");
//                    break;
//                case 4:
//                    sliderView.setImageUrl("https://cdn.homeshopping.pk/product_images/o/934/item_XXL_37627014_146475845__16827_zoom.jpg");
//                            break;
//
//            }
            sliderView.setImageUrl(imagelist[i]);

//            Toast.makeText(this, ""+imagelist[i], Toast.LENGTH_SHORT).show();
            sliderView.setImageScaleType(ImageView.ScaleType.FIT_CENTER);

//            sliderView.setDescription("setDescription " + (i + 1));
//            final int finalI = i;

//            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
//                @Override
//                public void onSliderClick(SliderView sliderView) {
//                    Toast.makeText(Dashboard.this, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
//                }
//            });

            //at last add this view in your layout :
            productsliderLayout.addSliderView(sliderView);
        }
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {

    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }


    @Override
    public void onAddProduct(int pid, String Qty, String userid, String combinationid, String productTitle, String Productprice, String Productimage, String ProductBrand, String feildid, String feildOption,String bankId) {
        Log.d("add to database ","pid "+Integer.toString(pid)+"\nQty "+Qty+"\nuserID "+userid+"\ncombinationid "+combinationid+"\nproductTitle "+productTitle+"\nProductprice "+Productprice+"\nProductimage "+Productimage+"\nProductBrand "+ProductBrand+"\nfeildid "+feildid+"\nfeildOption "+feildOption+"\nbankId "+bankId);
        db.addRecord(Integer.toString(pid),Qty,userid,combinationid,0,productTitle,Productprice,Productimage,ProductBrand,feildid,feildOption,bankId);
        Boolean res = Dashboard.prefConfig.readLoginStatus();
        if (res == false) {

            Cursor cursor1 = db.getcountnotlogin();
            if(cursor1.moveToFirst())
            {

                Dashboard.cart_count =cursor1.getInt(0);
                invalidateOptionsMenu();

//            Cursor cursor = db.getnotlogincartforDisplay();
//            if (cursor != null) {
//                if (cursor.moveToFirst()) {
//                    do {
//                        //calling the method to save the unsynced name to MySQL
//                        Dashboard.cart_count = cursor.getCount();
//                        invalidateOptionsMenu();
//
////                    Toast.makeText(this, "aa "+Dashboard.cart_count, Toast.LENGTH_SHORT).show();
//                    } while (cursor.moveToNext());
//                }
                try {
//                    Toast.makeText(getApplicationContext(), "Throughing", Toast.LENGTH_SHORT).show();
                    getApplicationContext().registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    getApplicationContext().registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                }
                catch (Exception e)
                {
                    Log.e("error",""+e);
                }

            }
        }else {
            Cursor cursor1 = db.getRecord();
            if (cursor1 != null) {
//                if (cursor1.moveToFirst()) {
//                    do {
//                        //calling the method to save the unsynced name to MySQL
//
//
//
////                    Toast.makeText(this, "aa "+Dashboard.cart_count, Toast.LENGTH_SHORT).show();
//                    } while (cursor1.moveToNext());
//                }
                Dashboard.cart_count =Dashboard.cart_count+Integer.parseInt(Qty);
                invalidateOptionsMenu();
                try {
//                    Toast.makeText(getApplicationContext(), "Throughing", Toast.LENGTH_SHORT).show();
                    getApplicationContext().registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    getApplicationContext().registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                }
                catch (Exception e)
                {
                    Log.e("error",""+e);
                }

            }
        }
        this.invalidateOptionsMenu();

//        db.addRecord(Integer.toString(pid),Qty,userid,bankid,0);
//        Toast.makeText(context, "Imei number: " + Dashboard.Imei+"\nPhone number "+Dashboard.Phonenumber, Toast.LENGTH_SHORT).show();


    }

    @Override
    public void onRemoveProduct() {
        Dashboard.cart_count--;
        invalidateOptionsMenu();

    }

    @Override
    public void onAddProduct() {
        Dashboard.cart_count++;
        invalidateOptionsMenu();
    }


    private int getScale(){
        Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        Double val = new Double(width)/new Double(360);
        val = val * 100d;
        return val.intValue();
    }


    public  class RecyclerViewAdapter2 extends RecyclerView.Adapter<RecyclerViewAdapter2.ViewHolder> {

        private static final String TAG = "RecyclerViewAdapter";

        //vars

        private ArrayList<Integer> mImageUrlsee = new ArrayList<>();
        ArrayList<String> Productimagee ;
        ArrayList<String> productnamee ;
        ArrayList<String> orignalpricetvv ;
        ArrayList<String> offpercenttvv ;
        ArrayList<String>discountedpricetvv;
        ArrayList<String>proddiscriptionn;
        String[][] prodimage2dd;
        ArrayList<Integer> pid ;
        ArrayList<String> prodbrandd;
        ArrayList<Integer> prodwishlistt ;
        ArrayList<Integer> prodisexistincartt ;




        public    Context mContext;


        public RecyclerViewAdapter2(ArrayList<String> Productimageee,ArrayList<String> productnameee,
                                    ArrayList<String> orignalpricetvvv,
                                    ArrayList<String> discountedpricetvvv,ArrayList<String>proddiscription,
                                    ArrayList<Integer> prodid,ArrayList<String> prodbrand,ArrayList<Integer> prodwishlist,ArrayList<Integer> prodisexistincart) {
            mContext = getApplicationContext();
            Productimagee = Productimageee;
            productnamee = productnameee;
            orignalpricetvv = orignalpricetvvv;
            discountedpricetvv = discountedpricetvvv;
            proddiscriptionn = proddiscription;
            pid = prodid;
            prodbrandd = prodbrand;
            prodwishlistt = prodwishlist;
            prodisexistincartt = prodisexistincart;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custommonthlytopitem, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Log.d(TAG, "onBindViewHolder: called.");

//            holder.name.setText(mNames.get(position));
            Glide.with(mContext).load(Productimagee.get(position))
                    .into(holder.Productimage);

//            holder.goalname.setText(mNames.get(position).toString());
//
//            holder.status.setText(mStatus.get(position).toString());


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
//            Date date = null;
//            String showdate = "";
//            try {
//                date = simpleDateFormat.parse(start_datee[position]);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            showdate = simpleDateFormat1.format(date);
//            holder.Productimage.setBackgroundResource(mImageUrls.get(1));
            Productprice = Math.round(Float.parseFloat(orignalpricetvv.get(position)));
            SalePrice = Math.round(Float.parseFloat(discountedpricetvv.get(position))) ;
            discoutnamount = Productprice-SalePrice;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            final String formatedproductprice = formatter.format(Productprice);
            final String formatedsaleprice = formatter.format(SalePrice);

            Log.e("Discount data",Productprice+"\n"+SalePrice+"\n"+discoutnamount);

            Boolean res2 = db.getproductforwishlist(pid.get(position));

            if(res2)
            {
                holder.wishlisticon.setBackgroundResource(R.drawable.heartfilled);
            }
            else
            {
                holder.wishlisticon.setBackgroundResource(R.drawable.heartgrey);
            }
            Boolean res = db.getproductforaddedcart(pid.get(position));


            if(res == false)
            {
                holder.addcart.setBackgroundResource(R.drawable.add_to_cart);
                addcartstatus = 0;
            }
            else
            {
                holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                holder.addcart.setEnabled(false);
                addcartstatus = 1;
            }

            holder.wishlisticon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Dashboard.prefConfig.readLoginStatus()) {


                        if (!db.getproductforwishlist(pid.get(position))) {
                            db.addRecordwishlist(String.valueOf(pid.get(position)),Dashboard.prefConfig.readID());


                            Toast.makeText(getApplicationContext(), "Please wait adding item to wishlist.", Toast.LENGTH_SHORT).show();
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(ROOT_URL) //Setting the Root URL
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                            AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
                            Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata, String.valueOf(pid.get(position)), Dashboard.prefConfig.readID());
                            //   Toast.makeText(mContext, " id = "+pid.get(position), Toast.LENGTH_SHORT).show();
                            call.enqueue(new Callback<generalstatusmessagePOJO>() {
                                @Override
                                public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                                    if (response.isSuccessful()) {
                                        int status = response.body().getStatus();
                                        String message = response.body().getMessage().toString();
                                        if (status == 1) {
                                            holder.wishlisticon.setBackgroundResource(R.drawable.heartfilled);
//                                View parentLayout = v.findViewById(android.R.id.content);
//                                Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
//                                View sbView = snackbar.getView();
//                                sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                                snackbar.show();

                                        } else {
                                            holder.wishlisticon.setBackgroundResource(R.drawable.heartgrey);
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), "" + response.message(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(), "Item already added to wishlist.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Kindly login first to make wishlist", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            if(SalePrice<Productprice)
            {
                holder.offpercenttv.setVisibility(View.VISIBLE);
                holder.discountedpricetv.setVisibility(View.VISIBLE);
                holder.orignalpricetv.setTextColor(Color.LTGRAY);
                int percentage = ((discoutnamount*100)/Productprice);
                if(productnamee.get(position).length()>=47) {
                    String stringlimit = productnamee.get(position).substring(0, 47);
                    holder.productname.setText(stringlimit + "...");
                }
                else
                {
                    holder.productname.setText(productnamee.get(position)+ "...");
                }
                holder.orignalpricetv.setText("PKR "+formatedproductprice);
                holder.orignalpricetv.setTextSize(10);
                holder.offpercenttv.setText( percentage+"%"+" OFF");
                holder.discountedpricetv.setText("PKR "+formatedsaleprice);
                holder.brandname.setText(prodbrandd.get(position));
                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid.get(position),"1",Dashboard.prefConfig.readID(),"0",productnamee.get(position),Float.toString(Math.round(Float.parseFloat(discountedpricetvv.get(position)))),Productimagee.get(position),prodbrandd.get(position),"","",BankId);
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
//                        Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setEnabled(false);
                        addcartstatus = 1;
                    }
                });

                SalePrice = 1;
            }
            else
            {
                if(productnamee.get(position).length()>=47) {
                    String stringlimit = productnamee.get(position).substring(0, 47);
                    holder.productname.setText(stringlimit + "...");
                }
                else
                {
                    holder.productname.setText(productnamee.get(position)+ "...");
                }
                holder.orignalpricetv.setText("PKR "+formatedsaleprice);
                holder.orignalpricetv.setTextColor(Color.BLACK);
                holder.orignalpricetv.setTextSize(20);
                holder.orignalpricetv.setPaintFlags(0);
                holder.offpercenttv.setVisibility(View.GONE);
                holder.discountedpricetv.setVisibility(View.GONE);
                holder.brandname.setText(prodbrandd.get(position));
//                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid.get(position),"1",Dashboard.prefConfig.readID(),"0",productnamee.get(position),Float.toString(Math.round(Float.parseFloat(orignalpricetvv.get(position)))),Productimagee.get(position),prodbrandd.get(position),"","",BankId);
//                        Toast.makeText(mContext, "Added to cart!"+pid.get(position) , Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        addcartstatus = 1;

                    }
                });

            }

//            holder.allcons.addOnItemTouchListener(new RecyclerItemClickListener(Dashboard.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//                @Override
//                public void onItemClick(View view, int position) {
//                    // ...
//                    // Toast.makeText(MainActivity.this, "hello "+position, Toast.LENGTH_SHORT).show();
//`
////                    Intent intent = new Intent(getApplicationContext(),EventDiscriptionActivity.class);
////                    intent.putExtra("ev_tittlee",ev_tittlee[position]);
////                    intent.putExtra("ev_excerptt",ev_excerptt[position]);
////                    intent.putExtra("ev_discriptionn",ev_discriptionn[position]);
////                    intent.putExtra("start_datee",start_datee[position]);
////                    intent.putExtra("end_datee",end_datee[position]);
////                    intent.putExtra("starttimee",starttimee[position]);
////                    intent.putExtra("endtimee",endtimee[position]);
////
////                    intent.putExtra("ev_imagee",ev_imagee[position]);
////                    intent.putExtra("ev_locationn",ev_locationn[position]);
////                    intent.putExtra("ev_addresss",ev_addresss[position]);
//////
////                    startActivity(intent);
//                        Intent intent = new Intent(getApplicationContext(),ProductDetailActivity.class);
//                        intent.putExtra("title",productnamee[position]);
//                        intent.putExtra("discription",proddiscriptionn[position]);
//                        startActivity(intent);
////                    onAddProduct();
//
//
//
//                }
//
//                @Override
//                public void onItemLongClick(View view, int position) {
//                    // ...
//                }
//            }));

            holder.allcons.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(mContext, "clic inside", Toast.LENGTH_SHORT).show();

//                    for(int j = 0 ;j<prodimage2d.length;j++ ) {
//                        for(int i = 0 ; i <prodimage2d[j].length;i++) {
//                            Toast.makeText(mContext, "" + prodimage2d[j][i], Toast.LENGTH_SHORT).show();
//                        }
//                        }
//
  try {

//      Toast.makeText(mContext, "try inside"+Integer.toString(pid.get(position)), Toast.LENGTH_SHORT).show();
    finish();

      Intent intent = new Intent(getApplicationContext(), ProductDetailActivity.class);
      intent.putExtra("title",productnamee.get(position));
//                    intent.putExtra("discription",proddiscriptionn.get(position));
      intent.putExtra("price",orignalpricetvv.get(position));
      intent.putExtra("saleprice",discountedpricetvv.get(position));
      intent.putExtra("productid",Integer.toString(pid.get(position)));
      intent.putExtra("prodbrand",prodbrandd.get(position));
      intent.putExtra("prodwishlist",prodwishlistt.get(position));
      intent.putExtra("addcartstatus",addcartstatus);
      startActivity(intent);
  }
  catch (Exception e)
  {

//      Toast.makeText(mContext, "catch inside", Toast.LENGTH_SHORT).show();

  }

//                    Toast.makeText(mContext, "product id ="+pid[position], Toast.LENGTH_SHORT).show();
                }
            });

//            holder.wishlisticon.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(final View v) {
//                    Retrofit retrofit = new Retrofit.Builder()
//                            .baseUrl(ROOT_URL) //Setting the Root URL
//                            .addConverterFactory(GsonConverterFactory.create())
//                            .build();
//                    AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
//                    Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata,String.valueOf(pid.get(position)),Dashboard.prefConfig.readID());
//                    Toast.makeText(mContext, " id = "+pid.get(position), Toast.LENGTH_SHORT).show();
//                    call.enqueue(new Callback<generalstatusmessagePOJO>() {
//                        @Override
//                        public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
//                            if (response.isSuccessful()) {
//                                int status = response.body().getStatus();
//                                String message = response.body().getMessage().toString();
//                                if (status == 1) {
//                                    holder.wishlisticon.setBackgroundResource(R.drawable.wishlistaddedicon);
////                                    View parentLayout = findViewById(android.R.id.content);
////                                    Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
////                                    View sbView = snackbar.getView();
////                                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
////                                    snackbar.show();
//                                    Toast.makeText(ProductDetailActivity.this, "Added to wishlist", Toast.LENGTH_SHORT).show();
//
//                                } else {
//                                    holder.wishlisticon.setBackgroundResource(R.drawable.wishlistnotaddedicon);
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {
//
//                        }
//                    });
//
//                }
//            });

        }


        @Override
        public int getItemCount() {
            return Productimagee.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            ImageView Productimage,addcart,wishlisticon;
            TextView productname,orignalpricetv,offpercenttv,discountedpricetv,brandname;
            ConstraintLayout allcons;
            com.iarcuschin.simpleratingbar.SimpleRatingBar ratingBaritem;



            public ViewHolder(View itemView) {
                super(itemView);
                Productimage = itemView.findViewById(R.id.imageView2);
                productname = itemView.findViewById(R.id.productname);
                orignalpricetv = itemView.findViewById(R.id.orignalpricetv);
                offpercenttv = itemView.findViewById(R.id.offpercenttv);
                discountedpricetv = itemView.findViewById(R.id.discountedpricetv);
                addcart = itemView.findViewById(R.id.addcart);
                allcons = itemView.findViewById(R.id.allcons);
                wishlisticon = itemView.findViewById(R.id.wishlisticon);
                brandname = itemView.findViewById(R.id.brandname);
            }
        }

    }




    public void buynow(View view)
            {
                if(buynow.getText().equals("Added to cart"))
                {}
                else {

                    try {
                        //    Toast.makeText(this, "qty "+qtyespinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                        Log.d("conffeilIF",""+configrablefeildIDforcart);
                        if(BankId.equals("0"))
                            onAddProduct(Integer.parseInt(pid), qtyespinner.getText().toString(), Dashboard.prefConfig.readID(), strvaricombid, title, Float.toString(pricefloat), imagelist[0], probrand.getText().toString(), Integer.toString(configrablefeildIDforcart), configrablefeildNameforcart,BankId);
                        else
                            onAddProduct(Integer.parseInt(pid), qtyespinner.getText().toString(), Dashboard.prefConfig.readID(), strvaricombid, title, Float.toString(salepricefloat), imagelist[0], probrand.getText().toString(), Integer.toString(configrablefeildIDforcart), configrablefeildNameforcart,BankId);




                        //              Dashboard.cart_count = (Dashboard.cart_count + Integer.parseInt(qtyespinner.getText().toString()));
//                Toast.makeText(this, ""+ (Dashboard.cart_count +Integer.parseInt(qtyespinner.getText().toString())), Toast.LENGTH_SHORT).show();

//                invalidateOptionsMenu();

//                        Toast.makeText(getApplicationContext(), "Added to cart!", Toast.LENGTH_SHORT).show();
                        //    holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        buynow.setText("Added to cart");

                    } catch (Exception e) {
                        Toast.makeText(this, "Try again.", Toast.LENGTH_SHORT).show();
                    }
                }

    }

    public void Loadproducts(String sort ,int pcatid) {
        if(pcatid == 0)
            pcatid = 2206;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        CategoryProductApi api = retrofit.create(CategoryProductApi.class);
//        Toast.makeText(this, "prod id = "+pcatid, Toast.LENGTH_SHORT).show();
        Call<ProductExample> call = api.getDetails(MainActivity.Tokendata,Integer.toString(pcatid), "0", "14", sort,Dashboard.prefConfig.readID());
        call.enqueue(new Callback<ProductExample>() {
            @Override
            public void onResponse(Call<ProductExample> call, Response<ProductExample> response) {
                if (response.isSuccessful())
                {
                int status = response.body().getStatus();
                if (status == 1) {
                    list = response.body().getDetails();
//                prodname = new String[list.size()];
//                prodprice = new String[list.size()];
//                prodoffper = new String[list.size()];
//                proddiscount = new String[list.size()];
//                prodimage = new String[list.size()];
//                proddiscription = new String[list.size()];
//                prodid = new String[list.size()];
//                prodbrand = new String[list.size()];
//
//                Toast.makeText(CategoryActivity.this, ""+response.body(), Toast.LENGTH_SHORT).show();
                    recomendProductID.clear();
                    recomendProductPrice.clear();
                    recomendProductSalePrice.clear();
                    recomendproductImages.clear();
                    mImageUrls.clear();
                    recomendProductID.clear();
                    recomendBrandDisplayName.clear();
                    recomendproductwishlist.clear();
                    recomendproductisExistInCart.clear();
                    for (int i = 0; i < list.size(); i++) {
                        recomendProductName.add(list.get(i).getProductName());
                        recomendProductPrice.add(list.get(i).getProductPrice());
                        recomendProductSalePrice.add(list.get(i).getProductSalePrice());
                        recomendproductImages.add(list.get(i).getProductImages().getImageThumb());
//
//                        mImageUrls.add(recomendproductImages.get(i));

                        recomendProductID.add(list.get(i).getProductID());
                        recomendBrandDisplayName.add(list.get(i).getBrandDisplayName());
                        recomendproductwishlist.add(list.get(i).getProductWishlist());
                        recomendproductisExistInCart.add(list.get(i).getIsExistInCart());
                    }
                    recyclerViewmostitem.setAdapter(null);
                    RecyclerViewAdapter2 recyclerViewAdapter2 = new RecyclerViewAdapter2(recomendproductImages, recomendProductName,
                            recomendProductPrice, recomendProductSalePrice, recomendProductDescription, recomendProductID, recomendBrandDisplayName,recomendproductwishlist,recomendproductisExistInCart);
                    recyclerViewmostitem.setAdapter(recyclerViewAdapter2);
                    recyclerViewmostitem.setAdapter(recyclerViewAdapter2);
                }
                }
                else
                {
                    Toast.makeText(getApplicationContext(), ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<ProductExample> call, Throwable t) {

            }
        });

    }

}

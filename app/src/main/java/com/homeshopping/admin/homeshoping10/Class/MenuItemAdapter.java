package com.homeshopping.admin.homeshoping10.Class;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.homeshopping.admin.homeshoping10.R;

import java.util.ArrayList;
import java.util.List;

public class MenuItemAdapter extends ArrayAdapter<MenuItem>
{
    private List<MenuItem> itemList = new ArrayList<MenuItem>();


    Context context;

        @Override
        public void add(MenuItem object) {
            itemList.add(object);
            super.add(object);
        }

        public MenuItemAdapter(List<MenuItem> rList, Context context) {
            super(context, R.layout.menu_custom_item_layout);
            this.context = context;
            this.itemList = rList;
        }

private class ViewHolder {
    TextView txtmenuItemName,statustext;
    ImageView textmenuchildarrow;
}

    public int getCount() {
        return this.itemList.size();
    }

    public MenuItem getItem(int index) {
        return this.itemList.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = new ViewHolder();

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.menu_custom_item_layout, parent, false);

            holder.txtmenuItemName = (TextView) convertView.findViewById(R.id.menu_textview);
            holder.statustext = (TextView) convertView.findViewById(R.id.statustext);
            holder.textmenuchildarrow = (ImageView) convertView.findViewById(R.id.menu_arrow);


            convertView.setTag(holder);
        } else

            holder = (ViewHolder) convertView.getTag();

        MenuItem item = getItem(position);

        holder.txtmenuItemName.setText(item.getItemname());
        if (item.getChildstatus() == 0)
        {
            holder.textmenuchildarrow.setVisibility(View.GONE);
            holder.statustext.setText("0");
        }
        else
        {
            holder.textmenuchildarrow.setVisibility(View.VISIBLE);
            holder.statustext.setText("1");
        }

        return convertView;
    }

}
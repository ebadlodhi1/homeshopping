package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface UpdateUserDetailApi {
    @PUT("user/putCustomerDetail")
    Call<generalstatusmessagePOJO> getDetails(@Header("data") String data, @Query("customerId") String customerId, @Query("customerFirstName") String customerFirstName,
                                              @Query("customerLastName") String customerLastName, @Query("customerEmail") String customerEmail, @Query("customerPhone") String customerPhone    );
}

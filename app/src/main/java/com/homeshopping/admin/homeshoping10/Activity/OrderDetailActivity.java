package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Interfaces.OrderdetailsAPI;
import com.homeshopping.admin.homeshoping10.POJO.OrderDetailPOJO.OrderDetail;
import com.homeshopping.admin.homeshoping10.POJO.OrderDetailPOJO.OrderdetailExample;
import com.homeshopping.admin.homeshoping10.POJO.OrderListPOJO.Detail;
import com.homeshopping.admin.homeshoping10.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class OrderDetailActivity extends AppCompatActivity {
    ListView orderlistview,orderdetailprodlist;
    List<Detail> list1 = new ArrayList<Detail>();
    String[] orderid,orderstatus,ordertotal,orderdate,Image;
    int[]  productQuantity;
    String[] itemprodid,itemprodtitle,itemprodqty,itemprodprice,itemprodcon,itemprodimageurl,brandDisplayName;
    String formattedDateyear,formattedDatemonth,formattedDateday,formatedproductprice;
    ProgressBar progressBar5;
    List<OrderDetail> listord = new ArrayList<>();
String orderidd , orderstatuss,orderdatee,ordertotall = "";
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle bundle = getIntent().getExtras();
        orderidd = bundle.getString("orderid");
        orderstatuss = bundle.getString("orderstatus");
        orderdatee = bundle.getString("orderdate");
        ordertotall = bundle.getString("ordertotal");
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        formatedproductprice = formatter.format(Math.round(Float.parseFloat(ordertotall)));
        String dateunix =orderdatee ;
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Order Detail Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());

        long unixSeconds = Long.parseLong(dateunix);
// convert seconds to milliseconds
        Date dateun = new java.util.Date(unixSeconds*1000L);
// the format of your date
        SimpleDateFormat sdfyear = new java.text.SimpleDateFormat("yyyy", Locale.US);
        SimpleDateFormat sdfmonth = new java.text.SimpleDateFormat("MMM", Locale.US);
        SimpleDateFormat sdfday = new java.text.SimpleDateFormat("dd", Locale.US);
// give a timezone reference for formatting (see comment at the bottom)

        formattedDateyear = sdfyear.format(dateun);
        formattedDatemonth = sdfmonth.format(dateun);
        formattedDateday = sdfday.format(dateun);

        TextView orderid = findViewById(R.id.textView43);
        TextView orderstatus = findViewById(R.id.textView54);

        TextView orderdate = findViewById(R.id.textView58);

        TextView ordertotal = findViewById(R.id.textView60);
        orderid.setText(""+orderidd);
        orderstatus.setText(""+orderstatuss);
        orderdate.setText(""+formattedDateday+"-"+formattedDatemonth+"-"+formattedDateyear);
        orderid.setText(""+orderidd);
        ordertotal.setText(""+formatedproductprice);

        orderdetailprodlist = findViewById(R.id.orderdetailprodlistt);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        OrderdetailsAPI orderdetailsAPI = retrofit.create(OrderdetailsAPI.class);
//        Toast.makeText(this, "dd "+ordid, Toast.LENGTH_SHORT).show();
        Call<OrderdetailExample> orderInfoCall = orderdetailsAPI.getDetails(MainActivity.Tokendata,orderidd);
        orderInfoCall.enqueue(new Callback<OrderdetailExample>() {
            @Override
            public void onResponse(Call<OrderdetailExample> call, Response<OrderdetailExample> response) {

                if (response.isSuccessful()) {
                    listord =  response.body().getOrderDetail();

//                    Toast.makeText(getApplicationContext(), "tot" + listord.size(), Toast.LENGTH_SHORT).show();

                    itemprodid = new String[listord.size()];
                    itemprodtitle = new String[listord.size()];
                    itemprodqty = new String[listord.size()];
                    itemprodprice = new String[listord.size()];
                    itemprodcon = new String[listord.size()];
                    itemprodimageurl = new String[listord.size()];
                    brandDisplayName = new String[listord.size()];
                    for (int i = 0; i < listord.size(); i++) {
                        itemprodid[i] = listord.get(i).getProductId().toString();
                        itemprodtitle[i] = listord.get(i).getProductTitle().toString();
                        itemprodqty[i] = listord.get(i).getProductQuantity().toString();
                        itemprodprice[i] = listord.get(i).getProductPrice().toString();
                        itemprodcon[i] = listord.get(i).getProductCondition().toString();
                        itemprodimageurl[i] = listord.get(i).getProductImageUrl().toString();
                        brandDisplayName[i] = listord.get(i).getBrandDisplayName();

                    }

                    MyListAdapter2 adapter = new MyListAdapter2(getApplicationContext());
                    orderdetailprodlist.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<OrderdetailExample> call, Throwable t) {
                Toast.makeText(getApplicationContext(), ""+t, Toast.LENGTH_SHORT).show();
                Log.e("Error",""+t);
            }
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);

    }



    public class MyListAdapter2 extends ArrayAdapter<String> {



        public MyListAdapter2(Context context ) {
            super(context, R.layout.customorderdetaillistitem, itemprodid);




        }

        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder2 vh;
            View v = view;
            if(v == null) {
                vh = new ViewHolder2();
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                v = li.inflate(R.layout.customorderdetaillistitem,null);
//            Example app = items.get(position);
//            Log.d("position",Integer.toString(position));
//            if(app != null) {

                vh.itemname = (TextView) v.findViewById(R.id.textView62);
                vh. itemqty = (TextView) v.findViewById(R.id.textView63);
                vh. itemcondition = (TextView) v.findViewById(R.id.textView65);
                vh.itemamount = (TextView) v.findViewById(R.id.textView67);
                vh.brandname = (TextView) v.findViewById(R.id.textView108);

                vh.itemimage = v.findViewById(R.id.imageView17);
                v.setTag(vh);
            }
            else{
                vh = (ViewHolder2) view.getTag();
            }



// give a timezone reference for formatting (see comment at the bottom)

            vh.brandname.setText(brandDisplayName[position]);
            vh.itemname.setText(itemprodtitle[position]);
            vh.itemqty.setText("Qty: "+itemprodqty[position]);
            vh.itemcondition.setText(itemprodcon[position]);

            DecimalFormat formatter = new DecimalFormat("#,###,###");
            formatedproductprice = formatter.format(Math.round(Float.parseFloat(itemprodprice[position])));
            vh.itemamount.setText("Rs "+formatedproductprice);
            Glide.with(getApplicationContext())
                    .load(itemprodimageurl[position])// image url
                    // any placeholder to load at start
                    .into( vh.itemimage);



            return v;

        };
    }
    static class ViewHolder2
    {
        // ImageView imageView;
        TextView itemname,itemqty,itemcondition,itemamount,brandname;
        ImageView itemimage;



    }


}

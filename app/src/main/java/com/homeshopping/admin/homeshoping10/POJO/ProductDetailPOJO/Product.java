package com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product {
    @SerializedName("ProductID")
    @Expose
    private Integer productID;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("ProductUrl")
    @Expose
    private String productUrl;
    @SerializedName("ProductDescription")
    @Expose
    private String productDescription;
    @SerializedName("ProductPrice")
    @Expose
    private String productPrice;
    @SerializedName("ProductSalePrice")
    @Expose
    private String productSalePrice;
    @SerializedName("ProductBankPrice")
    @Expose
    private String productBankPrice;
    @SerializedName("BrandDisplayName")
    @Expose
    private String brandDisplayName;
    @SerializedName("productRating")
    @Expose
    private Integer productRating;
    @SerializedName("VendorName")
    @Expose
    private String vendorName;
    @SerializedName("productCondition")
    @Expose
    private String productCondition;
    @SerializedName("productDelivery")
    @Expose
    private String productDelivery;
    @SerializedName("productAvailability")
    @Expose
    private Integer productAvailability;
    @SerializedName("isExistInCart")
    @Expose
    private Integer isExistInCart;
    @SerializedName("productCategoryId")
    @Expose
    private String productCategoryId;

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductSalePrice() {
        return productSalePrice;
    }

    public void setProductSalePrice(String productSalePrice) {
        this.productSalePrice = productSalePrice;
    }

    public String getProductBankPrice() {
        return productBankPrice;
    }

    public void setProductBankPrice(String productBankPrice) {
        this.productBankPrice = productBankPrice;
    }

    public String getBrandDisplayName() {
        return brandDisplayName;
    }

    public void setBrandDisplayName(String brandDisplayName) {
        this.brandDisplayName = brandDisplayName;
    }

    public Integer getProductRating() {
        return productRating;
    }

    public void setProductRating(Integer productRating) {
        this.productRating = productRating;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getProductCondition() {
        return productCondition;
    }

    public void setProductCondition(String productCondition) {
        this.productCondition = productCondition;
    }

    public String getProductDelivery() {
        return productDelivery;
    }

    public void setProductDelivery(String productDelivery) {
        this.productDelivery = productDelivery;
    }

    public Integer getProductAvailability() {
        return productAvailability;
    }

    public void setProductAvailability(Integer productAvailability) {
        this.productAvailability = productAvailability;
    }

    public Integer getIsExistInCart() {
        return isExistInCart;
    }

    public void setIsExistInCart(Integer isExistInCart) {
        this.isExistInCart = isExistInCart;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

}

package com.homeshopping.admin.homeshoping10.POJO.FilterList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Filter {
    @SerializedName("featureId")
    @Expose
    private Integer featureId;
    @SerializedName("featureName")
    @Expose
    private String featureName;
    @SerializedName("options")
    @Expose
    private List<Option> options = null;

    public Integer getFeatureId() {
        return featureId;
    }

    public void setFeatureId(Integer featureId) {
        this.featureId = featureId;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }

}

package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Class.NetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Class.NotLoginDataNetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Interfaces.AddorRemoveCallbacks;
import com.homeshopping.admin.homeshoping10.Interfaces.AddtowishlistAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.searchresultproductsAPI;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;
import com.homeshopping.admin.homeshoping10.POJO.monthlytopseller.Example;
import com.homeshopping.admin.homeshoping10.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;


public class SearchResultActivity extends AppCompatActivity implements  AddorRemoveCallbacks {

    GridView searchproductgrid;
    TextView searcresnum;
    ProgressBar searchprodprogress;
    public static final String ROOT_URL = "https://api.homeshopping.pk/";
    List<Example> list = new ArrayList<Example>();
    String catid = "";
    ArrayList<String> prodname  = new ArrayList<>();
    ArrayList<String> prodid  = new ArrayList<>();
    ArrayList<String> prodprice  = new ArrayList<>();
    ArrayList<String> prodoffper  = new ArrayList<>();
    ArrayList<String> proddiscount  = new ArrayList<>();
    ArrayList<String> prodimage  = new ArrayList<>();
    ArrayList<String> proddiscription = new ArrayList<>();
    ArrayList<String> prodbrand  = new ArrayList<>();
    ArrayList<String> mImageUrls = new ArrayList<>() ;

    int SalePrice = 1 ;
    int  Productprice = 1;
    int discoutnamount = 0 ;
    public DatabaseHelper db;
    int addcartstatus = 0;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        searchproductgrid = findViewById(R.id.searchproductgrid);
        searcresnum = findViewById(R.id.searcresnum);
        searchprodprogress = findViewById(R.id.progressBar3);
        searchprodprogress.setVisibility(View.VISIBLE);
        Bundle bundle = getIntent().getExtras();
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Account Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        final String keyword = bundle.getString("searchkeyword");
        this.setTitle("search result for "+keyword);
        db = new DatabaseHelper(getApplicationContext());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        searchresultproductsAPI api = retrofit.create(searchresultproductsAPI.class);
        Call<List<Example>> call = api.getDetails(MainActivity.Tokendata,keyword);
        call.enqueue(new Callback<List<Example>>() {
            @Override
            public void onResponse(Call<List<Example>> call, Response<List<Example>> response) {
              try {
                  if(response.isSuccessful()) {
                      list = response.body();
                      mImageUrls.clear();
                      for (int i = 0; i < list.size(); i++) {
                          prodname.add(list.get(i).getProductDisplayName());
                          prodprice.add(list.get(i).getProductPrice());
                          proddiscount.add(list.get(i).getProductSalePrice());
                          prodimage.add("https://cdn.homeshopping.pk/product_images/" + list.get(i).getImageFile());

                          mImageUrls.add(prodimage.get(i));
                          proddiscription.add(list.get(i).getProductDescription());
                          prodid.add(list.get(i).getProductID().toString());
                          prodbrand.add(list.get(i).getBrandDisplayName());
                      }


                      searchprodprogress.setVisibility(View.GONE);
                      searchproductgrid.setAdapter(new DataImageAdapter(getApplicationContext()));
                  }
                  else
                  {
                      Toast.makeText(SearchResultActivity.this, "Server Not Responding", Toast.LENGTH_SHORT).show();
                  }
                  }
              catch (Exception e)
              {

              }
              }

            @Override
            public void onFailure(Call<List<Example>> call, Throwable t) {

            }
        });



    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);
        finish();

    }



    @Override
    public void onAddProduct(int pid, String Qty, String userid, String combinationid, String productTitle, String Productprice, String Productimage, String ProductBrand, String feildid, String feildOption,String bankId) {
        db.addRecord(Integer.toString(pid),Qty,userid,combinationid,0,productTitle,Productprice,Productimage,ProductBrand,feildid,feildOption,bankId);
        Boolean res = prefConfig.readLoginStatus();
        if (res == false) {
            Cursor cursor1 = db.getcountnotlogin();
            if(cursor1.moveToFirst())
            {

                Dashboard.cart_count =cursor1.getInt(0);
                invalidateOptionsMenu();
//            Cursor cursor = db.getnotlogincartforDisplay();
//            if (cursor != null) {
//                if (cursor.moveToFirst()) {
//                    do {
//                        //calling the method to save the unsynced name to MySQL
//                        Dashboard.cart_count =cursor.getCount();
//                        invalidateOptionsMenu();
//
////                    Toast.makeText(this, "aa "+Dashboard.cart_count, Toast.LENGTH_SHORT).show();
//                    } while (cursor.moveToNext());
//                }
                try {
//                    Toast.makeText(getApplicationContext(), "Throughing", Toast.LENGTH_SHORT).show();
                    getApplicationContext().registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    getApplicationContext().registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                }
                catch (Exception e)
                {
                    Log.e("error",""+e);
                }

            }
        }else {
            Cursor cursor1 = db.getRecord();
            if (cursor1 != null) {
                if (cursor1.moveToFirst()) {
                    do {
                        //calling the method to save the unsynced name to MySQL
                        Dashboard.cart_count = Dashboard.cart_count +cursor1.getCount();
                        invalidateOptionsMenu();


//                    Toast.makeText(this, "aa "+Dashboard.cart_count, Toast.LENGTH_SHORT).show();
                    } while (cursor1.moveToNext());
                }
                Dashboard.cart_count ++;
                invalidateOptionsMenu();
                try {
//                    Toast.makeText(getApplicationContext(), "Throughing", Toast.LENGTH_SHORT).show();
                    getApplicationContext().registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    getApplicationContext().registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                }
                catch (Exception e)
                {
                    Log.e("error",""+e);
                }

            }
        }
        this.invalidateOptionsMenu();
    }

    @Override
    public void onRemoveProduct() {

    }

    @Override
    public void onAddProduct() {

    }

    public class DataImageAdapter extends BaseAdapter {

//        String[] IMAGE_URLS = prodname
//                ;


        private LayoutInflater inflater;

        Context c;

        DataImageAdapter(Context context) {
            inflater = LayoutInflater.from(context);
            c = context;

        }

        @Override
        public int getCount() {
            return prodname.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View view = convertView;
            if (view == null) {

                view = inflater.inflate(R.layout.custommonthlytopitem, parent, false);
                holder = new ViewHolder();
                assert view != null;

                holder.Productimage = (ImageView) view.findViewById(R.id.imageView2);
                holder.productname = view.findViewById(R.id.productname);
                holder.orignalpricetv = view.findViewById(R.id.orignalpricetv);
                holder.discountedpricetv = view.findViewById(R.id.discountedpricetv);
                holder.offpercenttv = view.findViewById(R.id.offpercenttv);
                holder.allcons = view.findViewById(R.id.allcons);
                holder.wishlisticon = view.findViewById(R.id.wishlisticon);
                holder.addcart = view.findViewById(R.id.addcart);
                holder.brandname = view.findViewById(R.id.brandname);

//              holder.imageView2.setScaleType(ImageView.ScaleType.FIT_CENTER);



                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            Boolean res2 = db.getproductforwishlist(Integer.parseInt(prodid.get(position)));

            if(res2 == true)
            {
                holder.wishlisticon.setBackgroundResource(R.drawable.heartfilled);
            }
            else
            {
                holder.wishlisticon.setBackgroundResource(R.drawable.heartgrey);
            }
            Boolean res = db.getproductforaddedcart(Integer.parseInt(prodid.get(position)));


            if(res == false)
            {
                holder.addcart.setBackgroundResource(R.drawable.add_to_cart);
                addcartstatus = 0;
            }
            else
            {
                holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                holder.addcart.setEnabled(false);
                addcartstatus = 1;
            }


            Productprice = Math.round(Float.parseFloat(prodprice.get(position)));
            SalePrice = Math.round(Float.parseFloat(proddiscount.get(position))) ;

            discoutnamount = Productprice-SalePrice;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            final String formatedproductprice = formatter.format(Productprice);
            final String formatedsaleprice = formatter.format(SalePrice);

            Log.e("Discount data",Productprice+"\n"+SalePrice+"\n"+discoutnamount);
            if(SalePrice!=0)
            {
                holder.offpercenttv.setVisibility(View.VISIBLE);
                holder.discountedpricetv.setVisibility(View.VISIBLE);
                holder.orignalpricetv.setTextColor(Color.LTGRAY);
                int percentage = ((discoutnamount*100)/Productprice);
                if(prodname.get(position).length()>=47) {
                    String stringlimit = prodname.get(position).substring(0, 47);
                    holder.productname.setText(stringlimit + "...");
                }
                else
                {
                    holder.productname.setText(prodname.get(position)+ "...");
                }
                holder.orignalpricetv.setTextSize(10);
                holder.orignalpricetv.setText("RS "+formatedproductprice);
                holder.offpercenttv.setText( percentage+"%"+" OFF");
                holder.discountedpricetv.setText("RS "+formatedsaleprice);
                holder.brandname.setText(prodbrand.get(position));
                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(mContext, "sp sale = "+Math.round(Float.parseFloat(offpercenttvv[position])), Toast.LENGTH_SHORT).show();
                        onAddProduct(Integer.parseInt(prodid.get(position)),"1", prefConfig.readID(),"0",prodname.get(position),Float.toString(Math.round(Float.parseFloat(proddiscount.get(position)))),prodimage.get(position),"","","","0");
//                        Toast.makeText(mContext, "Added to cart!"+Dashboard.prefConfig.readID(), Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setEnabled(false);
                         addcartstatus = 1;
                    }
                });

                SalePrice = 1;
            }
            else
            {
                if(prodname.get(position).length()>=47) {
                    String stringlimit = prodname.get(position).substring(0, 47);
                    holder.productname.setText(stringlimit + "...");
                }
                else
                {
                    holder.productname.setText(prodname.get(position)+ "...");
                }
                holder.orignalpricetv.setText("RS "+formatedproductprice);
                holder.orignalpricetv.setTextColor(Color.BLACK);
                holder.orignalpricetv.setTextSize(20);
                holder.orignalpricetv.setPaintFlags(0);
                holder.offpercenttv.setVisibility(View.GONE);
                holder.discountedpricetv.setVisibility(View.GONE);
                holder.brandname.setText(prodbrand.get(position));
//                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(Integer.parseInt(prodid.get(position)),"1", prefConfig.readID(),"0",prodname.get(position),Float.toString(Math.round(Float.parseFloat(prodprice.get(position)))),prodimage.get(position),"","","","0");//                        Toast.makeText(mContext, "sp "+Math.round(Float.parseFloat(orignalpricetvv[position])), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setEnabled(false);
                        addcartstatus = 1;
                    }
                });

            }



            holder.wishlisticon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prefConfig.readLoginStatus()) {

                        if (!db.getproductforwishlist(Integer.parseInt(prodid.get(position)))) {
                            db.addRecordwishlist(String.valueOf(prodid.get(position)), prefConfig.readID());

                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(ROOT_URL) //Setting the Root URL
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
                        Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata, prodid.get(position), prefConfig.readID());
                        call.enqueue(new Callback<generalstatusmessagePOJO>() {
                            @Override
                            public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                                if (response.isSuccessful()) {
                                    int status = response.body().getStatus();
                                    String message = response.body().getMessage().toString();
                                    if (status == 1) {
                                        holder.wishlisticon.setBackgroundResource(R.drawable.heartfilled);
//                                    View parentLayout = v.findViewById(android.R.id.content);
//                                    Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
//                                    View sbView = snackbar.getView();
//                                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                                    snackbar.show();
//                                    Toast.makeText(getApplicationContext(), "Added to wishlist", Toast.LENGTH_SHORT).show();

                                    } else {
                                        holder.wishlisticon.setBackgroundResource(R.drawable.heartgrey);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                            }
                        });
                        } else {
                            Toast.makeText(getApplicationContext(), "Item already added to wishlist.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Kindly login first to make wishlist", Toast.LENGTH_SHORT).show();
                    }
                }

            });


            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.brandnewdefault);
            requestOptions.error(R.drawable.imagecommingsoon);
            requestOptions.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            Glide.with(c).load(prodimage.get(position))
                    .apply(requestOptions)
                    .into(holder.Productimage);
//            Glide.with(c).load(imageUrl[position])
//                    .placeholder(R.drawable.waterempty)
//                    .error(R.drawable.waterhalf)
//                    .into(holder.imageView);

            holder.allcons.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


//                    for(int j = 0 ;j<prodimage2d.length;j++ ) {
//                        for(int i = 0 ; i <prodimage2d[j].length;i++) {
//                            Toast.makeText(mContext, "" + prodimage2d[j][i], Toast.LENGTH_SHORT).show();
//                        }
//                        }
//
                    try
                    {
                        Intent intent = new Intent(c,ProductDetailActivity.class);
                        intent.putExtra("title",prodname.get(position));
//                        intent.putExtra("discription",proddiscription.get(position));
                        intent.putExtra("price",prodprice.get(position));
                        intent.putExtra("productid",prodid.get(position));
                        intent.putExtra("prodbrand",prodbrand.get(position));
                        intent.putExtra("addcartstatus",addcartstatus);
                        startActivity(intent);
                    }
                    catch (Exception e)
                    {

                    }

//                    Toast.makeText(mContext, "product id ="+pid[position], Toast.LENGTH_SHORT).show();
                }
            });
            return view;
        }
    }

    static class ViewHolder {
        ImageView Productimage,addcart,wishlisticon;
        TextView productname,orignalpricetv,offpercenttv,discountedpricetv,brandname;
        ConstraintLayout allcons;

    }
}

package com.homeshopping.admin.homeshoping10.POJO.PaymentMethods;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class payment {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("detail")
    @Expose
    private List<paymentmethods> detail = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<paymentmethods> getDetail() {
        return detail;
    }

    public void setDetail(List<paymentmethods> detail) {
        this.detail = detail;
    }

}

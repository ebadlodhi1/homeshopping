package com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConfFieldDetail {
    @SerializedName("fieldId")
    @Expose
    private Integer fieldId;
    @SerializedName("fieldName")
    @Expose
    private String fieldName;
    @SerializedName("fieldType")
    @Expose
    private String fieldType;
    @SerializedName("fieldRequired")
    @Expose
    private Integer fieldRequired;
    @SerializedName("fieldOptions")
    @Expose
    private List<ConfFeildFieldOption> fieldOptions = null;

    public Integer getFieldId() {
        return fieldId;
    }

    public void setFieldId(Integer fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public Integer getFieldRequired() {
        return fieldRequired;
    }

    public void setFieldRequired(Integer fieldRequired) {
        this.fieldRequired = fieldRequired;
    }

    public List<ConfFeildFieldOption> getFieldOptions() {
        return fieldOptions;
    }

    public void setFieldOptions(List<ConfFeildFieldOption> fieldOptions) {
        this.fieldOptions = fieldOptions;
    }

}

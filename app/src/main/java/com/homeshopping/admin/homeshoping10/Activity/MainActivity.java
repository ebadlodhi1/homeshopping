package com.homeshopping.admin.homeshoping10.Activity;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.DocumentsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;

import com.google.firebase.messaging.FirebaseMessaging;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.R;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import retrofit2.http.Url;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Database.DatabaseHelper.DB_NAME;
import static java.lang.Thread.sleep;

public class MainActivity extends AppCompatActivity {
public static String Tokendata = "iuLX3yeaCxctL9KU1nK8aL7YLfjffqbn7lgoGHMB09rQ2borlz9iINUzSOlNfG1k";
ImageView logo;
    private Tracker mTracker;
    AppUpdateManager mAppUpdateManager;
    InstallStateUpdatedListener installStateUpdatedListener;
    String currentVersion, latestVersion;
    Dialog dialog;
    String url = "https://play.google.com/store/apps/details?id=com.homeshopping.admin.homeshoping10";
    URL urlOfAppFromPlayStore;

    {
        try {
            urlOfAppFromPlayStore = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
    public DatabaseHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logo = findViewById(R.id.imageView49);
        this.setTitle("");
        db = new DatabaseHelper(this);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Main Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        try {

            Animation animation1 =
                    AnimationUtils.loadAnimation(getApplicationContext(),
                            R.anim.fade);
            logo.startAnimation(animation1);
            sleep(2000);
            if (isNetworkConnected())
            getCurrentVersion();
            else
            {
                Toast.makeText(getApplicationContext(), "Kindly Check Internet Conectivity", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                startActivity(intent);
                finish();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }




    }


    private void getCurrentVersion(){
        PackageManager pm = this.getPackageManager();
        PackageInfo pInfo = null;

        try {
            pInfo =  pm.getPackageInfo(this.getPackageName(),0);

        } catch (PackageManager.NameNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        currentVersion = pInfo.versionName;

        new GetLatestVersion().execute();

    }
    private class GetLatestVersion extends AsyncTask<String, String, JSONObject> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
//It retrieves the latest version by scraping the content of current version from play store at runtime
                Document doc = Jsoup.connect(String.valueOf(urlOfAppFromPlayStore)).get();
                latestVersion = doc.getElementsByClass("htlgb").get(6).text();
//                Toast.makeText(MainActivity.this, ""+latestVersion, Toast.LENGTH_SHORT).show();

            }catch (Exception e){
                e.printStackTrace();

            }

            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(latestVersion!=null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)){
                    if(!isFinishing()){ //This would help to prevent Error : BinderProxy@45d459c0 is not valid; is your activity running? error
                        showUpdateDialog();
                    }
                }
                else
                {
                    Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                    startActivity(intent);
                    finish();
                }
            }
            else
            {
                Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                startActivity(intent);
                finish();
            }

            super.onPostExecute(jsonObject);
        }
    }

    private void showUpdateDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("A New Update is Available");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearApplicationData();

                             startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                        ("market://details?id=com.homeshopping.admin.homeshoping10")));
                dialog.dismiss();
            }
        });

//        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
////                background.start();
//            }
//        });

        builder.setCancelable(false);
        dialog = builder.show();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    if(deleteDir(new File(appDir, s)))
                    Log.i("TAG", "**************** File /data/data/mypackage/" + s + " DELETED *******************");
                    else
                        Log.i("TAG", "**************** File /data/data/mypackage/" + s + "NOT DELETED *******************");

                }
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

}

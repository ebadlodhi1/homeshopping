package com.homeshopping.admin.homeshoping10.POJO.PaymentMethods;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class paymentmethods {
    @SerializedName("displayname")
    @Expose
    private String displayname;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("modulename")
    @Expose
    private String modulename;

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getModulename() {
        return modulename;
    }

    public void setModulename(String modulename) {
        this.modulename = modulename;
    }
}

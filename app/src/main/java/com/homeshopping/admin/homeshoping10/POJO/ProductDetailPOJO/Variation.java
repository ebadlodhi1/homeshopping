package com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Variation {
    @SerializedName("variationId")
    @Expose
    private Integer variationId;
    @SerializedName("variationName")
    @Expose
    private String variationName;
    @SerializedName("variations")
    @Expose
    private List<Variation_> variations = null;

    public Integer getVariationId() {
        return variationId;
    }

    public void setVariationId(Integer variationId) {
        this.variationId = variationId;
    }

    public String getVariationName() {
        return variationName;
    }

    public void setVariationName(String variationName) {
        this.variationName = variationName;
    }

    public List<Variation_> getVariations() {
        return variations;
    }

    public void setVariations(List<Variation_> variations) {
        this.variations = variations;
    }
}

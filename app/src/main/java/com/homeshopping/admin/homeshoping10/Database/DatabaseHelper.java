package com.homeshopping.admin.homeshoping10.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.homeshopping.admin.homeshoping10.Activity.Dashboard;


public class
DatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME ="CartDetailsthree";
    public  static final String TABLE_NAME = "Cart";
    public  static final String TABLE_NAME_Wishlist = "Wishlist";

    public  static final String COLUMN_Wishlist_productid = "wishlistproductid";
    public  static final String COLUMN_Wishlist_userid  = "wishlistuserid";


    public  static final String COLUMN_id = "insert_id";
    public  static final String COLUMN_userid = "userid";
    public  static final String COLUMN_productid = "productid";
    public  static final String COLUMN_productqty = "productqty";
    public  static final String COLUMN_combinaitonId = "combinationid";
    public  static final String COLUMN_status = "status";
    public  static final String COLUMN_producttitle = "producttitle";
    public  static final String COLUMN_productprice = "productprice";
    public  static final String COLUMN_productimage = "productimage";
    public  static final String COLUMN_productbrand = "productbrand";
    public  static final String COLUMN_fieldId = "feildid";
    public  static final String COLUMN_fieldOption = "feildoption";
    public  static final String COLUMN_bankId = "bankId";
    int presentqty = 0 ;
    public DatabaseHelper(@Nullable Context context) {
        super(context, DB_NAME, null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e("InonCreate ","");
        String sql = "create table "+TABLE_NAME+" ("+COLUMN_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+COLUMN_productid+" INTEGER,"+COLUMN_productqty+" INTEGER,"+COLUMN_userid+" INTEGER,"+ COLUMN_combinaitonId+" INTEGER,"+COLUMN_status+" INTEGER,"+COLUMN_producttitle+" text,"+COLUMN_productprice+" text,"+COLUMN_productimage+" text,"+COLUMN_productbrand+" text,"+COLUMN_fieldId+" text,"+COLUMN_fieldOption+" text,"+COLUMN_bankId+" text  )";
        db.execSQL(sql);
        String sql2 = "create table "+TABLE_NAME_Wishlist+" ("+COLUMN_id+" INTEGER PRIMARY KEY AUTOINCREMENT,"+COLUMN_Wishlist_productid+" INTEGER,"+COLUMN_Wishlist_userid+" INTEGER)";
        db.execSQL(sql2);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("InonUpgrade ","old version "+oldVersion+"\nnewversion "+newVersion);
        if(oldVersion<3) {
            String sql = "DROP TABLE IF EXISTS " + TABLE_NAME + "";
            db.execSQL(sql);
            String sql2 = "DROP TABLE IF EXISTS " + TABLE_NAME_Wishlist + "";
            db.execSQL(sql2);

            onCreate(db);
        }
    }

    public boolean addRecord(String prod_id,String prod_qty,
                             String user_id,String combination_id,int status,String prod_title
            ,String prod_price,String prod_image,String prod_brand ,String feild_id,String feild_option,String bank_Id  ) {
        SQLiteDatabase db = this.getWritableDatabase();

        int pid = Integer.parseInt(prod_id);
        String sql = "SELECT * FROM "+TABLE_NAME+" where "+COLUMN_productid+" = "+pid+" ";
        Cursor c = db.rawQuery(sql, null);
        Log.d("cursur result",""+c.getCount());
        if(c.getCount()>0)
        {

        }
            if (c.moveToFirst()) {
                do {
             presentqty = c.getInt(2);
                } while (c.moveToNext());
            SQLiteDatabase db2 = this.getWritableDatabase();
            db2.beginTransaction();
            ContentValues contentValues1 = new ContentValues();
            Log.d("quantity",""+prod_qty);
            Log.d("prod id",""+prod_id);
            contentValues1.put(COLUMN_productqty, (Integer.parseInt(prod_qty)+presentqty));
            int a =db2.update(TABLE_NAME, contentValues1, COLUMN_productid+ "="+ pid+"", null);
            Log.d("res code ",""+a);
            db2.setTransactionSuccessful();
            db2.endTransaction();
            db2.close();

        }
        else {
            ContentValues contentValues = new ContentValues();

            contentValues.put(COLUMN_productid, prod_id);
            contentValues.put(COLUMN_productqty, prod_qty);
            contentValues.put(COLUMN_userid, user_id);
            contentValues.put(COLUMN_combinaitonId, combination_id);
            contentValues.put(COLUMN_status, status);
            contentValues.put(COLUMN_producttitle, prod_title);
            contentValues.put(COLUMN_productprice, prod_price);
            contentValues.put(COLUMN_productimage, prod_image);
            contentValues.put(COLUMN_productbrand, prod_brand);
            contentValues.put(COLUMN_fieldId, feild_id);
            contentValues.put(COLUMN_fieldOption, feild_option);
            contentValues.put(COLUMN_bankId, bank_Id);


                db.insert(TABLE_NAME, null, contentValues);
            Log.d("Inserted in database", "hogaya");
            db.close();
        }
        return true;
    }
    public Cursor getRecord() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME+"";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }
    public Cursor getcount()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT count("+COLUMN_id+") from "+TABLE_NAME+"";
        Cursor c = db.rawQuery(sql,null);
        return c;
    }
    public Cursor getcountnotlogin()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT sum("+COLUMN_productqty+") from "+TABLE_NAME+" where "+COLUMN_status+" = 0 ";
        Cursor c = db.rawQuery(sql,null);
        return c;
    }
    public Cursor getUnsyncedNames() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME+" where "+COLUMN_status+" = 0 ";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }
    public Cursor getnotlogincart() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME+" where "+COLUMN_userid+" = 0 AND "+COLUMN_status+" = 0 ";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }
    public Cursor getnotlogincartforDisplay() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME+" where "+COLUMN_userid+" = 0 ";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

    public boolean getproductforaddedcart(int pid) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME+" where "+COLUMN_productid+" = "+pid;
        Cursor c = db.rawQuery(sql, null);
        if (c.getCount()>0)
        {
            return true;
        }
        else
        {
            return  false;
        }

    }
    public void deletesync()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,COLUMN_status +"= 1 AND "+COLUMN_userid +"!= 0 " ,null);
        // db.delete(TABLE_NAME,COLUMN_status +"="+ 1 +"and STR_TO_DATE('21,5,2013 extra characters','%d,%mmm,%Y'),)
        db.close();
    }


    public void delete()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME,COLUMN_status +"= 1 AND "+COLUMN_userid +"= 0 " ,null);
        // db.delete(TABLE_NAME,COLUMN_status +"="+ 1 +"and STR_TO_DATE('21,5,2013 extra characters','%d,%mmm,%Y'),)
        db.close();
    }
    public void deletecartproduct(String productid)
    {

        SQLiteDatabase db = this.getWritableDatabase();

//       db.delete(TABLE_NAME,COLUMN_productid +"="+productid ,null);
        String sql = "DELETE  FROM "+TABLE_NAME+" where "+COLUMN_productid+" = "+productid;
        db.execSQL(sql);

        // db.delete(TABLE_NAME,COLUMN_status +"="+ 1 +"and STR_TO_DATE('21,5,2013 extra characters','%d,%mmm,%Y'),)
        db.close();
    }
    public void deleteallcartproductofuser(String userid)
    {

        SQLiteDatabase db = this.getWritableDatabase();

//       db.delete(TABLE_NAME,COLUMN_productid +"="+productid ,null);
        String sql = "DELETE  FROM "+TABLE_NAME+" where "+COLUMN_userid+" = "+userid;
        db.execSQL(sql);

        // db.delete(TABLE_NAME,COLUMN_status +"="+ 1 +"and STR_TO_DATE('21,5,2013 extra characters','%d,%mmm,%Y'),)
        db.close();
    }
    public  boolean updateStatus()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(COLUMN_status, 1);
        db.update(TABLE_NAME, contentValues1, COLUMN_status+ "="+ 0, null);
        db.close();

        return true;
    }
    public  boolean updateUserIdStatus(int userid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(COLUMN_userid, userid);
        db.update(TABLE_NAME, contentValues1, COLUMN_userid+ "="+ 0, null);
        db.close();

        return true;
    }
    public  boolean updateQuantity(int qty,int pid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues1 = new ContentValues();
        contentValues1.put(COLUMN_productqty, qty);
        db.update(TABLE_NAME, contentValues1, COLUMN_productid+ "="+ pid, null);
        db.close();

        return true;
    }
    public boolean addRecordwishlist(String prod_id,String userid  ) {
        SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();

            contentValues.put(COLUMN_Wishlist_productid, prod_id);
            contentValues.put(COLUMN_Wishlist_userid, userid);

            db.insert(TABLE_NAME_Wishlist, null, contentValues);
            Log.d("Inserted in database", "hogaya");
            db.close();

        return true;
    }

    public void deletewishlistproduct(String productid)
    {

        SQLiteDatabase db = this.getWritableDatabase();

//       db.delete(TABLE_NAME,COLUMN_productid +"="+productid ,null);
        String sql = "DELETE  FROM "+TABLE_NAME_Wishlist+" where "+COLUMN_Wishlist_productid+" = "+productid;
        db.execSQL(sql);

        // db.delete(TABLE_NAME,COLUMN_status +"="+ 1 +"and STR_TO_DATE('21,5,2013 extra characters','%d,%mmm,%Y'),)
        db.close();
    }

    public boolean getproductforwishlist(int pid) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME_Wishlist+" where "+COLUMN_Wishlist_productid+" = "+pid;
        Cursor c = db.rawQuery(sql, null);
        if (c.getCount()>0)
        {
            return true;
        }
        else
        {
            return  false;
        }

    }



}

package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO.ProductDetailExample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface GetProductDetailAPI {
    @GET("products/getProductDetail")
    Call<ProductDetailExample> getDetails(@Header("data") String data, @Query("productId") String productId, @Query("userId") String userId, @Query("bankId") String bankId);
}

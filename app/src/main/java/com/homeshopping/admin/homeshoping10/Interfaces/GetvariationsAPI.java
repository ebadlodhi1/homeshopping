package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.Variation.VariationExample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface GetvariationsAPI {
    @GET("/products/getVariationsByProduct")
    Call<VariationExample> getDetails(@Header("data") String data, @Query("productId") String productId);

}

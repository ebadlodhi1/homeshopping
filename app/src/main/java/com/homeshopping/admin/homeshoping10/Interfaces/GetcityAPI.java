package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.City.city;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface GetcityAPI {
    @GET("/checkout/getCities")
    Call<city> getDetails(@Header("data") String data, @Query("country") String country);

}

package com.homeshopping.admin.homeshoping10.Fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.homeshopping.admin.homeshoping10.Activity.Dashboard;
import com.homeshopping.admin.homeshoping10.Activity.MainActivity;
import com.homeshopping.admin.homeshoping10.Activity.ProductDetailActivity;
import com.homeshopping.admin.homeshoping10.Interfaces.GetProductDetailAPI;
import com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO.ProductDetailExample;
import com.homeshopping.admin.homeshoping10.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.CONNECTIVITY_SERVICE;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;


public class DiscriptionFragment  extends Fragment {
    private WebView wv1;
    String discription = "";



    public DiscriptionFragment() {
        // Required empty public constructor



    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_discription, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = new Bundle();
final ProductDetailActivity productDetailActivity = (ProductDetailActivity) getActivity();
        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GetProductDetailAPI getProductDetailAPI = retrofit2.create(GetProductDetailAPI.class);
        Call<ProductDetailExample> productDetailExampleCall = getProductDetailAPI.getDetails(MainActivity.Tokendata,ProductDetailActivity.pid, Dashboard.prefConfig.readID(),"0");
        productDetailExampleCall.enqueue(new Callback<ProductDetailExample>() {
            @Override
            public void onResponse(Call<ProductDetailExample> call, Response<ProductDetailExample> response) {
                if (response.isSuccessful()) {
                    int status = response.body().getStatus();
                    Log.d("status", "" + status);
                    if (status == 1) {

                        discription = response.body().getDetails().getProduct().getProductDescription();

                        Log.d("fragmentdiscription", "nullaa" + ProductDetailActivity.pid);
                        String newdis = "<style> img{width:100%; height:auto;},table{width:100%; height:auto;},ol{width:100%; height:auto;},ul{width:100%; height:auto;}</style>" + discription;
                        Log.d("Dis", newdis);
                        // Toast.makeText(getContext(), "dd"+discription, Toast.LENGTH_SHORT).show();
                        wv1 = (WebView) view.findViewById(R.id.productwebview);
                        WebSettings mWebSettings = wv1.getSettings();
                        wv1.setVerticalScrollBarEnabled(true);
//                        wv1.requestFocus();
                        mWebSettings.setBuiltInZoomControls(true);

                        wv1.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
                        wv1.setScrollbarFadingEnabled(false);
                        wv1.setWebViewClient(new MyBrowser());
                        wv1.getSettings().setLoadsImagesAutomatically(true);
                        wv1.getSettings().setJavaScriptEnabled(true);

                        wv1.getSettings().setAppCacheMaxSize(5 * 1024 * 1024); // 5MB
//                    wv1.getSettings().setAppCachePath(getContext().getCacheDir().getAbsolutePath());
                        wv1.getSettings().setAllowFileAccess(true);
                        wv1.getSettings().setAppCacheEnabled(true);
                        wv1.getSettings().setJavaScriptEnabled(true);
                        wv1.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT); // load online by default
                        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        wv1.setPadding(0, 0, 0, 0);
//        wv1.setInitialScale(getScale());
//        ProductDetailActivity.viewPager.setScrollBarSize(getScale());
//                    if (!isNetworkAvailable()) { // loading offline
//
//                        wv1.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
//                    }
                        wv1.loadData(newdis, "text/html", "UTF-8");
                        wv1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

                    }
                }
                else
                {
                    Toast.makeText(getActivity(), ""+response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ProductDetailExample> call, Throwable t) {
                Log.d("fail res",""+t);
            }
        });




    }
    private int getScale(){
        Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        int width = display.getWidth();
        int PIC_WIDTH= wv1.getRight()-wv1.getLeft();
        Double val = new Double(width)/new Double(PIC_WIDTH);
        val = val * 100d;
        return val.intValue();
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    private boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getContext().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
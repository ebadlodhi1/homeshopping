package com.homeshopping.admin.homeshoping10.POJO.HomeCategoryProducts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeCategoryProduct {
    @SerializedName("categoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("categoryTitle")
    @Expose
    private String categoryTitle;
    @SerializedName("categoryBanner")
    @Expose
    private CategoryBanner categoryBanner;
    @SerializedName("categoryProducts")
    @Expose
    private List<CategoryProduct> categoryProducts = null;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public CategoryBanner getCategoryBanner() {
        return categoryBanner;
    }

    public void setCategoryBanner(CategoryBanner categoryBanner) {
        this.categoryBanner = categoryBanner;
    }

    public List<CategoryProduct> getCategoryProducts() {
        return categoryProducts;
    }

    public void setCategoryProducts(List<CategoryProduct> categoryProducts) {
        this.categoryProducts = categoryProducts;
    }
}

package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Class.NetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Class.NotLoginDataNetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Interfaces.AddorRemoveCallbacks;
import com.homeshopping.admin.homeshoping10.Interfaces.getwhishlistAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.removewishlistAPI;
import com.homeshopping.admin.homeshoping10.POJO.whishlist.whishlist;
import com.homeshopping.admin.homeshoping10.POJO.whishlist.whishlistdetails;
import com.homeshopping.admin.homeshoping10.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.constraintlayout.widget.ConstraintLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class WhishlistActivity extends AppCompatActivity implements  AddorRemoveCallbacks {

    ProgressBar progressBar;
    ListView whishlistview;
    List<whishlistdetails> wishlist = new ArrayList<>();
    TextView totalitem;

    private Tracker mTracker;



    ArrayList<String> wishlistId  = new ArrayList<>();
    ArrayList<String> brandName  = new ArrayList<>();
    ArrayList<String> productTitle  = new ArrayList<>();
    ArrayList<String> productTotalPrice  = new ArrayList<>();
    ArrayList<String> productSalePrice  = new ArrayList<>();
    ArrayList<String> productImageUrl  = new ArrayList<>();
    ArrayList<String> productId  = new ArrayList<>();
    ArrayList<Integer> isExistInCart  = new ArrayList<>();
    public DatabaseHelper db;
    boolean flag_loading = false;
    int catpagecount = 0;
    ConstraintLayout wishlistemptylayout,wishlistfilledlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whishlist);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.setTitle("Wishlist");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Wishlist Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        db = new DatabaseHelper(getApplicationContext());
        whishlistview = findViewById(R.id.whishlistview);
        progressBar = findViewById(R.id.progressBar6);
        totalitem = findViewById(R.id.textView68);
        wishlistemptylayout = findViewById(R.id.wishlistemptylayout);
        wishlistfilledlayout = findViewById(R.id.wishlistfilledlayout);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        getwhishlistAPI api = retrofit.create(getwhishlistAPI.class);
        Call<whishlist> whishlistdetailsCall = api.getDetails(MainActivity.Tokendata, prefConfig.readID(),Integer.toString(catpagecount));
        whishlistdetailsCall.enqueue(new Callback<whishlist>() {
            @Override
            public void onResponse(Call<whishlist> call, Response<whishlist> response) {

                    int status = response.body().getStatus();
                    if (status == 1) {
                        wishlistemptylayout.setVisibility(View.GONE);
                        wishlistfilledlayout.setVisibility(View.VISIBLE);

                    wishlist = response.body().getWishlist();


                    progressBar.setVisibility(View.GONE);
                    for (int i = 0; i < wishlist.size(); i++) {
                        wishlistId.add(wishlist.get(i).getWishlistId().toString());
                        brandName.add(wishlist.get(i).getBrandName());
                        productTitle.add(wishlist.get(i).getProductTitle());
                        productTotalPrice.add(wishlist.get(i).getProductTotalPrice());
                        productSalePrice.add(wishlist.get(i).getProductSalePrice());
                        productImageUrl.add(wishlist.get(i).getProductImageUrl());
                        productId.add(wishlist.get(i).getProductId().toString());
                        isExistInCart.add(wishlist.get(i).getIsExistInCart());

                    }
                    totalitem.setText("Wishlist (" + brandName.size() + " Items)");
                    getwishlistAdapter2 getwishlistAdapter2 = new getwishlistAdapter2(getApplicationContext(), wishlist);
                    whishlistview.setAdapter(getwishlistAdapter2);
                }
                else
                {
                    wishlistemptylayout.setVisibility(View.VISIBLE);
                    wishlistfilledlayout.setVisibility(View.GONE);
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar snackbar = Snackbar.make(parentLayout, ""+response.body().getMessage(), Snackbar.LENGTH_LONG);
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
                    snackbar.show();
                }
            }


            @Override
            public void onFailure(Call<whishlist> call, Throwable t) {

            }
        });
        whishlistview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0){
                    // here you have reached end of list, load more data


                    if(flag_loading == false)
                    {
                        flag_loading = true;
                        catpagecount = catpagecount+16;
                        fetchMoreItems(catpagecount);
                    }


                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);
        finish();

    }



    @Override
    public void onAddProduct(int pid, String Qty, String userid, String combinationid, String productTitle, String Productprice, String Productimage, String ProductBrand, String feildid, String feildOption,String bankId) {
        db.addRecord(Integer.toString(pid),Qty,userid,combinationid,0,productTitle,Productprice,Productimage,ProductBrand,feildid,feildOption,bankId);
        Boolean res = prefConfig.readLoginStatus();
        if (res == false) {
            Cursor cursor1 = db.getcountnotlogin();
            if(cursor1.moveToFirst())
            {



//            Cursor cursor = db.getnotlogincartforDisplay();
//            if (cursor != null) {
//                if (cursor.moveToFirst()) {
//                    do {
//                        //calling the method to save the unsynced name to MySQL
//                        Dashboard.cart_count =cursor.getCount();
//                        invalidateOptionsMenu();
//
////                    Toast.makeText(this, "aa "+Dashboard.cart_count, Toast.LENGTH_SHORT).show();
//                    } while (cursor.moveToNext());
//                }
                try {
//                    Toast.makeText(getApplicationContext(), "Throughing", Toast.LENGTH_SHORT).show();
                    getApplicationContext().registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    getApplicationContext().registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                }
                catch (Exception e)
                {
                    Log.e("error",""+e);
                }

            }
            Dashboard.cart_count++;
            invalidateOptionsMenu();
        }else {
            Cursor cursor1 = db.getRecord();
            if (cursor1 != null) {
                if (cursor1.moveToFirst()) {
                    do {
                        //calling the method to save the unsynced name to MySQL


//                    Toast.makeText(this, "aa "+Dashboard.cart_count, Toast.LENGTH_SHORT).show();
                    } while (cursor1.moveToNext());

                    Dashboard.cart_count ++;
                    invalidateOptionsMenu();
                }
                try {
//                    Toast.makeText(getApplicationContext(), "Throughing", Toast.LENGTH_SHORT).show();
                    getApplicationContext().registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    getApplicationContext().registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                }
                catch (Exception e)
                {
                    Log.e("error",""+e);
                }

            }
        }
        this.invalidateOptionsMenu();
//        db.addRecord(Integer.toString(pid),Qty,userid,bankid,0);
//        Toast.makeText(context, "Imei number: " + Dashboard.Imei+"\nPhone number "+Dashboard.Phonenumber, Toast.LENGTH_SHORT).show();



    }

    @Override
    public void onRemoveProduct() {

    }

    @Override
    public void onAddProduct() {
        Dashboard.cart_count++;
        invalidateOptionsMenu();
    }


    public class getwishlistAdapter2 extends ArrayAdapter<String> {



        public getwishlistAdapter2(Context context, List<whishlistdetails> list ) {
            super(context, R.layout.whishlistcustomitemlayout, productId);




        }

        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder2 vh;
            View v = view;
            if(v == null) {
                vh = new ViewHolder2();
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                v = li.inflate(R.layout.whishlistcustomitemlayout,null);
//            Example app = items.get(position);
//            Log.d("position",Integer.toString(position));
//            if(app != null) {

                vh.wlbrand = (TextView) v.findViewById(R.id.wlbrand);
                vh. wlproductname = (TextView) v.findViewById(R.id.wlproductname);
                vh. wlprice = (TextView) v.findViewById(R.id.wlprice);
                vh.wlmovetocart = v.findViewById(R.id.wlmovetocart);
                vh.wlremove = v.findViewById(R.id.wlremove);
                vh. wishlistlayout = v.findViewById(R.id.wishlistlayout);
                vh. cartlayout = v.findViewById(R.id.cartlayout);
                vh.addtocart = v.findViewById(R.id.textView71);
                vh. cartlayout.setVisibility(View.GONE);
                vh. wishlistlayout.setVisibility(View.VISIBLE);

                vh.wlimage = v.findViewById(R.id.wlimage);
                vh.addedtocart = v.findViewById(R.id.imageView19);
                v.setTag(vh);
            }
            else{
                vh = (ViewHolder2) view.getTag();
            }



// give a timezone reference for formatting (see comment at the bottom)
            Boolean res = db.getproductforaddedcart(Integer.parseInt(productId.get(position)));
        if (res)
        {
            vh.addtocart.setText("Added to cart");
            vh.addedtocart.setBackgroundResource(R.drawable.check_circle);
            vh.wlmovetocart.setEnabled(false);
        }
            vh.wlbrand.setText(brandName.get(position));
            vh.wlproductname.setText(productTitle.get(position));
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            final String formatedproductprice = formatter.format(Math.round(Float.parseFloat(productTotalPrice.get(position))));
            vh.wlprice.setText("Rs "+formatedproductprice);

//            DecimalFormat formatter = new DecimalFormat("#,###,###");
//            formatedproductprice = formatter.format(Math.round(Float.parseFloat(itemprodprice[position])));
//            vh.itemamount.setText("Rs "+formatedproductprice);
            Glide.with(getApplicationContext())
                    .load(productImageUrl.get(position))// image url
                    // any placeholder to load at start
                    .into( vh.wlimage);
            vh.wlremove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    View parentLayout = findViewById(android.R.id.content);
                    Snackbar snackbar = Snackbar.make(parentLayout, "Remove from wishlist", Snackbar.LENGTH_LONG);
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
                    snackbar.show();

        try {
            db.deletewishlistproduct(productId.get(position));
        }catch (Exception e)
        {

        }
//                    Toast.makeText(WhishlistActivity.this, ""+brandName.size(), Toast.LENGTH_SHORT).show();
                   if(brandName.size() == 1)
                   {
                       wishlistemptylayout.setVisibility(View.VISIBLE);
                       wishlistfilledlayout.setVisibility(View.GONE);
                   }

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    removewishlistAPI  api = retrofit.create(removewishlistAPI.class);
                    Call<whishlist> whishlistremoveCall = api.getDetails(MainActivity.Tokendata,wishlistId.get(position),productId.get(position), prefConfig.readID());
                    whishlistremoveCall.enqueue(new Callback<whishlist>() {
                        @Override
                        public void onResponse(Call<whishlist> call, Response<whishlist> response) {
//                            Toast.makeText(WhishlistActivity.this, ""+response.body(), Toast.LENGTH_SHORT).show();

                            if(response.isSuccessful()) {
                                if(response.body().getStatus() == 1) {
                                    wishlist = response.body().getWishlist();

                                    wishlistId.clear();
                                    brandName.clear();
                                    productTitle.clear();
                                    productTotalPrice.clear();
                                    productSalePrice.clear();
                                    productImageUrl.clear();
                                    productId.clear();
                                    progressBar.setVisibility(View.GONE);
                                    for (int i = 0; i < wishlist.size(); i++) {
                                        wishlistId.add(wishlist.get(i).getWishlistId().toString());
                                        brandName.add(wishlist.get(i).getBrandName());
                                        productTitle.add(wishlist.get(i).getProductTitle());
                                        productTotalPrice.add(wishlist.get(i).getProductTotalPrice());
                                        productSalePrice.add(wishlist.get(i).getProductSalePrice());
                                        productImageUrl.add(wishlist.get(i).getProductImageUrl());
                                        productId.add(wishlist.get(i).getProductId().toString());

                                    }
                                    totalitem.setText("Wishlist (" + brandName.size() + " Items)");
                                    getwishlistAdapter2 getwishlistAdapter2 = new getwishlistAdapter2(getApplicationContext(), wishlist);
                                    whishlistview.setAdapter(getwishlistAdapter2);
                                    progressBar.setVisibility(View.GONE);
//            catproductgrid.setAdapter(dataImageAdapter);

                                    whishlistview.deferNotifyDataSetChanged();

                                }
                            else
                                {
                                    wishlistemptylayout.setVisibility(View.VISIBLE);
                                    wishlistfilledlayout.setVisibility(View.GONE);
                                }
                            }
                            else
                            {  wishlistemptylayout.setVisibility(View.VISIBLE);
                                wishlistfilledlayout.setVisibility(View.GONE);}
                            }

                        @Override
                        public void onFailure(Call<whishlist> call, Throwable t) {

                        }
                    });
                }
            });

            vh.wlmovetocart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    View parentLayout = findViewById(android.R.id.content);
//                    Snackbar snackbar = Snackbar.make(parentLayout, "Added to cart", Snackbar.LENGTH_LONG);
//                    View sbView = snackbar.getView();
//                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                    snackbar.show();
                    vh.addtocart.setText("Added to cart");
                    vh.addedtocart.setBackgroundResource(R.drawable.check_circle);
                    vh.wlmovetocart.setEnabled(false);
                    onAddProduct(Integer.parseInt(productId.get(position)),"1", prefConfig.readID(),"0",productTitle.get(position),productTotalPrice.get(position),productImageUrl.get(position),brandName.get(position),"","","0");
                }
            });
            return v;

        };
    }
    static class ViewHolder2
    {
        // ImageView imageView;
        TextView wlbrand,wlproductname,wlprice,addtocart;
        ImageView wlimage,addedtocart;
        ConstraintLayout wlmovetocart,wlremove;
        ConstraintLayout wishlistlayout,cartlayout;



    }
    public  void fetchMoreItems(int count)
    {
//    Toast.makeText(this, "Loading..", Toast.LENGTH_SHORT).show();
        View parentLayout = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(parentLayout, "Loading more...", Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(Color.rgb(161, 201, 19));
        snackbar.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        getwhishlistAPI api = retrofit.create(getwhishlistAPI.class);
        Call<whishlist> whishlistdetailsCall = api.getDetails(MainActivity.Tokendata, prefConfig.readID(),Integer.toString(count));
        whishlistdetailsCall.enqueue(new Callback<whishlist>() {
            @Override
            public void onResponse(Call<whishlist> call, Response<whishlist> response) {
                int status  = response.body().getStatus();
                if(status == 1) {
                    wishlist = response.body().getWishlist();


                    progressBar.setVisibility(View.GONE);
                    for (int i = 0; i < wishlist.size(); i++) {
                        wishlistId.add(wishlist.get(i).getWishlistId().toString());
                        brandName.add(wishlist.get(i).getBrandName());
                        productTitle.add(wishlist.get(i).getProductTitle());
                        productTotalPrice.add(wishlist.get(i).getProductTotalPrice());
                        productSalePrice.add(wishlist.get(i).getProductSalePrice());
                        productImageUrl.add(wishlist.get(i).getProductImageUrl());
                        productId.add(wishlist.get(i).getProductId().toString());

                    }
                    totalitem.setText("Wishlist (" + brandName.size() + " Items)");
                    getwishlistAdapter2 getwishlistAdapter2 = new getwishlistAdapter2(getApplicationContext(), wishlist);
                    whishlistview.setAdapter(getwishlistAdapter2);
                    progressBar.setVisibility(View.GONE);
//            catproductgrid.setAdapter(dataImageAdapter);

                    whishlistview.deferNotifyDataSetChanged();
                    if (wishlist.size() < 16) {
                        flag_loading = true;
                    } else {
                        flag_loading = false;
                    }
                }
                else
                {
//                    View parentLayout = findViewById(android.R.id.content);
//                    Snackbar snackbar = Snackbar.make(parentLayout, ""+response.body().getMessage(), Snackbar.LENGTH_LONG);
//                    View sbView = snackbar.getView();
//                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                    snackbar.show();
//                    wishlistemptylayout.setVisibility(View.VISIBLE);
//                    wishlistfilledlayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<whishlist> call, Throwable t) {

            }
        });

            }
public void home(View view)
{
    Intent intent = new Intent(getApplicationContext(),Dashboard.class);
    startActivity(intent);
    finish();

}

    }




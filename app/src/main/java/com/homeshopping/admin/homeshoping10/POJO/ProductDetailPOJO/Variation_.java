package com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Variation_ {
    @SerializedName("combinationId")
    @Expose
    private String combinationId;
    @SerializedName("variationId")
    @Expose
    private String variationId;
    @SerializedName("variationName")
    @Expose
    private String variationName;
    @SerializedName("variationFunction")
    @Expose
    private String variationFunction;
    @SerializedName("variationPrice")
    @Expose
    private String variationPrice;
    @SerializedName("variationWeightFunction")
    @Expose
    private String variationWeightFunction;
    @SerializedName("variationWeightPrice")
    @Expose
    private String variationWeightPrice;

    public String getCombinationId() {
        return combinationId;
    }

    public void setCombinationId(String combinationId) {
        this.combinationId = combinationId;
    }

    public String getVariationId() {
        return variationId;
    }

    public void setVariationId(String variationId) {
        this.variationId = variationId;
    }

    public String getVariationName() {
        return variationName;
    }

    public void setVariationName(String variationName) {
        this.variationName = variationName;
    }

    public String getVariationFunction() {
        return variationFunction;
    }

    public void setVariationFunction(String variationFunction) {
        this.variationFunction = variationFunction;
    }

    public String getVariationPrice() {
        return variationPrice;
    }

    public void setVariationPrice(String variationPrice) {
        this.variationPrice = variationPrice;
    }

    public String getVariationWeightFunction() {
        return variationWeightFunction;
    }

    public void setVariationWeightFunction(String variationWeightFunction) {
        this.variationWeightFunction = variationWeightFunction;
    }

    public String getVariationWeightPrice() {
        return variationWeightPrice;
    }

    public void setVariationWeightPrice(String variationWeightPrice) {
        this.variationWeightPrice = variationWeightPrice;
    }

}

package com.homeshopping.admin.homeshoping10.POJO.City;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class cityDetails {
    @SerializedName("cityId")
    @Expose
    private Integer cityId;
    @SerializedName("cityName")
    @Expose
    private String cityName;

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}

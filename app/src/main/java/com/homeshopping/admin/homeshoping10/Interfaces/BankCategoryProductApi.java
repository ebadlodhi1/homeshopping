package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.CategoryProductPOJO.ProductExample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface BankCategoryProductApi {
    @GET("products/getProductsByBank")
    Call<ProductExample> getDetails(@Header("data") String data, @Query("bankId") String bankId, @Query("skip") String skip,
                                    @Query("perPage") String perPage, @Query("sort") String sort, @Query("userId") String userId);
}

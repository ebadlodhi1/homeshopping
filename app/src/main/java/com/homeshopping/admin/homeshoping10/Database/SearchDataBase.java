package com.homeshopping.admin.homeshoping10.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class SearchDataBase extends SQLiteOpenHelper {

    public static final String searchDB_NAME ="SearchDB";
    public  static final String TABLE_NAME_Search = "Search";
    public  static final String COLUMN_Search_searchId = "searchId";
    public  static final String COLUMN_Search_searchtext  = "searchtext";
    public  static final String COLUMN_Search_searchtime  = "searchtime";


    public SearchDataBase(@Nullable Context context) {
        super(context, searchDB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql3 = "create table "+TABLE_NAME_Search+" ("+COLUMN_Search_searchId+" INTEGER PRIMARY KEY AUTOINCREMENT,"+COLUMN_Search_searchtext+" text,"+COLUMN_Search_searchtime+" text)";
        db.execSQL(sql3);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql3 = "DROP TABLE IF EXISTS " + TABLE_NAME_Search + "";
        db.execSQL(sql3);
    }

    public boolean addRecordsearch(String searchtext,String searchtime  ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_Search_searchtext, searchtext);
        contentValues.put(COLUMN_Search_searchtime, searchtime);

        db.insert(TABLE_NAME_Search, null, contentValues);
        Log.d("Inserted in database", "hogaya");
        db.close();

        return true;
    }
    public void deletesearchrecord(int searchindex)
    {

        SQLiteDatabase db = this.getWritableDatabase();

//       db.delete(TABLE_NAME,COLUMN_productid +"="+productid ,null);
        String sql = "DELETE  FROM "+TABLE_NAME_Search+" where "+COLUMN_Search_searchId+" = "+searchindex;
        db.execSQL(sql);

        // db.delete(TABLE_NAME,COLUMN_status +"="+ 1 +"and STR_TO_DATE('21,5,2013 extra characters','%d,%mmm,%Y'),)
        db.close();
    }

    public Cursor getsearchitems() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM "+TABLE_NAME_Search;
        Cursor c = db.rawQuery(sql, null);
        return c;

    }
}

package com.homeshopping.admin.homeshoping10.Class;

public class MenuItem {
    private  String Itemname = "";
    private int Itemid = 0;
    private int childstatus = 0;

    public MenuItem(String itemname, int childstatus) {
        Itemname = itemname;
        this.childstatus = childstatus;
    }

    public MenuItem() {
    }

    public MenuItem(String itemname, int itemid, int childstatus) {
        Itemname = itemname;
        Itemid = itemid;
        this.childstatus = childstatus;
    }

    public String getItemname() {
        return Itemname;
    }

    public void setItemname(String itemname) {
        Itemname = itemname;
    }

    public int getChildstatus() {
        return childstatus;
    }

    public void setChildstatus(int childstatus) {
        this.childstatus = childstatus;
    }


    public int getItemid() {
        return Itemid;
    }

    public void setItemid(int itemid) {
        Itemid = itemid;
    }
}

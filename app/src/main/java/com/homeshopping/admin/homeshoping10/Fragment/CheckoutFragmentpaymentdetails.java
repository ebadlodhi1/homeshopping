package com.homeshopping.admin.homeshoping10.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.homeshopping.admin.homeshoping10.Activity.CartActivity;
import com.homeshopping.admin.homeshoping10.Activity.CheckOutActivity;
import com.homeshopping.admin.homeshoping10.Activity.Dashboard;
import com.homeshopping.admin.homeshoping10.Activity.MainActivity;
import com.homeshopping.admin.homeshoping10.Interfaces.SipmentrateAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.getPaymentmethodAPI;
import com.homeshopping.admin.homeshoping10.POJO.PaymentMethods.payment;
import com.homeshopping.admin.homeshoping10.POJO.PaymentMethods.paymentmethods;
import com.homeshopping.admin.homeshoping10.POJO.ShipmentratePOJO;
import com.homeshopping.admin.homeshoping10.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.DEV_ROOT_URL;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class CheckoutFragmentpaymentdetails extends Fragment {
    Button ConfirmOrder;
    ListView payment_list;

    List<paymentmethods> paymentmethods = new ArrayList<>();
    String[] displayname,price,modulename;
    int selectedid = 0;

    public CheckoutFragmentpaymentdetails() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chekoutpaymentdetailfragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ConfirmOrder = view.findViewById(R.id.button18);
        payment_list = view.findViewById(R.id.payment_list);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        getPaymentmethodAPI api = retrofit.create(getPaymentmethodAPI.class);

        Call<payment> paymentCall = api.getDetails(MainActivity.Tokendata,CartActivity.cityidd);
        paymentCall.enqueue(new Callback<payment>() {
            @Override
            public void onResponse(Call<payment> call, Response<payment> response) {
                if (response.isSuccessful())
                {
                int status = response.body().getStatus();
                if(status == 1)
                {
                    paymentmethods = response.body().getDetail();
                    displayname = new String[paymentmethods.size()];
                    price = new String[paymentmethods.size()];
                    modulename = new String[paymentmethods.size()];

                    for (int i = 0 ;i<paymentmethods.size();i++)
                    {

                        displayname[i]  = paymentmethods.get(i).getDisplayname();
                        price[i] = paymentmethods.get(i).getPrice();
                        modulename[i] = paymentmethods.get(i).getModulename();

                    }
                    try {

                        payment_list.setSelection(selectedid);
                        MypaymentListAdapter mypaymentListAdapter = new MypaymentListAdapter(getActivity(), displayname);
                        payment_list.setAdapter(mypaymentListAdapter);

                    }
                    catch (Exception e)
                    {
                        Log.d("payment method not load",""+e);
                    }

                }
                }
                else
                {
                    Toast.makeText(getActivity(), ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<payment> call, Throwable t) {

            }
        });

        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(DEV_ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SipmentrateAPI sipmentrateAPI = retrofit2.create(SipmentrateAPI.class);
        Call<ShipmentratePOJO> shipmentratePOJOCall = sipmentrateAPI.getDetails(MainActivity.Tokendata, Dashboard.prefConfig.readID(),CartActivity.citynamee);
        shipmentratePOJOCall.enqueue(new Callback<ShipmentratePOJO>() {
            @Override
            public void onResponse(Call<ShipmentratePOJO> call, Response<ShipmentratePOJO> response) {
                if (response.isSuccessful())
                {
                    int status1 = Integer.parseInt(response.body().getStatus());
                    if(status1 == 1)
                    {

                   CheckOutActivity.Productshipmentcharges = response.body().getShippmentCostWithQty();

                    }
                    else
                    {

                        CheckOutActivity.Productshipmentcharges = "0.0000";

                    }
                    }
                else
                {
                    Toast.makeText(getActivity(), ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }



            @Override
            public void onFailure(Call<ShipmentratePOJO> call, Throwable t) {

            }
        });

payment_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectedid = position+1;
        CheckOutActivity.finalpaymentMethod = displayname[position];
        CheckOutActivity.finalpaymentModule = modulename[position];
        CheckOutActivity.finalshipmentcharges = price[position];
//        Toast.makeText(getActivity(), ""+CheckOutActivity.finalpaymentMethod, Toast.LENGTH_SHORT).show();
    }
});
        ConfirmOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedid == 0)
                {
                    Toast.makeText(getActivity(), "Select Payment Method to proceed.", Toast.LENGTH_SHORT).show();
                }
                else
                {showFragment(new CheckoutFragmentConfirmDetails());}


            }
        });
    }

    public class MypaymentListAdapter extends ArrayAdapter<String> {

        private final Activity context;
        private final String[] paymenttitlee;


        public MypaymentListAdapter(Activity context, String[] paymenttitle
                ) {
            super(context, R.layout.paymentmethodcustomitem, paymenttitle);


            this.context=context;
            this.paymenttitlee=paymenttitle;


        }

        public View getView(int position,View view,ViewGroup parent) {
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.paymentmethodcustomitem, null,true);

            TextView paymentmethod = (TextView) rowView.findViewById(R.id.textView102);


            paymentmethod.setText(displayname[position]+" (Rs "+price[position]+")");



            return rowView;

        };
    }
    private void showFragment(Fragment fragment) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.checkout_fragmentlayout, fragment).addToBackStack("orderdetail").commitAllowingStateLoss();


    }
}

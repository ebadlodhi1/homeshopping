package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.OrderDetailPOJO.OrderdetailExample;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface OrderdetailsAPI {
    @GET("/order/getOrderDetails")
    Call<OrderdetailExample> getDetails(@Header("data") String data, @Query("orderId") String orderId);
}

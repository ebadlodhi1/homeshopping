package com.homeshopping.admin.homeshoping10.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.arlib.floatingsearchview.FloatingSearchView;
//import com.crashlytics.android.Crashlytics;
//import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.MenuCategoryIDandNames;
import com.homeshopping.admin.homeshoping10.Class.MenuItemAdapter;
import com.homeshopping.admin.homeshoping10.Class.NonScrollExpandableListView;
import com.homeshopping.admin.homeshoping10.Class.PrefConfig;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Fragment.BaseExampleFragment;
import com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment;
import com.homeshopping.admin.homeshoping10.Interfaces.getcartquantityApi;
import com.homeshopping.admin.homeshoping10.POJO.Cart.CartQuantity;
import com.homeshopping.admin.homeshoping10.R;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.SearchResultActivity.ROOT_URL;



public class Dashboard extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,BaseExampleFragment.BaseExampleFragmentCallbacks {
//    private FirebaseAnalytics mFirebaseAnalytics;
//    int[] Productimagee = {R.drawable.iphonex,R.drawable.samsungnote,R.drawable.iphonex} ;
//    String[] productnamee = {"Iphone X","Samsung Note 8","Iphone 8"} ;
//    String[] orignalpricetvv = {"113000","90000","100000"} ;
//    String[] offpercenttvv  = {"10%","10%","10%"} ;
//    String[]discountedpricetvv = {"113000","90000","100000"} ;

    ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;

    ListView menulistview;
    MenuItemAdapter MenuItemAdapter;
    List<com.homeshopping.admin.homeshoping10.Class.MenuItem> menuItemList = new ArrayList<com.homeshopping.admin.homeshoping10.Class.MenuItem>();
    List<MenuCategoryIDandNames> menuidnames = new ArrayList<MenuCategoryIDandNames>();
    TextView menuheader;

    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    public static int cart_count=0;
    public DatabaseHelper db;
    String TAG = "PhoneActivityTAG";
    Activity activity = Dashboard.this;
    String wantPermission = Manifest.permission.READ_PHONE_STATE;
    private static final int PERMISSION_REQUEST_CODE = 1;
    ArrayList<String> _mst=new ArrayList<>();
   public static ArrayList<String> _lst =new ArrayList<>();
   public  static  String Imei = "";
    public  static  String Phonenumber = "";
    Handler hand = new Handler();
    Handler menu = new Handler();
    public static ProgressBar progressBar4;
    public static  ConstraintLayout loginbefore,loginafter,logout;
//    public static TextView username,email;
    public static PrefConfig prefConfig;
    androidx.constraintlayout.widget.ConstraintLayout contactuscons , helpcons,whatsapp;
    private Tracker mTracker;
    static ConstraintLayout constraintLayout90;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        expandableListView = findViewById(R.id.expandableListView);
        menulistview = findViewById(R.id.menulistview);
        menuheader = findViewById(R.id.menuheader);

        contactuscons = findViewById(R.id.constraintLayout95);
        whatsapp = findViewById(R.id.constraintLayout94);
        constraintLayout90 = findViewById(R.id.constraintLayout90);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Dashboard Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());

        whatsapp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                String contact = "+92 3111476725"; // use country code with your phone number
                String url = "https://api.whatsapp.com/send?phone=" + contact;
                try {
                    PackageManager pm = getApplicationContext().getPackageManager();
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
        helpcons = findViewById(R.id.constraintLayout96);
        contactuscons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ContactusActivity.class);
                startActivity(intent);
            }
        });
        helpcons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ContactusActivity.class);
                startActivity(intent);
            }
        });

//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        Crashlytics.getInstance().crash();
        setSupportActionBar(toolbar);
        prefConfig = new PrefConfig(this);
//
        progressBar4 = findViewById(R.id.progressBar4);

        cart_count = 0 ;
        db  =new DatabaseHelper(this);

//       Toast.makeText(getApplicationContext(), "id  "+Dashboard.prefConfig.readID().toString(), Toast.LENGTH_SHORT).show();


        menu.post(loadmenu);
        populatestage1();
        MenuItemAdapter = new MenuItemAdapter(menuItemList,this);
        menulistview.setAdapter(MenuItemAdapter);
        menulistview.setFastScrollEnabled(true);
        justifyListViewHeightBasedOnChildren(menulistview);
//        menuNameandID();

            menuheader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(), ""+menuheader.getText().toString(), Toast.LENGTH_SHORT).show();
//                    if(menuheader.getText().toString().equals("< Back To Main Categories"))
//                    {
//                    menuheader.setText("All Categories");
//                    populatestage1();
//                    MenuItemAdapter.notifyDataSetChanged();
//                    }
                }
            });

        menulistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                menuheader.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(getApplicationContext(), ""+menuheader.getText().toString(), Toast.LENGTH_SHORT).show();
                    if(menuheader.getText().toString().equals("< Back To Main Categories"))
                    {
                    menuheader.setText("All Categories");
                    populatestage1();
                    MenuItemAdapter.notifyDataSetChanged();
                    }
                    }
                });
                if (MenuItemAdapter.getItem(position).getChildstatus() == 1)
                {


                    if (MenuItemAdapter.getItem(position).getItemname().equals("Mobile & Tablets"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatemobiletablet();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                menuheader.setText("All Categories");
                                populatestage1();
                                MenuItemAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                  else  if (MenuItemAdapter.getItem(position).getItemname().equals("Mobile Accessories"))
                    {
                        menuheader.setText("< Mobile & Tablets");
                        populatemobileAccesories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                        if(menuheader.getText().toString().equals("< Back To Main Categories"))
                            {
                            menuheader.setText("All Categories");
                            populatestage1();
                            MenuItemAdapter.notifyDataSetChanged();
                            }
                        else
                        {
                            menuheader.setText("< Back To Main Categories");
                            populatemobiletablet();
                            MenuItemAdapter.notifyDataSetChanged();
                        }
                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Computer & Laptops"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatecomputerandlaptop();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                menuheader.setText("All Categories");
                                populatestage1();
                                MenuItemAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                   else if (MenuItemAdapter.getItem(position).getItemname().equals("Peripherals"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populateperipherals();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                   else if (MenuItemAdapter.getItem(position).getItemname().equals("Computer Components"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populatecomputercomponents();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Storage"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populatestorage();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Networking"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populatenetworking();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Computer Accessories"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populatecomputeraccessories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Laptop Accessories"))
                {
                    menuheader.setText("< Computer & Laptops");
                    populatelaptopaccessories();
                    MenuItemAdapter.notifyDataSetChanged();
                    menuheader.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if(menuheader.getText().toString().equals("< Back To Main Categories"))
                            {
                                menuheader.setText("All Categories");
                                populatestage1();
                                MenuItemAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                menuheader.setText("< Back To Main Categories");
                                populatecomputerandlaptop();
                                MenuItemAdapter.notifyDataSetChanged();
                            }
                        }
                    });
                }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Printers & Accessories"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populateprintersandaccessories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                   else if (MenuItemAdapter.getItem(position).getItemname().equals("Cameras & Accessories"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatecamera();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                menuheader.setText("All Categories");
                                populatestage1();
                                MenuItemAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Camera Accessories"))
                    {
                        menuheader.setText("< Cameras & Accessories");
                        populatecamerasandaccessories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecamera();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Surveillance Cameras"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatesurveillancecameras();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecamera();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("TV & Video"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatetvandvideo();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatetvandvideo();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Home Audio & Video"))
                    {
                        menuheader.setText("< TV & Video");
                        populatehomeaudioandvideo();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatetvandvideo();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("TV Accessories"))
                    {
                        menuheader.setText("< TV & Video");
                        populatetvaccesories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatetvandvideo();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Home Appliances"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatehomeappliances();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Small Appliances"))
                    {
                        menuheader.setText("< Home Appliances");
                        populatesmallappliances();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Kitchen Appliances"))
                    {
                        menuheader.setText("< Small Appliances");
                        populatekitchenappliances();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else if(menuheader.getText().toString().equals("< Home Appliances"))
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Home Appliances");
                                    populatesmallappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Large Appliances"))
                    {
                        menuheader.setText("< Home Appliances");
                        populatelargeappliances();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Ovens"))
                    {
                        menuheader.setText("< Large Appliances");
                        populateovens();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else if(menuheader.getText().toString().equals("< Home Appliances"))
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Home Appliances");
                                    populatelargeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Personal Care"))
                    {
                        menuheader.setText("< Home Appliances");
                        populatepersonalcare();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Hardware & Tools"))
                    {
                        menuheader.setText("< Home Appliances");
                        populatehardwaretools();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Musical Instruments"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatemusicalinstruments();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatemusicalinstruments();;
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Guitars & Accessories"))
                    {
                        menuheader.setText("< Musical Instruments");
                        populateguitarsandaccessories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatemusicalinstruments();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Studio Equipment"))
                    {
                        menuheader.setText("< Musical Instruments");
                        populatestudioequipments();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatemusicalinstruments();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Keyboards & Pianos"))
                    {
                        menuheader.setText("< Musical Instruments");
                        populatekeyboardandpiano();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatemusicalinstruments();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }


                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Books & Stationery"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatebooksandstationery();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatebooksandstationery();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Books"))
                    {
                        menuheader.setText("< Books & Stationery");
                        populatebooks();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatebooksandstationery();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Stationery"))
                    {
                        menuheader.setText("< Books & Stationery");
                        populatestationery();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatebooksandstationery();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Gaming"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populategaming();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populategaming();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Xbox One S"))
                    {
                        menuheader.setText("< Gaming");
                        populatexboxones();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populategaming();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("PlayStation 4"))
                    {
                        menuheader.setText("< Gaming");
                        populateplaystation4();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populategaming();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Xbox 360"))
                    {
                        menuheader.setText("< Gaming");
                        populatexbox360();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populategaming();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Baby & Toys"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatebabyandtoys();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatebabyandtoys();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Mother & Baby"))
                    {
                        menuheader.setText("< Baby & Toys");
                        populatemotherandbaby();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatebabyandtoys();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Fashion"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatefashion();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatefashion();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Men"))
                    {
                        menuheader.setText("< Fashion");
                        populatemen();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatefashion();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Clothing"))
                    {
                        menuheader.setText("< Men");
                        populateclothing();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                              else if(menuheader.getText().toString().equals("< Men"))
                                {
                                    menuheader.setText("< Fashion");
                                    populatemen();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatefashion();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Kids"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatekids();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatefashion();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                }
                else if(MenuItemAdapter.getItem(position).getChildstatus() == 0)
                {
                    String name = MenuItemAdapter.getItem(position).getItemname().toString();

                    int catid = MenuItemAdapter.getItem(position).getItemid();
//                    Toast.makeText(getApplicationContext(), ""+name+" "+catid, Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                    intent.putExtra("catname", name);
                    intent.putExtra("catid", catid);
                    startActivity(intent);
                    populatestage1();
                    menuheader.setText("All Categories");
                    MenuItemAdapter.notifyDataSetChanged();
                }
            }

        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.setDrawerIndicatorEnabled(false);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START);
            }
        });
        toggle.setHomeAsUpIndicator(R.drawable.hamburger_1);

        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        loginbefore  =header.findViewById(R.id.loginbefore);
        loginafter  = header.findViewById(R.id.loginafter);
//        username  = header.findViewById(R.id.loginname);
//        email = header.findViewById(R.id.textView18);
          if(prefConfig.readLoginStatus())
          {
              loginbefore.setVisibility(View.GONE);
              loginafter.setVisibility(View.VISIBLE);
//              username.setText("Hello "+prefConfig.readName());
//              email.setText(prefConfig.reademail());
              Retrofit retrofit = new Retrofit.Builder()
                      .baseUrl(ROOT_URL) //Setting the Root URL
                      .addConverterFactory(ScalarsConverterFactory.create())
                      .addConverterFactory(GsonConverterFactory.create())
                      .build();
              getcartquantityApi api = retrofit.create(getcartquantityApi.class);
              Call<CartQuantity> cartQuantityCall = api.getDetails(MainActivity.Tokendata,Dashboard.prefConfig.readID());
              cartQuantityCall.enqueue(new Callback<CartQuantity>() {
                  @Override
                  public void onResponse(Call<CartQuantity> call, Response<CartQuantity> response) {
                      if (response.isSuccessful()) {
                          int status = response.body().getStatus();
                          if (status == 1) {
//                          Toast.makeText(getApplicationContext(), "cart count server "+response.body().getCartQuantity(), Toast.LENGTH_SHORT).show();
                              Dashboard.cart_count = response.body().getCartQuantity();
                              invalidateOptionsMenu();
                          }
                      }
                  }

                  @Override
                  public void onFailure(Call<CartQuantity> call, Throwable t) {

                  }
              });
          }
          else
          {
              loginbefore.setVisibility(View.VISIBLE);
              loginafter.setVisibility(View.GONE);


          }

//        logout  = header.findViewById(R.id.logout);
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                loginafter.setVisibility(View.GONE);
//                loginbefore.setVisibility(View.VISIBLE);
//
//            }
//        });
        ConstraintLayout Acount = header.findViewById(R.id.constraintLayout23);
        Acount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent = new Intent(getApplicationContext(),AccountActivity.class);
               startActivity(intent);

            }
        });
    ConstraintLayout wishlist = header.findViewById(R.id.constraintLayout24);
        wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),WhishlistActivity.class);
                startActivity(intent);
            }
        });
        ConstraintLayout home = header.findViewById(R.id.constraintLayoutfnhome);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        ConstraintLayout home1 = header.findViewById(R.id.constraintLayoutfn1);
        home1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });
        ConstraintLayout signuplayout = header.findViewById(R.id.signuplayout);
        signuplayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(intent);

            }
        });
        ConstraintLayout cartlayout = header.findViewById(R.id.constraintLayout20);
        cartlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intent);

            }
        });
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        ConstraintLayout signinlayout = header.findViewById(R.id.signinlayout);
        signinlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("comingcart","0");
                startActivity(intent);

            }
        });
        ConstraintLayout constraintLayout20 = header.findViewById(R.id.constraintLayout25);
        constraintLayout20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intent);

            }
        });


//        banner1dash.setBackgroundResource(R.drawable.mobilebannernew);

//        showFragment(new ScrollingSearchExampleFragment());
//    //    requestPermission(wantPermission);
//        if (!checkPermission(wantPermission)) {
//            requestPermission(wantPermission);
//        } else {
//
//
//            Log.d(TAG, "Phone number: " + getPhone());
//            Toast.makeText(getApplicationContext(), "MYPhone number: " + getPhone(), Toast.LENGTH_SHORT).show();
//
//            _mst = getPhone();
//
//            for (String op : _mst) {
//
//
//                Log.i("Device Information", String.valueOf(op));
//
//
//            }
//        }
//runOnUiThread(new Runnable() {
//    @Override
//    public void run() {
//
//    }
//});

        hand.postDelayed(run, 1000);


    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        invalidateOptionsMenu();
    }


    Runnable run = new Runnable()
    { @Override public void run() {

        showFragment(new ScrollingSearchExampleFragment(getApplicationContext()));
    }
    };
    Runnable loadmenu = new Runnable() {
        @Override
        public void run() {
//            prepareMenuData();
//            populateExpandableList();
        }
    };
//    @TargetApi(Build.VERSION_CODES.O)
//    public ArrayList<String> getPhone() {
//        TelephonyManager phoneMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        if (ActivityCompat.checkSelfPermission(activity, wantPermission) != PackageManager.PERMISSION_GRANTED) {
//            return null;
//        }
//
////        _lst.add(String.valueOf(phoneMgr.getCallState()));
//        _lst.add("IMEI NUMBER :-"+phoneMgr.getImei());
////        _lst.add("MOBILE NUMBER :-"+phoneMgr.getLine1Number());
////        _lst.add("SERIAL NUMBER :-"+phoneMgr.getSimSerialNumber());
////        _lst.add("SIM OPERATOR NAME :-"+phoneMgr.getSimOperatorName());
////        _lst.add("MEI NUMBER :-"+phoneMgr.getMeid());
////        _lst.add("SIM STATE :-"+String.valueOf(phoneMgr.getSimState()));
////        _lst.add("COUNTRY ISO :-"+phoneMgr.getSimCountryIso());
//        Imei = phoneMgr.getImei().toString();
//        Phonenumber = phoneMgr.getLine1Number().toString();
//        return _lst;
//    }

    private void requestPermission(String permission){
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)){
            Toast.makeText(activity, "Phone state permission allows us to get Device ID. Please allow it for additional functionality.", Toast.LENGTH_LONG).show();
            requestPermission(wantPermission);
        }
        ActivityCompat.requestPermissions(activity, new String[]{permission},PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case PERMISSION_REQUEST_CODE:
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Log.d(TAG, "Phone number: " + getPhone());
//
////                    Toast.makeText(getApplicationContext(), "NEWPhone number: " + getPhone(), Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(activity,"Permission Denied. We can't able to add product on cart", Toast.LENGTH_LONG).show();
//                    requestPermission(wantPermission);
//                }
//                break;
//        }
    }

    private boolean checkPermission(String permission){
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(activity, permission);
            if (result == PackageManager.PERMISSION_GRANTED){
                return true;
            } else {
                checkPermission(wantPermission);
                return true;
            }
        } else {
            return true;
        }




    }
    private void showFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment).commitAllowingStateLoss();
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
//            super.onBackPressed();
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)

                    .setMessage("Are you sure you want to Exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showFragment(new ScrollingSearchExampleFragment(getApplicationContext()));
                        }
                    })
                    .show();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.normalmenu, menu);
//        MenuItem menuItem = menu.findItem(R.id.refresh2);
//        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),cart_count,R.drawable.ttcart));
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.refresh2) {
//            Intent intent = new Intent(getApplicationContext(),CartActivity.class);
//            startActivity(intent);
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//    private void prepareMenuData() {
//
//        MenuModel menuModel = new MenuModel("Electronics", true, true, "https://www.journaldev.com/9333/android-webview-smarteist-tutorial"); //Menu of Android Tutorial. No sub menus
//        headerList.add(menuModel);
//        List<MenuModel> childModelsList = new ArrayList<>();
//        MenuModel childModel = new MenuModel("Mobile Phone & Tablets", false, false, "1031");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Cameras & Photo", false, false, "55");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Computers & Laptops", false, false, "2106");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("TV & Video", false, false, "2216");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Musical Instruments", false, false, "2287");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Gaming", false, false, "2131");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Home Appliances", false, false, "844");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Headsets & Earphones", false, false, "2486");
//        childModelsList.add(childModel);
//
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//        childModelsList = new ArrayList<>();
//        menuModel = new MenuModel("Fashion", true, true, ""); //Menu of Java Tutorials
//        headerList.add(menuModel);
//
//        childModel = new MenuModel("Women", false, false, "2409");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Men", false, false, "2408");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Kids", false, false, "2656");
//        childModelsList.add(childModel);
//
//
//
//        if (menuModel.hasChildren) {
//            Log.d("API123","here");
//            childList.put(menuModel, childModelsList);
//        }
//
//        childModelsList = new ArrayList<>();
//        menuModel = new MenuModel("Baby & Toys", true, true, ""); //Menu of Python Tutorials
//        headerList.add(menuModel);
//        childModel = new MenuModel("Toys", false, false, "1028");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Mother & Baby", false, false, "3032");
//        childModelsList.add(childModel);
//
//
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//
//
//        childModelsList = new ArrayList<>();
//        menuModel = new MenuModel("Beauty & Health", true, true, ""); //Menu of Python Tutorials
//        headerList.add(menuModel);
//        childModel = new MenuModel("MakeUp", false, false, "3037");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Beauty & Grooming", false, false, "2788");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Bath & Body", false, false, "567");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Shavers", false, false, "2474");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Conditioners", false, false, "2484");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Shampoo", false, false, "2483");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Health & Supplements", false, false, "2993");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Contact Lenses", false, false, "2350");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Hair Treatment", false, false, "1342");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Hair Products", false, false, "2488");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Medical Equipment & Accessories", false, false, "3083");
//        childModelsList.add(childModel);
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//
//        childModelsList = new ArrayList<>();
//        menuModel = new MenuModel("Sports & Fitness", true, true, ""); //Menu of Python Tutorials
//        headerList.add(menuModel);
//        childModel = new MenuModel("Gym Equipments", false, false, "2798");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Fishing", false, false, "2611");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Cricket", false, false, "2570");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Bicycle", false, false, "2685");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Volleyball", false, false, "3078");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Badminton", false, false, "3079");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Outdoor Sports Equipment", false, false, "3025");
//        childModel = new MenuModel("Football", false, false, "2591");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Basketball", false, false, "2595");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Table Tennis", false, false, "2592");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Health & Beauty", false, false, "64");
//        childModel = new MenuModel("Hockey", false, false, "2597");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Tennis", false, false, "2593");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Golf", false, false, "2594");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Sports Wear", false, false, "2577");
//        childModelsList.add(childModel);
//
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//        childModelsList = new ArrayList<>();
//        menuModel = new MenuModel("Home & Kitchen", true, true, ""); //Menu of Python Tutorials
//        headerList.add(menuModel);
//        childModel = new MenuModel("Home Improvement", false, false, "3019");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Table & Wall Clocks", false, false, "3015");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Paintings", false, false, "2867");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Furniture", false, false, "2205");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Home & Decoration", false, false, "2734");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("BedSheet, Pillow & Comforters", false, false, "1256");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Handi Crafts", false, false, "3087");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Photo Frames", false, false, "2571");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Cushion", false, false, "2194");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Pest Repeller", false, false, "676");
//        childModelsList.add(childModel);
//
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//
//        childModelsList = new ArrayList<>();
//
//        menuModel = new MenuModel("Cars, Bikes & Parts", true, true, ""); //Menu of Python Tutorials
//        headerList.add(menuModel);
//        childModel = new MenuModel("Cars", false, false, "2737");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Car Accessories", false, false, "2189");
//
//        childModel = new MenuModel("Motorcycles/Bikes", false, false, "3205");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Car Parts & Accessories", false, false, "58");
//        childModelsList.add(childModel);
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//
//        childModelsList = new ArrayList<>();
//        menuModel = new MenuModel("Digital Store", true, true, ""); //Menu of Python Tutorials
//        headerList.add(menuModel);
//        childModel = new MenuModel("Amazon Gift Cards", false, false, " 2446");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("PlayStation Store Gift Cards", false, false, "2449");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Rixty Gift Cards", false, false, "3146");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Tickets", false, false, "3361");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Visa Gift Cards", false, false, "3143");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Steam Gift Cards", false, false, "2989");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Google Play Gift Card", false, false, "2990");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Facebook Game eCards", false, false, "2453");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Xbox Digital Gift Cards", false, false, "2452");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Homeshopping Gift Cards", false, false, "2458");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Ebay Gift Cards", false, false, "2454");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Apple iTunes Gift Cards", false, false, "2448");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Minecraft Game Card", false, false, "3028");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Skype Credit", false, false, "2447");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Nintendo eShop Game Card", false, false, "3027");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Karma Koin Game Card", false, false, "3029");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("League of Legends Game Card", false, false, "3030");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Zenmate VPN Vouchers", false, false, "2912");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Spotify Gift Cards", false, false, "2450");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Windows Store Gift Cards", false, false, "2451");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Insurance", false, false, "3323");
//        childModelsList.add(childModel);
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//        childModelsList = new ArrayList<>();
//        menuModel = new MenuModel("Clearance Sale", true, true, ""); //Menu of Python Tutorials
//        headerList.add(menuModel);
//        childModel = new MenuModel("Mobile Phones On Sale", false, false, "2129");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Tablet PC's On Sale", false, false, "2130");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Clothing On Sale", false, false, "2830");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Laptops On Sale", false, false, "2200");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Camera On Sale", false, false, "2199");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Watches Phones On Sale", false, false, "2757");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Headphones On Sale", false, false, "2517");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Other Items", false, false, "2201");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Special Deals", false, false, "2756");
//        childModelsList.add(childModel);
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//
//        childModelsList = new ArrayList<>();
//        menuModel = new MenuModel("Daily Deals", true, true, ""); //Menu of Python Tutorials
//        headerList.add(menuModel);
//        childModel = new MenuModel("Baby & Toys Deals", false, false, "3368");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Books Deals", false, false, "3883");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Camera Deals", false, false, "2911");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Fashion Deals", false, false, "2972");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Gaming Deals", false, false, "2971");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Home Appliances Deals", false, false, "2957");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Laptops & Computers Deals", false, false, "2910");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Mobile & Tablets Deals", false, false, "2909");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Musical Instrument Deals", false, false, "2958");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("TV And Video Deals", false, false, "2956");
//        childModelsList.add(childModel);
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//
//        childModelsList = new ArrayList<>();
//        menuModel = new MenuModel("More...", true, true, ""); //Menu of Python Tutorials
//        headerList.add(menuModel);
//        childModel = new MenuModel("Bank Alfalah Mega Discount Deals", false, false, "3957");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Bank Alfalah Rs 1 Shop Mega Discount Deals", false, false, "3958");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Premier Sales Day", false, false, "3956");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Bakra On Wheels", false, false, "3949");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Turkey Duty Free Products", false, false, "3948");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Fathers Day", false, false, "3943");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Mothers Day", false, false, "3884");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Future Tech", false, false, "2423");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Books & Stationery", false, false, "3763");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("HSN Services", false, false, "2751");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Food Items", false, false, "3769");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Gift Items", false, false, "2343");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Movies", false, false, "2374");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("E-Liquids and E-Cigarettes", false, false, "2663");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Ramadan Charity Packages", false, false, "3093");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Send Gifts To Pakistan", false, false, "2907");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Credit Card Deals", false, false, "2992");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Electronics Gala", false, false, "3729");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Gem Stones Price in Pakistan", false, false, "3086");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Installments For Karachi Only", false, false, "2679");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Traveller Accessories", false, false, "2825");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Zippo", false, false, " 2444");
//        childModelsList.add(childModel);
//
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//
//        childModelsList = new ArrayList<>();
//        menuModel = new MenuModel("Dubai Duty Free Products", true, true, ""); //Menu of Python Tutorials
//        headerList.add(menuModel);
//        childModel = new MenuModel("Chocolates", false, false, "3799");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Perfumes", false, false, "3781");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Toys", false, false, "3938");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Cosmetics", false, false, "3783");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Watches", false, false, "3782");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Electronics", false, false, "3780");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Cuban", false, false, "3778");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("Tobacco", false, false, "3779");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("American Cigars", false, false, "3776");
//        childModelsList.add(childModel);
//
//        childModel = new MenuModel("Ciggerates", false, false, "3775");
//        childModelsList.add(childModel);
//        childModel = new MenuModel("European Cigars", false, false, "3777");
//        childModelsList.add(childModel);
//
//        if (menuModel.hasChildren) {
//            childList.put(menuModel, childModelsList);
//        }
//
//
//    }
//private void prepareMenuData() {
//
//    MenuModel menuModel = new MenuModel("Mobile & Tablets", true, true, "");
//    List<MenuModel> childModelsList = new ArrayList<>();
//    MenuModel childModel = new MenuModel("Mobile Phones", false, false, "1031");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Tablets & eBook Readers", false, false, "578");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Mobile Accessories", false, false, "2125");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Headsets & Earphones", false, false, "2486");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Smart Watches", false, false, "2212");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Tablet Accessories", false, false, "1047");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Prepaid Scratch Cards", false, false, "2550");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Refurbished Mobiles", false, false, "2520");
//    childModelsList.add(childModel);
//
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//
//
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Computer & Laptops", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Laptops", false, false, "1032");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Computer Components", false, false, "2300");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Storage", false, false, "2346");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Networking", false, false, "2301");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Computer Accessories", false, false, "2336");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Laptop Accessories", false, false, "2461");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Printers", false, false, "2297");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Printers & Accessories ", false, false, "2278");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Computer Monitors", false, false, "1599");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Desktop Computers", false, false, "2014");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Software", false, false, "2298");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Development Tools", false, false, "2686");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Attendance Machhine & Finger Print Readers ", false, false, "2999");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Gaming Laptops", false, false, "2640");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Bitcoin", false, false, "2721");
//    childModelsList.add(childModel);
//
//
//
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//
//
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Cameras & Accessories", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Digital Cameras", false, false, "637");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("DSLR Cameras", false, false, "747");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("DSLM Cameras", false, false, "3004");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Camera Accessories", false, false, "1511");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Surveillance Cameras", false, false, "2269");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Handy Cam & Camcorders", false, false, "1315");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Camera Drones", false, false, "2351");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Camera Gimble Stabilizers", false, false, "3140");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Instant Cameras", false, false, "3325");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Camera Deals", false, false, "2227");
//    childModelsList.add(childModel);
////    childModel = new MenuModel("Camera Sliders", false, false, "3083");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Camera Straps", false, false, "3083");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Audio Equipment", false, false, "3083");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Lens Skin", false, false, "3083");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Camera Baterry Grips", false, false, "3083");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Blackmagic Accessories", false, false, "3083");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("DJI Accessories", false, false, "3083");
//    childModelsList.add(childModel);
//
//
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("TV and Video", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Televisions", false, false, "112");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Home Audio & Video", false, false, "1251");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Projectors", false, false, "152");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("TV Accessories", false, false, "1241");
//    childModelsList.add(childModel);
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Home Appliances", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Small Appliances", false, false, "2503");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Large Appliances", false, false, "2504");
//    childModelsList.add(childModel);
//
//
//    childModel = new MenuModel("Personal Care", false, false, "2325");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Hardware & Tools", false, false, "2827");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Clinical Products", false, false, "3150");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Telephones Video Phones Door Phones", false, false, "2559");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Lighting,Sockets & Switches", false, false, "2847");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Indoor & Outdoor Living Products", false, false, "2799");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Smart Home Gadgets", false, false, "31");
//    childModelsList.add(childModel);
//
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Musical Instruments", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Guitars", false, false, "2288");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Guitar & Accessories", false, false, "2763");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Studio Equipments", false, false, "2775");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Keyboards & Pianos", false, false, "2716");
//    childModelsList.add(childModel);
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Books & Stationery", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Books", false, false, "2870");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Stationery", false, false, "3662");
//    childModelsList.add(childModel);
//
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Gaming", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Xbox One S", false, false, "2914");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("PlayStation 4", false, false, "2134");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Xbox 360", false, false, "2133");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("PlayStation VR", false, false, "2991");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("PlayStation 4 Pro", false, false, "2950");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Xbox Project Scorpio", false, false, "3010");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Nintendo Switch", false, false, "3006");
//    childModelsList.add(childModel);
//
//
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Fashion", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Women", false, false, "2409");
//    childModelsList.add(childModel);
//
//
//    childModel = new MenuModel("Men", false, false, "2408");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Kids", false, false, "2656");
//    childModelsList.add(childModel);
//
//
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//
//
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Baby & Toys", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel(" Baby Toys", false, false, "2665");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Toys", false, false, "1028");
//    childModelsList.add(childModel);
//
//
//    childModel = new MenuModel("Mother & Baby", false, false, "3033");
//    childModelsList.add(childModel);
//
//
//
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//
//
////    childModelsList = new ArrayList<>();
////    menuModel = new MenuModel("Sports & Fitness", true, true, ""); //Menu of Python Tutorials
////    headerList.add(menuModel);
////    childModel = new MenuModel("Gym Equipments", false, false, "2798");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Fishing", false, false, "2611");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Cricket", false, false, "2570");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Bicycle", false, false, "2685");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Volleyball", false, false, "3078");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Badminton", false, false, "3079");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Outdoor Sports Equipment", false, false, "3025");
////    childModel = new MenuModel("Football", false, false, "2591");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Basketball", false, false, "2595");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Table Tennis", false, false, "2592");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Health & Beauty", false, false, "64");
////    childModel = new MenuModel("Hockey", false, false, "2597");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Tennis", false, false, "2593");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Golf", false, false, "2594");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Sports Wear", false, false, "2577");
////    childModelsList.add(childModel);
////
////    if (menuModel.hasChildren) {
////        childList.put(menuModel, childModelsList);
////    }
////    childModelsList = new ArrayList<>();
////    menuModel = new MenuModel("Home & Kitchen", true, true, ""); //Menu of Python Tutorials
////    headerList.add(menuModel);
////    childModel = new MenuModel("Home Improvement", false, false, "3019");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Table & Wall Clocks", false, false, "3015");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Paintings", false, false, "2867");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Furniture", false, false, "2205");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Home & Decoration", false, false, "2734");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("BedSheet, Pillow & Comforters", false, false, "1256");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Handi Crafts", false, false, "3087");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Photo Frames", false, false, "2571");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Cushion", false, false, "2194");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Pest Repeller", false, false, "676");
////    childModelsList.add(childModel);
////
////    if (menuModel.hasChildren) {
////        childList.put(menuModel, childModelsList);
////    }
////
////    childModelsList = new ArrayList<>();
////
////    menuModel = new MenuModel("Cars, Bikes & Parts", true, true, ""); //Menu of Python Tutorials
////    headerList.add(menuModel);
////    childModel = new MenuModel("Cars", false, false, "2737");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Car Accessories", false, false, "2189");
////
////    childModel = new MenuModel("Motorcycles/Bikes", false, false, "3205");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Car Parts & Accessories", false, false, "58");
////    childModelsList.add(childModel);
////    if (menuModel.hasChildren) {
////        childList.put(menuModel, childModelsList);
////    }
//
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Digital Store", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Amazon Gift Cards", false, false, " 2446");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("PlayStation Store Gift Cards", false, false, "2449");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Rixty Gift Cards", false, false, "3146");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Tickets", false, false, "3361");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Visa Gift Cards", false, false, "3143");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Steam Gift Cards", false, false, "2989");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Google Play Gift Card", false, false, "2990");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Facebook Game eCards", false, false, "2453");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Xbox Digital Gift Cards", false, false, "2452");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Homeshopping Gift Cards", false, false, "2458");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Ebay Gift Cards", false, false, "2454");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Apple iTunes Gift Cards", false, false, "2448");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Minecraft Game Card", false, false, "3028");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Skype Credit", false, false, "2447");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Nintendo eShop Game Card", false, false, "3027");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Karma Koin Game Card", false, false, "3029");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("League of Legends Game Card", false, false, "3030");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Zenmate VPN Vouchers", false, false, "2912");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Spotify Gift Cards", false, false, "2450");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Windows Store Gift Cards", false, false, "2451");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Insurance", false, false, "3323");
//    childModelsList.add(childModel);
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Clearance Sale", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Mobile Phones On Sale", false, false, "2129");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Tablet PC's On Sale", false, false, "2130");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Clothing On Sale", false, false, "2830");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Laptops On Sale", false, false, "2200");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Camera On Sale", false, false, "2199");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Watches Phones On Sale", false, false, "2757");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Headphones On Sale", false, false, "2517");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Other Items", false, false, "2201");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Special Deals", false, false, "2756");
//    childModelsList.add(childModel);
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//
//    childModelsList = new ArrayList<>();
//    menuModel = new MenuModel("Daily Deals", true, true, ""); //Menu of Python Tutorials
//    headerList.add(menuModel);
//    childModel = new MenuModel("Baby & Toys Deals", false, false, "3368");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Books Deals", false, false, "3883");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Camera Deals", false, false, "2911");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Fashion Deals", false, false, "2972");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Gaming Deals", false, false, "2971");
//    childModelsList.add(childModel);
//
//    childModel = new MenuModel("Home Appliances Deals", false, false, "2957");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Laptops & Computers Deals", false, false, "2910");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Mobile & Tablets Deals", false, false, "2909");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("Musical Instrument Deals", false, false, "2958");
//    childModelsList.add(childModel);
//    childModel = new MenuModel("TV And Video Deals", false, false, "2956");
//    childModelsList.add(childModel);
//    if (menuModel.hasChildren) {
//        childList.put(menuModel, childModelsList);
//    }
//
////    childModelsList = new ArrayList<>();
////    menuModel = new MenuModel("More...", true, true, ""); //Menu of Python Tutorials
////    headerList.add(menuModel);
////    childModel = new MenuModel("Bank Alfalah Mega Discount Deals", false, false, "3957");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Bank Alfalah Rs 1 Shop Mega Discount Deals", false, false, "3958");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Premier Sales Day", false, false, "3956");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Bakra On Wheels", false, false, "3949");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Turkey Duty Free Products", false, false, "3948");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Fathers Day", false, false, "3943");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Mothers Day", false, false, "3884");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Future Tech", false, false, "2423");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Books & Stationery", false, false, "3763");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("HSN Services", false, false, "2751");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Food Items", false, false, "3769");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Gift Items", false, false, "2343");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Movies", false, false, "2374");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("E-Liquids and E-Cigarettes", false, false, "2663");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Ramadan Charity Packages", false, false, "3093");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Send Gifts To Pakistan", false, false, "2907");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Credit Card Deals", false, false, "2992");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Electronics Gala", false, false, "3729");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Gem Stones Price in Pakistan", false, false, "3086");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Installments For Karachi Only", false, false, "2679");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Traveller Accessories", false, false, "2825");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Zippo", false, false, " 2444");
////    childModelsList.add(childModel);
////
////    if (menuModel.hasChildren) {
////        childList.put(menuModel, childModelsList);
////    }
////
////    childModelsList = new ArrayList<>();
////    menuModel = new MenuModel("Dubai Duty Free Products", true, true, ""); //Menu of Python Tutorials
////    headerList.add(menuModel);
////    childModel = new MenuModel("Chocolates", false, false, "3799");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Perfumes", false, false, "3781");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Toys", false, false, "3938");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Cosmetics", false, false, "3783");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Watches", false, false, "3782");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Electronics", false, false, "3780");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Cuban", false, false, "3778");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("Tobacco", false, false, "3779");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("American Cigars", false, false, "3776");
////    childModelsList.add(childModel);
////
////    childModel = new MenuModel("Ciggerates", false, false, "3775");
////    childModelsList.add(childModel);
////    childModel = new MenuModel("European Cigars", false, false, "3777");
////    childModelsList.add(childModel);
////
////    if (menuModel.hasChildren) {
////        childList.put(menuModel, childModelsList);
////    }
//
//
//}
//    private void populateExpandableList() {
//
//        expandableListAdapter = new com.homeshopping.admin.homeshoping10.Adapter.ExpandableListAdapter(this, headerList, childList);
//        expandableListView.setAdapter(expandableListAdapter);
//
//
//        final int[] prevExpandPosition = {-1};
//        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                if (prevExpandPosition[0] >= 0 && prevExpandPosition[0] != groupPosition) {
//                    expandableListView.collapseGroup(prevExpandPosition[0]);
//                }
//                prevExpandPosition[0] = groupPosition;
//            }
//        });
//
//        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//
//                if (childList.get(headerList.get(groupPosition)) != null) {
//                    MenuModel model = childList.get(headerList.get(groupPosition)).get(childPosition);
//                    if (model.url.length() > 0) {
////                        WebView webView = findViewById(R.id.webView);
////                        webView.loadUrl(model.url);
//
//                        Intent intent = new Intent(getApplicationContext(),CategoryActivity.class);
////                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
//                        intent.putExtra("catname",model.menuName);
//                        intent.putExtra("catid",model.url);
//                        startActivity(intent);
//                        onBackPressed();
//                    }
//                }
//
//                return false;
//            }
//        });
//    }


    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

    }

    @Override
    public void attachSearchViewActivityDrawer(FloatingSearchView searchView) {

    }

    @Override
    public boolean onActivityBackPress() {
        return false;
    }

    @Override
    public void onAttachSearchViewToDrawer(FloatingSearchView searchView) {

    }


    public  void populatestage1()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile & Tablets",2909,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer & Laptops",1032,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cameras & Accessories",55,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("TV & Video",2216,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Home Appliances",844,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Musical Instruments",2287,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Books & Stationery",3763,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming",2971,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fashion",3568,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Baby & Toys",3368,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Digital Store", 2445,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Daily Deal", 2719,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clearance Sale", 1583,0));
    }
    public  void populatemobiletablet()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile Phones", 1031,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tablets & eBook Readers", 578,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile Accessories",2125,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headsets & Earphones", 2486,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Smart Watches",2212,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tablet Accessories", 1047,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Prepaid Scratch Cards", 2550,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Refurbished Mobiles", 2520,0));
    }
    public  void populatemobileAccesories()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile Cover & Cases",2179,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Charger Cables & Docks", 2213,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power Banks",2353,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Screen Protectors", 2310,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Other Accessories", 3016,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Phone Stands", 2661,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Monopod & Selfie Sticks", 2687,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Battery Packs", 2214,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("VR Glasses", 3031,0));
    }

    public  void populatecomputerandlaptop()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptops", 1032,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Peripherals",3209,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer Components",2300,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Storage",2346,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Networking",2301,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer Accessories",2336,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Accessories",2461,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Printers", 2297,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Printers & Accessories",2278,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer Monitors", 1599,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Desktop Computers", 2014,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Software", 3483,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Development Tools", 2686,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Attendance Machines", 2999,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Laptops", 3070,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Bitcoin", 2721,0));
    }
    public  void populateperipherals()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pc Gaming Accessories", 2666,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headsets ", 2486,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mouse", 1763,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Keyboard", 1762,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mouse Pads", 2664,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Webcams & Chatpacks", 2033,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Multimedia Speaker", 1853,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Presenters", 3023,0));

    }

    public  void populatecomputercomponents()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Graphic Cards", 2162,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cooling Solutions", 2183,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("RAM/Memory", 1779,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Casing", 2181,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power Supply", 2182,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Motherboard", 2169,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Accessories", 2324,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Casing Fans", 2348,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("PC Cooling", 2480,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Combo Deals", 2424,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Sound Cards", 2283,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Processor", 2177,0));


    }

    public  void populatestorage()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("SSD", 2031,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("USB Flash drives", 1809,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("External Drives", 1815,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("HDD Adapters & Docks", 2477,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hard Drives", 1781,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Network Storage Devices", 2252,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("RAID Products", 2419,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Optical Drive", 2354,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Hard Drives", 2339,0));

    }
    public  void populatenetworking()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Switches", 2363,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Router", 2810,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Range Extenders & Adapters", 2732,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Networking Essentials", 2549,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Access Points", 2754,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power over Ethernet (PoE)", 2779,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Antennas", 2778,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wireless Adapters", 3207,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Modems", 2731,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Powerline Adapters", 3208,0));

    }

    public  void populatecomputeraccessories()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cables", 2673,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("USB Hubs", 2457,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mac Accessories", 1254,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Docks", 2442,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("TV Tuner", 2180,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("USB Port Cards", 2432,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Chairs", 3067,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Bundles", 2436,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Paper & CD Shredders", 3014,0));

    }



    public  void populatelaptopaccessories() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Bags", 2337,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Chargers", 2340,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Other accessories for Laptops", 3082,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Cooling Pads & Tablets", 2338,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Batteries", 2341,0));
    }
    public  void populateprintersandaccessories() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Printer Toners & Cartridges", 2296,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Other Printer Accessories", 2998,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("3D Printing", 2430,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Price Tag Guns & Cash Drawers", 2997,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Barcode Labels & Printer Rolls", 2996,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Barcode Scanners Accessories", 2995,0));
    }






    public  void populatecamera()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Digital Cameras", 637,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DSLR Cameras", 747,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DSLM Cameras", 3004,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Accessories", 1511,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Surveillance Cameras", 2269,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Handy Cams & Camcorders", 1315,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Drones", 2351,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Gimbal Stabilizer", 3140,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Instant Cameras", 3325,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cameras Deals", 2227,0));


    }



    public  void populatecamerasandaccessories()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Lighting & Studio",2243,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Filters", 2274,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Flash & Triggers", 2250,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Memory Cards", 2225,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("GoPro Accessories", 2239,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tripods", 2234,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Video Tripods", 2236,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Bags & Cases", 2242,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Rigs & Support",2244,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tripod Heads",2238,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Sliders", 2418,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Straps", 2249,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio Equipment", 2251,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Lens Skin", 2802,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Battery Grips", 2273,0));

    }





    public  void populatesurveillancecameras() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Security Cameras", 2302,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Security Camera Accessories", 3012,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DVR", 2303,0));
    }

    public  void populatetvandvideo() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Televisions", 112,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Home Audio & Video", 1251,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("TV Accessories", 1241,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Projectors", 152,0));
    }
    public  void populatehomeaudioandvideo() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Speakers", 978,0));


        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Home Theatre", 1097,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Digital Media Player", 660,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Amplifiers & AV Receivers", 1480,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio & Video Cables", 2680,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Video Glasses", 151,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hi-Fi System", 1250,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DVD & Blu-Ray Players", 2478,0));
    }
    public  void populatetvaccesories() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio & Video Cables", 2680,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wi-Fi Dongle", 2501,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wireless Keyboard", 2502,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("3D Glasses", 2258,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Remote Controls", 2256,0));
    }
    public  void populatehomeappliances() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Small Appliances", 2503,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Large Appliances", 2504,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Personal Care", 2325,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hardware & Tools",  2827,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clinical Products",  3150,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Telephones Video Phones Door Phones",  2559,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Lighting,Sockets & Switches",  2874,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Indoor & Outdoor Living Products",  2799,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Smart Home Gadgets",  3033,0));
    }
    public  void populatesmallappliances() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Appliances",  848,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Utensils",  3142,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Induction Stoves",  2529,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Vacuum Cleaner",  1279,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Heater",  2475,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Iron",  1306,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Insect Killer",  1267,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Garment Steamer",  2154,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("UPS Digital/Manual",  2507,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Stabilizer",  2568,0));
    }
    public  void populatekitchenappliances()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Food Steamer", 3248,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kneading Machines", 3252,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Rice Cooker", 3250,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Coffee Maker/Grinder", 1945,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Blender Grinder", 1302,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Juicer Blender", 1943,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electric Kettle", 1961,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Sandwich Maker", 1960,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Meat Mincer Multi Chopper", 1883,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Grinder/Chopper", 1948,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Toaster", 1958,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Egg Beater/Mixer", 1946,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hand Blender", 1348,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Food Processor", 1956,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Accessories", 2489,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fruit Cutter/Vegetable Cutter", 2459,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Juicer/Extractor", 1944,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fryers", 1985,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pop Corn Maker", 2471,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Ice Cream Machine", 2465,0));

    }
    public  void populatelargeappliances() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Refrigerator",  849,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Freezers",  3243,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Air Conditioners",  846,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Washing Machine",  850,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Dryers & Spinners",  3088,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Water Dispenser",  2104,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Humidifiers",  3148,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Air Purifiers",  3149,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Ovens",  3244,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Hood",  3068,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gas Hobs",  3069,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Dish Washer",  2473,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Room Air Cooler",  2718,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fan",  2494,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Generator ",  2506,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electric Geysers & Gas Geysers",  2801,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Barbecue Grill",  2456,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pressure Washer",  2811,0));
    }
    public  void populateovens() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Built-In Microwave Oven",  3246,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Built-In Oven",  3245,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Microwave Oven",  1954,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Oven Toaster",  1957,0));
    }
    public  void populatepersonalcare() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Massager",  3166,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Body Weight Scale",  2487,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Shaver/Hair Straightener/Haier Dryer/Grooming Kit",  2035,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Hand Wash Dispenser",  2491,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Medical Product & Foot Massager",  2490,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Toothbrush",  2662,0));
    }
    public  void populatehardwaretools() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clamp Multi-Meter",  2851,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Thimble Pliers",  2850,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tool Kits",  2856,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Measuring Meters",  2877,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tool Accessories",  2873,0));
    }
    public  void populatemusicalinstruments() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitars",  2288,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitars & Accessories",  2763,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Studio Equipment",  2775,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Keyboards & Pianos",  2716,1));
    }
    public  void populateguitarsandaccessories() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitar’s Pedals & Processors",  2926,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitar’s Amplifiers",  2899,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitar Bags",  2916,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitar Amplifications",  2924,0));
    }
    public  void populatestudioequipments()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio Interfaces", 3096,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DJ Instruments", 2800,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Microphones", 2753,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wireless Mic System ", 2698,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Microphone Accessories", 2700,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power Amplifier", 2695,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Professional Speakers", 2690,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mixers", 2693,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Effect & Signal Processors", 3098,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Portable PA Systems", 2696,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Studio Monitors", 2702,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headphones", 2517,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headphones Amplifier", 2704,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cable", 2708,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Stands", 2706,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Rack Cases", 2707,0));
    }
    public  void populatekeyboardandpiano() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Keyboards",  2927,0));
    }
    public  void populatebooksandstationery() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Books",  2870,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Stationery",  3765,1));
    }
    public  void populatebooks() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Children",  3383,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Autobiographies",  3129,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fiction",  3099,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Business & Money",  3137,0));
    }
    public  void populatestationery() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Office Supplies",  3964,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("School Supplies",  3963,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Paper & Paper Products",  3965,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Business & Money",  3137,0));
    }
    public  void populategaming() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S",  2947,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("PlayStation 4",  2630,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox 360",  2633,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Play Station VR",  2991,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("PlayStation 4 Pro",  2950,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox Project Scorpio",  3010,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Nintendo Switch",  3006,0));
    }
    public  void populatexboxones() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S Console",  2947,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S Games",  2948,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S Accessories",  2949,0));
    }
    public  void populateplaystation4() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Playstation 4 Console", 2630,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Playstation 4 Accessories", 2631,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Playstation 4 Games", 2632,0));
    }
    public  void populatexbox360() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One Consoles", 2629,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One Accessories", 2628,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One Games", 2627,0));
    }
    public  void populatebabyandtoys() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Toys", 1028,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mother & Baby", 3032,1));
    }
    public  void populatemotherandbaby() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Baby Gear", 3742,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Feeding", 3620,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Diapering & Potty", 3622,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Personal Care", 2325,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Diapering & Potty", 3622,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pacifiers & Accessories", 3658,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Baby Furniture", 3655,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clothing & Accessories", 3740,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mattresses & Bedding", 3657,0));
    }

    public  void populatefashion() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Women", 2409,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Men", 2408,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kids", 2656,1));
    }
    public  void populatemen() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clothing", 2576,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Watches", 678,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Shoes and Slippers", 2355,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Perfumes", 2414,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Ties", 2747,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Jackets/Hoodies/Coats", 2196,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Sunglasses", 2416,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cuff Links", 2735,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Socks", 2493,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wallets & Card Holders", 2681,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Belts", 2426,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Jewls", 2876,0));

    }
    public  void populateclothing() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("T-shirts", 3733,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kurta", 2362,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kameez Shalwar", 2906,0));
    }
    public  void populatekids() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Boys", 2657,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Girls", 2658,0));
    }










//    public  void populatestage1()
//     {
//         menuItemList.clear();
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile & Tablets",1031,1));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer & Laptops",1599,1));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cameras & Accessories",1031,1));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("TV & Video",1031,1));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Home Appliances",1031,1));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Musical Instruments",1031,1));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Books & Stationery",1031,1));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming",1031,1));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fashion",1031,1));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Baby & Toys",1031,1));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Digital Store",1031,0));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Daily Deal",1031,0));
//         menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clearance Sale",1031,0));
//     }
//    public  void populatemobiletablet()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile Phones",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tablets & eBook Readers",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile Accessories",1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headsets & Earphones",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Smart Watches",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tablet Accessories",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Prepaid Scratch Cards",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Refurbished Mobiles",1031,0));
//    }
//    public  void populatemobileAccesories()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile Cover & Cases",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Charger Cables & Docks",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power Banks",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Screen Protectors",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Other Accessories",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Phone Stands",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Monopod & Selfie Sticks",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Battery Packs",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("VR Glasses",1031,0));
//    }

//    public  void populatecomputerandlaptop()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptops",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Peripherals",1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer Components",1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Storage",1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Networking",1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer Accessories",1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Accessories",1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Printers",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Printers & Accessories",1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer Monitors",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Desktop Computers",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Software",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Development Tools",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Attendance Machines",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Laptops",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Bitcoin",1031,0));
//    }
//    public  void populateperipherals()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pc Gaming Accessories",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headsets",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mouse",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Keyboard",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mouse Pads",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Webcams & Chatpacks",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Multimedia Speaker",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Presenters",1031,0));
//
//    }
//
//    public  void populatecomputercomponents()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Graphic Cards",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cooling Solutions",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("RAM/Memory",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Casing",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power Supply",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Motherboard",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Accessories",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Casing Fans",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("PC Cooling",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Combo Deals",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Sound Cards",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Processor",1031,0));
//
//
//    }
//
//    public  void populatestorage()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("SSD",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("USB Flash drives",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("External Drives",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("HDD Adapters & Docks",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hard Drives",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Network Storage Devices",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("RAID Products",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Optical Drive",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Hard Drives",1031,0));
//
//    }
//    public  void populatenetworking()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Switches",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Router",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Range Extenders & Adapters",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Networking Essentials",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Access Points",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power over Ethernet (PoE)",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Antennas",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wireless Adapters",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Modems",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Powerline Adapters",1031,0));
//
//    }
//
//    public  void populatecomputeraccessories()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cables",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("USB Hubs",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mac Accessories",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Docks",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("TV Tuner",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("USB Port Cards",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Chairs",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Bundles",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Paper & CD Shredders",1031,0));
//
//    }
//
//
//
//    public  void populatelaptopaccessories() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Bags", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Chargers", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Other accessories for Laptops", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Cooling Pads & Tablets", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Batteries", 1031,0));
//    }
//    public  void populateprintersandaccessories() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Printer Toners & Cartridges", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Other Printer Accessories", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("3D Printing", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Price Tag Guns & Cash Drawers", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Barcode Labels & Printer Rolls", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Barcode Scanners Accessories", 1031,0));
//    }
//
//
//
//
//
//
//    public  void populatecamera()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Digital Cameras",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DSLR Cameras",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DSLM Cameras",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Accessories",1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Surveillance Cameras",1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Handy Cams & Camcorders",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Drones",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Gimbal Stabilizer",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Instant Cameras",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cameras Deals",1031,0));
//
//
//    }
//
//
//
//    public  void populatecamerasandaccessories()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Lighting & Studio",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Filters",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Flash & Triggers",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Memory Cards",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("GoPro Accessories",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tripods",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Video Tripods",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Bags & Cases",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Rigs & Support",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tripod Heads",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Sliders",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Straps",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio Equipment",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Lens Skin",1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Battery Grips",1031,0));
//
//    }
//
//
//
//
//
//    public  void populatesurveillancecameras() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Security Cameras", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Security Camera Accessories", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DVR", 1031,0));
//    }
//
//    public  void populatetvandvideo() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Televisions", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Home Audio & Video", 1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("TV Accessories", 1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Projectors", 1031,0));
//    }
//    public  void populatehomeaudioandvideo() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Speakers", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Home Theatre", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Digital Media Player", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Amplifiers & AV Receivers", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio & Video Cables", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Video Glasses", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hi-Fi System", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DVD & Blu Ray Players", 1031,0));
//    }
    public void menuNameandID()
    {
        menuidnames.clear();
        menuidnames.add(new MenuCategoryIDandNames("Mobile Phones",1031));
        menuidnames.add(new MenuCategoryIDandNames("Headsets & Earphones",2486));
        menuidnames.add(new MenuCategoryIDandNames("Refurbished Mobiles",2520));
        menuidnames.add(new MenuCategoryIDandNames("Tablets Phablets & eBook Readers",578));
        menuidnames.add(new MenuCategoryIDandNames("Smart Watches",2212));
        menuidnames.add(new MenuCategoryIDandNames("Prepaid Scratch Cards",2550));
        menuidnames.add(new MenuCategoryIDandNames("Mobile Cover & Cases",1031));
        menuidnames.add(new MenuCategoryIDandNames("Charger Cables & Docks",1031));
        menuidnames.add(new MenuCategoryIDandNames("Power Banks",1031));
        menuidnames.add(new MenuCategoryIDandNames("Screen Protectors",1031));
        menuidnames.add(new MenuCategoryIDandNames("Other Accessories",1031));
        menuidnames.add(new MenuCategoryIDandNames("Phone Stands",1031));
        menuidnames.add(new MenuCategoryIDandNames("Monopod & Selfie Sticks",1031));
        menuidnames.add(new MenuCategoryIDandNames("Battery Packs",1031));
        menuidnames.add(new MenuCategoryIDandNames("VR Glasses",1031));
        menuidnames.add(new MenuCategoryIDandNames("Pc Gaming Accessories",1031));
        menuidnames.add(new MenuCategoryIDandNames("Headsets",1031));
        menuidnames.add(new MenuCategoryIDandNames("Mouse",1031));
        menuidnames.add(new MenuCategoryIDandNames("Keyboard",1031));
        menuidnames.add(new MenuCategoryIDandNames("Mouse Pads",1031));
        menuidnames.add(new MenuCategoryIDandNames("Webcams & Chatpacks",1031));
        menuidnames.add(new MenuCategoryIDandNames("Multimedia Speaker",1031));
        menuidnames.add(new MenuCategoryIDandNames("Presenters",1031));
        menuidnames.add(new MenuCategoryIDandNames("Graphic Cards",1031));
        menuidnames.add(new MenuCategoryIDandNames("Cooling Solutions",1031));
        menuidnames.add(new MenuCategoryIDandNames("RAM/Memory",1031));
        menuidnames.add(new MenuCategoryIDandNames("Casing",1031));
        menuidnames.add(new MenuCategoryIDandNames("Power Supply",1031));
        menuidnames.add(new MenuCategoryIDandNames("Motherboard",1031));
        menuidnames.add(new MenuCategoryIDandNames("Gaming Accessories",1031));
        menuidnames.add(new MenuCategoryIDandNames("Casing Fans",1031));
        menuidnames.add(new MenuCategoryIDandNames("PC Cooling",1031));
        menuidnames.add(new MenuCategoryIDandNames("Combo Deals",1031));
        menuidnames.add(new MenuCategoryIDandNames("Sound Cards",1031));
        menuidnames.add(new MenuCategoryIDandNames("Processor",1031));
        menuidnames.add(new MenuCategoryIDandNames("SSD",1031));
        menuidnames.add(new MenuCategoryIDandNames("USB Flash drives",1031));
        menuidnames.add(new MenuCategoryIDandNames("External Drives",1031));
        menuidnames.add(new MenuCategoryIDandNames("HDD Adapters & Docks",1031));
        menuidnames.add(new MenuCategoryIDandNames("Hard Drives",1031));
        menuidnames.add(new MenuCategoryIDandNames("Network Storage Devices",1031));
        menuidnames.add(new MenuCategoryIDandNames("RAID Products",1031));
        menuidnames.add(new MenuCategoryIDandNames("Optical Drive",1031));
        menuidnames.add(new MenuCategoryIDandNames("Laptop Hard Drives",1031));
        menuidnames.add(new MenuCategoryIDandNames("Switches",1031));
        menuidnames.add(new MenuCategoryIDandNames("Router",1031));
        menuidnames.add(new MenuCategoryIDandNames("Range Extenders & Adapters",1031));
        menuidnames.add(new MenuCategoryIDandNames("Networking Essentials",1031));
        menuidnames.add(new MenuCategoryIDandNames("Access Points",1031));
        menuidnames.add(new MenuCategoryIDandNames("Power over Ethernet (PoE)",1031));
        menuidnames.add(new MenuCategoryIDandNames("Wireless Adapters",1031));
        menuidnames.add(new MenuCategoryIDandNames("Modems",1031));
        menuidnames.add(new MenuCategoryIDandNames("Powerline Adapters",1031));
        menuidnames.add(new MenuCategoryIDandNames("Laptop Bags",1031));
        menuidnames.add(new MenuCategoryIDandNames("Laptop Chargers",1031));
        menuidnames.add(new MenuCategoryIDandNames("Other accessories for Laptops",1031));
        menuidnames.add(new MenuCategoryIDandNames("Laptop Cooling Pads & Tablets",1031));
        menuidnames.add(new MenuCategoryIDandNames("Laptop Batteries",1031));
        menuidnames.add(new MenuCategoryIDandNames("Printer Toners & Cartridges",1031));
        menuidnames.add(new MenuCategoryIDandNames("Other Printer Accessories",1031));
        menuidnames.add(new MenuCategoryIDandNames("3D Printing",1031));
        menuidnames.add(new MenuCategoryIDandNames("Price Tag Guns & Cash Drawers",1031));
        menuidnames.add(new MenuCategoryIDandNames("Barcode Labels & Printer Rolls",1031));
        menuidnames.add(new MenuCategoryIDandNames("Barcode Scanners Accessories",1031));
        menuidnames.add(new MenuCategoryIDandNames("Digital Cameras",1031));
        menuidnames.add(new MenuCategoryIDandNames("DSLR Cameras",1031));
        menuidnames.add(new MenuCategoryIDandNames("DSLM Cameras",1031));
        menuidnames.add(new MenuCategoryIDandNames("Camera Accessories",1031));
        menuidnames.add(new MenuCategoryIDandNames("Surveillance Cameras",1031));
        menuidnames.add(new MenuCategoryIDandNames("Handy Cams & Camcorders",1031));
        menuidnames.add(new MenuCategoryIDandNames("Camera Drones",1031));
        menuidnames.add(new MenuCategoryIDandNames("Camera Gimbal Stabilizer",1031));
        menuidnames.add(new MenuCategoryIDandNames("Instant Cameras",1031));
        menuidnames.add(new MenuCategoryIDandNames("Cameras Deals",1031));
        menuidnames.add(new MenuCategoryIDandNames("Lighting & Studio",1031));
        menuidnames.add(new MenuCategoryIDandNames("Camera Filters",1031));
        menuidnames.add(new MenuCategoryIDandNames("Flash & Triggers",1031));
        menuidnames.add(new MenuCategoryIDandNames("Memory Cards",1031));
        menuidnames.add(new MenuCategoryIDandNames("GoPro Accessories",1031));
        menuidnames.add(new MenuCategoryIDandNames("Tripods",1031));
        menuidnames.add(new MenuCategoryIDandNames("Video Tripods",1031));
        menuidnames.add(new MenuCategoryIDandNames("Bags & Cases",1031));
        menuidnames.add(new MenuCategoryIDandNames("Bags & Cases",1031));
        menuidnames.add(new MenuCategoryIDandNames("Camera Rigs & Support",1031));
        menuidnames.add(new MenuCategoryIDandNames("Tripod Heads",1031));
        menuidnames.add(new MenuCategoryIDandNames("Camera Sliders",1031));
        menuidnames.add(new MenuCategoryIDandNames("Camera Straps",1031));
        menuidnames.add(new MenuCategoryIDandNames("Audio Equipment",1031));
        menuidnames.add(new MenuCategoryIDandNames("Lens Skin",1031));
        menuidnames.add(new MenuCategoryIDandNames("Camera Battery Grips",1031));
        menuidnames.add(new MenuCategoryIDandNames("Security Cameras",1031));
        menuidnames.add(new MenuCategoryIDandNames("Security Camera Accessories",1031));
        menuidnames.add(new MenuCategoryIDandNames("DVR",1031));
        menuidnames.add(new MenuCategoryIDandNames("Televisions",1031));
        menuidnames.add(new MenuCategoryIDandNames("Home Audio & Video",1031));
        menuidnames.add(new MenuCategoryIDandNames("TV Accessories",1031));
        menuidnames.add(new MenuCategoryIDandNames("Projectors",1031));
        menuidnames.add(new MenuCategoryIDandNames("Speakers",1031));
        menuidnames.add(new MenuCategoryIDandNames("Home Theatre",1031));
        menuidnames.add(new MenuCategoryIDandNames("Digital Media Player",1031));
        menuidnames.add(new MenuCategoryIDandNames("Amplifiers & AV Receivers",1031));
        menuidnames.add(new MenuCategoryIDandNames("Audio & Video Cables",1031));
        menuidnames.add(new MenuCategoryIDandNames("Video Glasses",1031));
        menuidnames.add(new MenuCategoryIDandNames("Hi-Fi System",1031));
        menuidnames.add(new MenuCategoryIDandNames("DVD & Blu Ray Players",1031));
        menuidnames.add(new MenuCategoryIDandNames("Audio & Video Cables",1031));
        menuidnames.add(new MenuCategoryIDandNames("WiFi Dongle",1031));
        menuidnames.add(new MenuCategoryIDandNames("Wireless Keyboard",1031));
        menuidnames.add(new MenuCategoryIDandNames("3D Glasses",1031));
        menuidnames.add(new MenuCategoryIDandNames("Remote Controls",1031));
        menuidnames.add(new MenuCategoryIDandNames("Small Appliances",1031));
        menuidnames.add(new MenuCategoryIDandNames("Large Appliances",1031));
        menuidnames.add(new MenuCategoryIDandNames("Personal Care",1031));
        menuidnames.add(new MenuCategoryIDandNames("Hardware & Tools",1031));
        menuidnames.add(new MenuCategoryIDandNames("Cilinical Product",1031));
        menuidnames.add(new MenuCategoryIDandNames("Telephones Video Phones Door Phones",1031));
        menuidnames.add(new MenuCategoryIDandNames("Lighting,Sockets & Switches",1031));
        menuidnames.add(new MenuCategoryIDandNames("Indoor & Outdoor Living Products",1031));
        menuidnames.add(new MenuCategoryIDandNames("Smart Home Gadgets",1031));
        menuidnames.add(new MenuCategoryIDandNames("Speakers",1031));
        menuidnames.add(new MenuCategoryIDandNames("Speakers",1031));


    }
//    public  void populatehomeappliances() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Small Appliances", 1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Large Appliances", 1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Personal Care", 1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hardware & Tools",  1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cilinical Products",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Telephones Video Phones Door Phones",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Lighting,Sockets & Switches",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Indoor & Outdoor Living Products",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Smart Home Gadgets",  1031,0));
//    }
//    public  void populatesmallappliances() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Appliances",  1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Utensils",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Induction Stoves",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Vaccum Cleaner",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Heater",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Iron",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Insect Killer",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Garment Steamer",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("UPS Digital/Manual",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Stabilizer",  1031,0));
//    }
//    public  void populatetvaccesories() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio & Video Cables", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("WiFi Dongle", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wireless Keyboard", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("3D Glasses", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Remote Controls", 1031,0));
//    }
//    public  void populatekitchenappliances()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Food Steamer", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kneading Machines", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Rice Cooker", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Coffee Maker/Grinder", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Blender Grinder", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Juicer Blender", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electric Kettle", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Sandwich Maker", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Meat Mincer Multi Chopper", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Grinder/Chopper", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Toaster", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Egg Beater/Mixer", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hand Blender", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Food Processor", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Accessories", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fruit Cutter/Vegetable Cutter", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Juicer/Extractor", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fryers", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pop Corn Maker", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Ice Cream Machine", 1031,0));
//
//    }
//    public  void populatelargeappliances() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Refrigerator",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Freezers",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Air Conditioners",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Washing Machine",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Dryers & Spinners",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Water Dispenser",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Humidifiers",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Air Purifiers",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Ovens",  1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Hood",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gas Hobs",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Dish Washer",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Room Air Cooler",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fan",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Generator ",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electric Geysers & Gas Geysers",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Barbecue Grill",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pressure Washer",  1031,0));
//    }
//    public  void populateovens() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Built-In Microwave Oven",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Built-In Oven",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Microwave Oven",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Oven Toaster",  1031,0));
//    }
//    public  void populatepersonalcare() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Massager",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Body Weight Scale",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Shaver/Hair Straingner/Haier Dryer/Grooming Kit",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Hand Wash Dispenser",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Medical Product & Foot Massager",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Toothbrush",  1031,0));
//    }
//    public  void populatehardwaretools() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clamp Multi-Meter",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Thimble Pliers",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tool Kits",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Measuring Meters",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tool Accessories",  1031,0));
//    }
//    public  void populatemusicalinstruments() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitars",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitars & Accessories",  1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Studio Equipments",  1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Keyboards & Pianos",  1031,1));
//    }
//    public  void populateguitarsandaccessories() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitars Pedals & Processors",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitars Amplifier's",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitars Bags",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitars Amplifications",  1031,0));
//    }
//    public  void populatestudioequipments()
//    {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio Interfaces", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DJ Instruments", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Microphones", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wireless Mic System ", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Microphone Accessories", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power Amplifier", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Professional Speakers", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mixers", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Effect & Signal Processors", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Portable PA Systems", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Studio Monitors", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headphones", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headphones Amplifier", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cable", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Stands", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Rack Cases", 1031,0));
//    }
//    public  void populatekeyboardandpiano() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Keyboards",  1031,0));
//    }
//    public  void populatebooksandstationery() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("books",  1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("sationery",  1031,1));
//    }
//    public  void populatebooks() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Childeren",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Autobiographies",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fiction",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Business & Money",  1031,0));
//    }
//    public  void populatestationery() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Office Supplies",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("School Supplies",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Paper & Paper Products",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Business & Money",  1031,0));
//    }
//    public  void populategaming() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("xbox one s",  1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("PlayStation 4",  1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox 360",  1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Play Station VR",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("PlayStation 4 Pro",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox Project Scorpio",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Nintendo Switch",  1031,0));
//    }
//    public  void populatexboxones() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S Console",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S Games",  1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S Accessories",  1031,0));
//    }
//    public  void populateplaystation4() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Playstation 4 Console", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Playstation 4 Accessories", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Playstation 4 Games", 1031,0));
//    }
//    public  void populatexbox360() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One Consoles", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One Accessories", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One Games", 1031,0));
//    }
//    public  void populatebabyandtoys() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Toys", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mother & Baby", 1031,1));
//    }
//    public  void populatemotherandbaby() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Baby Gear", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Feeding", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Diapering & Potty", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Personal Care", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Diapering & Potty", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pacifiers & Accessories", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Baby Furniture", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clothing & Accessories", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mattresses & Bedding", 1031,0));
//    }
//
//    public  void populatefashion() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Women", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Men", 1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kids", 1031,1));
//    }
//    public  void populatemen() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clothing", 1031,1));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Watches", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Shoes and Slippers", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Perfumes", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Ties", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Jackets/Hoodies/Coats", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Sunglasses", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cuff Links", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Socks", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wallets & Card Holders", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Belts", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Jewls", 1031,0));
//
//    }
//    public  void populateclothing() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tshits", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kurta", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kameez Shalwar", 1031,0));
//    }
//    public  void populatekids() {
//        menuItemList.clear();
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Boys", 1031,0));
//        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Girls", 1031,0));
//    }
    public static void justifyListViewHeightBasedOnChildren (ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        Log.d("adapter count",""+adapter.getCount());
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
            Log.d("heigt in loop",""+totalHeight);
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) constraintLayout84.getLayoutParams();
////        lp.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//lp.horizontalBias = Float.parseFloat(Integer.toString(totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1))));
//        constraintLayout84.setLayoutParams(lp);
//        constraintLayout84.requestLayout();
        listView.setLayoutParams(par);
        listView.requestLayout();
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(constraintLayout90);
        Log.d("heigt",""+par.height);
        constraintSet.constrainHeight(R.id.constraintLayout90,par.height);
//        constraintSet.setVerticalBias(R.id.constraintLayout84,7000.0f);
        constraintSet.applyTo(constraintLayout90);
    }

}

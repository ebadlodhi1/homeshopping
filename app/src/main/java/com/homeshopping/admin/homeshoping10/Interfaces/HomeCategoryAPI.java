package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.HomeCategoryProducts.HomeExample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface HomeCategoryAPI {
    @GET("home/getHomeProducts")
    Call<HomeExample> getDetails(@Header("data") String data,@Query("userId") String userId);

}

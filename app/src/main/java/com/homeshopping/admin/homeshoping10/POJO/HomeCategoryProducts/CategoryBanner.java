package com.homeshopping.admin.homeshoping10.POJO.HomeCategoryProducts;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryBanner {
    @SerializedName("imagePath")
    @Expose
    private String imagePath;
    @SerializedName("pageId")
    @Expose
    private Integer pageId;
    @SerializedName("pageType")
    @Expose
    private String pageType;

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

}

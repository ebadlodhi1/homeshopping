package com.homeshopping.admin.homeshoping10.POJO.CategoryProductPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {
    @SerializedName("ProductID")
    @Expose
    private Integer productID;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("ProductUrl")
    @Expose
    private String productUrl;
    @SerializedName("ProductPrice")
    @Expose
    private String productPrice;
    @SerializedName("ProductSalePrice")
    @Expose
    private String productSalePrice;
    @SerializedName("ProductBankPrice")
    @Expose
    private String productBankPrice;
    @SerializedName("BrandDisplayName")
    @Expose
    private String brandDisplayName;
    @SerializedName("productImages")
    @Expose
    private ProductImages productImages;
    @SerializedName("productRating")
    @Expose
    private Integer productRating;
    @SerializedName("productWishlist")
    @Expose
    private Integer productWishlist;
    @SerializedName("isExistInCart")
    @Expose
    private Integer isExistInCart;

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductSalePrice() {
        return productSalePrice;
    }

    public void setProductSalePrice(String productSalePrice) {
        this.productSalePrice = productSalePrice;
    }

    public String getProductBankPrice() {
        return productBankPrice;
    }

    public void setProductBankPrice(String productBankPrice) {
        this.productBankPrice = productBankPrice;
    }

    public String getBrandDisplayName() {
        return brandDisplayName;
    }

    public void setBrandDisplayName(String brandDisplayName) {
        this.brandDisplayName = brandDisplayName;
    }

    public ProductImages getProductImages() {
        return productImages;
    }

    public void setProductImages(ProductImages productImages) {
        this.productImages = productImages;
    }

    public Integer getProductRating() {
        return productRating;
    }

    public void setProductRating(Integer productRating) {
        this.productRating = productRating;
    }

    public Integer getProductWishlist() {
        return productWishlist;
    }

    public void setProductWishlist(Integer productWishlist) {
        this.productWishlist = productWishlist;
    }

    public Integer getIsExistInCart() {
        return isExistInCart;
    }

    public void setIsExistInCart(Integer isExistInCart) {
        this.isExistInCart = isExistInCart;
    }
}

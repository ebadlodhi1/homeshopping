package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ForgetpasswordAPI {
    @PUT("/user/forgetPassword")
    Call<generalstatusmessagePOJO> getDetails(@Header("data") String data, @Query("customerEmail") String customerEmail);

}

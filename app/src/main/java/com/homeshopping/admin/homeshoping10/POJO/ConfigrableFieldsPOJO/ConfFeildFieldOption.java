package com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfFeildFieldOption {

    @SerializedName("fieldOptions")
    @Expose
    private String fieldOptions;

    public String getFieldOptions() {
        return fieldOptions;
    }

    public void setFieldOptions(String fieldOptions) {
        this.fieldOptions = fieldOptions;
    }
}

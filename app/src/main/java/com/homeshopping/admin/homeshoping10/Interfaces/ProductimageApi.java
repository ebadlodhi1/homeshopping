package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.GetProductImages.ProductImagesExample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface ProductimageApi {

    @GET("products/getProductImages")
    Call<ProductImagesExample>   getDetails(@Header("data") String data, @Query("productId") int productId);
}

package com.homeshopping.admin.homeshoping10.POJO.shippingadddress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class shipaddress {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("shipAddress")
    @Expose
    private List<shipaddresslist> shipAddress = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<shipaddresslist> getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(List<shipaddresslist> shipAddress) {
        this.shipAddress = shipAddress;
    }

}

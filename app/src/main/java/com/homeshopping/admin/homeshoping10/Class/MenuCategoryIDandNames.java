package com.homeshopping.admin.homeshoping10.Class;

public class MenuCategoryIDandNames {
    private  String Categoryname = "";
    private int CategoryID = 0;

    public MenuCategoryIDandNames(String categoryname, int categoryID) {
        Categoryname = categoryname;
        CategoryID = categoryID;
    }

    public String getCategoryname() {
        return Categoryname;
    }

    public void setCategoryname(String categoryname) {
        Categoryname = categoryname;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }
}

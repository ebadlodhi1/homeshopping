package com.homeshopping.admin.homeshoping10.Interfaces;

public interface AddorRemoveCallbacks {
    public  void onAddProduct(int pid,String Qty,String userid,String combinationid,String productTitle,String Productprice ,String Productimage,String ProductBrand,String feildid,String feildOption,String bankId);
    public void onRemoveProduct();
    public void onAddProduct();
}

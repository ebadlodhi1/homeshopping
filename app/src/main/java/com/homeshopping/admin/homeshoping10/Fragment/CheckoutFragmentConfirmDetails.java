package com.homeshopping.admin.homeshoping10.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.homeshopping.admin.homeshoping10.Activity.CartActivity;
import com.homeshopping.admin.homeshoping10.Activity.CheckOutActivity;
import com.homeshopping.admin.homeshoping10.Activity.Dashboard;
import com.homeshopping.admin.homeshoping10.Activity.MainActivity;
import com.homeshopping.admin.homeshoping10.Activity.SuccessOrderActivity;
import com.homeshopping.admin.homeshoping10.Interfaces.PlaceOrderAPi;
import com.homeshopping.admin.homeshoping10.POJO.Cart.CartDetail;
import com.homeshopping.admin.homeshoping10.POJO.PlaceOrderPOJO.OrderId;
import com.homeshopping.admin.homeshoping10.POJO.PlaceOrderPOJO.PlaceOrderExample;
import com.homeshopping.admin.homeshoping10.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.context;

public class CheckoutFragmentConfirmDetails extends Fragment {

    ArrayList<String> cartId  = new ArrayList<>();
    ArrayList<String> productId  = new ArrayList<>();
    ArrayList<String> productImage  = new ArrayList<>();
    ArrayList<String> productTitle  = new ArrayList<>();
    ArrayList<String> productAvailability  = new ArrayList<>();
    ArrayList<String> quantity  = new ArrayList<>();
    ArrayList<String> productPrice  = new ArrayList<>();
    ArrayList<String> variationName  = new ArrayList<>();
    ArrayList<String> variationOptionName  = new ArrayList<>();
    ArrayList<String> variationPrice  = new ArrayList<>();
    ArrayList<String> variationFunction  = new ArrayList<>();
    ArrayList<String> BrandName  = new ArrayList<>();
    androidx.constraintlayout.widget.ConstraintLayout discountcouponconstraint;

   List<CartDetail> cart = new ArrayList<>();
   TextView summarytext;
    int totalamount = 0 , quant = 0;
 static android.support.constraint.ConstraintLayout  listsummaryitemconstraint;
 JSONObject mainobj = new JSONObject();
    JSONObject shipperDetail = new JSONObject();

    JSONObject shippmentDetail = new JSONObject();

    TextView shipaddfnamereciept,shipaddreciept,shipaddcityreciept,shipaddpnoreciept,choosepaymentmethod,recieptsubtotal,
         recieptcoupon,recieptshipmentcharges,reciepttotalamount,recieptgrandtotal;
 ListView listsummaryitem;
 Button PlaceOrder;
 ProgressBar progressBar8;
 int Subtotal = 0;
 int Shipmentratefinal = 0 ;
    public static List<OrderId> orderIds = new ArrayList<>();

    String ordeerid = "";
    public CheckoutFragmentConfirmDetails() {
    cart = CartActivity.cartforreciept;
        for(int i = 0 ;i<cart.size();i++)
        {
            cartId.add(cart.get(i).getCartId().toString());
            productId.add(cart.get(i).getProductId().toString());
            productImage.add(cart.get(i).getProductImage());
            productTitle.add(cart.get(i).getProductTitle());
            productAvailability.add(cart.get(i).getProductAvailability());
            quantity.add(cart.get(i).getQuantity().toString());
            productPrice.add(cart.get(i).getProductPrice());
            variationName.add(cart.get(i).getVariationName());
            variationOptionName.add(cart.get(i).getVariationOptionName());
            variationPrice.add(cart.get(i).getVariationPrice());
            variationFunction.add(cart.get(i).getVariationFunction());
            BrandName.add(cart.get(i).getBrandName());

            if(variationFunction.get(i).equals("add"))
            {
                //            Toast.makeText(context,""+Math.round(Float.parseFloat(productPrice.get(i))+ Float.parseFloat(variationPrice.get(i)))*cart.get(i).getQuantity(),Toast.LENGTH_LONG).show();

                float amountfloat = Math.round(Float.parseFloat(productPrice.get(i))+ Float.parseFloat(variationPrice.get(i)))*cart.get(i).getQuantity();
                totalamount = (int) (totalamount+amountfloat);
                quant = quant +Integer.parseInt(quantity.get(i));
            }
           else if(variationFunction.get(i).equals("subtract"))
            {
                float amountfloat = Math.round(Float.parseFloat(productPrice.get(i))- Float.parseFloat(variationPrice.get(i)))*cart.get(i).getQuantity();
                totalamount = (int) (totalamount+amountfloat);
                quant = quant +Integer.parseInt(quantity.get(i));
            }
            else {
                float amountfloat = Math.round(Float.parseFloat(productPrice.get(i))) * cart.get(i).getQuantity();

                totalamount = (int) (totalamount + amountfloat);
                quant = quant + Integer.parseInt(quantity.get(i));
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.checkoutrecipfragment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listsummaryitemconstraint = view.findViewById(R.id.listsummaryitemconstraint);
        TextView shipaddfnamereciept,shipaddreciept,shipaddcityreciept,shipaddpnoreciept,choosepaymentmethod,
                recieptsubtotal,
                recieptcoupon,recieptshipmentcharges,reciepttotalamount,recieptgrandtotal;
        ListView listsummaryitem;
        shipaddfnamereciept = view.findViewById(R.id.shipaddfnamereciept);
        shipaddreciept = view.findViewById(R.id.shipaddreciept);
        shipaddcityreciept = view.findViewById(R.id.shipaddcityreciept);
        shipaddpnoreciept = view.findViewById(R.id.shipaddpnoreciept);
        choosepaymentmethod = view.findViewById(R.id.choosepaymentmethod);
        recieptsubtotal = view.findViewById(R.id.recieptsubtotal);
        recieptcoupon = view.findViewById(R.id.recieptcoupon);
        recieptshipmentcharges = view.findViewById(R.id.recieptshipmentcharges);
        discountcouponconstraint  = view.findViewById(R.id.constraintLayout109);
//        reciepttotalamount = view.findViewById(R.id.reciepttotalamount);
        recieptgrandtotal = view.findViewById(R.id.recieptgrandtotal);
        listsummaryitem = view.findViewById(R.id.listsummaryitem);
        PlaceOrder = view.findViewById(R.id.button20);
        progressBar8 = view.findViewById(R.id.progressBar8);
        summarytext = view.findViewById(R.id.textView112);
        summarytext.setText("Summary ("+cart.size()+")");

        shipaddfnamereciept.setText(""+CheckOutActivity.finalfirstName );
        shipaddreciept.setText(""+CheckOutActivity.finaladdress);
        shipaddcityreciept.setText(""+CheckOutActivity.finalstate);
        shipaddpnoreciept.setText(""+CheckOutActivity.finalphone);
        if(CartActivity.finalvoucher.isEmpty())
        {
            recieptcoupon.setText("Coupon not applied");
            discountcouponconstraint.setVisibility(View.GONE);
            String prodshipmentprice= CheckOutActivity.Productshipmentcharges;
            double doubleShipmentrate = Double.parseDouble(prodshipmentprice);
            int Shipmentrate = (int) doubleShipmentrate;
            int total = totalamount+Integer.parseInt(CheckOutActivity.finalshipmentcharges)+Shipmentrate;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String pricee = formatter.format(Math.round(total));


//            reciepttotalamount.setText("Rs "+pricee);
            recieptgrandtotal.setText("Rs "+pricee);
        }
        else
        {
            discountcouponconstraint.setVisibility(View.VISIBLE);
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String pricee = formatter.format(CartActivity.finalvoucherprice);
            recieptcoupon.setText("Rs -"+pricee);
            String prodshipmentprice= CheckOutActivity.Productshipmentcharges;
            double doubleShipmentrate = Double.parseDouble(prodshipmentprice);
            int Shipmentrate = (int) doubleShipmentrate;
            int total = totalamount+Integer.parseInt(CheckOutActivity.finalshipmentcharges)-CartActivity.finalvoucherprice+Shipmentrate;

            String priceee = formatter.format(Math.round(total));

//            reciepttotalamount.setText("Rs "+pricee);
            recieptgrandtotal.setText("Rs "+priceee);

        }
        choosepaymentmethod.setText(""+CheckOutActivity.finalpaymentMethod);
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String pricee = formatter.format(Math.round(totalamount));
        recieptsubtotal.setText("Rs "+pricee);

        int priceee =Math.round(Integer.parseInt(CheckOutActivity.finalshipmentcharges));
        String prodshipmentprice= CheckOutActivity.Productshipmentcharges;
        double doubleShipmentrate = Double.parseDouble(prodshipmentprice);
        int Shipmentrate = (int) doubleShipmentrate;

        recieptshipmentcharges.setText("Rs "+(priceee+Shipmentrate));
        Shipmentratefinal  = priceee+Shipmentrate;
        getcartAdapter2 getcartAdapter2 = new getcartAdapter2(getActivity(),cart);

        listsummaryitem.setAdapter(getcartAdapter2);
        justifyListViewHeightBasedOnChildren(listsummaryitem);

        PlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    progressBar8.setVisibility(View.VISIBLE);
                    getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    shipperDetail.put("firstName",CheckOutActivity.finalfirstName);
                    shipperDetail.put("lastName",CheckOutActivity.finallastName);
                    shipperDetail.put("address",CheckOutActivity.finaladdress);
                    shipperDetail.put("country_id",CheckOutActivity.finalcountry_id);
                    shipperDetail.put("country",CheckOutActivity.finalcountry);
                    shipperDetail.put("state_id",CheckOutActivity.finalstate_id);
                    shipperDetail.put("state",CheckOutActivity.finalstate);
                    shipperDetail.put("phone",CheckOutActivity.finalphone);
                    shipperDetail.put("country_iso",CheckOutActivity.finalcountry_iso);
                    shipperDetail.put("customerId",CheckOutActivity.finalcustomerId);

                    shippmentDetail.put("voucher",CartActivity.finalvoucher);
                    shippmentDetail.put("paymentMethod",CheckOutActivity.finalpaymentMethod);
                    shippmentDetail.put("paymentModule",CheckOutActivity.finalpaymentModule);
                    shippmentDetail.put("remarks",CheckOutActivity.finalremarks);
                    shippmentDetail.put("paymentAmount",Shipmentratefinal);



                    mainobj.put("shipperDetail",shipperDetail);
                    mainobj.put("shippmentDetail",shippmentDetail);
                    Log.d("sending json",""+mainobj);

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    PlaceOrderAPi placeOrderAPi = retrofit.create(PlaceOrderAPi.class);
                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), mainobj.toString());
                    Call<PlaceOrderExample> placeOrderExampleCall = placeOrderAPi.getDetails(MainActivity.Tokendata,body);
                    placeOrderExampleCall.enqueue(new Callback<PlaceOrderExample>() {
                        @Override
                        public void onResponse(Call<PlaceOrderExample> call, Response<PlaceOrderExample> response) {
                            if (response.isSuccessful()) {
                                int status = response.body().getStatus();
                                if (status == 1) {
                                    progressBar8.setVisibility(View.GONE);

                                    orderIds = response.body().getOrderId();


//                                    Toast.makeText(getActivity(), "Congratulations Order has been placed\n" + response.body().getMessage().getMessage(), Toast.LENGTH_SHORT).show();
                                    String msg = "";
                                    try {
                                        msg = ""+response.body().getMessage().getMessage();
                                        Intent intent = new Intent(getActivity(), SuccessOrderActivity.class);
                                        intent.putExtra("ordermessage", msg );


                                        startActivity(intent);
                                        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                                        getActivity().finish();

                                    }
                                    catch (Exception e)
                                    {

                                    }
                                } else {
                                    progressBar8.setVisibility(View.GONE);
                                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    Toast.makeText(getActivity(), "Order not Placed", Toast.LENGTH_SHORT).show();
                                }
                            }
                            else
                            {
                                Toast.makeText(getActivity(), "Order not placed due to server not responding\nPlease try again later.", Toast.LENGTH_LONG).show();
                                progressBar8.setVisibility(View.GONE);
                                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            }
                        }


                        @Override
                        public void onFailure(Call<PlaceOrderExample> call, Throwable t) {
                            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            progressBar8.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Server not responding.", Toast.LENGTH_SHORT).show();

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }






    public class getcartAdapter2 extends ArrayAdapter<String> {



        public getcartAdapter2(Context context, List<CartDetail> list ) {
            super(context, R.layout.finalcheckoutcartcard, productId);




        }

        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder2 vh;
            LayoutInflater inflater=LayoutInflater.from(getActivity());
            View rowView=inflater.inflate(R.layout.finalcheckoutcartcard, null,true);

                vh = new ViewHolder2();
                LayoutInflater li = LayoutInflater.from(getActivity());
            rowView = li.inflate(R.layout.finalcheckoutcartcard,null);
//            Example app = items.get(position);
//            Log.d("position",Integer.toString(position));
//            if(app != null) {

                vh.wlbrand = (TextView) rowView.findViewById(R.id.wlbrand);
                vh. wlproductname = (TextView) rowView.findViewById(R.id.wlproductname);
                vh. wlprice = (TextView) rowView.findViewById(R.id.wlprice);
//                vh. qty = (TextView) rowView.findViewById(R.id.qty);
                vh.wlimage = rowView.findViewById(R.id.wlimage);
                vh.pvariation = rowView.findViewById(R.id.pvariation);




            rowView.setTag(vh);




// give a timezone reference for formatting (see comment at the bottom)


//            vh.wlbrand.setText(brandName.get(position));
//            ArrayAdapter aa = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,qty);
////            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
////            //Setting the ArrayAdapter data on the Spinner
////            vh.spinnercartqty.setAdapter(aa);
            if(Dashboard.prefConfig.readLoginStatus())
            {
                vh.wlbrand.setText(BrandName.get(position));

                if(productTitle.get(position).length()>=37) {
                    String stringlimit = productTitle.get(position).substring(0, 37);
//                    Toast.makeText(getContext(),position+" feild names "+cart.get(position).getFieldName(),Toast.LENGTH_LONG).show();
                    Log.d("feild names",""+ position+" "+cart.get(position).getFieldName());
                    vh.wlproductname.setText(stringlimit + "...\n"+""+cart.get(position).getVariationName()+"\n"+cart.get(position).getVariationOptionName()+"\n"+cart.get(position).getFieldName()+" "+cart.get(position).getFieldOption());
                }
                else
                {
//                    Toast.makeText(getContext(),position+" feild names "+cart.get(position).getFieldName(),Toast.LENGTH_LONG).show();
                    Log.d("feild names",""+ position+" "+cart.get(position).getFieldName());
                    vh.wlproductname.setText(productTitle.get(position)+ "..\n"+""+cart.get(position).getVariationName()+"\n"+cart.get(position).getVariationOptionName()+"\n"+cart.get(position).getFieldName()+" "+cart.get(position).getFieldOption());

                }


//                vh.wlproductname.setText(productTitle.get(position)+"\n"+""+cart.get(position).getVariationName()+"\n"+cart.get(position).getVariationOptionName());
                DecimalFormat formatter = new DecimalFormat("#,###,###");
                String pricee = formatter.format(Math.round(Float.parseFloat(productPrice.get(position)))+Math.round(Float.parseFloat(variationPrice.get(position))));
                vh.wlprice.setText("Rs "+pricee+"  Qty "+quantity.get(position));
//                vh.qty.setText(quantity.get(position));

//            DecimalFormat formatter = new DecimalFormat("#,###,###");
//            formatedproductprice = formatter.format(Math.round(Float.parseFloat(itemprodprice[position])));
//            vh.itemamount.setText("Rs "+formatedproductprice);
                Glide.with(getActivity())
                        .load(productImage.get(position))// image url
                        // any placeholder to load at start
                        .into( vh.wlimage);

//            vh.pvariation.setText(""+cart.get(position).getVariationName()+"\n"+cart.get(position).getVariationOptionName());
//
            }


            return rowView;

        };
    }
    static class ViewHolder2
    {
        // ImageView imageView;
        TextView wlbrand,wlproductname,wlprice,qty,pvariation,pavailability;
        ImageView wlimage;
        androidx.constraintlayout.widget.ConstraintLayout cartmovetowl,cartremove;
        androidx.constraintlayout.widget.ConstraintLayout wishlistlayout,cartlayout;
        Spinner spinnercartqty;




    }
    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                float px = 320 * (listView.getResources().getDisplayMetrics().density);
                item.measure(View.MeasureSpec.makeMeasureSpec((int) px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);
            // Get padding
            int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight + totalPadding;
            listView.setLayoutParams(params);
            listView.requestLayout();
            //setDynamicHeight(listView);
            return true;

        } else {
            return false;
        }

    }
    public static void justifyListViewHeightBasedOnChildren (ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        Log.d("adapter count",""+adapter.getCount());
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
            Log.d("heigt in loop",""+totalHeight);
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) constraintLayout84.getLayoutParams();
////        lp.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//lp.horizontalBias = Float.parseFloat(Integer.toString(totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1))));
//        constraintLayout84.setLayoutParams(lp);
//        constraintLayout84.requestLayout();
        listView.setLayoutParams(par);
        listView.requestLayout();
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(listsummaryitemconstraint);
        Log.d("heigt",""+par.height);
        constraintSet.constrainHeight(R.id.listsummaryitemconstraint,par.height);
//        constraintSet.setVerticalBias(R.id.constraintLayout84,7000.0f);
        constraintSet.applyTo(listsummaryitemconstraint);
    }
}

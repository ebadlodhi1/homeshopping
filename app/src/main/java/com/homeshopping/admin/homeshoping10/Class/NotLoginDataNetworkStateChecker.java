package com.homeshopping.admin.homeshoping10.Class;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.homeshopping.admin.homeshoping10.Activity.Dashboard;
import com.homeshopping.admin.homeshoping10.Activity.MainActivity;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Interfaces.AddcartAPI;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class NotLoginDataNetworkStateChecker extends BroadcastReceiver {
    private Context context;
    private DatabaseHelper db;
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        db = new DatabaseHelper(context);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
//            Toast.makeText(context, "this is active network", Toast.LENGTH_SHORT).show();
            //if connected to wifi or mobile data plan
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(context, "this is active network inside", Toast.LENGTH_SHORT).show();
                //getting all the unsynced names
                Cursor cursor = db.getnotlogincart();
                if (cursor != null) {
//                    Toast.makeText(context, "inside cursur "+cursor.getCount(), Toast.LENGTH_SHORT).show();

                    if (cursor.moveToFirst()) {
                        do {
                            //calling the method to save the unsynced name to MySQL
                            try {
                                saveName(
                                        Integer.parseInt(Dashboard.prefConfig.readID()),
                                        cursor.getInt(1),
                                        cursor.getInt(2),
                                        cursor.getInt(4),
                                        cursor.getInt(5),
                                        cursor.getString(10),
                                        cursor.getString(11),
                                        cursor.getString(12)

                                );
                            }
                            catch (Exception e)
                            {

                            }
                        } while (cursor.moveToNext());
//                        Toast.makeText(context, "Data came", Toast.LENGTH_SHORT).show();
                    }
                }


                else{
//                    Toast.makeText(context, "Database is Empty", Toast.LENGTH_SHORT).show();
                }

            }
        }

    }

    private  void saveName(final int  userid,final int  productid,final int  productqty,final int  Combinationid,final int  status,final String feildID,String feildName,String bankId)
    {
//        Toast.makeText(context, "save name   \nuser id "+Integer.toString(userid)+"\npid "+Integer.toString(productid)+"\n p qty"+Integer.toString(productqty)+"\n bank id"+Integer.toString(bankID)+"\n status"+Integer.toString(status), Toast.LENGTH_SHORT).show();
        Log.d("Data","save name   \nuser id "+Integer.toString(userid)+"\npid "+Integer.toString(productid)+"\n p qty"+Integer.toString(productqty)+"\n Combinationid id"+Integer.toString(Combinationid)+"\n status"+Integer.toString(status)+"\nfeildID "+feildID+"\nfeildName "+feildName+"\nbankId "+bankId);
        Retrofit retrofitaddcart = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AddcartAPI addcartAPI = retrofitaddcart.create(AddcartAPI.class);
        Call<generalstatusmessagePOJO> call = addcartAPI.getDetails(MainActivity.Tokendata,Integer.toString(productid),Integer.toString(productqty),Integer.toString(userid),Integer.toString(Combinationid),feildID,feildName,bankId);
        call.enqueue(new Callback<generalstatusmessagePOJO>() {
            @Override
            public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                if (response.isSuccessful())
                {
                int status = response.body().getStatus();
                String message  = response.body().getMessage().toString();
                if(status == 1) {
//                    Toast.makeText(context, ""+message, Toast.LENGTH_SHORT).show();
                    boolean res =   db.updateStatus();
                    if(res == true)
                    {
                        db.updateUserIdStatus(userid);
//                        db.deletesync();
                    }
                    else
                    {
                        Log.d("err","update name status error");
                    }

                }
                else {
                    Log.d("server","no insert to server");
                }

                }
                else
                {
//                            Toast.makeText(, "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {
                Log.d("net","network error");
            }
        });


    }
}


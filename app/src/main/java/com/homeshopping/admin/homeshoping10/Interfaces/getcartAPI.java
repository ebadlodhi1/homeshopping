package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.Cart.Cart;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface getcartAPI {
    @GET("/cart/getCart")
    Call<Cart> getDetails(@Header("data") String data, @Query("id") String id);
}

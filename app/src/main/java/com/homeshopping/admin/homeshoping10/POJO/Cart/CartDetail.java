package com.homeshopping.admin.homeshoping10.POJO.Cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartDetail {
    @SerializedName("cartId")
    @Expose
    private Integer cartId;
    @SerializedName("productId")
    @Expose
    private Integer productId;
    @SerializedName("productImage")
    @Expose
    private String productImage;
    @SerializedName("productTitle")
    @Expose
    private String productTitle;
    @SerializedName("productAvailability")
    @Expose
    private String productAvailability;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("productPrice")
    @Expose
    private String productPrice;
    @SerializedName("variationName")
    @Expose
    private String variationName;
    @SerializedName("variationOptionName")
    @Expose
    private String variationOptionName;
    @SerializedName("variationPrice")
    @Expose
    private String variationPrice;
    @SerializedName("variationFunction")
    @Expose
    private String variationFunction;
    @SerializedName("fieldName")
    @Expose
    private String fieldName;
    @SerializedName("fieldOption")
    @Expose
    private String fieldOption;
    @SerializedName("OutOfStock")
    @Expose
    private Integer outOfStock;
    @SerializedName("BrandName")
    @Expose
    private String brandName;
    @SerializedName("productWishlist")
    @Expose
    private Integer productWishlist;

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public String getProductAvailability() {
        return productAvailability;
    }

    public void setProductAvailability(String productAvailability) {
        this.productAvailability = productAvailability;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getVariationName() {
        return variationName;
    }

    public void setVariationName(String variationName) {
        this.variationName = variationName;
    }

    public String getVariationOptionName() {
        return variationOptionName;
    }

    public void setVariationOptionName(String variationOptionName) {
        this.variationOptionName = variationOptionName;
    }

    public String getVariationPrice() {
        return variationPrice;
    }

    public void setVariationPrice(String variationPrice) {
        this.variationPrice = variationPrice;
    }

    public String getVariationFunction() {
        return variationFunction;
    }

    public void setVariationFunction(String variationFunction) {
        this.variationFunction = variationFunction;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldOption() {
        return fieldOption;
    }

    public void setFieldOption(String fieldOption) {
        this.fieldOption = fieldOption;
    }

    public Integer getOutOfStock() {
        return outOfStock;
    }

    public void setOutOfStock(Integer outOfStock) {
        this.outOfStock = outOfStock;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Integer getProductWishlist() {
        return productWishlist;
    }

    public void setProductWishlist(Integer productWishlist) {
        this.productWishlist = productWishlist;
    }
}

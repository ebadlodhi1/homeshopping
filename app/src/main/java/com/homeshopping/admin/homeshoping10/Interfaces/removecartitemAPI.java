package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.Cart.Cart;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.HTTP;
import retrofit2.http.Header;

public interface removecartitemAPI {
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "/cart/deleteCart", hasBody = true)
    Call<Cart> getDetails(@Header("data") String data, @Field("id") String id, @Field("customerid") String customerid);

}

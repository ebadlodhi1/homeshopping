package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.Cart.CartQuantity;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface getcartquantityApi {
    @GET("cart/countCart")
    Call<CartQuantity> getDetails(@Header("data") String data, @Query("customerId") String customerId);
}

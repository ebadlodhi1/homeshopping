package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Interfaces.UpdateshipaddressAPI;
import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddress;
import com.homeshopping.admin.homeshoping10.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class EditShippingAddressActivity extends AppCompatActivity {
    String ShipAddressId,ShipAddressFirstName,ShipAddressLastName,ShipAddressCompany,ShipAddressAddress,ShipAddressCity
            ,ShipAddressPhone;
    EditText shipaddressfnfull_name,shipaddressfnlast_name,shipaddressfncompany,shipaddressfnaddress
            ,shipaddressfncity,shipaddressfnphone;
    Button updateshipaddress;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_shipping_address);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Edit Shipping Adresses");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Edit Shipping Address Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        shipaddressfnfull_name = findViewById(R.id.shipaddressfnfull_name);
        shipaddressfnlast_name = findViewById(R.id.shipaddressfnlast_name);
        shipaddressfncompany = findViewById(R.id.shipaddressfncompany);
        shipaddressfnaddress = findViewById(R.id.shipaddressfnaddress);
        shipaddressfncity = findViewById(R.id.shipaddressfncity);
        shipaddressfnphone = findViewById(R.id.shipaddressfnphone);
        updateshipaddress = findViewById(R.id.button16);
        Bundle bundle = getIntent().getExtras();
        ShipAddressId = bundle.getString("ShipAddressId");
        ShipAddressFirstName = bundle.getString("ShipAddressFirstName");
        ShipAddressLastName = bundle.getString("ShipAddressLastName");
        ShipAddressCompany = bundle.getString("ShipAddressCompany");
        ShipAddressAddress = bundle.getString("ShipAddressAddress");
        ShipAddressCity = bundle.getString("ShipAddressCity");
        ShipAddressPhone = bundle.getString("ShipAddressPhone");
        shipaddressfnfull_name.setText(ShipAddressFirstName);
        shipaddressfnlast_name.setText(ShipAddressLastName);
        shipaddressfncompany.setText(ShipAddressCompany);
        shipaddressfnaddress.setText(ShipAddressAddress);
        shipaddressfncity.setText(ShipAddressCity);
        shipaddressfnphone.setText(ShipAddressPhone);
        updateshipaddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateshipaddresss();
            }
        });
    }

    public  void updateshipaddresss()
    {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UpdateshipaddressAPI api = retrofit.create(UpdateshipaddressAPI.class);

//        String fname = shipaddressfnfull_name.getText().toString();
//        Toast.makeText(this, ""+fname, Toast.LENGTH_SHORT).show();
        Call<shipaddress> call = api.getDetails(MainActivity.Tokendata, shipaddressfnfull_name.getText().toString(),
                shipaddressfnlast_name.getText().toString(),
                shipaddressfncompany.getText().toString(),
                shipaddressfnaddress.getText().toString(),
                shipaddressfncity.getText().toString(),
                shipaddressfnphone.getText().toString(), prefConfig.readID(),ShipAddressId);
        call.enqueue(new Callback<shipaddress>() {
            @Override
            public void onResponse(Call<shipaddress> call, Response<shipaddress> response) {
                String res = response.body().getStatus().toString();
                if(res.equals("1"))
                {
                    Toast.makeText(EditShippingAddressActivity.this, "Updated", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(),ShipingAddressActivity.class);
                startActivity(intent);
                finish();
                }
            }

            @Override
            public void onFailure(Call<shipaddress> call, Throwable t) {

            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);
        finish();

    }
}

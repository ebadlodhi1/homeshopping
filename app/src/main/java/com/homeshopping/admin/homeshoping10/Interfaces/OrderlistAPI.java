package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.OderListPOJO;
import com.homeshopping.admin.homeshoping10.POJO.OrderListPOJO.OrderlistExample;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface OrderlistAPI {
    @GET("/order/getOrdersByCustomerId")
    Call<OrderlistExample> getDetails(@Header("data") String data, @Query("customerId") String customerId);
}

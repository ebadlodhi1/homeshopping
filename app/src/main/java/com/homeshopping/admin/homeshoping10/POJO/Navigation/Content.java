package com.homeshopping.admin.homeshoping10.POJO.Navigation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Content {
    @SerializedName("linktext")
    @Expose
    private String linktext;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("items")
    @Expose
    private String items;

    public String getLinktext() {
        return linktext;
    }

    public void setLinktext(String linktext) {
        this.linktext = linktext;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

}

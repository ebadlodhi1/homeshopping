package com.homeshopping.admin.homeshoping10.POJO.ProductDetailPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductImage {
    @SerializedName("ImageFile")
    @Expose
    private String imageFile;
    @SerializedName("ImageThumb")
    @Expose
    private String imageThumb;

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public String getImageThumb() {
        return imageThumb;
    }

    public void setImageThumb(String imageThumb) {
        this.imageThumb = imageThumb;
    }
}

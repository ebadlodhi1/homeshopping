package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Fragment.CheckoutFragmentPersonaldetails;
import com.homeshopping.admin.homeshoping10.Interfaces.shipaddressAPI;
import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddress;
import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddresslist;
import com.homeshopping.admin.homeshoping10.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class CheckOutActivity extends AppCompatActivity {
    public static List<shipaddresslist> shiplist ;

    public static ArrayList<String> ShipAddressId  ;
    public static ArrayList<String> ShipAddressFirstName ;
    public static ArrayList<String> ShipAddressLastName  ;
    public static ArrayList<String> ShipAddressCompany  ;
    public static ArrayList<String> ShipAddressAddress ;
    public static ArrayList<String> ShipAddressCity  ;
    public static ArrayList<String> ShipAddressPhone  ;
    ProgressBar progressBar9;
    public static String finalfirstName = "";
    public static String finallastName = "";
    public static String finaladdress = "";
    public static String finalcountry_id = "162";
    public static String finalcountry = "Pakistan";
    public static String finalstate_id = "";
    public static String finalstate = "";
    public static String finalphone = "";
    public static String finalcountry_iso = "PK";
    public static String finalcustomerId = "";

    public static String finalpaymentMethod = "";
    public static String finalpaymentModule = "";
    public static String finalremarks = "";
    public static String finalshipmentcharges = "";
    public static String Productshipmentcharges = "";
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Checkout Activity");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Checkout Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        progressBar9 = findViewById(R.id.progressBar9);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        shipaddressAPI api23 = retrofit.create(shipaddressAPI.class);
        Call<shipaddress> call3 = api23.getDetails(MainActivity.Tokendata, prefConfig.readID(), "0");
        call3.enqueue(new Callback<shipaddress>() {
            @Override
            public void onResponse(Call<shipaddress> call, Response<shipaddress> response) {
                int status = response.body().getStatus();
                if(status == 1)
                {
                    shiplist = new ArrayList<>();
                    shiplist = response.body().getShipAddress();
                    ShipAddressId = new ArrayList<>();;
                    ShipAddressFirstName = new ArrayList<>();;
                    ShipAddressLastName = new ArrayList<>();;
                    ShipAddressCompany = new ArrayList<>();;
                    ShipAddressAddress = new ArrayList<>();;
                    ShipAddressCity = new ArrayList<>();;
                    ShipAddressPhone = new ArrayList<>();;
//            Toast.makeText(ShipingAddressActivity.this, "list size "+shiplist.size(), Toast.LENGTH_SHORT).show();
                    for(int i = 0 ; i<shiplist.size();i++)
                    {
                        ShipAddressId.add(shiplist.get(i).getShipAddressId().toString());
                        ShipAddressFirstName.add(shiplist.get(i).getShipAddressFirstName());
                        ShipAddressLastName.add(shiplist.get(i).getShipAddressLastName());
                        ShipAddressCompany.add(shiplist.get(i).getShipAddressCompany());
                        ShipAddressAddress.add(shiplist.get(i).getShipAddressAddress());
                        ShipAddressCity.add(shiplist.get(i).getShipAddressCity());
                        ShipAddressPhone.add(shiplist.get(i).getShipAddressPhone());
                    }
                    progressBar9.setVisibility(View.GONE);
                    showFragment(new CheckoutFragmentPersonaldetails());
                }
                else {
                    Toast.makeText(CheckOutActivity.this, "No Shipping Address\nPlease Enter shiping Address.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(),AddShippingAddressActivity.class);
                    intent.putExtra("activityname","checkout");
                    startActivity(intent);
                    finish();
                }



            }

            @Override
            public void onFailure(Call<shipaddress> call, Throwable t) {
                Toast.makeText(CheckOutActivity.this, "Data loading error\nPlease try again.", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(),CartActivity.class);
                startActivity(intent);
                finish();
            }
        });

//        Gateway gateway = new Gateway();
//        gateway.setMerchantId("");
//        gateway.setMerchantId(String.valueOf(Gateway.Region.ASIA_PACIFIC));



//        Sess
//        gateway.updateSession();

    }

    @Override
    public void onBackPressed() {

        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        if (fragments == 1) {
            finish();
        } else if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

//        super.onBackPressed();
//        Intent intent = new Intent(getApplicationContext(),Dashboard.class);
//        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);
        finish();

    }

    private void showFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.checkout_fragmentlayout, fragment).addToBackStack("personaldetail").commitAllowingStateLoss();
    }
}

package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.MonthlyTopSellerPOJO.MonthlyTopsellerExample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface MonthlytopsellerApi {

    @GET("home/getTopPicks")
    Call<MonthlyTopsellerExample> getDetails(@Header("data") String data,@Query("userId") String userId);

}

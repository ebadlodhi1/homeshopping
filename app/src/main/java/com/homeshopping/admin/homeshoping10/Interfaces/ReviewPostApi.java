package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ReviewPostApi {
    @POST("products/addProductRating")
    Call<generalstatusmessagePOJO> getDetails(@Header("data") String data,
                                              @Query("productId") String productId,
                                              @Query("userName") String userName,
                                              @Query("productRatting") String productRatting,
                                              @Query("productReview") String productReview,
                                              @Query("reviewTitle") String reviewTitle);
}

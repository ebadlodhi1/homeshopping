package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.PricetrendPOJO.PriceTrendExample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface PricetrendAPI {
    @GET("products/getProductRating")
    Call<PriceTrendExample> getDetails(@Header("data") String data, @Query("productId") String productId);

}

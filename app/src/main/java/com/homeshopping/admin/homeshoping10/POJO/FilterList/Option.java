package com.homeshopping.admin.homeshoping10.POJO.FilterList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Option {
    @SerializedName("featureOptionName")
    @Expose
    private String featureOptionName;
    @SerializedName("featureOptionId")
    @Expose
    private Integer featureOptionId;

    public String getFeatureOptionName() {
        return featureOptionName;
    }

    public void setFeatureOptionName(String featureOptionName) {
        this.featureOptionName = featureOptionName;
    }

    public Integer getFeatureOptionId() {
        return featureOptionId;
    }

    public void setFeatureOptionId(Integer featureOptionId) {
        this.featureOptionId = featureOptionId;
    }

}

package com.homeshopping.admin.homeshoping10.POJO.shippingadddress;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class shipaddresslist {
    @SerializedName("ShipAddressId")
    @Expose
    private Integer shipAddressId;
    @SerializedName("ShipAddressFirstName")
    @Expose
    private String shipAddressFirstName;
    @SerializedName("ShipAddressLastName")
    @Expose
    private String shipAddressLastName;
    @SerializedName("ShipAddressCompany")
    @Expose
    private String shipAddressCompany;
    @SerializedName("ShipAddressAddress")
    @Expose
    private String shipAddressAddress;
    @SerializedName("ShipAddressCity")
    @Expose
    private String shipAddressCity;
    @SerializedName("ShipAddressPhone")
    @Expose
    private String shipAddressPhone;

    public Integer getShipAddressId() {
        return shipAddressId;
    }

    public void setShipAddressId(Integer shipAddressId) {
        this.shipAddressId = shipAddressId;
    }

    public String getShipAddressFirstName() {
        return shipAddressFirstName;
    }

    public void setShipAddressFirstName(String shipAddressFirstName) {
        this.shipAddressFirstName = shipAddressFirstName;
    }

    public String getShipAddressLastName() {
        return shipAddressLastName;
    }

    public void setShipAddressLastName(String shipAddressLastName) {
        this.shipAddressLastName = shipAddressLastName;
    }

    public String getShipAddressCompany() {
        return shipAddressCompany;
    }

    public void setShipAddressCompany(String shipAddressCompany) {
        this.shipAddressCompany = shipAddressCompany;
    }

    public String getShipAddressAddress() {
        return shipAddressAddress;
    }

    public void setShipAddressAddress(String shipAddressAddress) {
        this.shipAddressAddress = shipAddressAddress;
    }

    public String getShipAddressCity() {
        return shipAddressCity;
    }

    public void setShipAddressCity(String shipAddressCity) {
        this.shipAddressCity = shipAddressCity;
    }

    public String getShipAddressPhone() {
        return shipAddressPhone;
    }

    public void setShipAddressPhone(String shipAddressPhone) {
        this.shipAddressPhone = shipAddressPhone;
    }

}

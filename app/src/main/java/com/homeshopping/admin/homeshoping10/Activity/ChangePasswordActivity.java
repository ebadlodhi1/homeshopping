package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Interfaces.UpdatepaswordApi;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;
import com.homeshopping.admin.homeshoping10.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class ChangePasswordActivity extends AppCompatActivity {
        EditText upcurpas,upnewpass,upretypepass;
        ImageView eyecurpass,eyenewpass,eyeretype;
        Button upupdate;
        ProgressBar progressBar12;
        Boolean flag= true;
    Boolean flag1= true;
    Boolean  flag2= true;
    Boolean   flag3 = true;
    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Update Password");


        
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Change password  Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        upcurpas = findViewById(R.id.upcurpas);
        upnewpass = findViewById(R.id.upnewpass);
        upretypepass = findViewById(R.id.upretypepass);
        eyecurpass = findViewById(R.id.eyecurpass);
        eyenewpass = findViewById(R.id.eyenewpass);
        eyeretype = findViewById(R.id.eyeretype);
        upupdate = findViewById(R.id.upupdate);
        progressBar12 = findViewById(R.id.progressBar12);
        progressBar12.setVisibility(View.GONE);
        eyecurpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag1 == true)
                {
                    eyecurpass.setBackgroundResource(R.drawable.eye_off);
                    flag1 = false;
                    upcurpas.setTransformationMethod(new PasswordTransformationMethod());
                }
                else
                {
                    eyecurpass.setBackgroundResource(R.drawable.eye);
                    flag1 = true;
                    upcurpas.setTransformationMethod(null);
                }
            }
        });
        eyenewpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag2 == true)
                {
                    eyenewpass.setBackgroundResource(R.drawable.eye_off);
                    flag2 = false;
                    upnewpass.setTransformationMethod(new PasswordTransformationMethod());
                }
                else
                {
                    eyenewpass.setBackgroundResource(R.drawable.eye);
                    flag2 = true;
                    upnewpass.setTransformationMethod(null);
                }
            }
        });
        eyeretype.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag3 == true)
                {
                    eyeretype.setBackgroundResource(R.drawable.eye_off);
                    flag3 = false;
                    upretypepass.setTransformationMethod(new PasswordTransformationMethod());
                }
                else
                {
                    eyeretype.setBackgroundResource(R.drawable.eye);
                    flag3 = true;
                    upretypepass.setTransformationMethod(null);
                }
            }
        });

        upupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar12.setVisibility(View.VISIBLE);

                String currentpass = upcurpas.getText().toString().trim();
                String newpass = upnewpass.getText().toString().trim();
                String retypepass = upretypepass.getText().toString().trim();

                flag = true;
                        if(currentpass.isEmpty())
                        {
                            progressBar12.setVisibility(View.GONE);
                            upcurpas.setError("Enter current password");
                            upcurpas.setBackgroundResource(R.drawable.costraintborderred);
                            upcurpas.requestFocus();
                            flag = false;
                        }
                        else if(newpass.isEmpty())
                        {
                            progressBar12.setVisibility(View.GONE);
                            upnewpass.setError("Enter new password");
                            upnewpass.setBackgroundResource(R.drawable.costraintborderred);
                            upnewpass.requestFocus();
                            flag = false;
                        }
                        else if(retypepass.isEmpty())
                        {
                            progressBar12.setVisibility(View.GONE);
                            upretypepass.setError("Re-type password");
                            upretypepass.setBackgroundResource(R.drawable.costraintborderred);
                            upretypepass.requestFocus();
                            flag = false;
                        }

                        else if(!retypepass.equals(newpass))
                        {
                            progressBar12.setVisibility(View.GONE);
                            upretypepass.setError("Re-type password not match");
                            upretypepass.setBackgroundResource(R.drawable.costraintborderred);
                            upretypepass.requestFocus();
                            flag = false;
                        }
                 if(flag)
                 {
//                     Toast.makeText(ChangePasswordActivity.this, ""+Dashboard.prefConfig.readID(), Toast.LENGTH_SHORT).show();
                     Retrofit retrofit = new Retrofit.Builder()
                             .baseUrl(ROOT_URL) //Setting the Root URL
                             .addConverterFactory(ScalarsConverterFactory.create())
                             .addConverterFactory(GsonConverterFactory.create())
                             .build();
                     UpdatepaswordApi updatepaswordApi = retrofit.create(UpdatepaswordApi.class);
                     Call<generalstatusmessagePOJO> call = updatepaswordApi.getDetails(MainActivity.Tokendata, prefConfig.readID(),newpass,retypepass,currentpass);
                     call.enqueue(new Callback<generalstatusmessagePOJO>() {
                         @Override
                         public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                             if (response.isSuccessful())
                             {
                                 int status = response.body().getStatus();
                                 if(status == 1) {
                                     progressBar12.setVisibility(View.GONE);
                                     Toast.makeText(ChangePasswordActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                 }
                                 else
                                 {
                                     progressBar12.setVisibility(View.GONE);
                                     Toast.makeText(ChangePasswordActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                 }
                                 }

                         }

                         @Override
                         public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                         }
                     });
                 }

            }
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);

    }
}

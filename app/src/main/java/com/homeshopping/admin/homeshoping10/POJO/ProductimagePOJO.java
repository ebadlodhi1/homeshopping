package com.homeshopping.admin.homeshoping10.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductimagePOJO {

    @SerializedName("ImageFileStd")
    @Expose
    private String imageFileStd;

    public String getImageFileStd() {
        return imageFileStd;
    }

    public void setImageFileStd(String imageFileStd) {
        this.imageFileStd = imageFileStd;
    }
}

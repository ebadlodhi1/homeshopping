package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Interfaces.addnewshipaddressAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.removeaddresslistAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.shipaddressAPI;
import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddress;
import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddresslist;
import com.homeshopping.admin.homeshoping10.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class ShipingAddressActivity extends AppCompatActivity {
        ListView addresslistview;
        ProgressBar progressBar2;
        Button addnewaddress;
    List<shipaddresslist> shiplist = new ArrayList<>();

    ArrayList<String> ShipAddressId  = new ArrayList<>();
    ArrayList<String> ShipAddressFirstName  = new ArrayList<>();
    ArrayList<String> ShipAddressLastName  = new ArrayList<>();
    ArrayList<String> ShipAddressCompany  = new ArrayList<>();
    ArrayList<String> ShipAddressAddress  = new ArrayList<>();
    ArrayList<String> ShipAddressCity  = new ArrayList<>();
    ArrayList<String> ShipAddressPhone  = new ArrayList<>();
    public DatabaseHelper db;
    boolean flag_loading = false;
    int catpagecount = 0;
    TextInputEditText shipaddressfnfull_name,shipaddressfnlast_name,shipaddressfncompany,shipaddressfnaddress
            ,shipaddressfncity,shipaddressfnphone;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shiping_address);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Shipping Adresses");
        addresslistview = findViewById(R.id.addresslistview);
        progressBar2 = findViewById(R.id.progressBar2);
        addnewaddress = findViewById(R.id.button15);
        db = new DatabaseHelper(getApplicationContext());
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName(" Shiping Address Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        shipaddressAPI api = retrofit.create(shipaddressAPI.class);
        Call<shipaddress> call = api.getDetails(MainActivity.Tokendata, prefConfig.readID(), String.valueOf(catpagecount));
call.enqueue(new Callback<shipaddress>() {
    @Override
    public void onResponse(Call<shipaddress> call, Response<shipaddress> response) {
        int status = response.body().getStatus();
        if(status == 1)
        {
            shiplist = response.body().getShipAddress();
            progressBar2.setVisibility(View.GONE);
            ShipAddressId.clear();
            ShipAddressFirstName.clear();
            ShipAddressLastName.clear();
            ShipAddressCompany.clear();
            ShipAddressAddress.clear();
            ShipAddressCity.clear();
            ShipAddressPhone.clear();
//            Toast.makeText(ShipingAddressActivity.this, "list size "+shiplist.size(), Toast.LENGTH_SHORT).show();
            for(int i = 0 ; i<shiplist.size();i++)
            {
                ShipAddressId.add(shiplist.get(i).getShipAddressId().toString());
                ShipAddressFirstName.add(shiplist.get(i).getShipAddressFirstName());
                ShipAddressLastName.add(shiplist.get(i).getShipAddressLastName());
                ShipAddressCompany.add(shiplist.get(i).getShipAddressCompany());
                ShipAddressAddress.add(shiplist.get(i).getShipAddressAddress());
                ShipAddressCity.add(shiplist.get(i).getShipAddressCity());
                ShipAddressPhone.add(shiplist.get(i).getShipAddressPhone());
            }
            getshiplistAdapter getshiplistAdapter = new getshiplistAdapter(getApplicationContext(), shiplist);
            addresslistview.setAdapter(getshiplistAdapter);
        }
        else
        {
//            View parentLayout = findViewById(android.R.id.content);
//            Snackbar snackbar = Snackbar.make(parentLayout, ""+response.body().getMessage(), Snackbar.LENGTH_LONG);
//            View sbView = snackbar.getView();
//            sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//            snackbar.show();
        }
        addresslistview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0){
                    // here you have reached end of list, load more data


                    if(flag_loading == false)
                    {
                        flag_loading = true;
                        catpagecount = catpagecount+16;
                        fetchMoreItems(catpagecount);
                    }


                }
            }
        });

    }

    @Override
    public void onFailure(Call<shipaddress> call, Throwable t) {

    }
});

addnewaddress.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
//        showDialogeditshipaddress();
        Intent intent = new Intent(getApplicationContext(),AddShippingAddressActivity.class);
        intent.putExtra("activityname","a");
        startActivity(intent);
        finish();
    }
});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);
        finish();

    }


    public class getshiplistAdapter extends ArrayAdapter<String> {



        public getshiplistAdapter(Context context, List<shipaddresslist> list ) {
            super(context, R.layout.customshipaddressitem, ShipAddressId);




        }

        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder2 vh;
            View v = view;
            if(v == null) {
                vh = new ViewHolder2();
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                v = li.inflate(R.layout.customshipaddressitem,null);
//            Example app = items.get(position);
//            Log.d("position",Integer.toString(position));
//            if(app != null) {

                vh.shipaddfname = (TextView) v.findViewById(R.id.shipaddfname);
                vh. shipadd = (TextView) v.findViewById(R.id.shipadd);
                vh. shipaddcity = (TextView) v.findViewById(R.id.shipaddcity);
                vh. shipaddpno = (TextView) v.findViewById(R.id.shipaddpno);
                vh.shipaddedit = v.findViewById(R.id.shipaddedit);
                vh.companyconstraint = v.findViewById(R.id.constraintLayout129);
//                vh.shipadddelete = v.findViewById(R.id.shipadddelete);


                v.setTag(vh);
            }
            else{
                vh = (ViewHolder2) view.getTag();
            }



// give a timezone reference for formatting (see comment at the bottom)


            vh.shipaddfname.setText(ShipAddressFirstName.get(position)+" "+ShipAddressLastName.get(position));
            vh.shipadd.setText(ShipAddressAddress.get(position)+", "+ShipAddressCity.get(position));

            vh.shipaddpno.setText("Phone no: "+ShipAddressPhone.get(position));

            if(ShipAddressCompany.get(position).equals(""))
            {
                vh.companyconstraint.setVisibility(View.GONE);
            }
            else
            {
                vh.companyconstraint.setVisibility(View.VISIBLE);
                vh.shipaddcity.setText(ShipAddressCompany.get(position));}

            vh.shipaddedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                Intent  intent = new Intent(getApplicationContext(),EditShippingAddressActivity.class);
                    intent.putExtra("ShipAddressId",ShipAddressId.get(position));
                    intent.putExtra("ShipAddressFirstName",ShipAddressFirstName.get(position));
                    intent.putExtra("ShipAddressLastName",ShipAddressLastName.get(position));
                    intent.putExtra("ShipAddressCompany",ShipAddressCompany.get(position));
                    intent.putExtra("ShipAddressAddress",ShipAddressAddress.get(position));
                    intent.putExtra("ShipAddressCity",ShipAddressCity.get(position));
                    intent.putExtra("ShipAddressPhone",ShipAddressPhone.get(position));
                    startActivity(intent);

                }
            });
//            vh.shipadddelete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    progressBar2.setVisibility(View.VISIBLE);
//                    View parentLayout = findViewById(android.R.id.content);
//                    Snackbar snackbar = Snackbar.make(parentLayout, "Remove Address from list", Snackbar.LENGTH_LONG);
//                    View sbView = snackbar.getView();
//                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                    snackbar.show();
//                    Retrofit retrofit = new Retrofit.Builder()
//                            .baseUrl(ROOT_URL) //Setting the Root URL
//                            .addConverterFactory(GsonConverterFactory.create())
//                            .build();
//                    removeaddresslistAPI  api = retrofit.create(removeaddresslistAPI.class);
//                    Call<shipaddress> call = api.getDetails(MainActivity.Tokendata,ShipAddressId.get(position),Dashboard.prefConfig.readID());
//                    call.enqueue(new Callback<shipaddress>() {
//                        @Override
//                        public void onResponse(Call<shipaddress> call, Response<shipaddress> response) {
//                            int status = response.body().getStatus();
//                            if(status == 1) {
//                                shiplist = response.body().getShipAddress();
//                                ShipAddressId.clear();
//                                ShipAddressFirstName.clear();
//                                ShipAddressLastName.clear();
//                                ShipAddressCompany.clear();
//                                ShipAddressAddress.clear();
//                                ShipAddressCity.clear();
//                                ShipAddressPhone.clear();
//
//                                progressBar2.setVisibility(View.GONE);
//                                for (int i = 0; i < shiplist.size(); i++) {
//                                    ShipAddressId.add(shiplist.get(i).getShipAddressId().toString());
//                                    ShipAddressFirstName.add(shiplist.get(i).getShipAddressFirstName());
//                                    ShipAddressLastName.add(shiplist.get(i).getShipAddressLastName());
//                                    ShipAddressCompany.add(shiplist.get(i).getShipAddressCompany());
//                                    ShipAddressAddress.add(shiplist.get(i).getShipAddressAddress());
//                                    ShipAddressCity.add(shiplist.get(i).getShipAddressCity());
//                                    ShipAddressPhone.add(shiplist.get(i).getShipAddressPhone());
//                                }
//
//                                getshiplistAdapter getshiplistAdapter = new getshiplistAdapter(getApplicationContext(), shiplist);
//                                addresslistview.setAdapter(getshiplistAdapter);
//                                progressBar2.setVisibility(View.GONE);
////            catproductgrid.setAdapter(dataImageAdapter);
//
//                                addresslistview.deferNotifyDataSetChanged();
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Call<shipaddress> call, Throwable t) {
//
//                        }
//                    });
//                }
//            });


            return v;

        }
    }
    static class ViewHolder2
    {
        // ImageView imageView;
        TextView shipaddfname,shipadd,shipaddcity,shipaddpno;
        ImageView shipaddedit,shipadddelete;
       ConstraintLayout companyconstraint;



    }


    public  void fetchMoreItems(int count)
    {
//    Toast.makeText(this, "Loading..", Toast.LENGTH_SHORT).show();
        View parentLayout = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar.make(parentLayout, "Loading more...", Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(Color.rgb(161, 201, 19));
        snackbar.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        shipaddressAPI api = retrofit.create(shipaddressAPI.class);
        Call<shipaddress> call = api.getDetails(MainActivity.Tokendata, prefConfig.readID(), String.valueOf(count));
        call.enqueue(new Callback<shipaddress>() {
            @Override
            public void onResponse(Call<shipaddress> call, Response<shipaddress> response) {
                int status = response.body().getStatus();
                if(status == 1)
                {
                    shiplist = response.body().getShipAddress();
                    progressBar2.setVisibility(View.GONE);
                    for(int i = 0 ; i<shiplist.size();i++)
                    {
                        ShipAddressId.add(shiplist.get(i).getShipAddressId().toString());
                        ShipAddressFirstName.add(shiplist.get(i).getShipAddressFirstName());
                        ShipAddressLastName.add(shiplist.get(i).getShipAddressLastName());
                        ShipAddressCompany.add(shiplist.get(i).getShipAddressCompany());
                        ShipAddressAddress.add(shiplist.get(i).getShipAddressAddress());
                        ShipAddressCity.add(shiplist.get(i).getShipAddressCity());
                        ShipAddressPhone.add(shiplist.get(i).getShipAddressPhone());
                    }

                    getshiplistAdapter getshiplistAdapter = new getshiplistAdapter(getApplicationContext(), shiplist);
                    addresslistview.setAdapter(getshiplistAdapter);
                    progressBar2.setVisibility(View.GONE);
//            catproductgrid.setAdapter(dataImageAdapter);

                    addresslistview.deferNotifyDataSetChanged();
                    if (shiplist.size() < 16) {
                        flag_loading = true;
                    } else {
                        flag_loading = false;
                    }
                }
                else
                {
//                    View parentLayout = findViewById(android.R.id.content);
//                    Snackbar snackbar = Snackbar.make(parentLayout, ""+response.body().getMessage(), Snackbar.LENGTH_LONG);
//                    View sbView = snackbar.getView();
//                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                    snackbar.show();
                }
            }

            @Override
            public void onFailure(Call<shipaddress> call, Throwable t) {

            }
        });

    }


    public void showDialogeditshipaddress()
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.activity_edit_shipping_address, null);
        dialogBuilder.setView(dialogView);

        Button add  = dialogView.findViewById(R.id.button16);
        shipaddressfnfull_name = dialogView.findViewById(R.id.shipaddressfnfull_name);
        shipaddressfnlast_name = dialogView.findViewById(R.id.shipaddressfnlast_name);
        shipaddressfncompany = dialogView.findViewById(R.id.shipaddressfncompany);
        shipaddressfnaddress = dialogView.findViewById(R.id.shipaddressfnaddress);
        shipaddressfncity = dialogView.findViewById(R.id.shipaddressfncity);
        shipaddressfnphone = dialogView.findViewById(R.id.shipaddressfnphone);
        add.setText("Add");
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ROOT_URL) //Setting the Root URL
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                addnewshipaddressAPI api = retrofit.create(addnewshipaddressAPI.class);
                Call<shipaddress> call = api.getDetails(MainActivity.Tokendata,shipaddressfnfull_name.getText().toString(),
                        shipaddressfnlast_name.getText().toString(),
                        shipaddressfncompany.getText().toString(),
                        shipaddressfnaddress.getText().toString(),
                        shipaddressfncity.getText().toString(),
                        shipaddressfnphone.getText().toString(), prefConfig.readID());
                call.enqueue(new Callback<shipaddress>() {
                    @Override
                    public void onResponse(Call<shipaddress> call, Response<shipaddress> response) {
                        if (response.isSuccessful())
                        {
                        String res = response.body().getStatus().toString();
                        if(res.equals("1"))
                        {
                            Toast.makeText(ShipingAddressActivity.this, "Added", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(),ShipingAddressActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), ""+response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<shipaddress> call, Throwable t) {

                    }
                });
            }
        });




        View view = null;
        dialogBuilder.setTitle("Add new Ship Address ");
        dialogBuilder.setCancelable(true);
        final  AlertDialog b = dialogBuilder.create();

        b.show();

    }



}

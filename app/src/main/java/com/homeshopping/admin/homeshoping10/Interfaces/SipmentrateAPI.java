package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.LoginPOJO;
import com.homeshopping.admin.homeshoping10.POJO.ShipmentratePOJO;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SipmentrateAPI {
    @POST("/checkout/getShippingRate")
    Call<ShipmentratePOJO> getDetails(@Header("data") String data, @Query("customerId") String customerId,
                                      @Query("city") String city);
}

package com.homeshopping.admin.homeshoping10.POJO.ReviewRatingPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {
    @SerializedName("reviewTitle")
    @Expose
    private String reviewTitle;
    @SerializedName("reviewDate")
    @Expose
    private Integer reviewDate;
    @SerializedName("reviewFrom")
    @Expose
    private String reviewFrom;
    @SerializedName("reviewText")
    @Expose
    private String reviewText;
    @SerializedName("reviewRating")
    @Expose
    private Integer reviewRating;

    public String getReviewTitle() {
        return reviewTitle;
    }

    public void setReviewTitle(String reviewTitle) {
        this.reviewTitle = reviewTitle;
    }

    public Integer getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Integer reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReviewFrom() {
        return reviewFrom;
    }

    public void setReviewFrom(String reviewFrom) {
        this.reviewFrom = reviewFrom;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public Integer getReviewRating() {
        return reviewRating;
    }

    public void setReviewRating(Integer reviewRating) {
        this.reviewRating = reviewRating;
    }
}

package com.homeshopping.admin.homeshoping10.POJO.monthlytopseller;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Example {


    @SerializedName("ProductID")
    @Expose
    private Integer productID;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("ProductDisplayName")
    @Expose
    private String productDisplayName;
    @SerializedName("ProductDescription")
    @Expose
    private String productDescription;
    @SerializedName("ProductAvailability")
    @Expose
    private String productAvailability;
    @SerializedName("ProductPrice")
    @Expose
    private String productPrice;
    @SerializedName("ProductRetailPrice")
    @Expose
    private String productRetailPrice;
    @SerializedName("ProductSalePrice")
    @Expose
    private String productSalePrice;
    @SerializedName("ProdCalculatedPrice")
    @Expose
    private String prodCalculatedPrice;
    @SerializedName("ProductCurrentInventory")
    @Expose
    private Integer productCurrentInventory;
    @SerializedName("ProductFixedShippingCost")
    @Expose
    private String productFixedShippingCost;
    @SerializedName("ProductRatingTotal")
    @Expose
    private Integer productRatingTotal;
    @SerializedName("ProductNumberRatings")
    @Expose
    private Integer productNumberRatings;
    @SerializedName("ProductBrandID")
    @Expose
    private Integer productBrandID;
    @SerializedName("ProductVariationID")
    @Expose
    private Integer productVariationID;
    @SerializedName("ProductAllowPurchases")
    @Expose
    private Integer productAllowPurchases;
    @SerializedName("ProductHidePrice")
    @Expose
    private Integer productHidePrice;
    @SerializedName("ProductCallForPricingLabel")
    @Expose
    private String productCallForPricingLabel;
    @SerializedName("ProductCallForComingSoon")
    @Expose
    private String productCallForComingSoon;
    @SerializedName("ProductCategoryIds")
    @Expose
    private String productCategoryIds;
    @SerializedName("ProductcConfigFields")
    @Expose
    private String productcConfigFields;
    @SerializedName("ProductCondition")
    @Expose
    private String productCondition;
    @SerializedName("ProductPreOrder")
    @Expose
    private Integer productPreOrder;
    @SerializedName("ProductReleaseDate")
    @Expose
    private Integer productReleaseDate;
    @SerializedName("ProductSupplierID")
    @Expose
    private Integer productSupplierID;
    @SerializedName("ProductDisplayNameUrdu")
    @Expose
    private String productDisplayNameUrdu;
    @SerializedName("ProductDescriptionUrdu")
    @Expose
    private Object productDescriptionUrdu;
    @SerializedName("ProductAvailabilityUrdu")
    @Expose
    private String productAvailabilityUrdu;
    @SerializedName("ProductSold")
    @Expose
    private Integer productSold;
    @SerializedName("DealTime")
    @Expose
    private String dealTime;
    @SerializedName("ProductSupplierFeatured")
    @Expose
    private Integer productSupplierFeatured;
    @SerializedName("ProductBooking")
    @Expose
    private Integer productBooking;
    @SerializedName("ProductPaymentMethods")
    @Expose
    private String productPaymentMethods;
    @SerializedName("ImageID")
    @Expose
    private Integer imageID;
    @SerializedName("ImageProductID")
    @Expose
    private Integer imageProductID;
    @SerializedName("ImageFile")
    @Expose
    private String imageFile;
    @SerializedName("ImageIsThumb")
    @Expose
    private Integer imageIsThumb;
    @SerializedName("ImageSort")
    @Expose
    private Integer imageSort;
    @SerializedName("ImageFileTiny")
    @Expose
    private String imageFileTiny;
    @SerializedName("ImageFileThumb")
    @Expose
    private String imageFileThumb;
    @SerializedName("ImageFileStandard")
    @Expose
    private String imageFileStandard;
    @SerializedName("ImageFileZoom")
    @Expose
    private String imageFileZoom;
    @SerializedName("SupplierName")
    @Expose
    private String supplierName;
    @SerializedName("BrandDisplayName")
    @Expose
    private String brandDisplayName;

    public Integer getProductID() {
        return productID;
    }

    public void setProductID(Integer productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDisplayName() {
        return productDisplayName;
    }

    public void setProductDisplayName(String productDisplayName) {
        this.productDisplayName = productDisplayName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductAvailability() {
        return productAvailability;
    }

    public void setProductAvailability(String productAvailability) {
        this.productAvailability = productAvailability;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductRetailPrice() {
        return productRetailPrice;
    }

    public void setProductRetailPrice(String productRetailPrice) {
        this.productRetailPrice = productRetailPrice;
    }

    public String getProductSalePrice() {
        return productSalePrice;
    }

    public void setProductSalePrice(String productSalePrice) {
        this.productSalePrice = productSalePrice;
    }

    public String getProdCalculatedPrice() {
        return prodCalculatedPrice;
    }

    public void setProdCalculatedPrice(String prodCalculatedPrice) {
        this.prodCalculatedPrice = prodCalculatedPrice;
    }

    public Integer getProductCurrentInventory() {
        return productCurrentInventory;
    }

    public void setProductCurrentInventory(Integer productCurrentInventory) {
        this.productCurrentInventory = productCurrentInventory;
    }

    public String getProductFixedShippingCost() {
        return productFixedShippingCost;
    }

    public void setProductFixedShippingCost(String productFixedShippingCost) {
        this.productFixedShippingCost = productFixedShippingCost;
    }

    public Integer getProductRatingTotal() {
        return productRatingTotal;
    }

    public void setProductRatingTotal(Integer productRatingTotal) {
        this.productRatingTotal = productRatingTotal;
    }

    public Integer getProductNumberRatings() {
        return productNumberRatings;
    }

    public void setProductNumberRatings(Integer productNumberRatings) {
        this.productNumberRatings = productNumberRatings;
    }

    public Integer getProductBrandID() {
        return productBrandID;
    }

    public void setProductBrandID(Integer productBrandID) {
        this.productBrandID = productBrandID;
    }

    public Integer getProductVariationID() {
        return productVariationID;
    }

    public void setProductVariationID(Integer productVariationID) {
        this.productVariationID = productVariationID;
    }

    public Integer getProductAllowPurchases() {
        return productAllowPurchases;
    }

    public void setProductAllowPurchases(Integer productAllowPurchases) {
        this.productAllowPurchases = productAllowPurchases;
    }

    public Integer getProductHidePrice() {
        return productHidePrice;
    }

    public void setProductHidePrice(Integer productHidePrice) {
        this.productHidePrice = productHidePrice;
    }

    public String getProductCallForPricingLabel() {
        return productCallForPricingLabel;
    }

    public void setProductCallForPricingLabel(String productCallForPricingLabel) {
        this.productCallForPricingLabel = productCallForPricingLabel;
    }

    public String getProductCallForComingSoon() {
        return productCallForComingSoon;
    }

    public void setProductCallForComingSoon(String productCallForComingSoon) {
        this.productCallForComingSoon = productCallForComingSoon;
    }

    public String getProductCategoryIds() {
        return productCategoryIds;
    }

    public void setProductCategoryIds(String productCategoryIds) {
        this.productCategoryIds = productCategoryIds;
    }

    public String getProductcConfigFields() {
        return productcConfigFields;
    }

    public void setProductcConfigFields(String productcConfigFields) {
        this.productcConfigFields = productcConfigFields;
    }

    public String getProductCondition() {
        return productCondition;
    }

    public void setProductCondition(String productCondition) {
        this.productCondition = productCondition;
    }

    public Integer getProductPreOrder() {
        return productPreOrder;
    }

    public void setProductPreOrder(Integer productPreOrder) {
        this.productPreOrder = productPreOrder;
    }

    public Integer getProductReleaseDate() {
        return productReleaseDate;
    }

    public void setProductReleaseDate(Integer productReleaseDate) {
        this.productReleaseDate = productReleaseDate;
    }

    public Integer getProductSupplierID() {
        return productSupplierID;
    }

    public void setProductSupplierID(Integer productSupplierID) {
        this.productSupplierID = productSupplierID;
    }

    public String getProductDisplayNameUrdu() {
        return productDisplayNameUrdu;
    }

    public void setProductDisplayNameUrdu(String productDisplayNameUrdu) {
        this.productDisplayNameUrdu = productDisplayNameUrdu;
    }

    public Object getProductDescriptionUrdu() {
        return productDescriptionUrdu;
    }

    public void setProductDescriptionUrdu(Object productDescriptionUrdu) {
        this.productDescriptionUrdu = productDescriptionUrdu;
    }

    public String getProductAvailabilityUrdu() {
        return productAvailabilityUrdu;
    }

    public void setProductAvailabilityUrdu(String productAvailabilityUrdu) {
        this.productAvailabilityUrdu = productAvailabilityUrdu;
    }

    public Integer getProductSold() {
        return productSold;
    }

    public void setProductSold(Integer productSold) {
        this.productSold = productSold;
    }

    public String getDealTime() {
        return dealTime;
    }

    public void setDealTime(String dealTime) {
        this.dealTime = dealTime;
    }

    public Integer getProductSupplierFeatured() {
        return productSupplierFeatured;
    }

    public void setProductSupplierFeatured(Integer productSupplierFeatured) {
        this.productSupplierFeatured = productSupplierFeatured;
    }

    public Integer getProductBooking() {
        return productBooking;
    }

    public void setProductBooking(Integer productBooking) {
        this.productBooking = productBooking;
    }

    public String getProductPaymentMethods() {
        return productPaymentMethods;
    }

    public void setProductPaymentMethods(String productPaymentMethods) {
        this.productPaymentMethods = productPaymentMethods;
    }

    public Integer getImageID() {
        return imageID;
    }

    public void setImageID(Integer imageID) {
        this.imageID = imageID;
    }

    public Integer getImageProductID() {
        return imageProductID;
    }

    public void setImageProductID(Integer imageProductID) {
        this.imageProductID = imageProductID;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public Integer getImageIsThumb() {
        return imageIsThumb;
    }

    public void setImageIsThumb(Integer imageIsThumb) {
        this.imageIsThumb = imageIsThumb;
    }

    public Integer getImageSort() {
        return imageSort;
    }

    public void setImageSort(Integer imageSort) {
        this.imageSort = imageSort;
    }

    public String getImageFileTiny() {
        return imageFileTiny;
    }

    public void setImageFileTiny(String imageFileTiny) {
        this.imageFileTiny = imageFileTiny;
    }

    public String getImageFileThumb() {
        return imageFileThumb;
    }

    public void setImageFileThumb(String imageFileThumb) {
        this.imageFileThumb = imageFileThumb;
    }

    public String getImageFileStandard() {
        return imageFileStandard;
    }

    public void setImageFileStandard(String imageFileStandard) {
        this.imageFileStandard = imageFileStandard;
    }

    public String getImageFileZoom() {
        return imageFileZoom;
    }

    public void setImageFileZoom(String imageFileZoom) {
        this.imageFileZoom = imageFileZoom;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getBrandDisplayName() {
        return brandDisplayName;
    }

    public void setBrandDisplayName(String brandDisplayName) {
        this.brandDisplayName = brandDisplayName;
    }

}


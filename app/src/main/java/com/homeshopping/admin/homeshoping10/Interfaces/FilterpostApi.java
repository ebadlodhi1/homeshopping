package com.homeshopping.admin.homeshoping10.Interfaces;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface FilterpostApi {
    @POST("/products/getProductbyFilters")
    Call<String> getDetails(@Header("data") String data,
                            @Query("perPage") String perPage,
                            @Query("skip") String skip,
                            @Query("sort") String sort);
}

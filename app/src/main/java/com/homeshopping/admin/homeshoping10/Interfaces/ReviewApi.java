package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.ReviewRatingPOJO.ReviewExample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface ReviewApi {
    @GET("products/getProductRating")
    Call<ReviewExample> getDetails(@Header("data") String data, @Query("productId") String productId);
}

package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddress;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface shipaddressAPI {
    @GET("/shipAddress/getShipAddress")
    Call<shipaddress> getDetails(@Header("data") String data, @Query("id") String id, @Query("skip") String skip);
}

package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.RegisterPOJO;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RegisterApi {
    @POST("user/register")
    Call<RegisterPOJO> getDetails(@Header("data") String data, @Query("email") String email,
                                  @Query("password") String password,
                                  @Query("address") String address,
                                  @Query("firstname") String firstname,
                                  @Query("lastname") String lastname,
                                  @Query("city") String city,
                                  @Query("country") String country,
                                  @Query("mobile") String mobile,
                                  @Query("application") String application);
}

package com.homeshopping.admin.homeshoping10.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.JsonObject;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Class.MenuCategoryIDandNames;
import com.homeshopping.admin.homeshoping10.Class.MenuItemAdapter;
import com.homeshopping.admin.homeshoping10.Class.NetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Class.NotLoginDataNetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Interfaces.AddorRemoveCallbacks;
import com.homeshopping.admin.homeshoping10.Interfaces.AddtowishlistAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.CategoryFilterProduct;
import com.homeshopping.admin.homeshoping10.Interfaces.CategoryProductApi;
import com.homeshopping.admin.homeshoping10.Interfaces.FilterGetApi;
import com.homeshopping.admin.homeshoping10.Interfaces.getcartquantityApi;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;
import com.homeshopping.admin.homeshoping10.POJO.Cart.CartQuantity;
import com.homeshopping.admin.homeshoping10.POJO.CategoryProductPOJO.Detail;
import com.homeshopping.admin.homeshoping10.POJO.CategoryProductPOJO.ProductExample;
import com.homeshopping.admin.homeshoping10.POJO.FilterList.Filter;
import com.homeshopping.admin.homeshoping10.POJO.FilterList.Filterlist;
import com.homeshopping.admin.homeshoping10.POJO.FilterList.Option;
import com.homeshopping.admin.homeshoping10.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;

public class CategoryActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, AbsListView.OnScrollListener, AddorRemoveCallbacks {
    //    static String[] productnamee = {"Iphone X","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8","Samsung Note 8","Iphone 8"} ;
//    static String[] orignalpricetvv = {"113000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000"} ;
//    static  String[] offpercenttvv  = {"10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%","10%"} ;;
//    static  String[]discountedpricetvv = {"113000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000","90000","100000"} ;
    static int[] Productimagee = {R.drawable.iphonex, R.drawable.samsungnote, R.drawable.iphonex, R.drawable.samsungnote, R.drawable.iphonex, R.drawable.samsungnote, R.drawable.iphonex, R.drawable.samsungnote, R.drawable.iphonex, R.drawable.samsungnote, R.drawable.iphonex, R.drawable.samsungnote, R.drawable.iphonex, R.drawable.samsungnote, R.drawable.iphonex, R.drawable.samsungnote, R.drawable.iphonex, R.drawable.samsungnote, R.drawable.iphonex, R.drawable.samsungnote, R.drawable.iphonex};
    GridView catproductgrid;
    int catpagecount = 1;
    public DatabaseHelper db;
    android.widget.ExpandableListAdapter expandableListAdapter;
    ExpandableListView expandableListView;
    List<MenuModel> headerList = new ArrayList<>();
    HashMap<MenuModel, List<MenuModel>> childList = new HashMap<>();
    List<Filter> filterlists = new ArrayList<>();
    List<Option> filteroption = new ArrayList<>();
    ArrayList<String> featurename = new ArrayList<>();
    ArrayList<String> featurenamealter = new ArrayList<>();
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    HashMap<String, List<Integer>> listDataChildid;
    JSONObject mainObj = new JSONObject();


    ProgressDialog progressDialog;
    int addcartstatus = 0;
    public static final String ROOT_URL = "https://api.homeshopping.pk/";

    List<Detail> list = new ArrayList<Detail>();
    String catid = "";
    String slidertype = "";
    ArrayList<String> prodname = new ArrayList<>();
    ArrayList<String> prodid = new ArrayList<>();
    ArrayList<String> prodprice = new ArrayList<>();
    ArrayList<String> prodoffper = new ArrayList<>();
    ArrayList<String> proddiscount = new ArrayList<>();
    ArrayList<String> prodimage = new ArrayList<>();
    ArrayList<String> proddiscription = new ArrayList<>();
    ArrayList<String> prodbrand = new ArrayList<>();
    ArrayList<Integer> prodwishlist = new ArrayList<>();
    ArrayList<Integer> isExistInCart = new ArrayList<>();
    RadioGroup rg;
    RadioButton radioButton;
    ArrayList<String> mImageUrls = new ArrayList<>();
    TextView lblListHeader;

    private int mVisibleThreshold = 5;
    private int mCurrentPage = 0;
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private boolean mLastPage = false;
    Handler menu = new Handler();
    TextView cattext;
    DataImageAdapter dataImageAdapter;
    boolean flag_loading = false;

    int SalePrice = 1;
    int Productprice = 1;
    int discoutnamount = 0;
    public String Sortvalue = "popular";
    CheckBox txtListChild;
    int checkboxstatus = 0;
    ArrayList<Integer> featureoptionid;
    ArrayList<String> featurenamealteroption;
    JSONObject jo = new JSONObject();
    JSONObject jo2 = new JSONObject();
    int filteid, avalibilityid = 0;
    public static ConstraintLayout loginbefore, loginafter, logout;
    Boolean filterflag = false;
    ArrayList<Integer> filterselectedlist = new ArrayList<Integer>();
    TextView filtertext;
    ImageView filterimage;
    androidx.constraintlayout.widget.ConstraintLayout contactuscons , helpcons,whatsapp;
    private Tracker mTracker;
    ListView menulistview;
    com.homeshopping.admin.homeshoping10.Class.MenuItemAdapter MenuItemAdapter;
    List<com.homeshopping.admin.homeshoping10.Class.MenuItem> menuItemList = new ArrayList<com.homeshopping.admin.homeshoping10.Class.MenuItem>();
    List<MenuCategoryIDandNames> menuidnames = new ArrayList<MenuCategoryIDandNames>();
    TextView menuheader;
    static ConstraintLayout constraintLayout90;
    //    public static TextView username,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        dataImageAdapter = new DataImageAdapter(getApplicationContext());
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        expandableListView = findViewById(R.id.expandableListView);
        menulistview = findViewById(R.id.menulistview);
        menuheader = findViewById(R.id.menuheader);
        contactuscons = findViewById(R.id.constraintLayout95);
        helpcons = findViewById(R.id.constraintLayout96);
        constraintLayout90 = findViewById(R.id.constraintLayout90);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Category Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        whatsapp = findViewById(R.id.constraintLayout94);

        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contact = "+92 3111476725"; // use country code with your phone number
                String url = "https://api.whatsapp.com/send?phone=" + contact;
                try {
                    PackageManager pm = getApplicationContext().getPackageManager();
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "Whatsapp app not installed in your phone", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });
        contactuscons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ContactusActivity.class);
                startActivity(intent);
            }
        });
        helpcons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ContactusActivity.class);
                startActivity(intent);
            }
        });
        filtertext = findViewById(R.id.textView23);
        filterimage = findViewById(R.id.imageView13);
        Bundle bundle = new Bundle(getIntent().getExtras());
        catproductgrid = findViewById(R.id.catproductgrid);
        catproductgrid.setOnScrollListener(this);
        progressDialog = new ProgressDialog(CategoryActivity.this);
        progressDialog.setMessage("Loading..."); // Setting Message
        progressDialog.setTitle(""); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(true);
        menu.post(loadmenu);
        populatestage1();
        MenuItemAdapter = new MenuItemAdapter(menuItemList,this);
        menulistview.setAdapter(MenuItemAdapter);
        menulistview.setFastScrollEnabled(true);
        justifyListViewHeightBasedOnChildren(menulistview);
//        menuNameandID();

        menuheader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getApplicationContext(), ""+menuheader.getText().toString(), Toast.LENGTH_SHORT).show();
//                    if(menuheader.getText().toString().equals("< Back To Main Categories"))
//                    {
//                    menuheader.setText("All Categories");
//                    populatestage1();
//                    MenuItemAdapter.notifyDataSetChanged();
//                    }
            }
        });

        menulistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                menuheader.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(getApplicationContext(), ""+menuheader.getText().toString(), Toast.LENGTH_SHORT).show();
                        if(menuheader.getText().toString().equals("< Back To Main Categories"))
                        {
                            menuheader.setText("All Categories");
                            populatestage1();
                            MenuItemAdapter.notifyDataSetChanged();
                        }
                    }
                });
                if (MenuItemAdapter.getItem(position).getChildstatus() == 1)
                {


                    if (MenuItemAdapter.getItem(position).getItemname().equals("Mobile & Tablets"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatemobiletablet();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                menuheader.setText("All Categories");
                                populatestage1();
                                MenuItemAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                    else  if (MenuItemAdapter.getItem(position).getItemname().equals("Mobile Accessories"))
                    {
                        menuheader.setText("< Mobile & Tablets");
                        populatemobileAccesories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatemobiletablet();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Computer & Laptops"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatecomputerandlaptop();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                menuheader.setText("All Categories");
                                populatestage1();
                                MenuItemAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Peripherals"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populateperipherals();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Computer Components"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populatecomputercomponents();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Storage"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populatestorage();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Networking"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populatenetworking();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Computer Accessories"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populatecomputeraccessories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Laptop Accessories"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populatelaptopaccessories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Printers & Accessories"))
                    {
                        menuheader.setText("< Computer & Laptops");
                        populateprintersandaccessories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecomputerandlaptop();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Cameras & Accessories"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatecamera();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                menuheader.setText("All Categories");
                                populatestage1();
                                MenuItemAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Camera Accessories"))
                    {
                        menuheader.setText("< Cameras & Accessories");
                        populatecamerasandaccessories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecamera();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Surveillance Cameras"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatesurveillancecameras();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatecamera();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("TV & Video"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatetvandvideo();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatetvandvideo();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Home Audio & Video"))
                    {
                        menuheader.setText("< TV & Video");
                        populatehomeaudioandvideo();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatetvandvideo();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("TV Accessories"))
                    {
                        menuheader.setText("< TV & Video");
                        populatetvaccesories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatetvandvideo();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Home Appliances"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatehomeappliances();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Small Appliances"))
                    {
                        menuheader.setText("< Home Appliances");
                        populatesmallappliances();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Kitchen Appliances"))
                    {
                        menuheader.setText("< Small Appliances");
                        populatekitchenappliances();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else if(menuheader.getText().toString().equals("< Home Appliances"))
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Home Appliances");
                                    populatesmallappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Large Appliances"))
                    {
                        menuheader.setText("< Home Appliances");
                        populatelargeappliances();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Ovens"))
                    {
                        menuheader.setText("< Large Appliances");
                        populateovens();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else if(menuheader.getText().toString().equals("< Home Appliances"))
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Home Appliances");
                                    populatelargeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Personal Care"))
                    {
                        menuheader.setText("< Home Appliances");
                        populatepersonalcare();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Hardware & Tools"))
                    {
                        menuheader.setText("< Home Appliances");
                        populatehardwaretools();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatehomeappliances();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Musical Instruments"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatemusicalinstruments();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatemusicalinstruments();;
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Guitars & Accessories"))
                    {
                        menuheader.setText("< Musical Instruments");
                        populateguitarsandaccessories();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatemusicalinstruments();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Studio Equipment"))
                    {
                        menuheader.setText("< Musical Instruments");
                        populatestudioequipments();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatemusicalinstruments();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Keyboards & Pianos"))
                    {
                        menuheader.setText("< Musical Instruments");
                        populatekeyboardandpiano();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatemusicalinstruments();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }


                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Books & Stationery"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatebooksandstationery();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatebooksandstationery();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Books"))
                    {
                        menuheader.setText("< Books & Stationery");
                        populatebooks();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatebooksandstationery();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Stationery"))
                    {
                        menuheader.setText("< Books & Stationery");
                        populatestationery();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatebooksandstationery();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Gaming"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populategaming();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populategaming();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Xbox One S"))
                    {
                        menuheader.setText("< Gaming");
                        populatexboxones();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populategaming();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("PlayStation 4"))
                    {
                        menuheader.setText("< Gaming");
                        populateplaystation4();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populategaming();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Xbox 360"))
                    {
                        menuheader.setText("< Gaming");
                        populatexbox360();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populategaming();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Baby & Toys"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatebabyandtoys();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatebabyandtoys();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Mother & Baby"))
                    {
                        menuheader.setText("< Baby & Toys");
                        populatemotherandbaby();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatebabyandtoys();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Fashion"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatefashion();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatefashion();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Men"))
                    {
                        menuheader.setText("< Fashion");
                        populatemen();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatefashion();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Clothing"))
                    {
                        menuheader.setText("< Men");
                        populateclothing();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else if(menuheader.getText().toString().equals("< Men"))
                                {
                                    menuheader.setText("< Fashion");
                                    populatemen();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatefashion();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }

                            }
                        });
                    }
                    else if (MenuItemAdapter.getItem(position).getItemname().equals("Kids"))
                    {
                        menuheader.setText("< Back To Main Categories");
                        populatekids();
                        MenuItemAdapter.notifyDataSetChanged();
                        menuheader.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if(menuheader.getText().toString().equals("< Back To Main Categories"))
                                {
                                    menuheader.setText("All Categories");
                                    populatestage1();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                                else
                                {
                                    menuheader.setText("< Back To Main Categories");
                                    populatefashion();
                                    MenuItemAdapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }

                }
                else if(MenuItemAdapter.getItem(position).getChildstatus() == 0)
                {
                    finish();
                    String name = MenuItemAdapter.getItem(position).getItemname().toString();

                    int catid = MenuItemAdapter.getItem(position).getItemid();
//                    Toast.makeText(getApplicationContext(), ""+name+" "+catid, Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                    intent.putExtra("catname", name);
                    intent.putExtra("catid", catid);
                    startActivity(intent);
                    populatestage1();
                    menuheader.setText("All Categories");
                    MenuItemAdapter.notifyDataSetChanged();
                }
            }

        });



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        db = new DatabaseHelper(getApplicationContext());
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        loginbefore = header.findViewById(R.id.loginbefore);
        loginafter = header.findViewById(R.id.loginafter);
//        username  = header.findViewById(R.id.loginname);
//        email = header.findViewById(R.id.textView18);
       try {
           if (prefConfig.readLoginStatus()) {
               loginbefore.setVisibility(View.GONE);
               loginafter.setVisibility(View.VISIBLE);
//            username.setText("Hello "+Dashboard.prefConfig.readName());
//            email.setText(Dashboard.prefConfig.reademail());
               Retrofit retrofit = new Retrofit.Builder()
                       .baseUrl(ROOT_URL) //Setting the Root URL
                       .addConverterFactory(ScalarsConverterFactory.create())
                       .addConverterFactory(GsonConverterFactory.create())
                       .build();
               getcartquantityApi api = retrofit.create(getcartquantityApi.class);
               Call<CartQuantity> cartQuantityCall = api.getDetails(MainActivity.Tokendata, prefConfig.readID());
               cartQuantityCall.enqueue(new Callback<CartQuantity>() {
                   @Override
                   public void onResponse(Call<CartQuantity> call, Response<CartQuantity> response) {
                       if (response.isSuccessful()) {
                           int status = response.body().getStatus();
                           if (status == 1) {
//                          Toast.makeText(getApplicationContext(), "cart count server "+response.body().getCartQuantity(), Toast.LENGTH_SHORT).show();
                               Dashboard.cart_count = response.body().getCartQuantity();
                               invalidateOptionsMenu();
                           }
                       } else {
//                        Toast.makeText(CategoryActivity.this, "", Toast.LENGTH_SHORT).show();
                       }
                   }

                   @Override
                   public void onFailure(Call<CartQuantity> call, Throwable t) {

                   }
               });
           } else {
               loginbefore.setVisibility(View.VISIBLE);
               loginafter.setVisibility(View.GONE);
           }
       }
       catch ( Exception e)
       {

       }
        ConstraintLayout signuplayout = header.findViewById(R.id.signuplayout);
        signuplayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(intent);
                finish();

            }
        });
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        ConstraintLayout signinlayout = header.findViewById(R.id.signinlayout);
        signinlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.putExtra("comingcart", "0");
                startActivity(intent);
                finish();

            }
        });
        ImageView home = header.findViewById(R.id.imageView9);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(intent);

            }
        });
        ConstraintLayout constraintLayoutfn = header.findViewById(R.id.constraintLayoutfn1);
        constraintLayoutfn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(intent);

            }
        });
        ConstraintLayout constraintLayoutfnhome = header.findViewById(R.id.constraintLayoutfnhome);
        constraintLayoutfnhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                startActivity(intent);

            }
        });
        ConstraintLayout constraintLayout25 = header.findViewById(R.id.constraintLayout25);
        constraintLayout25.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intent);

            }
        });
        ConstraintLayout constraintLayout20 = header.findViewById(R.id.constraintLayout20);
        constraintLayout20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intent);

            }
        });
        ConstraintLayout Acount = header.findViewById(R.id.constraintLayout23);
        Acount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AccountActivity.class);
                startActivity(intent);

            }
        });
        ConstraintLayout wishlist = header.findViewById(R.id.constraintLayout24);
        wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WhishlistActivity.class);
                startActivity(intent);
            }
        });

        cattext = findViewById(R.id.cattext);
        cattext.setText(bundle.getString("catname"));
//this.setTitle(bundle.getString("catname"));
        catid = Integer.toString(bundle.getInt("catid",3770));

        slidertype = bundle.getString("slidertype","category");
//        Toast.makeText(application, ""+slidertype, Toast.LENGTH_SHORT).show();
//       Toast.makeText(this, "catpage  "+catid, Toast.LENGTH_SHORT).show();

        Loadproducts(Sortvalue);
        catproductgrid.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                final int lastItem = firstVisibleItem + visibleItemCount;
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                    // here you have reached end of list, load more data

                    if(filterflag == false) {
                        if (flag_loading == false) {
                            flag_loading = true;
                            catpagecount = catpagecount + 13;
                            fetchMoreItems(catpagecount, Sortvalue);
                        }
                    }


                }
            }
        });


//        Toast.makeText(this, ""+catid, Toast.LENGTH_SHORT).show();

        catproductgrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    Intent intent = new Intent(getApplicationContext(), ProductDetailActivity.class);
                    intent.putExtra("title", prodname.get(position));
                    startActivity(intent);
                } catch (Exception e) {

                }


            }
        });


        ConstraintLayout sort = findViewById(R.id.constraintLayout30);
        sort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogsort();
            }
        });
        ConstraintLayout filter = findViewById(R.id.constraintLayout29);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogfilter();
            }
        });


    }

    public void Loadproducts(String sort) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        CategoryProductApi api = retrofit.create(CategoryProductApi.class);
        Call<ProductExample> call = api.getDetails(MainActivity.Tokendata, catid, "0", "14", sort, prefConfig.readID());
        call.enqueue(new Callback<ProductExample>() {
            @Override
            public void onResponse(Call<ProductExample> call, Response<ProductExample> response) {
                if (response.isSuccessful()) {
                    int status = response.body().getStatus();
                    if (status == 1) {
                        list = response.body().getDetails();
//                prodname = new String[list.size()];
//                prodprice = new String[list.size()];
//                prodoffper = new String[list.size()];
//                proddiscount = new String[list.size()];
//                prodimage = new String[list.size()];
//                proddiscription = new String[list.size()];
//                prodid = new String[list.size()];
//                prodbrand = new String[list.size()];
//
//                Toast.makeText(CategoryActivity.this, ""+response.body(), Toast.LENGTH_SHORT).show();
                        prodname.clear();
                        prodprice.clear();
                        proddiscount.clear();
                        prodimage.clear();
                        mImageUrls.clear();
                        prodid.clear();
                        prodbrand.clear();
                        prodwishlist.clear();
                        isExistInCart.clear();
                        for (int i = 0; i < list.size(); i++) {
                            prodname.add(list.get(i).getProductName());
                            prodprice.add(list.get(i).getProductPrice());
                            proddiscount.add(list.get(i).getProductSalePrice());
                            prodimage.add(list.get(i).getProductImages().getImageFile());

                            mImageUrls.add(prodimage.get(i));

                            prodid.add(list.get(i).getProductID().toString());
                            prodbrand.add(list.get(i).getBrandDisplayName());
                            prodwishlist.add(list.get(i).getProductWishlist());
                            isExistInCart.add(list.get(i).getIsExistInCart());

                        }
                        catproductgrid.setAdapter(null);
//                    progressBar.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        catproductgrid.setAdapter(dataImageAdapter);
                    } else if (status == 0) {
                        progressDialog.dismiss();
                        View parentLayout = findViewById(android.R.id.content);
                        Snackbar snackbar = Snackbar.make(parentLayout, "No Product Found", Snackbar.LENGTH_LONG);
                        View sbView = snackbar.getView();
                        sbView.setBackgroundColor(Color.rgb(161, 201, 19));
                        snackbar.show();


                    }
                } else {
                    Toast.makeText(getApplicationContext(), "" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<ProductExample> call, Throwable t) {

            }
        });

    }

    Runnable loadmenu = new Runnable() {
        @Override
        public void run() {
//            prepareMenuData();
//            populateExpandableList();
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(), Dashboard.cart_count, R.drawable.ttcart));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home: {
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void refresh() {

        Intent intent = new Intent(getApplicationContext(), CartActivity.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mLoading) {
            if (totalItemCount > mPreviousTotal) {

                mLoading = false;
                mPreviousTotal = totalItemCount;
                mCurrentPage++;

                // Find your own condition in order to know when you
                // have finished displaying all items
                if (mCurrentPage + 1 > 50) {
                    mLastPage = true;
                }
            }
        }
        if (!mLastPage && !mLoading &&
                (totalItemCount - visibleItemCount) <= (firstVisibleItem + mVisibleThreshold)) {

            mLoading = true;
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onAddProduct(int pid, String Qty, String userid, String combinationid, String productTitle, String Productprice, String Productimage, String ProductBrand, String feildid, String feildOption,String bankID) {
        db.addRecord(Integer.toString(pid), Qty, userid, combinationid, 0, productTitle, Productprice, Productimage, ProductBrand, feildid, feildOption,"0");
        Boolean res = prefConfig.readLoginStatus();
        if (res == false) {

            Cursor cursor1 = db.getcountnotlogin();
            if (cursor1.moveToFirst()) {
                Dashboard.cart_count = cursor1.getInt(0);
                invalidateOptionsMenu();


//            Cursor cursor = db.getnotlogincartforDisplay();
//            if (cursor != null) {
//                if (cursor.moveToFirst()) {
//                    do {
//                        //calling the method to save the unsynced name to MySQL
//
////                    Toast.makeText(this, "aa "+Dashboard.cart_count, Toast.LENGTH_SHORT).show();
//                    } while (cursor.moveToNext());
//                }
                try {
//                    Toast.makeText(getApplicationContext(), "Throughing", Toast.LENGTH_SHORT).show();
                    getApplicationContext().registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    getApplicationContext().registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                } catch (Exception e) {
                    Log.e("error", "" + e);
                }

            }
        } else {
            Cursor cursor1 = db.getRecord();
            if (cursor1 != null) {
                if (cursor1.moveToFirst()) {
                    do {
                        //calling the method to save the unsynced name to MySQL



//                    Toast.makeText(this, "aa "+Dashboard.cart_count, Toast.LENGTH_SHORT).show();
                    } while (cursor1.moveToNext());
                }
                try {
//                    Toast.makeText(getApplicationContext(), "Throughing", Toast.LENGTH_SHORT).show();
                    getApplicationContext().registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    getApplicationContext().registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                } catch (Exception e) {
                    Log.e("error", "" + e);
                }

            }
            Dashboard.cart_count++;
            invalidateOptionsMenu();

        }
        this.invalidateOptionsMenu();

//        db.addRecord(Integer.toString(pid),Qty,userid,bankid,0);
//        Toast.makeText(context, "Imei number: " + Dashboard.Imei+"\nPhone number "+Dashboard.Phonenumber, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onRemoveProduct() {

    }

    @Override
    public void onAddProduct() {

    }


    public class DataImageAdapter extends BaseAdapter {

//        String[] IMAGE_URLS = prodname
//                ;


        private LayoutInflater inflater;

        Context c;

        DataImageAdapter(Context context) {
            inflater = LayoutInflater.from(context);
            c = context;

        }

        @Override
        public int getCount() {
            return prodname.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View view = convertView;
            if (view == null) {

                view = inflater.inflate(R.layout.customcategorypageitem, parent, false);
                holder = new ViewHolder();
                assert view != null;

                holder.Productimage = (ImageView) view.findViewById(R.id.imageView2);
                holder.productname = view.findViewById(R.id.productname);
                holder.orignalpricetv = view.findViewById(R.id.orignalpricetv);
                holder.discountedpricetv = view.findViewById(R.id.discountedpricetv);
                holder.offpercenttv = view.findViewById(R.id.offpercenttv);
                holder.allcons = view.findViewById(R.id.allcons);
                holder.wishlisticon = view.findViewById(R.id.wishlisticon);
                holder.addcart = view.findViewById(R.id.addcart);
                holder.brandname = view.findViewById(R.id.brandname);

//              holder.imageView2.setScaleType(ImageView.ScaleType.FIT_CENTER);


                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            Productprice = Math.round(Float.parseFloat(prodprice.get(position)));
            SalePrice = Math.round(Float.parseFloat(proddiscount.get(position)));

            discoutnamount = Productprice - SalePrice;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            final String formatedproductprice = formatter.format(Productprice);
            final String formatedsaleprice = formatter.format(SalePrice);

            Log.e("Discount data", Productprice + "\n" + SalePrice + "\n" + discoutnamount);

            Boolean res2 = db.getproductforwishlist(Integer.parseInt(prodid.get(position)));

            if (res2 == true) {
                holder.wishlisticon.setBackgroundResource(R.drawable.heartfilled);
            } else {
                holder.wishlisticon.setBackgroundResource(R.drawable.heartgrey);
            }
            Boolean res = db.getproductforaddedcart(Integer.parseInt(prodid.get(position)));


            if (res == false) {
                holder.addcart.setBackgroundResource(R.drawable.add_to_cart);
                addcartstatus = 0;
            } else {
                holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                holder.addcart.setEnabled(false);
                addcartstatus = 1;
            }
            holder.wishlisticon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (prefConfig.readLoginStatus()) {


                        if (!db.getproductforwishlist(Integer.parseInt(prodid.get(position)))) {
                            db.addRecordwishlist(String.valueOf(prodid.get(position)), prefConfig.readID());


                            Toast.makeText(getApplicationContext(), "Please wait adding item to wishlist.", Toast.LENGTH_SHORT).show();
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(ROOT_URL) //Setting the Root URL
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                            AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
                            Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata, String.valueOf(prodid.get(position)), prefConfig.readID());
                            //   Toast.makeText(mContext, " id = "+pid.get(position), Toast.LENGTH_SHORT).show();
                            call.enqueue(new Callback<generalstatusmessagePOJO>() {
                                @Override
                                public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                                    if (response.isSuccessful()) {
                                        int status = response.body().getStatus();
                                        String message = response.body().getMessage().toString();
                                        if (status == 1) {
                                            holder.wishlisticon.setBackgroundResource(R.drawable.heartfilled);
//                                View parentLayout = v.findViewById(android.R.id.content);
//                                Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
//                                View sbView = snackbar.getView();
//                                sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                                snackbar.show();

                                        } else {
                                            holder.wishlisticon.setBackgroundResource(R.drawable.heartgrey);
                                        }
                                    } else {
                                        Toast.makeText(getApplicationContext(), "" + response.message(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(), "Item already added to wishlist.", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "Kindly login first to make wishlist", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            if (SalePrice <Productprice) {
                holder.offpercenttv.setVisibility(View.VISIBLE);
                holder.discountedpricetv.setVisibility(View.VISIBLE);
                holder.orignalpricetv.setTextColor(Color.LTGRAY);
                int percentage = ((discoutnamount * 100) / Productprice);

                if (prodname.get(position).length() >= 47) {
                    String stringlimit = prodname.get(position).substring(0, 47);
                    holder.productname.setText(stringlimit + "...");
                } else {
                    holder.productname.setText(prodname.get(position) + "...");
                }

                holder.orignalpricetv.setText("PKR " + formatedproductprice);
                holder.orignalpricetv.setTextSize(10);
                holder.offpercenttv.setText(percentage + "%" + " OFF");
                holder.discountedpricetv.setText("PKR " + formatedsaleprice);
                holder.brandname.setText(prodbrand.get(position));
                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(mContext, "sp sale = "+Math.round(Float.parseFloat(offpercenttvv[position])), Toast.LENGTH_SHORT).show();
                        onAddProduct(Integer.parseInt(prodid.get(position)), "1", prefConfig.readID(), "0", prodname.get(position), Float.toString(Math.round(Float.parseFloat(proddiscount.get(position)))), prodimage.get(position), prodbrand.get(position), "", "","0");
//                        Toast.makeText(mContext, "Added to cart!"+Dashboard.prefConfig.readID(), Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setEnabled(false);
                        addcartstatus = 1;
                    }
                });

                SalePrice = 1;
            } else {
                if (prodname.get(position).length() >= 47) {
                    String stringlimit = prodname.get(position).substring(0, 47);
                    holder.productname.setText(stringlimit + "...");
                } else {
                    holder.productname.setText(prodname.get(position) + "...");
                }


                holder.orignalpricetv.setText("PKR " + formatedsaleprice);
                holder.orignalpricetv.setTextColor(Color.BLACK);
                holder.orignalpricetv.setTextSize(20);
                holder.orignalpricetv.setTextColor(Color.BLACK);
                holder.orignalpricetv.setTextSize(20);
                holder.orignalpricetv.setPaintFlags(0);
                holder.offpercenttv.setVisibility(View.GONE);
                holder.discountedpricetv.setVisibility(View.GONE);
                holder.brandname.setText(prodbrand.get(position));
//                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(Integer.parseInt(prodid.get(position)), "1", prefConfig.readID(), "0", prodname.get(position), Float.toString(Math.round(Float.parseFloat(prodprice.get(position)))), prodimage.get(position), prodbrand.get(position), "", "","0");//
                        //           Toast.makeText(mContext, "sp "+Math.round(Float.parseFloat(orignalpricetvv[position])), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(getApplicationContext(), "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setEnabled(false);
                        addcartstatus = 1;

                    }
                });

            }


//


            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.brandnewdefault);
            requestOptions.error(R.drawable.imagecommingsoon);
            requestOptions.override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            Glide.with(c).load(prodimage.get(position))
                    .apply(requestOptions)
                    .into(holder.Productimage);
//            Glide.with(c).load(imageUrl[position])
//                    .placeholder(R.drawable.waterempty)
//                    .error(R.drawable.waterhalf)
//                    .into(holder.imageView);

            holder.allcons.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


//
                    try {
                        Intent intent = new Intent(c, ProductDetailActivity.class);
                        intent.putExtra("title", prodname.get(position));

                        intent.putExtra("price", prodprice.get(position));
                        intent.putExtra("productid", prodid.get(position));
                        intent.putExtra("prodbrand", prodbrand.get(position));
                        intent.putExtra("prodwishlist", prodwishlist.get(position));
                        intent.putExtra("addcartstatus", addcartstatus);
                        startActivity(intent);
                    } catch (Exception e) {

                    }

//                    Toast.makeText(mContext, "product id ="+pid[position], Toast.LENGTH_SHORT).show();
                }
            });
            return view;
        }
    }

    static class ViewHolder {
        ImageView Productimage, addcart, wishlisticon;
        TextView productname, orignalpricetv, offpercenttv, discountedpricetv, brandname;
        ConstraintLayout allcons;

    }

    public void showdialogsort() {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.customsortlayout, null);
        dialogBuilder.setView(dialogView);
        View view = null;
        dialogBuilder.setTitle("Sort");
        rg = (RadioGroup) dialogView.findViewById(R.id.radioGroup);

        dialogBuilder.setCancelable(true);
        final View finalView = view;

        final AlertDialog b = dialogBuilder.create();

        RadioButton newest = dialogView.findViewById(R.id.radioButton);
        RadioButton pricedesc = dialogView.findViewById(R.id.radioButton2);
        RadioButton priceasc = dialogView.findViewById(R.id.radioButton3);
        RadioButton alphaasc = dialogView.findViewById(R.id.radioButton4);
        RadioButton alphadesc = dialogView.findViewById(R.id.radioButton5);
        RadioButton popular = dialogView.findViewById(R.id.radioButton6);
        RadioButton featured = dialogView.findViewById(R.id.radioButton7);
        RadioButton bestselling = dialogView.findViewById(R.id.radioButton9);
        if(Sortvalue.equals("newest"))
            newest.setChecked(true);
        else if(Sortvalue.equals("pricedesc"))
            pricedesc.setChecked(true);
        else if(Sortvalue.equals("priceasc"))
            priceasc.setChecked(true);
        else if(Sortvalue.equals("alphaasc"))
            alphaasc.setChecked(true);
        else if(Sortvalue.equals("alphadesc"))
            alphadesc.setChecked(true);
        else if(Sortvalue.equals("popular"))
            popular.setChecked(true);
        else if(Sortvalue.equals("bestselling"))
            bestselling.setChecked(true);
        else if(Sortvalue.equals("featured"))
            featured.setChecked(true);

        Button apply = dialogView.findViewById(R.id.button8);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(CategoryActivity.this);
                progressDialog.setMessage("Loading..."); // Setting Message
                progressDialog.setTitle(""); // Setting Title
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
                progressDialog.show(); // Display Progress Dialog
                progressDialog.setCancelable(true);
                int selectedvalue = rg.getCheckedRadioButtonId();
                radioButton = dialogView.findViewById(selectedvalue);

                Sortvalue = radioButton.getText().toString();
                if (radioButton.getText().toString().equals("Newest")) {
                    Sortvalue = "newest";
                } else if (radioButton.getText().toString().equals("Price : High to Low")) {
                    Sortvalue = "pricedesc";
                } else if (radioButton.getText().toString().equals("Price : Low to High")) {
                    Sortvalue = "priceasc";
                } else if (radioButton.getText().toString().equals("A to Z")) {
                    Sortvalue = "alphaasc";
                } else if (radioButton.getText().toString().equals("Z to A")) {
                    Sortvalue = "alphadesc";
                } else if (radioButton.getText().toString().equals("Popular")) {
                    Sortvalue = "popular";
                } else if (radioButton.getText().toString().equals("Featured")) {
                    Sortvalue = "featured";
                } else if (radioButton.getText().toString().equals("Best Sellling")) {
                    Sortvalue = "bestselling";
                }

                Log.d("selected sort", Sortvalue);
                Loadproducts(Sortvalue);
//                Toast.makeText(CategoryActivity.this, " a "+Sortvalue, Toast.LENGTH_SHORT).show();
                b.dismiss();
            }
        });
        b.show();

    }

    public void showdialogfilter() {

        jo = new JSONObject();
        jo2 = new JSONObject();
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogViewfilter = inflater.inflate(R.layout.filterexpandalelist, null);
        Button clearfilter = dialogViewfilter.findViewById(R.id.button28);
        if(filterselectedlist.size()!=0)
        {
            clearfilter.setVisibility(View.VISIBLE);
        }
        else
        {
            clearfilter.setVisibility(View.GONE);
        }

        dialogBuilder.setView(dialogViewfilter);
        View view = null;
        dialogBuilder.setTitle("Filter");

        dialogBuilder.setCancelable(true);
        final View finalView = view;

        final AlertDialog b = dialogBuilder.create();
        clearfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterselectedlist.clear();
                filtertext.setTextColor(getResources().getColor(R.color.colorPrimary));
                filtertext.setText("Filter");
                filterimage.setBackgroundResource(R.drawable.funnel);

                Loadproducts(Sortvalue);
                filterflag = false;
                b.dismiss();
            }
        });
        expListView = dialogViewfilter.findViewById(R.id.lvExp);
//        prepareListData();
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        listDataChildid = new HashMap<String, List<Integer>>();


        final Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FilterGetApi filterGetApi = retrofit2.create(FilterGetApi.class);
        Call<Filterlist> call2 = filterGetApi.getDetails(MainActivity.Tokendata, catid);

        call2.enqueue(new Callback<Filterlist>() {
            @Override
            public void onResponse(Call<Filterlist> call, Response<Filterlist> response) {
                if (response.isSuccessful()) {
                    int status = response.body().getStatus();
//                    Toast.makeText(CategoryActivity.this, "aa\n " + response.body().getStatus(), Toast.LENGTH_SHORT).show();
                    Log.e("filterresponse", "" + response.body().toString());
                    if (status == 1) {
                        filterlists = response.body().getFilters();
                        Log.d("filter response", "" + filterlists);
                        for (int i = 0; i < filterlists.size(); i++) {
                            featurename.add(filterlists.get(i).getFeatureName());
                            listDataHeader.add(filterlists.get(i).getFeatureName());
                            featurenamealter.add(featurename.get(i).replaceAll("", "_").toLowerCase());
                            filteroption = filterlists.get(i).getOptions();

                            featurenamealteroption = new ArrayList<>();
                            featureoptionid = new ArrayList<>();
                            for (int k = 0; k < filteroption.size(); k++) {
                                featurenamealteroption.add(filteroption.get(k).getFeatureOptionName());
                                featureoptionid.add(filteroption.get(k).getFeatureOptionId());
                            }
//                        Toast.makeText(CategoryActivity.this, ""+listDataHeader.get(i)+" \n"+featurenamealteroption, Toast.LENGTH_SHORT).show();
                            listDataChild.put(listDataHeader.get(i), featurenamealteroption);
                            listDataChildid.put(listDataHeader.get(i), featureoptionid);
//                        System.out.println(Arrays.asList(listDataChild));

                        }
//                    filterlists.get(1).getFeatureName();
//                    filteroption = filterlists.get(0).getOptions();
//                    filteroption.get(1).getFeatureOptionName();
//                    Toast.makeText(CategoryActivity.this, "sss "+filterlists.get(0).getFeatureName()+"\n"+filteroption.get(1).getFeatureOptionName(), Toast.LENGTH_SHORT).show();

                        listAdapter = new ExpandableListAdapter(getApplicationContext(), listDataHeader, listDataChildid, listDataChild, featureoptionid, featurenamealteroption);

                        // setting list adapter
                        expListView.setAdapter(listAdapter);
                    } else {
                        Toast.makeText(CategoryActivity.this, "No filter response", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(getApplicationContext(), "" + response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Filterlist> call, Throwable t) {

            }
        });


        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
//                 Toast.makeText(getApplicationContext(),
//                 "Group Clicked " + listDataHeader.get(groupPosition),
//                 Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Expanded",
//                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Collapsed",
//                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @SuppressLint("ResourceAsColor")
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
//            if(txtListChild.isChecked())
//            {
//                 lblListHeader.setTextColor(R.color.colorPrimary);
//            }
//            else
//            {
//                lblListHeader.setTextColor(R.color.Azure);
//            }
//                Toast.makeText(
//                        getApplicationContext(),
//                        "hello"+listDataHeader.get(groupPosition)
//                                + " : "
//                                + listDataChild.get(
//                                listDataHeader.get(groupPosition)).get(
//                                childPosition), Toast.LENGTH_SHORT).show();

//                txtListChild.setChecked(true);
//                Toast.makeText(getApplicationContext(), "hahah", Toast.LENGTH_SHORT).show();
//                    Log.d("on child",listDataHeader.get(groupPosition)
//                                + " : "
//                                + listDataChild.get(
//                                listDataHeader.get(groupPosition)).get(
//                                childPosition));
                return false;
            }
        });

//        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
//                return false;
//            }
//        });


        Button applyfilter = dialogViewfilter.findViewById(R.id.button13);
        applyfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//            ArrayList<String> featureoption = new ArrayList<>();
//                featureoption.add("1");
//                featureoption.add("2");
//                featureoption.add("3");
//                featureoption.add("4");
//            ArrayList<String> availabilities = new ArrayList<>();
//                availabilities.add("2-4 hours");
//                availabilities.add("20-40 hours");
//                availabilities.add("200-400 hours");
//                Gson gson = new Gson();
//                JsonArray array = new JsonArray();
//                JsonArray featureoption = new JsonArray();
//
//                featureoption.add(new JsonPrimitive("1"));
//                featureoption.add(new JsonPrimitive("2"));
//                featureoption.add(new JsonPrimitive("3"));
//                featureoption.add(new JsonPrimitive("4"));
//                featureoption.add(new JsonPrimitive("5"));
//                JsonArray availabilities = new JsonArray();
//
//                availabilities.add(new JsonPrimitive("2-4 hours"));
//                availabilities.add(new JsonPrimitive("20-40 hours"));
//                availabilities.add(new JsonPrimitive("200-400 hours"));
//
//
////                for(int i=0;i<5;i++)
////                    array.add(child);
//
//
//                JsonObject jsonObject = new JsonObject();
//                jsonObject.add("featureOptionList", featureoption);
//                JsonObject jsonObject1 = new JsonObject();
//                jsonObject.add("availabilities",availabilities);
//
////                System.out.println(jsonObject);
//                Log.d("json object", ""+jsonObject);

                try {


                    mainObj.put("skip", 0);
                    mainObj.put("perPage", 50);
                    mainObj.put("sort", "newest");
                    mainObj.put("categoryid", catid);
                    mainObj.put("featureOptionList", jo);
                    mainObj.put("availabilities", jo2);
                    Log.d("json object", "" + mainObj);

//                    Toast.makeText(CategoryActivity.this, "DSD"+jo.length(), Toast.LENGTH_SHORT).show();

                    if(filterselectedlist.size() == 0 )
                    { jo = new JSONObject();
                        Toast.makeText(CategoryActivity.this, "No filter selected", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        progressDialog = new ProgressDialog(CategoryActivity.this);
                        progressDialog.setMessage("Loading..."); // Setting Message
                        progressDialog.setTitle(""); // Setting Title
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
                        progressDialog.show(); // Display Progress Dialog
                        progressDialog.setCancelable(true);
                        fetchMoreItemsSorting(0, "newest", "" + mainObj);
                        b.dismiss();
                        filtertext.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                        filtertext.setText("Filter ("+filterselectedlist.size()+")");
                        filterimage.setBackgroundResource(R.drawable.funnel_activeselected);

                    }
                    } catch (JSONException e) {
                    e.printStackTrace();
                }



            }
        });
        b.show();
    }


    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();


        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FilterGetApi filterGetApi = retrofit2.create(FilterGetApi.class);
        Call<Filterlist> call2 = filterGetApi.getDetails(MainActivity.Tokendata, "1031");
        call2.enqueue(new Callback<Filterlist>() {
            @Override
            public void onResponse(Call<Filterlist> call, Response<Filterlist> response) {

                int status = response.body().getStatus();
//                Toast.makeText(CategoryActivity.this, "aa\n " + response.body().getStatus(), Toast.LENGTH_SHORT).show();
//                Log.e("filterresponse",""+response.body().toString());
                if (status == 1) {
                    filterlists = response.body().getFilters();
                    for (int i = 0; i < filterlists.size(); i++) {
                        featurename.add(filterlists.get(i).getFeatureName());
                        listDataHeader.add(filterlists.get(i).getFeatureName());
                        featurenamealter.add(featurename.get(i).replaceAll("", "_").toLowerCase());
                        filteroption = filterlists.get(i).getOptions();

                        List<String> featurenamealteroption = new ArrayList<>();
                        for (int k = 0; k < filteroption.size(); k++) {
                            featurenamealteroption.add(filteroption.get(k).getFeatureOptionName());
                        }
//                        Toast.makeText(CategoryActivity.this, "" + listDataHeader.get(i) + " \n" + featurenamealteroption, Toast.LENGTH_SHORT).show();
                        listDataChild.put(listDataHeader.get(i), featurenamealteroption);
                        System.out.println(Arrays.asList(listDataChild));

                    }
//                    filterlists.get(1).getFeatureName();
//                    filteroption = filterlists.get(0).getOptions();
//                    filteroption.get(1).getFeatureOptionName();
//                    Toast.makeText(CategoryActivity.this, "sss "+filterlists.get(0).getFeatureName()+"\n"+filteroption.get(1).getFeatureOptionName(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(CategoryActivity.this, "No filter response", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<Filterlist> call, Throwable t) {

            }
        });


    }

    class ExpandableListAdapter extends BaseExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, List<String>> _listDataChild;
        private HashMap<String, List<Integer>> _listDataChildid;
        private ArrayList<String> listoptionname = new ArrayList<>();
        private ArrayList<Integer> listoptionid = new ArrayList<>();
        private final Set<Pair<Long, Long>> mCheckedItems = new HashSet<Pair<Long, Long>>();

        public ExpandableListAdapter(Context context, List<String> listDataHeader, HashMap<String, List<Integer>> listChildDataid,
                                     HashMap<String, List<String>> listChildData, ArrayList<Integer> listoptionid, ArrayList<String> listoptionname) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
            this._listDataChildid = listChildDataid;
            this.listoptionname = listoptionname;
            this.listoptionid = listoptionid;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }

        public Object getChildidtwo(int groupPosition, int childPosititon) {
            return this._listDataChildid.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final String childText = (String) getChild(groupPosition, childPosition);
            final int childtextID = (int) getChildidtwo(groupPosition, childPosition);


            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.filterlist_item, null);
            }

            txtListChild = (CheckBox) convertView
                    .findViewById(R.id.checkBox);
            TextView filterid = convertView.findViewById(R.id.textView77);

            txtListChild.setText(childText);
            final Pair<Long, Long> tag = new Pair<Long, Long>(
                    getGroupId(groupPosition),
                    getChildId(groupPosition, childPosition));
            txtListChild.setTag(tag);
            // set checked if groupId/childId in checked items
            txtListChild.setChecked(mCheckedItems.contains(tag));

            if(filterselectedlist.contains(childtextID))
            {
                txtListChild.setChecked(true);
            }
            txtListChild.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    final CheckBox cb = (CheckBox) v;
                    final Pair<Long, Long> tag = (Pair<Long, Long>) v.getTag();
                    if (cb.isChecked()) {
                        mCheckedItems.add(tag);
//                        Toast.makeText(
//                                getApplicationContext(),
//                                listDataHeader.get(groupPosition)
//                                        + " : "
//                                        + listDataChildid.get(
//                                        listDataHeader.get(groupPosition)).get(
//                                        childPosition), Toast.LENGTH_SHORT).show();

                        try {
                            jo.put(Integer.toString(filteid), Integer.toString(listDataChildid.get(
                                    listDataHeader.get(groupPosition)).get(
                                    childPosition)));
                            filterselectedlist.add(listDataChildid.get(
                                    listDataHeader.get(groupPosition)).get(childPosition));
                            filteid = filteid + 1;
//                            jo2.put("", "");
                            if (listDataHeader.get(groupPosition).equals("Shop By Availibility")) {

                                jo2.put(Integer.toString(avalibilityid + 1), listDataChild.get(
                                        listDataHeader.get(groupPosition)).get(
                                        childPosition));
                                avalibilityid = avalibilityid + 1;
                            }
//                            jo2.put("0", "2-4 hours");
//                            jo2.put("1", "20-40 hours");
//                            jo2.put("2", "200-400 hours");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

//                        int index = search( listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition),listoptionname);
//
//                        Toast.makeText(CategoryActivity.this, ""+listoptionid.get(index), Toast.LENGTH_SHORT).show();


                    } else {
                        mCheckedItems.remove(tag);
                        int index = filterselectedlist.indexOf(listDataChildid.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition));
//                        Toast.makeText(CategoryActivity.this, "sss"+index, Toast.LENGTH_SHORT).show();
                        filterselectedlist.remove(index);
                        if (filterselectedlist.isEmpty())
                            filterflag = false;
                        try {
                            jo.getJSONObject("featureOptionList").remove(String.valueOf(filteid));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                        Toast.makeText(
//                                getApplicationContext(),
//                                "hellojjjjjjjaaaaaaaa"+listDataHeader.get(groupPosition)
//                                        + " : "
//                                        + listDataChild.get(
//                                        listDataHeader.get(groupPosition)).get(
//                                        childPosition), Toast.LENGTH_SHORT).show();
                    }


                }
            });
            return convertView;
        }

        public Set<Pair<Long, Long>> getCheckedItems() {
            return mCheckedItems;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this._listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.filterlist_group, null);
            }

            lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                lblListHeader.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            }
            lblListHeader.setText(headerTitle);


            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    public static int search(String searchStr, ArrayList<String> aList) {

        boolean found = false;
        Iterator<String> iter = aList.iterator();
        String curItem = "";
        int pos = 0;

        while (iter.hasNext() == true) {
            pos = pos + 1;
            curItem = (String) iter.next();
            if (curItem.equals(searchStr)) {
                found = true;
                break;
            }

        }

        if (found == false) {
            pos = 0;
        }

        if (pos != 0) {
            System.out.println(searchStr + " City Found in position : " + pos);

        } else {

            System.out.println(searchStr + " City not found");
        }
        return pos;
    }

    public void fetchMoreItems(int count, String sort) {
//    Toast.makeText(this, "Loading..", Toast.LENGTH_SHORT).show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        CategoryProductApi api = retrofit.create(CategoryProductApi.class);
        Call<ProductExample> call = api.getDetails(MainActivity.Tokendata, catid, Integer.toString(count), Integer.toString(count), sort, prefConfig.readID());
        call.enqueue(new Callback<ProductExample>() {
            @Override
            public void onResponse(Call<ProductExample> call, Response<ProductExample> response) {
                if (response.isSuccessful()) {
                    int status = response.body().getStatus();
                    if (status == 1) {
                        list = response.body().getDetails();
//
//                Toast.makeText(CategoryActivity.this, ""+response.body(), Toast.LENGTH_SHORT).show();
                        if (list.size() > 0) {
                            View parentLayout = findViewById(android.R.id.content);
                            Snackbar snackbar = Snackbar.make(parentLayout, "Loading more products...", Snackbar.LENGTH_LONG);
                            View sbView = snackbar.getView();
                            sbView.setBackgroundColor(Color.rgb(161, 201, 19));
                            snackbar.show();

                            mImageUrls.clear();
                            for (int i = 0; i < list.size(); i++) {
                                prodname.add(list.get(i).getProductName());
                                prodprice.add(list.get(i).getProductPrice());
                                proddiscount.add(list.get(i).getProductSalePrice());
                                prodimage.add(list.get(i).getProductImages().getImageFile());

                                mImageUrls.add(prodimage.get(i));

                                prodid.add(list.get(i).getProductID().toString());
                                prodbrand.add(list.get(i).getBrandDisplayName());
                                prodwishlist.add(list.get(i).getProductWishlist());
                                isExistInCart.add(list.get(i).getIsExistInCart());
                            }
                            progressDialog.dismiss();

                            catproductgrid.deferNotifyDataSetChanged();
                            if (list.size() < 14) {
                                flag_loading = true;
                            } else {
                                flag_loading = false;
                            }
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<ProductExample> call, Throwable t) {
                Toast.makeText(CategoryActivity.this, "" + t, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public boolean fetchMoreItemsSorting(int count, String sort, String object) {
//    Toast.makeText(this, "Loading..", Toast.LENGTH_SHORT).show();
//        View parentLayout = findViewById(android.R.id.content);
//        Snackbar snackbar = Snackbar.make(parentLayout, "Loading Filter products...", Snackbar.LENGTH_LONG);
//        View sbView = snackbar.getView();
//        sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//        snackbar.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        CategoryFilterProduct api = retrofit.create(CategoryFilterProduct.class);
//        String ob = "{\"skip\":0,\"perPage\":14,\"sort\":\"newest\",\"featureOptionList\": {\"0\": \"1\",\"1\": 2},\"availabilities\": {\"0\": \"2 - 4 Working Days\"}}";

//        Toast.makeText(this, ""+object, Toast.LENGTH_SHORT).show();
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), object);
//        Toast.makeText(this, ""+object, Toast.LENGTH_SHORT).show();
        Log.d("ob", "" + object);
        Call<ProductExample> call = api.getDetails(MainActivity.Tokendata, body);
        call.enqueue(new Callback<ProductExample>() {
            @Override
            public void onResponse(Call<ProductExample> call, Response<ProductExample> response) {

//                Toast.makeText(CategoryActivity.this, "" + response.body(), Toast.LENGTH_SHORT).show();
                if (response.isSuccessful()) {
                    int status = response.body().getStatus();
                    if (status == 1) {
                        progressDialog.dismiss();
                        list = response.body().getDetails();
//                prodname = new String[list.size()];
//                prodprice = new String[list.size()];
//                prodoffper = new String[list.size()];
//                proddiscount = new String[list.size()];
//                prodimage = new String[list.size()];
//                proddiscription = new String[list.size()];
//                prodid = new String[list.size()];
//                prodbrand = new String[list.size()];
//
//                Toast.makeText(CategoryActivity.this, ""+response.body(), Toast.LENGTH_SHORT).show();
                        prodname.clear();
                        prodprice.clear();
                        proddiscount.clear();
                        prodimage.clear();
                        mImageUrls.clear();
                        prodid.clear();
                        prodbrand.clear();
                        prodwishlist.clear();
                        isExistInCart.clear();
                        for (int i = 0; i < list.size(); i++) {
                            prodname.add(list.get(i).getProductName());
                            prodprice.add(list.get(i).getProductPrice());
                            proddiscount.add(list.get(i).getProductSalePrice());
                            prodimage.add(list.get(i).getProductImages().getImageFile());

                            mImageUrls.add(prodimage.get(i));

                            prodid.add(list.get(i).getProductID().toString());
                            prodbrand.add(list.get(i).getBrandDisplayName());
                            prodwishlist.add(list.get(i).getProductWishlist());
                            isExistInCart.add(list.get(i).getIsExistInCart());
                        }
                        catproductgrid.setAdapter(null);
                        progressDialog.dismiss();

                        catproductgrid.setAdapter(dataImageAdapter);
                        filterflag = true;
                    } else {
                        progressDialog.dismiss();

                        Toast.makeText(CategoryActivity.this, "" + response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    progressDialog.dismiss();

                    Toast.makeText(CategoryActivity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }


            @Override
            public void onFailure(Call<ProductExample> call, Throwable t) {

            }
        });
        return true;
    }


    public  void populatestage1()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile & Tablets",2909,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer & Laptops",1032,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cameras & Accessories",55,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("TV & Video",2216,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Home Appliances",844,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Musical Instruments",2287,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Books & Stationery",3763,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming",2971,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fashion",3568,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Baby & Toys",3368,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Digital Store", 2445,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Daily Deal", 2719,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clearance Sale", 1583,0));
    }
    public  void populatemobiletablet()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile Phones", 1031,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tablets & eBook Readers", 578,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile Accessories",2125,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headsets & Earphones", 2486,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Smart Watches",2212,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tablet Accessories", 1047,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Prepaid Scratch Cards", 2550,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Refurbished Mobiles", 2520,0));
    }
    public  void populatemobileAccesories()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mobile Cover & Cases",2179,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Charger Cables & Docks", 2213,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power Banks",2353,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Screen Protectors", 2310,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Other Accessories", 3016,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Phone Stands", 2661,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Monopod & Selfie Sticks", 2687,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Battery Packs", 2214,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("VR Glasses", 3031,0));
    }

    public  void populatecomputerandlaptop()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptops", 1032,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Peripherals",3209,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer Components",2300,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Storage",2346,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Networking",2301,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer Accessories",2336,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Accessories",2461,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Printers", 2297,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Printers & Accessories",2278,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Computer Monitors", 1599,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Desktop Computers", 2014,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Software", 3483,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Development Tools", 2686,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Attendance Machines", 2999,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Laptops", 3070,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Bitcoin", 2721,0));
    }
    public  void populateperipherals()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pc Gaming Accessories", 2666,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headsets ", 2486,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mouse", 1763,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Keyboard", 1762,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mouse Pads", 2664,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Webcams & Chatpacks", 2033,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Multimedia Speaker", 1853,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Presenters", 3023,0));

    }

    public  void populatecomputercomponents()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Graphic Cards", 2162,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cooling Solutions", 2183,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("RAM/Memory", 1779,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Casing", 2181,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power Supply", 2182,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Motherboard", 2169,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Accessories", 2324,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Casing Fans", 2348,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("PC Cooling", 2480,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Combo Deals", 2424,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Sound Cards", 2283,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Processor", 2177,0));


    }

    public  void populatestorage()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("SSD", 2031,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("USB Flash drives", 1809,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("External Drives", 1815,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("HDD Adapters & Docks", 2477,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hard Drives", 1781,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Network Storage Devices", 2252,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("RAID Products", 2419,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Optical Drive", 2354,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Hard Drives", 2339,0));

    }
    public  void populatenetworking()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Switches", 2363,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Router", 2810,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Range Extenders & Adapters", 2732,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Networking Essentials", 2549,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Access Points", 2754,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power over Ethernet (PoE)", 2779,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Antennas", 2778,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wireless Adapters", 3207,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Modems", 2731,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Powerline Adapters", 3208,0));

    }

    public  void populatecomputeraccessories()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cables", 2673,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("USB Hubs", 2457,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mac Accessories", 1254,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Docks", 2442,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("TV Tuner", 2180,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("USB Port Cards", 2432,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Chairs", 3067,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gaming Bundles", 2436,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Paper & CD Shredders", 3014,0));

    }



    public  void populatelaptopaccessories() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Bags", 2337,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Chargers", 2340,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Other accessories for Laptops", 3082,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Cooling Pads & Tablets", 2338,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Laptop Batteries", 2341,0));
    }
    public  void populateprintersandaccessories() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Printer Toners & Cartridges", 2296,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Other Printer Accessories", 2998,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("3D Printing", 2430,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Price Tag Guns & Cash Drawers", 2997,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Barcode Labels & Printer Rolls", 2996,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Barcode Scanners Accessories", 2995,0));
    }






    public  void populatecamera()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Digital Cameras", 637,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DSLR Cameras", 747,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DSLM Cameras", 3004,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Accessories", 1511,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Surveillance Cameras", 2269,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Handy Cams & Camcorders", 1315,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Drones", 2351,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Gimbal Stabilizer", 3140,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Instant Cameras", 3325,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cameras Deals", 2227,0));


    }



    public  void populatecamerasandaccessories()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Lighting & Studio",2243,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Filters", 2274,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Flash & Triggers", 2250,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Memory Cards", 2225,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("GoPro Accessories", 2239,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tripods", 2234,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Video Tripods", 2236,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Bags & Cases", 2242,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Rigs & Support",2244,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tripod Heads",2238,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Sliders", 2418,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Straps", 2249,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio Equipment", 2251,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Lens Skin", 2802,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Camera Battery Grips", 2273,0));

    }





    public  void populatesurveillancecameras() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Security Cameras", 2302,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Security Camera Accessories", 3012,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DVR", 2303,0));
    }

    public  void populatetvandvideo() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Televisions", 112,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Home Audio & Video", 1251,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("TV Accessories", 1241,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Projectors", 152,0));
    }
    public  void populatehomeaudioandvideo() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Speakers", 978,0));


        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Home Theatre", 1097,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Digital Media Player", 660,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Amplifiers & AV Receivers", 1480,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio & Video Cables", 2680,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Video Glasses", 151,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hi-Fi System", 1250,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DVD & Blu-Ray Players", 2478,0));
    }
    public  void populatetvaccesories() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio & Video Cables", 2680,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wi-Fi Dongle", 2501,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wireless Keyboard", 2502,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("3D Glasses", 2258,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Remote Controls", 2256,0));
    }
    public  void populatehomeappliances() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Small Appliances", 2503,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Large Appliances", 2504,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Personal Care", 2325,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hardware & Tools",  2827,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clinical Products",  3150,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Telephones Video Phones Door Phones",  2559,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Lighting,Sockets & Switches",  2874,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Indoor & Outdoor Living Products",  2799,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Smart Home Gadgets",  3033,0));
    }
    public  void populatesmallappliances() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Appliances",  848,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Utensils",  3142,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Induction Stoves",  2529,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Vacuum Cleaner",  1279,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Heater",  2475,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Iron",  1306,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Insect Killer",  1267,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Garment Steamer",  2154,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("UPS Digital/Manual",  2507,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Stabilizer",  2568,0));
    }
    public  void populatekitchenappliances()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Food Steamer", 3248,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kneading Machines", 3252,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Rice Cooker", 3250,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Coffee Maker/Grinder", 1945,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Blender Grinder", 1302,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Juicer Blender", 1943,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electric Kettle", 1961,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Sandwich Maker", 1960,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Meat Mincer Multi Chopper", 1883,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Grinder/Chopper", 1948,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Toaster", 1958,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Egg Beater/Mixer", 1946,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Hand Blender", 1348,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Food Processor", 1956,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Accessories", 2489,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fruit Cutter/Vegetable Cutter", 2459,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Juicer/Extractor", 1944,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fryers", 1985,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pop Corn Maker", 2471,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Ice Cream Machine", 2465,0));

    }
    public  void populatelargeappliances() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Refrigerator",  849,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Freezers",  3243,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Air Conditioners",  846,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Washing Machine",  850,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Dryers & Spinners",  3088,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Water Dispenser",  2104,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Humidifiers",  3148,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Air Purifiers",  3149,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Ovens",  3244,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kitchen Hood",  3068,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Gas Hobs",  3069,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Dish Washer",  2473,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Room Air Cooler",  2718,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fan",  2494,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Generator ",  2506,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electric Geysers & Gas Geysers",  2801,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Barbecue Grill",  2456,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pressure Washer",  2811,0));
    }
    public  void populateovens() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Built-In Microwave Oven",  3246,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Built-In Oven",  3245,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Microwave Oven",  1954,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Oven Toaster",  1957,0));
    }
    public  void populatepersonalcare() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Massager",  3166,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Body Weight Scale",  2487,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Shaver/Hair Straightener/Haier Dryer/Grooming Kit",  2035,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Hand Wash Dispenser",  2491,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Medical Product & Foot Massager",  2490,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Toothbrush",  2662,0));
    }
    public  void populatehardwaretools() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clamp Multi-Meter",  2851,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Thimble Pliers",  2850,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tool Kits",  2856,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Measuring Meters",  2877,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Tool Accessories",  2873,0));
    }
    public  void populatemusicalinstruments() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitars",  2288,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitars & Accessories",  2763,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Studio Equipment",  2775,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Keyboards & Pianos",  2716,1));
    }
    public  void populateguitarsandaccessories() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitar’s Pedals & Processors",  2926,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitar’s Amplifiers",  2899,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitar Bags",  2916,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Guitar Amplifications",  2924,0));
    }
    public  void populatestudioequipments()
    {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Audio Interfaces", 3096,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("DJ Instruments", 2800,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Microphones", 2753,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wireless Mic System ", 2698,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Microphone Accessories", 2700,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Power Amplifier", 2695,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Professional Speakers", 2690,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mixers", 2693,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Effect & Signal Processors", 3098,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Portable PA Systems", 2696,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Studio Monitors", 2702,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headphones", 2517,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Headphones Amplifier", 2704,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cable", 2708,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Stands", 2706,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Rack Cases", 2707,0));
    }
    public  void populatekeyboardandpiano() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Electronic Keyboards",  2927,0));
    }
    public  void populatebooksandstationery() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Books",  2870,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Stationery",  3765,1));
    }
    public  void populatebooks() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Children",  3383,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Autobiographies",  3129,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Fiction",  3099,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Business & Money",  3137,0));
    }
    public  void populatestationery() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Office Supplies",  3964,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("School Supplies",  3963,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Paper & Paper Products",  3965,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Business & Money",  3137,0));
    }
    public  void populategaming() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S",  2947,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("PlayStation 4",  2630,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox 360",  2633,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Play Station VR",  2991,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("PlayStation 4 Pro",  2950,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox Project Scorpio",  3010,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Nintendo Switch",  3006,0));
    }
    public  void populatexboxones() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S Console",  2947,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S Games",  2948,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One S Accessories",  2949,0));
    }
    public  void populateplaystation4() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Playstation 4 Console", 2630,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Playstation 4 Accessories", 2631,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Playstation 4 Games", 2632,0));
    }
    public  void populatexbox360() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One Consoles", 2629,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One Accessories", 2628,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Xbox One Games", 2627,0));
    }
    public  void populatebabyandtoys() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Toys", 1028,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mother & Baby", 3032,1));
    }
    public  void populatemotherandbaby() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Baby Gear", 3742,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Feeding", 3620,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Diapering & Potty", 3622,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Personal Care", 2325,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Diapering & Potty", 3622,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Pacifiers & Accessories", 3658,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Baby Furniture", 3655,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clothing & Accessories", 3740,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Mattresses & Bedding", 3657,0));
    }

    public  void populatefashion() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Women", 2409,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Men", 2408,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kids", 2656,1));
    }
    public  void populatemen() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Clothing", 2576,1));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Watches", 678,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Shoes and Slippers", 2355,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Perfumes", 2414,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Ties", 2747,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Jackets/Hoodies/Coats", 2196,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Sunglasses", 2416,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Cuff Links", 2735,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Socks", 2493,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Wallets & Card Holders", 2681,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Belts", 2426,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Jewls", 2876,0));

    }
    public  void populateclothing() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("T-shirts", 3733,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kurta", 2362,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Kameez Shalwar", 2906,0));
    }
    public  void populatekids() {
        menuItemList.clear();
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Boys", 2657,0));
        menuItemList.add(new com.homeshopping.admin.homeshoping10.Class.MenuItem("Girls", 2658,0));
    }



    public static void justifyListViewHeightBasedOnChildren (ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        Log.d("adapter count",""+adapter.getCount());
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
            Log.d("heigt in loop",""+totalHeight);
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) constraintLayout84.getLayoutParams();
////        lp.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
//lp.horizontalBias = Float.parseFloat(Integer.toString(totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1))));
//        constraintLayout84.setLayoutParams(lp);
//        constraintLayout84.requestLayout();
        listView.setLayoutParams(par);
        listView.requestLayout();
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(constraintLayout90);
        Log.d("heigt",""+par.height);
        constraintSet.constrainHeight(R.id.constraintLayout90,par.height);
//        constraintSet.setVerticalBias(R.id.constraintLayout84,7000.0f);
        constraintSet.applyTo(constraintLayout90);
    }

}

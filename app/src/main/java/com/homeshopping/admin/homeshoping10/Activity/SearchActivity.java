package com.homeshopping.admin.homeshoping10.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.SearchSuggestionsAdapter;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.arlib.floatingsearchview.util.Util;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Data.ColorSuggestion;
import com.homeshopping.admin.homeshoping10.Data.ColorWrapper;
import com.homeshopping.admin.homeshoping10.Data.DataHelper;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Database.SearchDataBase;
import com.homeshopping.admin.homeshoping10.Fragment.BaseExampleFragment;
import com.homeshopping.admin.homeshoping10.Interfaces.findifysugessionAPI;
import com.homeshopping.admin.homeshoping10.POJO.findifysugestionPOJO;
import com.homeshopping.admin.homeshoping10.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Data.DataHelper.sColorSuggestions;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.FIND_SUGGESTION_SIMULATED_DELAY;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class SearchActivity extends AppCompatActivity implements BaseExampleFragment.BaseExampleFragmentCallbacks{
    static List<findifysugestionPOJO> sugessionlist = new ArrayList<findifysugestionPOJO>();
    private boolean mIsDarkSearchTheme = false;

    private String mLastQuery = "";
    private FloatingSearchView mSearchView;
    private final String TAG = "BlankFragment";
    private Tracker mTracker;
    SearchDataBase db;
    ListView search_listview;
    ArrayList<String> searchname,searchtime;
    ArrayList<Integer> searchindex;
    TextView searchtitle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        db = new SearchDataBase(getApplicationContext());
        this.setTitle("Search");
        mSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);
        mSearchView.setSearchFocused(false);
        setupDrawer();
        setupSearchBar();
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Search Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        searchname = new ArrayList<>();
        searchtime = new ArrayList<>();
        searchindex = new ArrayList<>();
        searchtitle  = findViewById(R.id.searchtitle);
        search_listview = findViewById(R.id.search_listview);
        Cursor cursor = db.getsearchitems();
        if(cursor.getCount()!=0)
        {
            searchindex.clear();
            searchtime.clear();
            searchname.clear();
            if(cursor.moveToFirst())
            {
                do {
                    searchindex.add(cursor.getInt(0));
                    searchname.add(cursor.getString(1));
                    searchtime.add(cursor.getString(2));
                }while (cursor.moveToNext());
                MyListAdapter myListAdapter = new MyListAdapter(SearchActivity.this , searchindex,searchname,searchtime);
                search_listview.setAdapter(myListAdapter);
                searchtitle.setText("Search History");

            }
        }
        else
        {
            search_listview.setAdapter(null);
            searchtitle.setText("No Search History");
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);
        finish();

    }

    private void setupSearchBar() {
        mSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {

            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ROOT_URL) //Setting the Root URL
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                findifysugessionAPI api = retrofit.create(findifysugessionAPI.class);
                Call<List<findifysugestionPOJO>> call = api.getDetails(MainActivity.Tokendata,newQuery);
                call.enqueue(new Callback<List<findifysugestionPOJO>>() {
                    @Override
                    public void onResponse(Call<List<findifysugestionPOJO>> call, Response<List<findifysugestionPOJO>> response) {
                        if (response.isSuccessful())
                        {
                            try {
                                sugessionlist.clear();
                                sugessionlist = response.body();

                                for (int  i = 0 ; i <sugessionlist.size();i++)
                                {

                                    sColorSuggestions.add(new ColorSuggestion(sugessionlist.get(i).getValue()));


                                }
                            }
                            catch (Exception e)
                            {
                                Log.d("Null list",""+e);
                            }
                        }
                        else
                        {
//                            Toast.makeText(getApplicationContext(), ""+response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<findifysugestionPOJO>> call, Throwable t) {

                    }
                });
//                Toast.makeText(context, ""+newQuery, Toast.LENGTH_SHORT).show();
                if (!oldQuery.equals("") && newQuery.equals("")) {
                    mSearchView.clearSuggestions();
                } else {

                    //this shows the top left circular progress
                    //you can call it where ever you want, but
                    //it makes sense to do it when loading something in
                    //the background.
                    mSearchView.showProgress();

                    //simulates a query call to a data source
                    //with a new query.
                    DataHelper.findSuggestions(getApplicationContext(), newQuery, 5,
                            FIND_SUGGESTION_SIMULATED_DELAY, new DataHelper.OnFindSuggestionsListener() {

                                @Override
                                public void onResults(List<ColorSuggestion> results) {

                                    //this will swap the data and
                                    //render the collapse/expand animations as necessary
                                    mSearchView.swapSuggestions(results);

                                    //let the users know that the background
                                    //process has completed
                                    mSearchView.hideProgress();
                                }
                            });
                }

                Log.d(TAG, "onSearchTextChanged()");
            }
        });

        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(final SearchSuggestion searchSuggestion) {

                ColorSuggestion colorSuggestion = (ColorSuggestion) searchSuggestion;
                DataHelper.findColors(getApplicationContext(), colorSuggestion.getBody(),
                        new DataHelper.OnFindColorsListener() {

                            @Override
                            public void onResults(List<ColorWrapper> results) {
                                //show search results
                            }

                        });
                Log.d(TAG, "onSuggestionClicked()");

                mLastQuery = searchSuggestion.getBody();
                if(mLastQuery.equals("")||mLastQuery.equals("."))
                {
                    Toast.makeText(getApplicationContext(), "Invalid search.", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => "+c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(c.getTime());
                    db.addRecordsearch(mLastQuery,formattedDate);
                    Intent  intent = new Intent(getApplicationContext(), SearchResultActivity.class);
                    intent.putExtra("searchkeyword",mLastQuery);
                    startActivity(intent);
                    mSearchView.clearSuggestions();
                    mSearchView.clearSearchFocus();
                    finish();
                }
//                Toast.makeText(context, ""+mLastQuery, Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onSearchAction(String query) {
                mLastQuery = query;

                DataHelper.findColors(getApplicationContext(), query,
                        new DataHelper.OnFindColorsListener() {


                            @Override
                            public void onResults(List<ColorWrapper> results) {
                                //show search results
                            }

                        });
                Log.d(TAG, "onSearchAction()");
                if(mLastQuery.equals("")||mLastQuery.equals("."))
                {
                    Toast.makeText(getApplicationContext(), "Invalid search.", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Calendar c = Calendar.getInstance();
                    System.out.println("Current time => "+c.getTime());

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String formattedDate = df.format(c.getTime());
                    db.addRecordsearch(mLastQuery,formattedDate);
                    Intent  intent = new Intent(getApplicationContext(), SearchResultActivity.class);
                    intent.putExtra("searchkeyword",mLastQuery);
                    startActivity(intent);
                    finish();
                }
//                Toast.makeText(getApplicationContext(), ""+mLastQuery, Toast.LENGTH_SHORT).show();
            }
        });

        mSearchView.setOnFocusChangeListener(new FloatingSearchView.OnFocusChangeListener() {
            @Override
            public void onFocus() {

                //show suggestions when search bar gains focus (typically history suggestions)
                try {
                    mSearchView.swapSuggestions(DataHelper.getHistory(getApplicationContext(), sugessionlist.size()));

                    Log.d(TAG, "onFocus()");
                }catch (Exception e)
                {
                    Log.d("Null list",""+e);
                }

            }

            @Override
            public void onFocusCleared() {

                //set the title of the bar so that when focus is returned a new query begins
                mSearchView.setSearchBarTitle(mLastQuery);

                //you can also set setSearchText(...) to make keep the query there when not focused and when focus returns
                //mSearchView.setSearchText(searchSuggestion.getBody());

                Log.d(TAG, "onFocusCleared()");
            }
        });


        //handle menu clicks the same way as you would
        //in a regular activity
//        mSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
//            @Override
//            public void onActionMenuItemSelected(MenuItem item) {
//
//                if (item.getItemId() == R.id.action_change_colors) {
//
//                    mIsDarkSearchTheme = true;
//
//                    //demonstrate setting colors for items
//                    mSearchView.setBackgroundColor(Color.parseColor("#787878"));
//                    mSearchView.setViewTextColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setHintTextColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setActionMenuOverflowColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setMenuItemIconColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setLeftActionIconColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setClearBtnColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setDividerColor(Color.parseColor("#BEBEBE"));
//                    mSearchView.setLeftActionIconColor(Color.parseColor("#e9e9e9"));
//                } else {
//
//                    //just print action
//                    Toast.makeText(getActivity().getApplicationContext(), item.getTitle(),
//                            Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });
//
//        //use this listener to listen to menu clicks when app:floatingSearch_leftAction="showHome"
//        mSearchView.setOnHomeActionClickListener(new FloatingSearchView.OnHomeActionClickListener() {
//            @Override
//            public void onHomeClicked() {
//
//                Log.d(TAG, "onHomeClicked()");
//            }
//        });

        /*
         * Here you have access to the left icon and the text of a given suggestion
         * item after as it is bound to the suggestion list. You can utilize this
         * callback to change some properties of the left icon and the text. For smarteist, you
         * can load the left icon images using your favorite image loading library, or change text color.
         *
         *
         * Important:
         * Keep in mind that the suggestion list is a RecyclerView, so views are reused for different
         * items in the list.
         */
        mSearchView.setOnBindSuggestionCallback(new SearchSuggestionsAdapter.OnBindSuggestionCallback() {
            @Override
            public void onBindSuggestion(View suggestionView, ImageView leftIcon,
                                         TextView textView, SearchSuggestion item, int itemPosition) {
                ColorSuggestion colorSuggestion = (ColorSuggestion) item;

                String textColor = mIsDarkSearchTheme ? "#ffffff" : "#000000";
                String textLight = mIsDarkSearchTheme ? "#bfbfbf" : "#a1c913";

                if (colorSuggestion.getIsHistory()) {
                    leftIcon.setImageDrawable(ResourcesCompat.getDrawable(getApplicationContext().getResources(),
                            R.drawable.ic_history_black_24dp, null));

                    Util.setIconColor(leftIcon, Color.parseColor(textColor));
                    leftIcon.setAlpha(.36f);
                } else {
                    leftIcon.setAlpha(0.0f);
                    leftIcon.setImageDrawable(null);
                }

                textView.setTextColor(Color.parseColor(textColor));
                String text = colorSuggestion.getBody()
                        .replaceFirst(mSearchView.getQuery(),
                                "<font color=\"" + textLight + "\">" + mSearchView.getQuery() + "</font>");
                textView.setText(Html.fromHtml(text));
            }

        });
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        mSearchView.setTranslationY(verticalOffset);
    }





    @Override
    public boolean onActivityBackPress() {
        //if mSearchView.setSearchFocused(false) causes the focused search
        //to close, then we don't want to close the activity. if mSearchView.setSearchFocused(false)
        //returns false, we know that the search was already closed so the call didn't change the focus
        //state and it makes sense to call supper onBackPressed() and close the activity
        if (!mSearchView.setSearchFocused(false)) {
            return false;
        }
        return true;
    }

    private void setupDrawer() {
        attachSearchViewActivityDrawer(mSearchView);
    }


    @Override
    public void attachSearchViewActivityDrawer(FloatingSearchView searchView) {

    }



    @Override
    public void onAttachSearchViewToDrawer(FloatingSearchView searchView) {

    }


    public class MyListAdapter extends ArrayAdapter<String> {

        private final Activity context;
        private final ArrayList<String> searchtext;
        private final ArrayList<String> searchtime;
        private final ArrayList<Integer> searchindex;


        public MyListAdapter(Activity context,ArrayList<Integer> searchindex, ArrayList<String> searchtext,ArrayList<String> searchtime) {
            super(context, R.layout.search_list_custom_item, searchtext);
            // TODO Auto-generated constructor stub

            this.context=context;
            this.searchtext=searchtext;
            this.searchtime=searchtime;
            this.searchindex=searchindex;

        }

        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.search_list_custom_item, null,true);

            TextView searchText = (TextView) rowView.findViewById(R.id.textView66);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.imageButton);

imageView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        db.deletesearchrecord(searchindex.get(position));
        Cursor cursor = db.getsearchitems();
        if(cursor.getCount()!=0)
        {
            searchindex.clear();
            searchtime.clear();
            searchname.clear();
            if(cursor.moveToFirst())
            {
                do {
                    searchindex.add(cursor.getInt(0));
                    searchname.add(cursor.getString(1));
                    searchtime.add(cursor.getString(2));
                }while (cursor.moveToNext());
                MyListAdapter myListAdapter = new MyListAdapter(SearchActivity.this , searchindex,searchname,searchtime);
                search_listview.setAdapter(myListAdapter);
                notifyDataSetChanged();

            }
        }
        else
        {
            search_listview.setAdapter(null);
            searchtitle.setText("No Search History");
        }


    }
});
            searchText.setText(searchtext.get(position));

   searchText.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View v) {
           Intent  intent = new Intent(getApplicationContext(), SearchResultActivity.class);
           intent.putExtra("searchkeyword",searchtext.get(position));
           startActivity(intent);
           finish();
       }
   });

            return rowView;

        };
    }

}

package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.PaymentMethods.payment;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface getPaymentmethodAPI {
    @GET("/checkout/getPaymentModule")
    Call<payment> getDetails(@Header("data") String data, @Query("cityId") String cityId);
}

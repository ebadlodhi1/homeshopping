package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.whishlist.whishlist;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface getwhishlistAPI {
    @GET("/wishlist/getWishList")
    Call<whishlist> getDetails(@Header("data") String data, @Query("id") String id, @Query("skip") String skip);
}

package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO.ConfFieldExample;
import com.homeshopping.admin.homeshoping10.POJO.Variation.VariationExample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface GetconfigrableAPI {
    @GET("/products/getConfigurableFieldByProduct")
    Call<ConfFieldExample> getDetails(@Header("data") String data, @Query("productId") String productId,@Query("bankId") String bankId);
}

package com.homeshopping.admin.homeshoping10.POJO.Variation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Detail {
    @SerializedName("variationId")
    @Expose
    private Integer variationId;
    @SerializedName("variationName")
    @Expose
    private String variationName;
    @SerializedName("variations")
    @Expose
    private List<Variation> variations = null;

    public Integer getVariationId() {
        return variationId;
    }

    public void setVariationId(Integer variationId) {
        this.variationId = variationId;
    }

    public String getVariationName() {
        return variationName;
    }

    public void setVariationName(String variationName) {
        this.variationName = variationName;
    }

    public List<Variation> getVariations() {
        return variations;
    }

    public void setVariations(List<Variation> variations) {
        this.variations = variations;
    }
}

package com.homeshopping.admin.homeshoping10.POJO.ConfigrableFieldsPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ConfFieldExample {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<ConfFieldDetail> details = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ConfFieldDetail> getDetails() {
        return details;
    }

    public void setDetails(List<ConfFieldDetail> details) {
        this.details = details;
    }
}

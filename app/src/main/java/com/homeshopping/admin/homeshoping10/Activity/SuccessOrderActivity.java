package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.POJO.PlaceOrderPOJO.OrderId;
import com.homeshopping.admin.homeshoping10.R;

import java.util.ArrayList;
import java.util.List;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.CheckoutFragmentConfirmDetails.orderIds;

public class SuccessOrderActivity extends AppCompatActivity {
        TextView ordermessage,Orderid;
        Button continueshopping;
        List<OrderId> orderIdsthis = new ArrayList<>();
    ArrayList<String> Orderidno  = new ArrayList<>();
    DatabaseHelper db;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_order);
        db = new DatabaseHelper(this);
        db.deleteallcartproductofuser(prefConfig.readID());
        this.setTitle("Order Status");
        ordermessage = findViewById(R.id.ordermessage);
        Orderid = findViewById(R.id.Orderid);
        continueshopping = findViewById(R.id.continueshopping);
        orderIdsthis = orderIds;
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Success Order Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        for (int i = 0 ; i<orderIds.size();i++)
        {
            Orderidno.add(orderIds.get(i).getOrderId());
//            Toast.makeText(getApplicationContext(), ""+orderIds.get(i).getOrderId(), Toast.LENGTH_SHORT).show();
        }
        Bundle bundle = getIntent().getExtras();
        ordermessage.setText(""+bundle.getString("ordermessage"));


        Orderid.setText(""+Orderidno);
        continueshopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                startActivity(intent);
                finish();

            }
        });
    }

    @Override
    public void onBackPressed () {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(),Dashboard.class);
        startActivity(intent);
        finish();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.withoutcartmenu, menu);
           return true;
    }
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        switch (id) {
//            case R.id.refresh2: {
//                refresh();
//                break;
//            }
//            case android.R.id.home:{
//                onBackPressed();
//                break;
//            }
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//
//
//    public void refresh()
//    {
//
//        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
//        startActivity(intent);
//        finish();
//
//    }
}

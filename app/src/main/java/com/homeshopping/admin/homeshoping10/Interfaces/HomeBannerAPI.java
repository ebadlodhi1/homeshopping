package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.HomeBanner.HomeBannerPOJO;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface HomeBannerAPI {
    @GET("/home/getBanner")
    Call<HomeBannerPOJO> getDetails(@Header("data") String data, @Query("deviceType") String deviceType, @Query("imageType") String imageType);

}

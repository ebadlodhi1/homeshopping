package com.homeshopping.admin.homeshoping10.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Interfaces.ForgetpasswordAPI;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;
import com.homeshopping.admin.homeshoping10.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class ForgetPasswordActivity extends AppCompatActivity {
        Button submitemailbutton;
        EditText forgetpassemail;
        Boolean flag = true;
        ProgressBar progressBar14;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Forget Password");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Forget Password Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        submitemailbutton = findViewById(R.id.button23);
        forgetpassemail = findViewById(R.id.forgetpassemail);
        progressBar14 = findViewById(R.id.progressBar14);
        progressBar14.setVisibility(View.GONE);
        submitemailbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar14.setVisibility(View.VISIBLE);
                String email = forgetpassemail.getText().toString();
                flag = true;
                if(email.isEmpty())
                {
                    progressBar14.setVisibility(View.GONE);
                    forgetpassemail.setError("Please enter email");
                    forgetpassemail.requestFocus();
                    flag = false;
                }
                else if(!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                {progressBar14.setVisibility(View.GONE);
                    forgetpassemail.setError("Invalid email");
                    forgetpassemail.requestFocus();
                    flag=false;

                }
                if (flag)
                {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    ForgetpasswordAPI api = retrofit.create(ForgetpasswordAPI.class);
                    Call<generalstatusmessagePOJO> call = api.getDetails(MainActivity.Tokendata,email);
                    call.enqueue(new Callback<generalstatusmessagePOJO>() {
                        @Override
                        public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                            if (response.isSuccessful())
                            {
                                int status = response.body().getStatus();
                                if(status == 1) {
                                open(response.body().getMessage());
                                    progressBar14.setVisibility(View.GONE);
                                }
                                else
                                {
                                    progressBar14.setVisibility(View.GONE);
                                    Toast.makeText(ForgetPasswordActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();

                                }
                                }
                            else
                            {
                                progressBar14.setVisibility(View.GONE);
                                Toast.makeText(ForgetPasswordActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {
                            progressBar14.setVisibility(View.GONE);
                            Toast.makeText(ForgetPasswordActivity.this, "Server Not Responding", Toast.LENGTH_SHORT).show();

                        }
                    });
                }

            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);

    }

    public void open(String msg){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(""+msg);
                alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {

                                finish();
                            }
                        });

//        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
//            Override
//            public void onClick(DialogInterface dialog, int which) {
//                finish();
//            }
//        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}

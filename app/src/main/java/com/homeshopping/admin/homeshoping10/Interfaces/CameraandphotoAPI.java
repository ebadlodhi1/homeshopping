package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.monthlytopseller.Example;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface CameraandphotoAPI {

    @GET("cameraprodcat")
    Call<List<Example>> getDetails(@Header("data") String data);
}

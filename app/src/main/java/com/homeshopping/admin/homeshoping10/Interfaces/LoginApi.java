package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.LoginPOJO;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface LoginApi
{
    @POST("user/login")
    Call<LoginPOJO> getDetails(@Header("data") String data, @Query("email") String email,
                               @Query("password") String password);
}

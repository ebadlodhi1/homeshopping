package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.whishlist.whishlist;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.HTTP;
import retrofit2.http.Header;

public interface removewishlistAPI {
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "/wishlist/deleteWishListProduct", hasBody = true)
    Call<whishlist>  getDetails(@Header("data") String data, @Field("id") String id, @Field("productid") String productid, @Field("customerid") String customerid);

}

package com.homeshopping.admin.homeshoping10.Fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.SearchSuggestionsAdapter;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.arlib.floatingsearchview.util.Util;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.homeshopping.admin.homeshoping10.Activity.BankCategoryActivity;
import com.homeshopping.admin.homeshoping10.Activity.CartActivity;
import com.homeshopping.admin.homeshoping10.Activity.CategoryActivity;
import com.homeshopping.admin.homeshoping10.Activity.Dashboard;
import com.homeshopping.admin.homeshoping10.Activity.MainActivity;
import com.homeshopping.admin.homeshoping10.Activity.ProductDetailActivity;
import com.homeshopping.admin.homeshoping10.Activity.SearchActivity;
import com.homeshopping.admin.homeshoping10.Activity.SearchResultActivity;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Class.NetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Class.NotLoginDataNetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Data.ColorSuggestion;
import com.homeshopping.admin.homeshoping10.Data.ColorWrapper;
import com.homeshopping.admin.homeshoping10.Data.DataHelper;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Interfaces.AddorRemoveCallbacks;
import com.homeshopping.admin.homeshoping10.Interfaces.AddtowishlistAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.HomeBannerAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.HomeCategoryAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.MonthlytopsellerApi;
import com.homeshopping.admin.homeshoping10.Interfaces.SliderApi;
import com.homeshopping.admin.homeshoping10.Interfaces.findifysugessionAPI;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;
import com.homeshopping.admin.homeshoping10.POJO.HomeBanner.Banner;
import com.homeshopping.admin.homeshoping10.POJO.HomeBanner.HomeBannerPOJO;
import com.homeshopping.admin.homeshoping10.POJO.HomeCategoryProducts.CategoryProduct;
import com.homeshopping.admin.homeshoping10.POJO.HomeCategoryProducts.HomeCategoryProduct;
import com.homeshopping.admin.homeshoping10.POJO.HomeCategoryProducts.HomeExample;
import com.homeshopping.admin.homeshoping10.POJO.MonthlyTopSellerPOJO.MonthlyTopsellerExample;
import com.homeshopping.admin.homeshoping10.POJO.MonthlyTopSellerPOJO.Topseller;
import com.homeshopping.admin.homeshoping10.POJO.SliderPOJO.Slider;
import com.homeshopping.admin.homeshoping10.POJO.SliderPOJO.SliderExample;
import com.homeshopping.admin.homeshoping10.POJO.findifysugestionPOJO;
import com.homeshopping.admin.homeshoping10.POJO.monthlytopseller.Example;
import com.homeshopping.admin.homeshoping10.R;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.IndicatorView.draw.data.Indicator;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Data.DataHelper.sColorSuggestions;

@SuppressLint("ValidFragment")
public class ScrollingSearchExampleFragment extends Fragment
        implements AppBarLayout.OnOffsetChangedListener,
        NavigationView.OnNavigationItemSelectedListener,
        AddorRemoveCallbacks {


    private final String TAG = "BlankFragment";
    int[]   dumProductimagee = {R.drawable.branddefault,R.drawable.branddefault,R.drawable.branddefault} ;
    String[] dumproductnamee = {"Loading...","Loading...","Loading..."} ;
    String[] dumorignalpricetvv = {"Loading...","Loading...","Loading..."} ;
    String[] dumoffpercenttvv  = {"0%","0%","0%"} ;
    String[] dumdiscountedpricetvv = {"Loading...","Loading...","Loading..."} ;
    public static final long FIND_SUGGESTION_SIMULATED_DELAY = 250;

//    private FloatingSearchView mSearchView;
    private AppBarLayout mAppBar;

    private boolean mIsDarkSearchTheme = false;

//    private String mLastQuery = "";

    SliderLayout sliderLayout;


    android.support.v4.widget.SwipeRefreshLayout swiperefresh;
    LinearLayoutManager layoutManager,layoutManager2,layoutManager3,layoutManager4,layoutManager5,layoutManager55;
    static RecyclerView recyclerView;
    static RecyclerView recyclerView2;
    static RecyclerView recyclerView3;
    static RecyclerView recyclerView4;
    static RecyclerView recyclerView5;
    static RecyclerView recyclerView55;
    static ArrayList<String> mImageUrls = new ArrayList<>() ;
//    ImageView banner1dash;

    String urlbaner1 = "https://cdn.homeshopping.pk/templates/ResGarden/images/Cellphone-GIF-Homepage.gif";
    android.support.v7.widget.SearchView searchview;
//    ImageView imvbanmus1,imvbanmus2,imvbanmus3,imvbanmus4,imvbanmus5;
//    ImageView imvbanmus1new ,imvbanmus2new,imvbanmus3new,imvbanmus4new,imvbanmus5new;

    ImageView Banner1,Banner2,Banner3,Banner4,Banner5,Banner6,Banner7,Banner8,Banner9,Banner10,
            Banner11,Banner12,Banner13,Banner14,Homeaudiobanner;
    List<Banner> bannerslist = new ArrayList<>();
    ArrayList<String> imagePath = new ArrayList<>();
    ArrayList<Integer> pageId = new ArrayList<>();
    ArrayList<String> title = new ArrayList<>();


    int count  = 0 ;
    public static final String ROOT_URL = "https://api.homeshopping.pk/";
    public static final String DEV_ROOT_URL = "https://api.homeshopping.pk/";

     static List<Topseller> list = new ArrayList<Topseller>();
    static List<Example> searchproductlist = new ArrayList<Example>();
    static List<findifysugestionPOJO> sugessionlist = new ArrayList<findifysugestionPOJO>();
    List<String> prodimagelist = new ArrayList<>();
   public static   String[] prodname,prodprice,prodoffper,proddiscount,prodimage,proddiscription,prodbrand;
   static int[] prodwishlist,Existincart;
    public static int[] prodid;
    String[][] prodimage2d;
    Button Viewalltopseller,Viewallmobile,Viewallcamera,Viewalltv,Viewallcomputer,Viewalllast,ViewallFashion;
    static Context context;
    public DatabaseHelper db;
    private ArrayList<Integer> mImageUrlsee = new ArrayList<>();
    String[] Productimagee ;
    String[] productnamee ;
    String[] orignalpricetvv ;
    String[] offpercenttvv ;
    String[]discountedpricetvv;
    String[]proddiscriptionn;
    String[][] prodimage2dd;
    int [] pid ;
    int [] Productimageedum;
    String[] prodbrandd;
    int[] prodwishlistt,Existincartt;
    Handler searchhand = new Handler();
    Handler loadingimages = new Handler();

    Handler monthlytopseler = new Handler();
    Handler mobilephoneandtablet = new Handler();
    Handler cameraandphoto = new Handler();
    Handler tvandvideo = new Handler();
    Handler computerandlaptop = new Handler();
    ProgressBar progressBarmotop,progressBarmob,progressBarcam,progressBartv,progressBarlap;

    int SalePrice = 1 ;
    int  Productprice = 1;
    int discoutnamount = 0 ;

    List<HomeCategoryProduct>  homeCategoryProducts = new ArrayList<>();
    ArrayList<Integer> categoryId ;
    ArrayList<String> categoryTitle  ;

    ArrayList<String> categoryBanners = new ArrayList<>();
    ArrayList<String>  imagePathurl = new ArrayList<>();
    ArrayList<Integer>  Pageid = new ArrayList<>();
    ArrayList<String> pageType = new ArrayList<>();

    List<CategoryProduct> categoryProducts = new ArrayList<>();
    ArrayList<Integer> ProductID ;
    ArrayList<String> ProductName ;
    ArrayList<String> ProductUrl ;
    ArrayList<String> ProductDescription;
    ArrayList<String> ProductPrice ;
    ArrayList<String> ProductSalePrice;
    ArrayList<String> BrandDisplayName ;
    ArrayList<Integer> productRating ;
    ArrayList<Integer> productWishlist ;
    ArrayList<Integer> isExistInCart ;
    ArrayList<String> productImages;
    ArrayList<String> ImageFile = new ArrayList<>();
    ArrayList<String>  ImageThumb = new ArrayList<>();
    TextView Pannelonename ,Panneltwoname,PannelThreename,Pannelfourname,Pannelfivename;
    List<Slider> sliders = new ArrayList<>();
    String[] sliderimages,slidertype,slidertitle;
    int[] sliderID;
    ConstraintLayout ToppickConstraint,sliderconstraintLayout;
    SliderView sliderView ;
    int addcartstatus = 0;
    ImageView searchimageview;

    AppBarLayout appbar;
    CollapsingToolbarLayout toolbar_layout;

    public ScrollingSearchExampleFragment(Context c) {
        // Required empty public constructor
        context = c;
        db = new DatabaseHelper(context);
        db.delete();
        final Boolean res = Dashboard.prefConfig.readLoginStatus();
//        Toast.makeText(c, " login status "+res , Toast.LENGTH_SHORT).show();
        if (res == false) {
            Cursor cursor1 = db.getcountnotlogin();
            if(cursor1.moveToFirst())
            {

                Dashboard.cart_count =cursor1.getInt(0);


            }
        }else {




            Cursor cursor1 = db.getRecord();
            if (cursor1 != null) {
                if (cursor1.moveToFirst()) {

//                    Toast.makeText(c, "login  "+cursor1.getCount(), Toast.LENGTH_SHORT).show();
                }
                try {
//                    Toast.makeText(c, "Throughing", Toast.LENGTH_SHORT).show();
                    context.registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    context.registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                }
                catch (Exception e)
                {
                    Log.e("error",""+e);
                }
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scrolling_search_example, container, false);

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        db = new DatabaseHelper(context);
//        mSearchView = (FloatingSearchView) view.findViewById(R.id.floating_search_view);

//        mSearchView.findViewById(R.id.search_query_section).setBackgroundResource(Color.TRANSPARENT);
//        mSearchView.findViewById(R.id.search_query_section).setMinimumHeight(10);


        mAppBar = (AppBarLayout) view.findViewById(R.id.appbar);
        mAppBar.addOnOffsetChangedListener(this);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView2 = (RecyclerView) view.findViewById(R.id.recyclerView2);
        recyclerView3 = (RecyclerView) view.findViewById(R.id.recyclerView3);
        recyclerView4 = (RecyclerView) view.findViewById(R.id.recyclerView4);
        recyclerView5 = (RecyclerView) view.findViewById(R.id.recyclerView5);
        recyclerView55 = (RecyclerView) view.findViewById(R.id.recyclerView55);




        Viewalltopseller = view.findViewById(R.id.button);
        Viewallmobile= view.findViewById(R.id.button2);
        Viewallcamera= view.findViewById(R.id.button4);
        Viewalltv= view.findViewById(R.id.button3);
        Viewallcomputer= view.findViewById(R.id.button333);
        Viewalllast = view.findViewById(R.id.button3333);
        ViewallFashion = view.findViewById(R.id.button22);
        Pannelonename = view.findViewById(R.id.textView3);
        Panneltwoname = view.findViewById(R.id.textView5);
        PannelThreename = view.findViewById(R.id.textView4);
        Pannelfourname = view.findViewById(R.id.textView233);
        Pannelfivename = view.findViewById(R.id.textView2333);
        searchimageview = view.findViewById(R.id.searchimageview);
        searchimageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SearchActivity.class);
                startActivity(intent);
            }
        });
        appbar = view.findViewById(R.id.appbar);
        appbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SearchActivity.class);
                startActivity(intent);
            }
        });
        ;
        toolbar_layout = view.findViewById(R.id.toolbar_layout);
        toolbar_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SearchActivity.class);
                startActivity(intent);
            }
        });
        layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        layoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        layoutManager3 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        layoutManager4 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        layoutManager5 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        layoutManager55 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView2.setLayoutManager(layoutManager2);
        recyclerView3.setLayoutManager(layoutManager3);
        recyclerView4.setLayoutManager(layoutManager4);
        recyclerView5.setLayoutManager(layoutManager5);
        recyclerView55.setLayoutManager(layoutManager55);

        DumRecyclerViewAdapter dumRecyclerViewAdapter = new DumRecyclerViewAdapter(dumProductimagee,
                dumproductnamee,dumorignalpricetvv,dumoffpercenttvv,dumdiscountedpricetvv);
        recyclerView.setAdapter(dumRecyclerViewAdapter);
        recyclerView2.setAdapter(dumRecyclerViewAdapter);
        recyclerView3.setAdapter(dumRecyclerViewAdapter);
        recyclerView4.setAdapter(dumRecyclerViewAdapter);
        recyclerView5.setAdapter(dumRecyclerViewAdapter);
        recyclerView55.setAdapter(dumRecyclerViewAdapter);
//        imvbanmus1 = view.findViewById(R.id.imvbanmus1);
//        imvbanmus2 = view.findViewById(R.id.imvbanmus2);
//        imvbanmus3 = view.findViewById(R.id.imvbanmus3);
//        imvbanmus4 = view.findViewById(R.id.imvbanmus4);
//        imvbanmus5 = view.findViewById(R.id.imvbanmus5);
//        imvbanmus1new = view.findViewById(R.id.imvbanmus1new);
//        imvbanmus2new = view.findViewById(R.id.imvbanmus2new);
//        imvbanmus3new = view.findViewById(R.id.imvbanmus3new);
//        imvbanmus4new = view.findViewById(R.id.imvbanmus4new);
//        imvbanmus5new = view.findViewById(R.id.imvbanmus5new);

        Banner1 = view.findViewById(R.id.imageView25);
        Banner2 = view.findViewById(R.id.imageView26);
        Banner3 = view.findViewById(R.id.imageView27);
        Banner4 = view.findViewById(R.id.imageView28);
        Banner5  = view.findViewById(R.id.imageView29);
        Banner6 = view.findViewById(R.id.imvbanmus1new);
        Banner7 = view.findViewById(R.id.imageView18);
        Banner8 = view.findViewById(R.id.imageView22);
        Banner9 = view.findViewById(R.id.imageView46);
        Banner10 = view.findViewById(R.id.imageView21);
        Banner11 = view.findViewById(R.id.imageView44);
        Banner12 = view.findViewById(R.id.imageView45);
        Banner13 = view.findViewById(R.id.imageView47);
        Banner14 = view.findViewById(R.id.imageView48);
        Homeaudiobanner = view.findViewById(R.id.imageView43);


        ToppickConstraint = view.findViewById(R.id.constraintLayout3);

        swiperefresh = view.findViewById(R.id.swiperefresh);
        try {
            swiperefresh.setProgressViewOffset(false, 0,
                    (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70,
                            getResources().getDisplayMetrics()));
            swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Intent intent = new Intent(context, Dashboard.class);
                    startActivity(intent);
                    getActivity().finish();
                    swiperefresh.setRefreshing(false);
                }
            });
        }
        catch (Exception e)
        {

        }
//        banner3 =    view.findViewById(R.id.banner3);
//        bannergif = view.findViewById(R.id.imageView4);
//        banner2dash = view.findViewById(R.id.imageView5);
//        banner3dash = view.findViewById(R.id.imageView6);

        sliderLayout = view.findViewById(R.id.imageSlider);
       sliderLayout.setIndicatorAnimation(IndicatorAnimations.DROP);

    //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(3);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SliderApi sliderApi = retrofit.create(SliderApi.class);
        Call<SliderExample> sliderExampleCall = sliderApi.getDetails(MainActivity.Tokendata,"mobile");
        sliderView = new SliderView(context);
        sliderExampleCall.enqueue(new Callback<SliderExample>() {
            @Override
            public void onResponse(Call<SliderExample> call, Response<SliderExample> response) {
                if(response.isSuccessful()) {
                    int status = response.body().getStatus();

                    if (status == 0) {
                        Toast.makeText(getActivity(), "" + response.message(), Toast.LENGTH_SHORT).show();
                    } else {
                        sliders = response.body().getSliders();
                        sliderimages = new String[sliders.size()];
                        sliderID = new int[sliders.size()];
                        slidertype = new String[sliders.size()];
                        slidertitle = new String[sliders.size()];
                        for (int i = 0; i < sliders.size(); i++) {
                            sliderView = new SliderView(context);
                            sliderimages[i] = sliders.get(i).getImagePath();
                            sliderID[i] = sliders.get(i).getPageId();
                            slidertype[i] = sliders.get(i).getPageType();
//                            Toast.makeText(context, ""+sliderID[i], Toast.LENGTH_SHORT).show();
                            slidertitle[i] = sliders.get(i).getTitle();
                            sliderView.setImageUrl(sliderimages[i]);
                            sliderView.setImageScaleType(ImageView.ScaleType.FIT_XY);

                            sliderLayout.addSliderView(sliderView);
                            if (sliderID[i] != 0) {
                                sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(SliderView sliderView) {
                                        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                        int position = sliderLayout.getCurrentPagePosition();
//                            Toast.makeText(getActivity(), "dfd "+sliderimages[position], Toast.LENGTH_SHORT).show();

                                        if (slidertype[position].equals("category")  ) {
                                            try {
                                                Intent intent = new Intent(context, CategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                                                intent.putExtra("slidertype",slidertype[position]);
                                                intent.putExtra("catid",  sliderID[position]);
                                                intent.putExtra("catname", "" + slidertitle[position]);
                                                startActivity(intent);
                                                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                            } catch (Exception e) {

                                            }

                                        }
                                        else if (slidertype[position].equals("bank")) {
                                            try {
                                                Intent intent = new Intent(context, BankCategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                                                intent.putExtra("slidertype",slidertype[position]);
                                                intent.putExtra("catid",  sliderID[position]);
                                                intent.putExtra("catname", "" + slidertitle[position]);
                                                startActivity(intent);
                                                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                            } catch (Exception e) {

                                            }

                                        }


                                        else if (slidertype[position].equals("product")) {
                                            try {
                                                Intent intent = new Intent(context, ProductDetailActivity.class);
                                                intent.putExtra("productid", "" + sliderID[position]);
                                                startActivity(intent);
                                                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                            } catch (Exception e) {

                                            }

                                        }

                                    }

                                });
                            }
                        }
                    }
                }
                else
                {
                    Toast.makeText(getActivity(), "No server response", Toast.LENGTH_SHORT).show();
                }


//                setSliderViews(sliderimages);

            }

            @Override
            public void onFailure(Call<SliderExample> call, Throwable t) {

            }
        });

//        sliderconstraintLayout =view.findViewById(R.id.sliderconstraintLayout);
//        sliderconstraintLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
//        sliderLayout.setSliderTransformAnimation(CLOCK_SPINTRANSFORMATION);
//        banner1dash = view.findViewById(R.id.banner1dash);
        progressBarmotop = view.findViewById(R.id.progressBarmotop);
        progressBarmob = view.findViewById(R.id.progressBarmob);
        progressBarcam = view.findViewById(R.id.progressBarcam);
        progressBartv = view.findViewById(R.id.progressBartv);
        progressBarlap = view.findViewById(R.id.progressBarlap);
Thread thread = new Thread()
{
    @Override
    public void run() {
        searchhand.post(search);
//    searchhand.removeCallbacks(search);
        loadingimages.postDelayed(load,500);
//        loadingimages.removeCallbacks(load);

        monthlytopseler.postDelayed(monthlytopselerhand,1000);
//        mobilephoneandtablet.postDelayed(mobilephoneandtablethand,3000);
        cameraandphoto.postDelayed(HomeProducts,1500);
//        tvandvideo.postDelayed(tvandvideohand,5000);
//        computerandlaptop.postDelayed(computerandlaptophand,6000);




    }
};

thread.start();





//        int screenSize = getResources().getConfiguration().screenLayout &
//                Configuration.SCREENLAYOUT_SIZE_MASK;
//
//        String toastMsg;
//        switch(screenSize) {
//            case Configuration.SCREENLAYOUT_SIZE_LARGE:
//                toastMsg = "Large screen";
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
//                toastMsg = "Normal screen";
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_SMALL:
//                toastMsg = "Small screen";
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
//                toastMsg = "Xlarge screen";
//                break;
//            default:
//                toastMsg = "Screen size is neither large, normal or small";
//        }
//        Toast.makeText(context, " j "+toastMsg, Toast.LENGTH_LONG).show();
//        Log.d("Screen size",toastMsg);
        Viewalltopseller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                Intent intent = new Intent(context, CategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                intent.putExtra("catname","Daily Deals");
                intent.putExtra("catid",2207);
                startActivity(intent);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                onBackPressed();
            }
        });
        Viewallmobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Intent intent = new Intent(context,CategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                intent.putExtra("catname",""+categoryTitle.get(0));
                intent.putExtra("catid",categoryId.get(0));
                startActivity(intent);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                onBackPressed();
            }
        });
        Viewallcamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Intent intent = new Intent(context,CategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                intent.putExtra("catname",""+categoryTitle.get(1));
                intent.putExtra("catid",categoryId.get(1));
                startActivity(intent);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                onBackPressed();
            }
        });
        Viewalltv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Intent intent = new Intent(context,CategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                intent.putExtra("catname",""+categoryTitle.get(2));
                intent.putExtra("catid",categoryId.get(2));
                startActivity(intent);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                onBackPressed();
            }
        });
        Viewallcomputer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Intent intent = new Intent(context,CategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                intent.putExtra("catname",""+categoryTitle.get(3));
                intent.putExtra("catid",categoryId.get(3));
                startActivity(intent);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                onBackPressed();
            }
        });
        Viewalllast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Intent intent = new Intent(context,CategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                intent.putExtra("catname",""+categoryTitle.get(4));
                intent.putExtra("catid",categoryId.get(4));
                startActivity(intent);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                onBackPressed();
            }
        });

        ViewallFashion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                Intent intent = new Intent(context,CategoryActivity.class);
//                        Toast.makeText(Dashboard.this, ""+model.menuName.toString(), Toast.LENGTH_SHORT).show();
                intent.putExtra("catname","Fashion");
                intent.putExtra("catid",60);
                startActivity(intent);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        });
//
    }
    Runnable search = new Runnable() {
        @Override
        public void run() {
//            setupDrawer();
//            setupSearchBar();
        }
    };
    Runnable load = new Runnable() {
        @Override
        public void run() {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ROOT_URL) //Setting the Root URL
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            HomeBannerAPI homeBannerAPI = retrofit.create(HomeBannerAPI.class);
            final Call<HomeBannerPOJO> homeBannerPOJOCall = homeBannerAPI.getDetails(MainActivity.Tokendata,"mobile","homebanner");
            homeBannerPOJOCall.enqueue(new Callback<HomeBannerPOJO>() {
                @Override
                public void onResponse(Call<HomeBannerPOJO> call, Response<HomeBannerPOJO> response) {
                    if (response.isSuccessful()) {
                        int status = response.body().getStatus();
                        if (status == 1) {
                            bannerslist = response.body().getBanners();
                            for (int i = 0; i < bannerslist.size(); i++) {
                                imagePath.add(bannerslist.get(i).getImagePath());
                                pageId.add(bannerslist.get(i).getPageId());
                                title.add(bannerslist.get(i).getTitle());
//                            Toast.makeText(context, "Page id "+pageId.get(i), Toast.LENGTH_SHORT).show();

                            }
                            RequestOptions requestOptions = RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(0)).into(Banner1);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(1)).into(Banner2);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(2)).into(Banner3);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(3)).into(Banner4);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(4)).into(Banner5);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(5)).into(Banner6);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(6)).into(Banner7);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(7)).into(Banner8);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(8)).into(Banner9);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(9)).into(Banner10);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(10)).into(Banner11);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(11)).into(Banner12);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(12)).into(Banner13);
                            Glide.with(context).applyDefaultRequestOptions(requestOptions)
                                    .load(imagePath.get(13)).into(Banner14);



                            Banner1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(0));
                                    intent.putExtra("catname",title.get(0).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(1));
                                    intent.putExtra("catname",title.get(1).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(2));
                                    intent.putExtra("catname",title.get(2).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner4.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(3));
                                    intent.putExtra("catname",title.get(3).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner5.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(4));
                                    intent.putExtra("catname",title.get(4).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner6.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(5));
                                    intent.putExtra("catname",title.get(5).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner7.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(6));
                                    intent.putExtra("catname",title.get(6).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner8.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(7));
                                    intent.putExtra("catname",title.get(7).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner9.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(8));
                                    intent.putExtra("catname",title.get(8).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner10.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(9));
                                    intent.putExtra("catname",title.get(9).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner11.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(10));
                                    intent.putExtra("catname",title.get(10).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner12.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(11));
                                    intent.putExtra("catname",title.get(11).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner13.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(12));
                                    intent.putExtra("catname",title.get(12).toString());
                                    startActivity(intent);

                                }
                            });
                            Banner14.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", pageId.get(13));
                                    intent.putExtra("catname",title.get(13).toString());
                                    startActivity(intent);

                                }
                            });
                            Homeaudiobanner.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getContext(), CategoryActivity.class);
                                    intent.putExtra("catid", 1097);
                                    intent.putExtra("catname","Home Audio");
                                    startActivity(intent);

                                }
                            });

                        }
                    }
                }
                @Override
                public void onFailure(Call<HomeBannerPOJO> call, Throwable t) {

                }
            });


//            Glide.with(context)
//                    .load(R.drawable.mobstore1)// image url
//                    // any placeholder to load at start
//                    .into(imvbanmus1);
//            Glide.with(context)
//                    .load(R.drawable.mobstore2)// image url
//                    // any placeholder to load at start
//
//                    .into(imvbanmus2);
//            Glide.with(context)
//                    .load(R.drawable.mobstore3)// image url
//                    // any placeholder to load at start
//                    .into(imvbanmus3);
//            Glide.with(context)
//                    .load(R.drawable.mobstore4)// image url
//                    // any placeholder to load at start
//                    .into(imvbanmus4);
//            Glide.with(context)
//                    .load(R.drawable.mobstore5)// image url
//                    // any placeholder to load at start
//                    .into(imvbanmus5);
//            Glide.with(context)
//                    .load(R.drawable.elecstore1)// image url
//                    // any placeholder to load at start
//                    .into(imvbanmus1new);
//            Glide.with(context)
//                    .load(R.drawable.elecstore2)// image url
//                    // any placeholder to load at start
//
//                    .into(imvbanmus2new);
//            Glide.with(context)
//                    .load(R.drawable.elecstore3)// image url
//                    // any placeholder to load at start
//                    .into(imvbanmus3new);
//            Glide.with(context)
//                    .load(R.drawable.elecstore4)// image url
//                    // any placeholder to load at start
//                    .into(imvbanmus4new);
//            Glide.with(context)
//                    .load(R.drawable.elecstore5)// image url
//                    // any placeholder to load at start
//                    .into(imvbanmus5new);
//            Glide.with(context)
//                    .load(R.drawable.flatbanner2mob)// image url
//                    // any placeholder to load at start
//                    .into(banner3);


//        RecyclerViewAdapter recyclerViewAdapter5 = new RecyclerViewAdapter(prodimage,prodname,prodprice
//                ,prodoffper,prodoffper);
//        recyclerView5.setAdapter(recyclerViewAdapter5);
//        searchview = findViewById(R.id.dashsearch);
//        EditText searchEditText = (EditText)searchview.findViewById(android.support.v7.appcompat.R.id.search_src_text);
//        searchEditText.setHint("I am shopping for...");
//        searchEditText.setTextSize(12);
//        searchEditText.setTextColor(getResources().getColor(R.color.Black));
//        searchEditText.setHintTextColor(getResources().getColor(R.color.Black));

//            Glide.with(context)
//                    .load(R.drawable.flatbanner3mob)// image url
//                    // any placeholder to load at start
//                    .into(bannergif);
//            bannergif.setBackgroundResource(R.drawable.flatbanner3mob);
////            Glide.with(context)
////                    .load(R.drawable.twobanner1mob)// image url
////                    // any placeholder to load at start
////                    .into(banner2dash);
//            banner2dash.setBackgroundResource(R.drawable.twobanner1mob);
////            Glide.with(context)
////                    .load(R.drawable.twobanner2mob)// image url
////                    // any placeholder to load at start
////                    .into(banner3dash);
//            banner3dash.setBackgroundResource(R.drawable.twobanner2mob);

            //set scroll delay in seconds :

//        mImageUrls.add(R.drawable.iphonex);
//        mImageUrls.add(R.drawable.samsungnote);
//        mImageUrls.add(R.drawable.iphonex);





//            Glide.with(context)
//                    .load(R.drawable.flatbanner1mob)// image url
//                    // any placeholder to load at start
//                    .into(banner1dash);
//            banner1dash.setBackgroundResource(R.drawable.banner1);
        }
    };
    //        getmonthlytopseler();
//
//        getmobilephoneandtablet();
//
//
//        getcameraandphoto();
//
//        gettvandvideo();
//
//
//        getcomputerandlaptop();
    Runnable monthlytopselerhand = new Runnable() {
        @Override
        public void run() {
            progressBarmotop.setVisibility(View.GONE);

            getmonthlytopseler();
        }
    };

    Runnable HomeProducts = new Runnable() {
        @Override
        public void run() {
            progressBarcam.setVisibility(View.GONE);
            progressBarmob.setVisibility(View.GONE);
            progressBarcam.setVisibility(View.GONE);
            progressBartv.setVisibility(View.GONE);
            Dashboard.progressBar4.setVisibility(View.GONE);
            progressBarlap.setVisibility(View.GONE);
            try {
                getHomeProducts();

            }
            catch (Exception e)
            {
                Toast.makeText(getActivity(), "Internal server error.", Toast.LENGTH_SHORT).show();
            }
        }
    };
    Runnable mobilephoneandtablethand = new Runnable() {
        @Override
        public void run() {

//            getmobilephoneandtablet();
        }
    };
    Runnable cameraandphotohand = new Runnable() {
        @Override
        public void run() {

//            getcameraandphoto();
        }
    };
    Runnable tvandvideohand = new Runnable() {
        @Override
        public void run() {

//            gettvandvideo();
        }
    };
    Runnable computerandlaptophand = new Runnable() {
        @Override
        public void run() {

//            getcomputerandlaptop();
        }
    };



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.withoutcartmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(context,Dashboard.cart_count,R.drawable.ttcart));

        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.refresh2) {
            Intent intent = new Intent(context, CartActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void getmonthlytopseler()
    {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(150, TimeUnit.SECONDS);
        client.readTimeout(150, TimeUnit.SECONDS);
        client.writeTimeout(150, TimeUnit.SECONDS);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL).client(client.build()) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MonthlytopsellerApi monthlytopsellerApi =retrofit.create(MonthlytopsellerApi.class);

        Call<MonthlyTopsellerExample> monthlyTopsellerExampleCall = monthlytopsellerApi.getDetails(MainActivity.Tokendata,Dashboard.prefConfig.readID());
        monthlyTopsellerExampleCall.enqueue(new Callback<MonthlyTopsellerExample>() {
            @Override
            public void onResponse(Call<MonthlyTopsellerExample> call, Response<MonthlyTopsellerExample> response) {
//                Toast.makeText(context, "in mot response", Toast.LENGTH_SHORT).show();
                try {
                int status  = response.body().getStatus();

                    if (status == 1) {
                        ToppickConstraint.setVisibility(View.VISIBLE);

                        list = response.body().getTopsellers();
                        prodname = new String[list.size()];
                        prodprice = new String[list.size()];
                        prodoffper = new String[list.size()];
                        proddiscount = new String[list.size()];
                        prodimage = new String[list.size()];
                        proddiscription = new String[list.size()];
                        prodid = new int[list.size()];
                        prodbrand = new String[list.size()];
                        prodwishlist= new int[list.size()];
                        Existincart= new int[list.size()];
                        int[] productidlocal = new int[list.size()];
//
                        mImageUrls.clear();
                        for (int i = 0; i < list.size(); i++) {
                            prodname[i] = list.get(i).getProductName();
                            prodprice[i] = list.get(i).getProductPrice();
                            proddiscount[i] = list.get(i).getProductSalePrice();
                            prodimage[i] = list.get(i).getProductImages().getImageThumb();

                            mImageUrls.add(prodimage[i]);
                            proddiscription[i] = list.get(i).getProductDescription();
                            prodid[i] = list.get(i).getProductID();
                            prodbrand[i] = list.get(i).getBrandDisplayName();
                            prodwishlist[i] = list.get(i).getProductWishlist();
                            Existincart[i] = list.get(i).getIsExistInCart();
                            productidlocal[i] = prodid[i];

                        }
                        recyclerView.setAdapter(null);
                        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(prodimage, prodname, prodprice
                                , proddiscount, prodoffper, proddiscription, productidlocal, prodbrand,prodwishlist,Existincart);
                        recyclerView.setAdapter(recyclerViewAdapter);
                    } else {
                        ToppickConstraint.setVisibility(View.GONE);
                    }
                }
                catch (Exception e)
                {
                    Log.d("response issue","response not comming at mtopsell");
                }

            }

            @Override
            public void onFailure(Call<MonthlyTopsellerExample> call, Throwable t) {
//                Toast.makeText(context, "MOT "+ t, Toast.LENGTH_SHORT).show();
            }
        });

//        Call<List<Example>> call =monthlytopsellerApi.getDetails();
//        call.enqueue(new Callback<List<Example>>() {
//            @Override
//            public void onResponse(Call<List<Example>> call, Response<List<Example>> response) {
//                list = response.body();
//
////                Toast.makeText(Dashboard.this, "aa "+list, Toast.LENGTH_SHORT).show();
//                prodname = new String[list.size()];
//                prodprice = new String[list.size()];
//                prodoffper = new String[list.size()];
//                proddiscount = new String[list.size()];
//                prodimage = new String[list.size()];
//                proddiscription = new String[list.size()];
//                prodid = new int[list.size()];
//                prodbrand = new String[list.size()];
//                int [] productidlocal = new int[list.size()];
////
//                mImageUrls.clear();
//                for(int i = 0 ;i<list.size();i++)
//                {
//                    prodname[i] =list.get(i).getProductDisplayName();
//                    prodprice[i] = list.get(i).getProductPrice();
//                    proddiscount[i] = list.get(i).getProductSalePrice();
//                    prodimage[i] = "https://cdn.homeshopping.pk/product_images/"+list.get(i).getImageFile();
//
//                    mImageUrls.add(prodimage[i]);
//                    proddiscription[i] = list.get(i).getProductDescription();
//                    prodid[i] = list.get(i).getProductID();
//                    prodbrand[i] = list.get(i).getBrandDisplayName();
//                    productidlocal[i] = prodid[i];
//
//                }

//                recyclerView.setAdapter(null);
//                RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(prodimage,prodname,prodprice
//                        ,proddiscount,prodoffper,proddiscription,productidlocal,prodbrand);
//                recyclerView.setAdapter(recyclerViewAdapter);


//



//            }
//
//            @Override
//            public void onFailure(Call<List<Example>> call, Throwable t) {
////                Toast.makeText(context, "reloading...", Toast.LENGTH_SHORT).show();
//                Log.e("Error : ",""+t);
//                getmonthlytopseler();
//            }
//        });

    }


    private void getHomeProducts()
    {
//        Toast.makeText(context, "in home products", Toast.LENGTH_SHORT).show();
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.connectTimeout(150, TimeUnit.SECONDS);
        client.readTimeout(150, TimeUnit.SECONDS);
        client.writeTimeout(150, TimeUnit.SECONDS);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL).client(client.build()) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        HomeCategoryAPI homeCategoryAPI = retrofit.create(HomeCategoryAPI.class);
        Call<HomeExample> homeExampleCall = homeCategoryAPI.getDetails(MainActivity.Tokendata,Dashboard.prefConfig.readID());
        homeExampleCall.enqueue(new Callback<HomeExample>() {
            @Override
            public void onResponse(Call<HomeExample> call, Response<HomeExample> response) {
                if(response.isSuccessful()) {
                    int status = response.body().getStatus();
//                Toast.makeText(context, "jajaja"+status, Toast.LENGTH_SHORT).show();
                    if (status == 1) {

                        homeCategoryProducts = response.body().getHomeCategoryProducts();
//                    Toast.makeText(context, ""+homeCategoryProducts.size(), Toast.LENGTH_SHORT).show();
                        categoryId = new ArrayList<>();
                        categoryTitle = new ArrayList<>();
                        for (int i = 0; i < homeCategoryProducts.size(); i++) {
                            categoryProducts.clear();
                            categoryId.add(homeCategoryProducts.get(i).getCategoryId());
                            categoryTitle.add(homeCategoryProducts.get(i).getCategoryTitle());
//                    categoryBanners.add(homeCategoryProducts.get(i).getCategoryBanner().getImagePath());
                            categoryProducts = homeCategoryProducts.get(i).getCategoryProducts();

//                    Toast.makeText(context, "rrrr"+categoryProducts.size(), Toast.LENGTH_SHORT).show();

                            ProductID = new ArrayList<>();
                            ProductName = new ArrayList<>();
                            ProductUrl = new ArrayList<>();
                            ProductDescription = new ArrayList<>();
                            ProductPrice = new ArrayList<>();
                            ProductSalePrice = new ArrayList<>();
                            BrandDisplayName = new ArrayList<>();
                            productImages = new ArrayList<>();
                            productRating = new ArrayList<>();
                            productWishlist = new ArrayList<>();
                            isExistInCart = new ArrayList<>();
                            for (int j = 0; j < categoryProducts.size(); j++) {
//                        Toast.makeText(context, "cat prd size "+categoryProducts.size(), Toast.LENGTH_SHORT).show();

                                ProductID.add(categoryProducts.get(j).getProductID());
                                ProductName.add(categoryProducts.get(j).getProductName());
                                ProductUrl.add(categoryProducts.get(j).getProductUrl());
//                       ProductDescription.add(categoryProducts.get(j).getProductDescription());
                                ProductDescription.add("a");
                                ProductPrice.add(categoryProducts.get(j).getProductPrice());
                                ProductSalePrice.add(categoryProducts.get(j).getProductSalePrice());
                                BrandDisplayName.add(categoryProducts.get(j).getBrandDisplayName());
                                if (categoryProducts.get(j).getProductImages() != null) {
                                    productImages.add(categoryProducts.get(j).getProductImages().getImageThumb());
                                } else {
                                    productImages.add("");
                                }
                                productRating.add(categoryProducts.get(j).getProductRating());
                                productWishlist.add(categoryProducts.get(j).getProductWishlist());
                                isExistInCart.add(categoryProducts.get(j).getIsExistInCart());
                            }
//                    Toast.makeText(context, "Prod name = "+ProductName.get(0), Toast.LENGTH_SHORT).show();


                            if (i == 0) {
                                recyclerView2.setAdapter(null);
                                Pannelonename.setText("" + categoryTitle.get(0));
//                            Toast.makeText(context, "rec new image size  "+productImages.size(), Toast.LENGTH_SHORT).show();
                                RecyclerViewAdapter3 recyclerViewAdapter21 = new RecyclerViewAdapter3(productImages, ProductName,
                                        ProductPrice, ProductSalePrice, ProductDescription, ProductID, BrandDisplayName,productWishlist,isExistInCart);
                                recyclerView2.setAdapter(recyclerViewAdapter21);
                            } else if (i == 1) {
                                recyclerView4.setAdapter(null);
                                Panneltwoname.setText("" + categoryTitle.get(1));
                                RecyclerViewAdapter3 recyclerViewAdapter211 = new RecyclerViewAdapter3(productImages, ProductName, ProductPrice
                                        , ProductSalePrice, ProductDescription, ProductID, BrandDisplayName,productWishlist,isExistInCart);
                                recyclerView4.setAdapter(recyclerViewAdapter211);
                            } else if (i == 2) {
                                recyclerView3.setAdapter(null);
                                PannelThreename.setText("" + categoryTitle.get(2));
                                RecyclerViewAdapter3 recyclerViewAdapter2111 = new RecyclerViewAdapter3(productImages, ProductName, ProductPrice
                                        , ProductSalePrice, ProductDescription, ProductID, BrandDisplayName,productWishlist,isExistInCart);
                                recyclerView3.setAdapter(recyclerViewAdapter2111);
                            } else if (i == 3) {
                                recyclerView5.setAdapter(null);
                                Pannelfourname.setText("" + categoryTitle.get(3));
                                RecyclerViewAdapter3 recyclerViewAdapter21111 = new RecyclerViewAdapter3(productImages, ProductName, ProductPrice
                                        , ProductSalePrice, ProductDescription, ProductID, BrandDisplayName,productWishlist,isExistInCart);
                                recyclerView5.setAdapter(recyclerViewAdapter21111);
                            }
                            else if (i == 4) {
                                recyclerView55.setAdapter(null);
                                Pannelfivename.setText("" + categoryTitle.get(4));
                                RecyclerViewAdapter3 recyclerViewAdapter211111 = new RecyclerViewAdapter3(productImages, ProductName, ProductPrice
                                        , ProductSalePrice, ProductDescription, ProductID, BrandDisplayName,productWishlist,isExistInCart);
                                recyclerView55.setAdapter(recyclerViewAdapter211111);
                            }

                        }

                    }
                }
            }
            @Override
            public void onFailure(Call<HomeExample> call, Throwable t) {
//                Toast.makeText(context, ""+t, Toast.LENGTH_SHORT).show();
                Log.e("home error",""+t);
               Toast.makeText(context, "Kindly check internet connection.", Toast.LENGTH_SHORT).show();
            }
        });
    }




    private void getmobilephoneandtablet()
    {

//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(ROOT_URL) //Setting the Root URL
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        MobilephonetabletApi mobilephonetabletApi =retrofit.create(MobilephonetabletApi.class);
//        Call<List<Example>> call =mobilephonetabletApi.getDetails();
//        call.enqueue(new Callback<List<Example>>() {
//            @Override
//            public void onResponse(Call<List<Example>> call, Response<List<Example>> response) {
//                list = response.body();
//
////                Toast.makeText(Dashboard.this, "aa "+list, Toast.LENGTH_SHORT).show();
//                prodname = new String[list.size()];
//                prodprice = new String[list.size()];
//                prodoffper = new String[list.size()];
//                proddiscount = new String[list.size()];
//                prodimage = new String[list.size()];
//                proddiscription = new String[list.size()];
//                prodid = new int[list.size()];
//                prodbrand = new String[list.size()];
////
//                mImageUrls.clear();
//                for(int i = 0 ;i<list.size();i++)
//                {
//                    prodname[i] =list.get(i).getProductDisplayName();
//                    prodprice[i] = list.get(i).getProductPrice();
//                    proddiscount[i] = list.get(i).getProductSalePrice();
//                    prodimage[i] = "https://cdn.homeshopping.pk/product_images/"+list.get(i).getImageFile();
//
//                    mImageUrls.add(prodimage[i]);
//                    proddiscription[i] = list.get(i).getProductDescription();
//                    prodid[i] = list.get(i).getProductID();
//                    prodbrand[i] = list.get(i).getBrandDisplayName();
//
//                }
//
//                recyclerView3.setAdapter(null);
//                RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(prodimage,prodname,prodprice
//                        ,proddiscount,prodoffper,proddiscription,prodid,prodbrand);
//                recyclerView3.setAdapter(recyclerViewAdapter);
//
//
////
//
//
//            }
//
//            @Override
//            public void onFailure(Call<List<Example>> call, Throwable t) {
////                Toast.makeText(context, "reloading...", Toast.LENGTH_SHORT).show();
//                Log.e("Error : ",""+t);
//                getmobilephoneandtablet();
//            }
//        });

    }

    public void getcameraandphoto()
    {

//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(ROOT_URL) //Setting the Root URL
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        CameraandphotoAPI cameraandphotoAPI =retrofit.create(CameraandphotoAPI.class);
//        Call<List<Example>> call =cameraandphotoAPI.getDetails();
//        call.enqueue(new Callback<List<Example>>() {
//            @Override
//            public void onResponse(Call<List<Example>> call, Response<List<Example>> response) {
//                list = response.body();
//
////                Toast.makeText(Dashboard.this, "aa "+list, Toast.LENGTH_SHORT).show();
//                prodname = new String[list.size()];
//                prodprice = new String[list.size()];
//                prodoffper = new String[list.size()];
//                proddiscount = new String[list.size()];
//                prodimage = new String[list.size()];
//                proddiscription = new String[list.size()];
//                prodid = new int[list.size()];
//                prodbrand = new String[list.size()];
////
//                mImageUrls.clear();
//                for(int i = 0 ;i<list.size();i++)
//                {
//                    prodname[i] =list.get(i).getProductDisplayName();
//                    prodprice[i] = list.get(i).getProductPrice();
//                    proddiscount[i] = list.get(i).getProductSalePrice();
//                    prodimage[i] = "https://cdn.homeshopping.pk/product_images/"+list.get(i).getImageFile();
//
//                    mImageUrls.add(prodimage[i]);
//                    proddiscription[i] = list.get(i).getProductDescription();
//                    prodid[i] = list.get(i).getProductID();
//                    prodbrand[i] = list.get(i).getBrandDisplayName();
//
//                }
//
////                recyclerView2.setAdapter(null);
////                RecyclerViewAdapter2 recyclerViewAdapter = new RecyclerViewAdapter2(prodimage,prodname,prodprice
////                        ,proddiscount,prodoffper,proddiscription,prodid,prodbrand);
////                recyclerView2.setAdapter(recyclerViewAdapter);
//
//
//
//
//
//            }
//
//            @Override
//            public void onFailure(Call<List<Example>> call, Throwable t) {
////                Toast.makeText(context, "reloading...", Toast.LENGTH_SHORT).show();
//                Log.e("Error : ",""+t);
//                getcameraandphoto();
//            }
//        });

    }

    public void getcomputerandlaptop()
    {

//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(ROOT_URL) //Setting the Root URL
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        ComputerandlaptoAPI computerandlaptoAPI =retrofit.create(ComputerandlaptoAPI.class);
//        Call<List<Example>> call =computerandlaptoAPI.getDetails();
//        call.enqueue(new Callback<List<Example>>() {
//            @Override
//            public void onResponse(Call<List<Example>> call, Response<List<Example>> response) {
//                list = response.body();
//
////                Toast.makeText(Dashboard.this, "aa "+list, Toast.LENGTH_SHORT).show();
//                prodname = new String[list.size()];
//                prodprice = new String[list.size()];
//                prodoffper = new String[list.size()];
//                proddiscount = new String[list.size()];
//                prodimage = new String[list.size()];
//                proddiscription = new String[list.size()];
//                prodid = new int[list.size()];
//                prodbrand = new String[list.size()];
////
//                mImageUrls.clear();
//                for(int i = 0 ;i<list.size();i++)
//                {
//                    prodname[i] =list.get(i).getProductDisplayName();
//                    prodprice[i] = list.get(i).getProductPrice();
//                    proddiscount[i] = list.get(i).getProductSalePrice();
//                    prodimage[i] = "https://cdn.homeshopping.pk/product_images/"+list.get(i).getImageFile();
//
//                    mImageUrls.add(prodimage[i]);
//                    proddiscription[i] = list.get(i).getProductDescription();
//                    prodid[i] = list.get(i).getProductID();
//                    prodbrand[i] = list.get(i).getBrandDisplayName();
//
//                }
//
//                recyclerView5.setAdapter(null);
//                RecyclerViewAdapter5 recyclerViewAdapter = new RecyclerViewAdapter5(prodimage,prodname,prodprice
//                        ,proddiscount,prodoffper,proddiscription,prodid,prodbrand);
//                recyclerView5.setAdapter(recyclerViewAdapter);
//            }
//
//            @Override
//            public void onFailure(Call<List<Example>> call, Throwable t) {
////                Toast.makeText(context, "reloading...", Toast.LENGTH_SHORT).show();
//                Log.e("Error : ",""+t);
//                getcomputerandlaptop();
//            }
//        });

    }
    public void gettvandvideo()
    {

//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(ROOT_URL) //Setting the Root URL
//                .addConverterFactory(GsonConverterFactory.create())
//                .build();
//        TVandVideoAPI tVandVideoAPI =retrofit.create(TVandVideoAPI.class);
//        Call<List<Example>> call =tVandVideoAPI.getDetails();
//        call.enqueue(new Callback<List<Example>>() {
//            @Override
//            public void onResponse(Call<List<Example>> call, Response<List<Example>> response) {
//                list = response.body();
//
////                Toast.makeText(Dashboard.this, "aa "+list, Toast.LENGTH_SHORT).show();
//                prodname = new String[list.size()];
//                prodprice = new String[list.size()];
//                prodoffper = new String[list.size()];
//                proddiscount = new String[list.size()];
//                prodimage = new String[list.size()];
//                proddiscription = new String[list.size()];
//                prodid = new int[list.size()];
//                prodbrand = new String[list.size()];
////
//                mImageUrls.clear();
//                for(int i = 0 ;i<list.size();i++)
//                {
//                    prodname[i] =list.get(i).getProductDisplayName();
//                    prodprice[i] = list.get(i).getProductPrice();
//                    proddiscount[i] = list.get(i).getProductSalePrice();
//                    prodimage[i] = "https://cdn.homeshopping.pk/product_images/"+list.get(i).getImageFile();
//
//                    mImageUrls.add(prodimage[i]);
//                    proddiscription[i] = list.get(i).getProductDescription();
//                    prodid[i] = list.get(i).getProductID();
//                    prodbrand[i] = list.get(i).getBrandDisplayName();
//
//                }
//
//                recyclerView4.setAdapter(null);
//                RecyclerViewAdapter4 recyclerViewAdapter = new RecyclerViewAdapter4(prodimage,prodname,prodprice
//                        ,proddiscount,prodoffper,proddiscription,prodid,prodbrand);
//                recyclerView4.setAdapter(recyclerViewAdapter);
//
//
//            }
//
//            @Override
//            public void onFailure(Call<List<Example>> call, Throwable t) {
//                Toast.makeText(context, "reloading...", Toast.LENGTH_SHORT).show();
//                Log.e("Error : ",""+t);
//                gettvandvideo();
//            }
//        });

    }
//    private void setupSearchBar() {
//        mSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
//
//            @Override
//            public void onSearchTextChanged(String oldQuery, final String newQuery) {
//                Retrofit retrofit = new Retrofit.Builder()
//                        .baseUrl(ROOT_URL) //Setting the Root URL
//                        .addConverterFactory(GsonConverterFactory.create())
//                        .build();
//
//                findifysugessionAPI api = retrofit.create(findifysugessionAPI.class);
//                Call<List<findifysugestionPOJO>> call = api.getDetails(MainActivity.Tokendata,newQuery);
//                call.enqueue(new Callback<List<findifysugestionPOJO>>() {
//                    @Override
//                    public void onResponse(Call<List<findifysugestionPOJO>> call, Response<List<findifysugestionPOJO>> response) {
//                        if (response.isSuccessful())
//                        {
//                        try {
//                        sugessionlist.clear();
//                        sugessionlist = response.body();
//
//                        for (int  i = 0 ; i <sugessionlist.size();i++)
//                        {
//
//                                sColorSuggestions.add(new ColorSuggestion(sugessionlist.get(i).getValue()));
//
//
//                        }
//                        }
//                        catch (Exception e)
//                        {
//                            Log.d("Null list",""+e);
//                        }
//                        }
//                        else
//                        {
////                            Toast.makeText(getActivity(), ""+response.message(), Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<List<findifysugestionPOJO>> call, Throwable t) {
//
//                    }
//                });
////                Toast.makeText(context, ""+newQuery, Toast.LENGTH_SHORT).show();
//                if (!oldQuery.equals("") && newQuery.equals("")) {
//                    mSearchView.clearSuggestions();
//                } else {
//
//                    //this shows the top left circular progress
//                    //you can call it where ever you want, but
//                    //it makes sense to do it when loading something in
//                    //the background.
//                    mSearchView.showProgress();
//
//                    //simulates a query call to a data source
//                    //with a new query.
//                    DataHelper.findSuggestions(getActivity(), newQuery, 5,
//                            FIND_SUGGESTION_SIMULATED_DELAY, new DataHelper.OnFindSuggestionsListener() {
//
//                                @Override
//                                public void onResults(List<ColorSuggestion> results) {
//
//                                    //this will swap the data and
//                                    //render the collapse/expand animations as necessary
//                                    mSearchView.swapSuggestions(results);
//
//                                    //let the users know that the background
//                                    //process has completed
//                                    mSearchView.hideProgress();
//                                }
//                            });
//                }
//
//                Log.d(TAG, "onSearchTextChanged()");
//            }
//        });

//        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
//            @Override
//            public void onSuggestionClicked(final SearchSuggestion searchSuggestion) {
//
//                ColorSuggestion colorSuggestion = (ColorSuggestion) searchSuggestion;
//                DataHelper.findColors(getActivity(), colorSuggestion.getBody(),
//                        new DataHelper.OnFindColorsListener() {
//
//                            @Override
//                            public void onResults(List<ColorWrapper> results) {
//                                //show search results
//                            }
//
//                        });
//                Log.d(TAG, "onSuggestionClicked()");
//
//                mLastQuery = searchSuggestion.getBody();
//                if(mLastQuery.equals("")||mLastQuery.equals("."))
//                {
//                    mSearchView.clearSuggestions();
//                    mSearchView.clearSearchFocus();
//                }
//                else
//                {
//                  Intent  intent = new Intent(context, SearchResultActivity.class);
//                  intent.putExtra("searchkeyword",mLastQuery);
//                  startActivity(intent);
//                    mSearchView.clearSuggestions();
//                    mSearchView.clearSearchFocus();
//                }
////                Toast.makeText(context, ""+mLastQuery, Toast.LENGTH_SHORT).show();
//
//
//            }
//
//            @Override
//            public void onSearchAction(String query) {
//                mLastQuery = query;
//                DataHelper.findColors(getActivity(), query,
//                        new DataHelper.OnFindColorsListener() {
//
//
//                            @Override
//                            public void onResults(List<ColorWrapper> results) {
//                                //show search results
//                            }
//
//                        });
//                Log.d(TAG, "onSearchAction()");
//                if(mLastQuery.equals("")||mLastQuery.equals("."))
//                {
////                    Toast.makeText(context, "Invalid search.", Toast.LENGTH_SHORT).show();
//                    mSearchView.clearSuggestions();
//                    mSearchView.clearSearchFocus();
//                }
//                else
//                {
//                    Intent  intent = new Intent(context, SearchResultActivity.class);
//                    intent.putExtra("searchkeyword",mLastQuery);
//                    startActivity(intent);
//                }
////                Toast.makeText(context, ""+mLastQuery, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        mSearchView.setOnFocusChangeListener(new FloatingSearchView.OnFocusChangeListener() {
//            @Override
//            public void onFocus() {
//
//                //show suggestions when search bar gains focus (typically history suggestions)
//                try {
//                    mSearchView.swapSuggestions(DataHelper.getHistory(getActivity(), sugessionlist.size()));
//
//                    Log.d(TAG, "onFocus()");
//                }catch (Exception e)
//                {
//                    Log.d("Null list",""+e);
//                }
//
//            }
//
//            @Override
//            public void onFocusCleared() {
//
//                //set the title of the bar so that when focus is returned a new query begins
//                mSearchView.setSearchBarTitle(mLastQuery);
//
//                //you can also set setSearchText(...) to make keep the query there when not focused and when focus returns
//                //mSearchView.setSearchText(searchSuggestion.getBody());
//
//                Log.d(TAG, "onFocusCleared()");
//            }
//        });


        //handle menu clicks the same way as you would
        //in a regular activity
//        mSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
//            @Override
//            public void onActionMenuItemSelected(MenuItem item) {
//
//                if (item.getItemId() == R.id.action_change_colors) {
//
//                    mIsDarkSearchTheme = true;
//
//                    //demonstrate setting colors for items
//                    mSearchView.setBackgroundColor(Color.parseColor("#787878"));
//                    mSearchView.setViewTextColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setHintTextColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setActionMenuOverflowColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setMenuItemIconColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setLeftActionIconColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setClearBtnColor(Color.parseColor("#e9e9e9"));
//                    mSearchView.setDividerColor(Color.parseColor("#BEBEBE"));
//                    mSearchView.setLeftActionIconColor(Color.parseColor("#e9e9e9"));
//                } else {
//
//                    //just print action
//                    Toast.makeText(getActivity().getApplicationContext(), item.getTitle(),
//                            Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });
//
//        //use this listener to listen to menu clicks when app:floatingSearch_leftAction="showHome"
//        mSearchView.setOnHomeActionClickListener(new FloatingSearchView.OnHomeActionClickListener() {
//            @Override
//            public void onHomeClicked() {
//
//                Log.d(TAG, "onHomeClicked()");
//            }
//        });

        /*
         * Here you have access to the left icon and the text of a given suggestion
         * item after as it is bound to the suggestion list. You can utilize this
         * callback to change some properties of the left icon and the text. For smarteist, you
         * can load the left icon images using your favorite image loading library, or change text color.
         *
         *
         * Important:
         * Keep in mind that the suggestion list is a RecyclerView, so views are reused for different
         * items in the list.
         */
//        mSearchView.setOnBindSuggestionCallback(new SearchSuggestionsAdapter.OnBindSuggestionCallback() {
//            @Override
//            public void onBindSuggestion(View suggestionView, ImageView leftIcon,
//                                         TextView textView, SearchSuggestion item, int itemPosition) {
//                ColorSuggestion colorSuggestion = (ColorSuggestion) item;
//
//                String textColor = mIsDarkSearchTheme ? "#ffffff" : "#000000";
//                String textLight = mIsDarkSearchTheme ? "#bfbfbf" : "#a1c913";
//
//                if (colorSuggestion.getIsHistory()) {
//                    leftIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
//                            R.drawable.ic_history_black_24dp, null));
//
//                    Util.setIconColor(leftIcon, Color.parseColor(textColor));
//                    leftIcon.setAlpha(.36f);
//                } else {
//                    leftIcon.setAlpha(0.0f);
//                    leftIcon.setImageDrawable(null);
//                }
//
//                textView.setTextColor(Color.parseColor(textColor));
//                String text = colorSuggestion.getBody()
//                        .replaceFirst(mSearchView.getQuery(),
//                                "<font color=\"" + textLight + "\">" + mSearchView.getQuery() + "</font>");
//                textView.setText(Html.fromHtml(text));
//            }
//
//        });
//    }
//
//    @Override
//    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//        mSearchView.setTranslationY(verticalOffset);
//    }
//
//    @Override
//    public void attachSearchViewActivityDrawer(FloatingSearchView searchView) {
//
//    }
//
//    @Override
//    public boolean onActivityBackPress() {
//        //if mSearchView.setSearchFocused(false) causes the focused search
//        //to close, then we don't want to close the activity. if mSearchView.setSearchFocused(false)
//        //returns false, we know that the search was already closed so the call didn't change the focus
//        //state and it makes sense to call supper onBackPressed() and close the activity
//        if (!mSearchView.setSearchFocused(false)) {
//            return false;
//        }
//        return true;
//    }
//
//    private void setupDrawer() {
//        attachSearchViewActivityDrawer(mSearchView);
//    }


    private void setSliderViews(String[] imagelist) {

        for (int i = 0; i < imagelist.length; i++) {

            sliderView = new SliderView(context);

//            switch (i) {
//                case 0:
////                    sliderView.setImageUrl("https://cdn.homeshopping.pk/templates/ResGarden/images/HomeSlider2018/Holiday-Deal-Home-page-banner.jpg");
//                    sliderView.setImageDrawable(R.drawable.slidermob);
//                    break;
//
//                case 1:
////                    sliderView.setImageUrl("https://cdn.homeshopping.pk/templates/ResGarden/images/HomeSlider2018/honor-10-lite-home-1440x330_A.jpg");
//                    sliderView.setImageDrawable(R.drawable.slidernew);
//                    break;
//                case 2:
////                    sliderView.setImageUrl("https://cdn.homeshopping.pk/templates/ResGarden/images/HomeSlider2018/Home-create-seller-banner.png");
//                    sliderView.setImageDrawable(R.drawable.slider3mob);
//                    break;
//
//                case 3:
////                    sliderView.setImageUrl("https://cdn.homeshopping.pk/templates/ResGarden/images/HomeSlider2018/Home-Mobile-Banner.jpg");
//
//                    sliderView.setImageDrawable(R.drawable.slider2mob);
//                    break;
////                case 5:
////                    sliderView.setImageUrl("https://cdn.homeshopping.pk/templates/ResGarden/images/HomeSlider2018/Home-Led-Banner.jpg");
////                    break;
//            }
            sliderView.setImageUrl(imagelist[i]);
            sliderView.setImageScaleType(ImageView.ScaleType.FIT_XY);


//            sliderView.setDescription("setDescription " + (i + 1));
//            final int finalI = i;

//            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
//                @Override
//                public void onSliderClick(SliderView sliderView) {
//                    Toast.makeText(Dashboard.this, "This is slider " + (finalI + 1), Toast.LENGTH_SHORT).show();
//                }
//            });

            //at last add this view in your layout :
            sliderLayout.addSliderView(sliderView);
        }
    }





//    @Override
//    public static void onAddProduct() {
//
//
//        cart_count++;
//        invalidateOptionsMenu();
////        Snackbar.make((CoordinatorLayout)findViewById(R.id.drawer_layout), "Added to cart !!", Snackbar.LENGTH_LONG)
////                .setAction("Action", null).show();
//
//    }
//
//    @Override
//    public void onRemoveProduct() {
//        cart_count--;
//        invalidateOptionsMenu();
////        Snackbar.make((CoordinatorLayout)findViewById(R.id.drawer_layout), "Removed from cart !!", Snackbar.LENGTH_LONG)
////                .setAction("Action", null).show();
//    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }




    @Override
    public void onAddProduct(int pid, String Qty, String userid, String combinationid, String productTitle, String Productprice, String Productimage, String ProductBrand, String feildid, String feildOption,String bankId) {
        db.addRecord(Integer.toString(pid),Qty,userid,combinationid,0,productTitle,Productprice,Productimage,ProductBrand,feildid,feildOption,"0");
        Boolean res = Dashboard.prefConfig.readLoginStatus();
        if (res == false) {

            Cursor cursor1 = db.getcountnotlogin();
            if(cursor1.moveToFirst())
            {

                Dashboard.cart_count =cursor1.getInt(0);
                getActivity().invalidateOptionsMenu();

//            Cursor cursor = db.getnotlogincartforDisplay();
//            if (cursor != null) {
//                if (cursor.moveToFirst()) {
//                    do {
//                        //calling the method to save the unsynced name to MySQL
//
//                        Dashboard.cart_count =cursor.getCount();
//                        getActivity().invalidateOptionsMenu();
//
////                    Toast.makeText(this, "aa "+Dashboard.cart_count, Toast.LENGTH_SHORT).show();
//                    } while (cursor.moveToNext());
//                }
                try {
                    //Toast.makeText(context, "Throughing", Toast.LENGTH_SHORT).show();
                    context.registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    context.registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                }
                catch (Exception e)
                {
                    Log.e("error",""+e);
                }

            }
        }else {
            Cursor cursor1 = db.getRecord();
            if (cursor1 != null) {
                if (cursor1.moveToFirst()) {
                    do {
                        //calling the method to save the unsynced name to MySQL



//                    Toast.makeText(this, "aa "+Dashboard.cart_count, Toast.LENGTH_SHORT).show();
                    } while (cursor1.moveToNext());
                }
                Dashboard.cart_count++;
                getActivity().invalidateOptionsMenu();
                try {
                  //  Toast.makeText(getContext(), "Throughing", Toast.LENGTH_SHORT).show();
                    context.registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
                    context.registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

                }
                catch (Exception e)
                {
                    Log.e("error",""+e);
                }

            }
        }

        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onRemoveProduct() {

    }

    @Override
    public void onAddProduct() {
        Dashboard.cart_count++;
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {

    }
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.normalmenu, menu);
//        MenuItem menuItem = menu.findItem(R.id.refresh2);
//        menuItem.setIcon(Converter.convertLayoutToImage(context,cart_count,R.drawable.ic_add_shopping_cart_white_24dp));
//        super.onCreateOptionsMenu(menu, inflater);
//    }


    public class DumRecyclerViewAdapter extends RecyclerView.Adapter<DumRecyclerViewAdapter.ViewHolder> {

        private static final String TAG = "RecyclerViewAdapter";

        //vars





        public    Context mContext;

        public DumRecyclerViewAdapter(int[] Productimageee,String[] productnameee,
                                   String[] orignalpricetvvv,String[] offpercenttvvv,
                                   String[] discountedpricetvvv) {

            Productimageedum = Productimageee;
            productnamee = productnameee;
            orignalpricetvv = orignalpricetvvv;
            offpercenttvv = offpercenttvvv;
            discountedpricetvv = discountedpricetvvv;

            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custommonthlytopitem, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            Log.d(TAG, "onBindViewHolder: called.");

//            holder.name.setText(mNames.get(position));
//            Glide.with(mContext).load(Productimagee[position])
//                    .into(holder.Productimage);
            holder.Productimage.setBackgroundResource(Productimageedum[position]);

//            holder.goalname.setText(mNames.get(position).toString());
//
//            holder.status.setText(mStatus.get(position).toString());


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
//            Date date = null;
//            String showdate = "";
//            try {
//                date = simpleDateFormat.parse(start_datee[position]);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            showdate = simpleDateFormat1.format(date);
//            holder.Productimage.setBackgroundResource(mImageUrls.get(1));
//            holder.productname.setText(""+productnamee[position]);
//            holder.orignalpricetv.setText("RS "+orignalpricetvv[position]);
//            holder.offpercenttv.setText( "off 10%");
//            holder.discountedpricetv.setText("RS "+orignalpricetvv[position]);
//            holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            //            holder.addcart.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onAddProduct(pid[position],"1",orignalpricetvv[position],productnamee[position],Productimagee[position]);
//                    Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
//                }
//            });


//            holder.allcons.addOnItemTouchListener(new RecyclerItemClickListener(Dashboard.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//                @Override
//                public void onItemClick(View view, int position) {
//                    // ...
//                    // Toast.makeText(MainActivity.this, "hello "+position, Toast.LENGTH_SHORT).show();
//`
////                    Intent intent = new Intent(getApplicationContext(),EventDiscriptionActivity.class);
////                    intent.putExtra("ev_tittlee",ev_tittlee[position]);
////                    intent.putExtra("ev_excerptt",ev_excerptt[position]);
////                    intent.putExtra("ev_discriptionn",ev_discriptionn[position]);
////                    intent.putExtra("start_datee",start_datee[position]);
////                    intent.putExtra("end_datee",end_datee[position]);
////                    intent.putExtra("starttimee",starttimee[position]);
////                    intent.putExtra("endtimee",endtimee[position]);
////
////                    intent.putExtra("ev_imagee",ev_imagee[position]);
////                    intent.putExtra("ev_locationn",ev_locationn[position]);
////                    intent.putExtra("ev_addresss",ev_addresss[position]);
//////
////                    startActivity(intent);
//                        Intent intent = new Intent(getApplicationContext(),ProductDetailActivity.class);
//                        intent.putExtra("title",productnamee[position]);
//                        intent.putExtra("discription",proddiscriptionn[position]);
//                        startActivity(intent);
////                    onAddProduct();
//
//
//
//                }
//
//                @Override
//                public void onItemLongClick(View view, int position) {
//                    // ...
//                }
//            }));

//            holder.allcons.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
////                    for(int j = 0 ;j<prodimage2d.length;j++ ) {
////                        for(int i = 0 ; i <prodimage2d[j].length;i++) {
////                            Toast.makeText(mContext, "" + prodimage2d[j][i], Toast.LENGTH_SHORT).show();
////                        }
////                        }
////
//                    Intent intent = new Intent(context, ProductDetailActivity.class);
//                    intent.putExtra("title",productnamee[position]);
//                    intent.putExtra("discription",proddiscriptionn[position]);
//                    intent.putExtra("price",orignalpricetvv[position]);
//                    intent.putExtra("productid",Integer.toString(pid[position]));
//                    intent.putExtra("prodbrand",prodbrandd[position]);
//                    mContext.startActivity(intent);
////                    Toast.makeText(mContext, "product id ="+pid[position], Toast.LENGTH_SHORT).show();
//                }
//            });


        }


        @Override
        public int getItemCount() {
            return Productimageedum.length;
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            ImageView Productimage,addcart;
            TextView productname,orignalpricetv,offpercenttv,discountedpricetv;
            ConstraintLayout allcons;
            com.iarcuschin.simpleratingbar.SimpleRatingBar ratingBaritem;



            public ViewHolder(View itemView) {
                super(itemView);
                Productimage = itemView.findViewById(R.id.imageView2);
                productname = itemView.findViewById(R.id.productname);
                orignalpricetv = itemView.findViewById(R.id.orignalpricetv);
                offpercenttv = itemView.findViewById(R.id.offpercenttv);
                discountedpricetv = itemView.findViewById(R.id.discountedpricetv);
                addcart = itemView.findViewById(R.id.addcart);
                allcons = itemView.findViewById(R.id.allcons);
            }
        }

    }





    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

        private static final String TAG = "RecyclerViewAdapter";

        //vars





        public    Context mContext;

        public RecyclerViewAdapter(String[] Productimageee,String[] productnameee,
                                   String[] orignalpricetvvv,String[] offpercenttvvv,
                                   String[] discountedpricetvvv,String[] proddiscription, int[] prodid,String[] prodbrand,int[] prodwishlist,int[] Existincart) {

            Productimagee = Productimageee;
            productnamee = productnameee;
            orignalpricetvv = orignalpricetvvv;
            offpercenttvv = offpercenttvvv;
            discountedpricetvv = discountedpricetvvv;
            proddiscriptionn = proddiscription;
            pid = prodid;
            prodbrandd = prodbrand;
            mContext = context;
            prodwishlistt = prodwishlist;
            Existincartt = Existincart;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custommonthlytopitem, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Log.d(TAG, "onBindViewHolder: called.");

//            holder.name.setText(mNames.get(position));
            Glide.with(mContext).load(Productimagee[position])
                    .into(holder.Productimage);

//            holder.goalname.setText(mNames.get(position).toString());
//
//            holder.status.setText(mStatus.get(position).toString());


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
//            Date date = null;
//            String showdate = "";
//            try {
//                date = simpleDateFormat.parse(start_datee[position]);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            showdate = simpleDateFormat1.format(date);
//            holder.Productimage.setBackgroundResource(mImageUrls.get(1));

              Productprice = Math.round(Float.parseFloat(orignalpricetvv[position]));
             SalePrice = Math.round(Float.parseFloat(offpercenttvv[position])) ;

             discoutnamount = Productprice-SalePrice;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            final String formatedproductprice = formatter.format(Productprice);
            final String formatedsaleprice = formatter.format(SalePrice);

//            Log.e("Discount data",Productprice+"\n"+SalePrice+"\n"+discoutnamount);
            Boolean res2 = db.getproductforwishlist(pid[position]);

            if(res2 == false)
                holder.wishlisticon.setBackgroundResource(R.drawable.heartgrey);
            else
            {
                holder.wishlisticon.setBackgroundResource(R.drawable.heartfilled);

            }
            Boolean res = db.getproductforaddedcart(pid[position]);


            if(res == false)
            {
                holder.addcart.setBackgroundResource(R.drawable.add_to_cart);
                addcartstatus = 0;
            }
            else
            {
                holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                holder.addcart.setEnabled(false);
                addcartstatus = 1;
            }

            if(SalePrice<Productprice)
            {
                holder.offpercenttv.setVisibility(View.VISIBLE);
                holder.discountedpricetv.setVisibility(View.VISIBLE);
                holder.orignalpricetv.setTextColor(Color.LTGRAY);
                int percentage = ((discoutnamount*100)/Productprice);
                if(productnamee[position].length()>=47) {
                    String stringlimit = productnamee[position].substring(0, 47);
                    holder.productname.setText(stringlimit + "...");
                }
                else
                {
                    holder.productname.setText(productnamee[position]+ "...");
                }
                holder.orignalpricetv.setText("PKR "+formatedproductprice);
                holder.orignalpricetv.setTextSize(10);
                holder.offpercenttv.setText( percentage+"%"+" OFF");
                holder.discountedpricetv.setText("PKR "+formatedsaleprice);
                holder.brandname.setText(prodbrandd[position]);
                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Toast.makeText(mContext, "sp sale = "+Math.round(Float.parseFloat(offpercenttvv[position])), Toast.LENGTH_SHORT).show();
                        onAddProduct(pid[position],"1",Dashboard.prefConfig.readID(),"0",productnamee[position],Float.toString(Math.round(Float.parseFloat(offpercenttvv[position]))),Productimagee[position],prodbrandd[position],"","","0");
//                        Toast.makeText(mContext, "Added to cart!"+Dashboard.prefConfig.readID(), Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setEnabled(false);
                        addcartstatus = 1;
                    }
                });

                SalePrice = 1;
            }
            else
            {
                if(productnamee[position].length()>=47) {
                    String stringlimit = productnamee[position].substring(0, 47);
                    holder.productname.setText(stringlimit + "...");
                }
                else
                {
                    holder.productname.setText(productnamee[position]+ "...");
                }

                holder.orignalpricetv.setText("PKR "+formatedsaleprice);
                holder.orignalpricetv.setTextColor(Color.BLACK);
                    holder.orignalpricetv.setTextSize(20);
                    holder.orignalpricetv.setPaintFlags(0);
                holder.offpercenttv.setVisibility(View.GONE);
                holder.discountedpricetv.setVisibility(View.GONE);
                holder.brandname.setText(prodbrandd[position]);
//                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid[position],"1",Dashboard.prefConfig.readID(),"0",productnamee[position],Float.toString(Math.round(Float.parseFloat(orignalpricetvv[position]))),Productimagee[position],prodbrandd[position],"","","0");
//                        Toast.makeText(mContext, "sp "+Math.round(Float.parseFloat(orignalpricetvv[position])), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setEnabled(false);
                        addcartstatus = 1;
                    }
                });

            }



//            holder.allcons.addOnItemTouchListener(new RecyclerItemClickListener(Dashboard.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//                @Override
//                public void onItemClick(View view, int position) {
//                    // ...
//                    // Toast.makeText(MainActivity.this, "hello "+position, Toast.LENGTH_SHORT).show();
//`
////                    Intent intent = new Intent(getApplicationContext(),EventDiscriptionActivity.class);
////                    intent.putExtra("ev_tittlee",ev_tittlee[position]);
////                    intent.putExtra("ev_excerptt",ev_excerptt[position]);
////                    intent.putExtra("ev_discriptionn",ev_discriptionn[position]);
////                    intent.putExtra("start_datee",start_datee[position]);
////                    intent.putExtra("end_datee",end_datee[position]);
////                    intent.putExtra("starttimee",starttimee[position]);
////                    intent.putExtra("endtimee",endtimee[position]);
////
////                    intent.putExtra("ev_imagee",ev_imagee[position]);
////                    intent.putExtra("ev_locationn",ev_locationn[position]);
////                    intent.putExtra("ev_addresss",ev_addresss[position]);
//////
////                    startActivity(intent);
//                        Intent intent = new Intent(getApplicationContext(),ProductDetailActivity.class);
//                        intent.putExtra("title",productnamee[position]);
//                        intent.putExtra("discription",proddiscriptionn[position]);
//                        startActivity(intent);
////                    onAddProduct();
//
//
//
//                }
//
//                @Override
//                public void onItemLongClick(View view, int position) {
//                    // ...
//                }
//            }));

            holder.allcons.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


//                    for(int j = 0 ;j<prodimage2d.length;j++ ) {
//                        for(int i = 0 ; i <prodimage2d[j].length;i++) {
//                            Toast.makeText(mContext, "" + prodimage2d[j][i], Toast.LENGTH_SHORT).show();
//                        }
//                        }
//
  try
  {
      progressBarmotop.setVisibility(View.VISIBLE);

      Intent intent = new Intent(context, ProductDetailActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      intent.putExtra("title",productnamee[position]);
      intent.putExtra("discription",proddiscriptionn[position]);
      intent.putExtra("price",orignalpricetvv[position]);
      intent.putExtra("productid",Integer.toString(pid[position]));
      intent.putExtra("prodbrand",prodbrandd[position]);
      intent.putExtra("prodwishlist",prodwishlistt[position]);
      intent.putExtra("addcartstatus",addcartstatus);

      mContext.startActivity(intent);
      progressBarmotop.setVisibility(View.GONE);

  }
  catch (Exception e)
  {

  }

//                    Toast.makeText(mContext, "product id ="+pid[position], Toast.LENGTH_SHORT).show();
                }
            });
            holder.wishlisticon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    if (Dashboard.prefConfig.readLoginStatus()) {

                        if (!db.getproductforwishlist(pid[position])) {
                            db.addRecordwishlist(Integer.toString(pid[position]) ,Dashboard.prefConfig.readID());
                            Toast.makeText(getActivity(), "Please wait adding item to wishlist.", Toast.LENGTH_SHORT).show();
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(ROOT_URL) //Setting the Root URL
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                            AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
                            Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata, String.valueOf(pid[position]), Dashboard.prefConfig.readID());
                            //   Toast.makeText(mContext, " id = "+pid[position], Toast.LENGTH_SHORT).show();
                            call.enqueue(new Callback<generalstatusmessagePOJO>() {
                                @Override
                                public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                                    if (response.isSuccessful()) {
                                        int status = response.body().getStatus();
                                        String message = response.body().getMessage().toString();
                                        if (status == 1) {
                                            holder.wishlisticon.setBackgroundResource(R.drawable.heartfilled);
//                                View parentLayout = findViewById(android.R.id.content);
//                                Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
//                                View sbView = snackbar.getView();
//                                sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                                snackbar.show();
                                            Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();

                                        } else {
                                            holder.wishlisticon.setBackgroundResource(R.drawable.heartgrey);
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                                }
                            });

                        } else {
                            Toast.makeText(getActivity(), "Item already added to wishlist.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(mContext, "Kindly login first to make wishlist", Toast.LENGTH_SHORT).show();
                    }
                }

            });

        }


        @Override
        public int getItemCount() {
            return Productimagee.length;
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            ImageView Productimage,addcart,wishlisticon;
            TextView productname,orignalpricetv,offpercenttv,discountedpricetv,brandname;
            ConstraintLayout allcons;
            com.iarcuschin.simpleratingbar.SimpleRatingBar ratingBaritem;



            public ViewHolder(View itemView) {
                super(itemView);
                Productimage = itemView.findViewById(R.id.imageView2);
                productname = itemView.findViewById(R.id.productname);
                orignalpricetv = itemView.findViewById(R.id.orignalpricetv);
                offpercenttv = itemView.findViewById(R.id.offpercenttv);
                discountedpricetv = itemView.findViewById(R.id.discountedpricetv);
                addcart = itemView.findViewById(R.id.addcart);
                allcons = itemView.findViewById(R.id.allcons);
                wishlisticon = itemView.findViewById(R.id.wishlisticon);
                brandname = itemView.findViewById(R.id.brandname);
            }
        }

    }







    public  class RecyclerViewAdapter2 extends RecyclerView.Adapter<RecyclerViewAdapter2.ViewHolder> {

        private static final String TAG = "RecyclerViewAdapter";

        //vars

        private ArrayList<Integer> mImageUrlsee = new ArrayList<>();
        ArrayList<String> Productimagee ;
        ArrayList<String> productnamee ;
        ArrayList<String> orignalpricetvv ;
        ArrayList<String> offpercenttvv ;
        ArrayList<String>discountedpricetvv;
        ArrayList<String>proddiscriptionn;
        String[][] prodimage2dd;
        ArrayList<Integer> pid ;
        ArrayList<String> prodbrandd;



        public    Context mContext;


        public RecyclerViewAdapter2(ArrayList<String> Productimageee,ArrayList<String> productnameee,
                                    ArrayList<String> orignalpricetvvv,
                                    ArrayList<String> discountedpricetvvv,ArrayList<String>proddiscription,
                                    ArrayList<Integer> prodid,ArrayList<String> prodbrand) {

            Productimagee = Productimageee;
            productnamee = productnameee;
            orignalpricetvv = orignalpricetvvv;

            discountedpricetvv = discountedpricetvvv;
            proddiscriptionn = proddiscription;
            pid = prodid;
            prodbrandd = prodbrand;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custommonthlytopitem, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Log.d(TAG, "onBindViewHolder: called.");

//            holder.name.setText(mNames.get(position));
            Glide.with(mContext).load(Productimagee.get(position))
                    .into(holder.Productimage);

//            holder.goalname.setText(mNames.get(position).toString());
//
//            holder.status.setText(mStatus.get(position).toString());


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
//            Date date = null;
//            String showdate = "";
//            try {
//                date = simpleDateFormat.parse(start_datee[position]);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            showdate = simpleDateFormat1.format(date);
//            holder.Productimage.setBackgroundResource(mImageUrls.get(1));
            Productprice = Math.round(Float.parseFloat(orignalpricetvv.get(position)));
            SalePrice = Math.round(Float.parseFloat(discountedpricetvv.get(position))) ;
            discoutnamount = Productprice-SalePrice;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            final String formatedproductprice = formatter.format(Productprice);
            final String formatedsaleprice = formatter.format(SalePrice);

            Log.e("Discount data",Productprice+"\n"+SalePrice+"\n"+discoutnamount);
            if(SalePrice!=0)
            {
                holder.offpercenttv.setVisibility(View.VISIBLE);
                holder.discountedpricetv.setVisibility(View.VISIBLE);
                holder.orignalpricetv.setTextColor(Color.LTGRAY);
                int percentage = ((discoutnamount*100)/Productprice);
                holder.productname.setText(productnamee.get(position));
                holder.orignalpricetv.setText("RS "+formatedproductprice);
                holder.offpercenttv.setText( percentage+"%"+" OFF");
                holder.discountedpricetv.setText("RS "+formatedsaleprice);
                holder.brandname.setText(prodbrandd.get(position));
                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid.get(position),"1",Dashboard.prefConfig.readID(),"0",productnamee.get(position),Float.toString(Math.round(Float.parseFloat(discountedpricetvv.get(position)))),Productimagee.get(position),prodbrandd.get(position),"","","0");

//                        Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                    }
                });

                SalePrice = 1;
            }
            else
            {
                holder.productname.setText(productnamee.get(position));
                holder.orignalpricetv.setText("RS "+formatedproductprice);
                holder.orignalpricetv.setTextColor(Color.BLACK);
                holder.orignalpricetv.setTextSize(16);
                holder.offpercenttv.setVisibility(View.GONE);
                holder.discountedpricetv.setVisibility(View.GONE);
                holder.brandname.setText(prodbrandd.get(position));
//                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid.get(position),"1",Dashboard.prefConfig.readID(),"0",productnamee.get(position),Float.toString(Math.round(Float.parseFloat(orignalpricetvv.get(position)))),Productimagee.get(position),prodbrandd.get(position),"","","0");
//                        Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                    }
                });

            }

//            holder.allcons.addOnItemTouchListener(new RecyclerItemClickListener(Dashboard.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//                @Override
//                public void onItemClick(View view, int position) {
//                    // ...
//                    // Toast.makeText(MainActivity.this, "hello "+position, Toast.LENGTH_SHORT).show();
//`
////                    Intent intent = new Intent(getApplicationContext(),EventDiscriptionActivity.class);
////                    intent.putExtra("ev_tittlee",ev_tittlee[position]);
////                    intent.putExtra("ev_excerptt",ev_excerptt[position]);
////                    intent.putExtra("ev_discriptionn",ev_discriptionn[position]);
////                    intent.putExtra("start_datee",start_datee[position]);
////                    intent.putExtra("end_datee",end_datee[position]);
////                    intent.putExtra("starttimee",starttimee[position]);
////                    intent.putExtra("endtimee",endtimee[position]);
////
////                    intent.putExtra("ev_imagee",ev_imagee[position]);
////                    intent.putExtra("ev_locationn",ev_locationn[position]);
////                    intent.putExtra("ev_addresss",ev_addresss[position]);
//////
////                    startActivity(intent);
//                        Intent intent = new Intent(getApplicationContext(),ProductDetailActivity.class);
//                        intent.putExtra("title",productnamee[position]);
//                        intent.putExtra("discription",proddiscriptionn[position]);
//                        startActivity(intent);
////                    onAddProduct();
//
//
//
//                }
//
//                @Override
//                public void onItemLongClick(View view, int position) {
//                    // ...
//                }
//            }));

            holder.allcons.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


//                    for(int j = 0 ;j<prodimage2d.length;j++ ) {
//                        for(int i = 0 ; i <prodimage2d[j].length;i++) {
//                            Toast.makeText(mContext, "" + prodimage2d[j][i], Toast.LENGTH_SHORT).show();
//                        }
//                        }
//
                    try
                    {
                        progressBarmob.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(context, ProductDetailActivity.class);
                        intent.putExtra("title",productnamee.get(position));
                        intent.putExtra("discription",proddiscriptionn.get(position));
                        intent.putExtra("price",orignalpricetvv.get(position));
                        intent.putExtra("saleprice",discountedpricetvv.get(position));

                        intent.putExtra("productid",Integer.toString(pid.get(position)));
                        intent.putExtra("prodbrand",prodbrandd.get(position));
                        mContext.startActivity(intent);
                        progressBarmob.setVisibility(View.GONE);
                    }
                    catch (Exception e)
                    {

                    }

//                    Toast.makeText(mContext, "product id ="+pid[position], Toast.LENGTH_SHORT).show();
                }
            });

            holder.wishlisticon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
                    Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata,String.valueOf(pid.get(position)),Dashboard.prefConfig.readID());
                   // Toast.makeText(mContext, " id = "+pid.get(position), Toast.LENGTH_SHORT).show();
                    call.enqueue(new Callback<generalstatusmessagePOJO>() {
                        @Override
                        public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                            if (response.isSuccessful()) {
                                int status = response.body().getStatus();
                                String message = response.body().getMessage().toString();
                                if (status == 1) {
                                    holder.wishlisticon.setBackgroundResource(R.drawable.wishlistaddedicon);
//                                    View parentLayout = v.findViewById(android.R.id.content);
//                                    Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
//                                    View sbView = snackbar.getView();
//                                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                                    snackbar.show();
                                    Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();

                                } else {
                                    Toast.makeText(getActivity(), "Not added to wishlist", Toast.LENGTH_SHORT).show();
                                    holder.wishlisticon.setBackgroundResource(R.drawable.wishlistnotaddedicon);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {
//                            Toast.makeText(getActivity(), ""+t, Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            });

        }


        @Override
        public int getItemCount() {
            return Productimagee.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            ImageView Productimage,addcart,wishlisticon;
            TextView productname,orignalpricetv,offpercenttv,discountedpricetv,brandname;
            ConstraintLayout allcons;
            com.iarcuschin.simpleratingbar.SimpleRatingBar ratingBaritem;



            public ViewHolder(View itemView) {
                super(itemView);
                Productimage = itemView.findViewById(R.id.imageView2);
                productname = itemView.findViewById(R.id.productname);
                orignalpricetv = itemView.findViewById(R.id.orignalpricetv);
                offpercenttv = itemView.findViewById(R.id.offpercenttv);
                discountedpricetv = itemView.findViewById(R.id.discountedpricetv);
                addcart = itemView.findViewById(R.id.addcart);
                allcons = itemView.findViewById(R.id.allcons);
                wishlisticon = itemView.findViewById(R.id.wishlisticon);
                brandname = itemView.findViewById(R.id.brandname);
            }
        }

    }



    public  class RecyclerViewAdapter3 extends RecyclerView.Adapter<RecyclerViewAdapter3.ViewHolder> {

        private static final String TAG = "RecyclerViewAdapter";

        //vars

        private ArrayList<Integer> mImageUrlsee = new ArrayList<>();
        ArrayList<String> Productimagee ;
        ArrayList<String> productnamee ;
        ArrayList<String> orignalpricetvv ;
        ArrayList<String> offpercenttvv ;
        ArrayList<String>discountedpricetvv;
        ArrayList<String>proddiscriptionn;
        String[][] prodimage2dd;
        ArrayList<Integer> pid ;
        ArrayList<String> prodbrandd;
        ArrayList<Integer> productWishlistt;
        ArrayList<Integer> isExistInCartt;



        public    Context mContext;


        public RecyclerViewAdapter3(ArrayList<String> Productimageee, ArrayList<String> productnameee,
                                    ArrayList<String> orignalpricetvvv,
                                    ArrayList<String> discountedpricetvvv, ArrayList<String>proddiscription,
                                    ArrayList<Integer> prodid, ArrayList<String> prodbrand, ArrayList<Integer> productWishlist,ArrayList<Integer> isExistInCart) {


            Productimagee = Productimageee;
            productnamee = productnameee;
            orignalpricetvv = orignalpricetvvv;

            discountedpricetvv = discountedpricetvvv;
            proddiscriptionn = proddiscription;
            pid = prodid;
            prodbrandd = prodbrand;
            productWishlistt = productWishlist;
            isExistInCartt = isExistInCart;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custommonthlytopitem, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder,  int position) {
            Log.d(TAG, "onBindViewHolder: called.");

//            holder.name.setText(mNames.get(position));
            Glide.with(mContext).load(Productimagee.get(position))
                    .into(holder.Productimage);

//            holder.goalname.setText(mNames.get(position).toString());
//
//            holder.status.setText(mStatus.get(position).toString());


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
//            Date date = null;
//            String showdate = "";
//            try {
//                date = simpleDateFormat.parse(start_datee[position]);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            showdate = simpleDateFormat1.format(date);
//            holder.Productimage.setBackgroundResource(mImageUrls.get(1));
            Productprice = Math.round(Float.parseFloat(orignalpricetvv.get(position)));
            SalePrice = Math.round(Float.parseFloat(discountedpricetvv.get(position))) ;
            discoutnamount = Productprice-SalePrice;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            final String formatedproductprice = formatter.format(Productprice);
            final String formatedsaleprice = formatter.format(SalePrice);

            Log.e("Discount data",Productprice+"\n"+SalePrice+"\n"+discoutnamount);
            Boolean res2 = db.getproductforwishlist(pid.get(position));

            if(res2 == false)
            {
                    holder.wishlisticon.setBackgroundResource(R.drawable.heartgrey);
            }
            else
            {
                holder.wishlisticon.setBackgroundResource(R.drawable.heartfilled);
            }

            Boolean res = db.getproductforaddedcart(pid.get(position));


            if(res == false)
            {
                holder.addcart.setBackgroundResource(R.drawable.add_to_cart);
                addcartstatus = 0;
            }
            else
            {
                holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                holder.addcart.setEnabled(false);
                addcartstatus = 1;
            }
            Log.d("UserId = ",""+Dashboard.prefConfig.readID().toString() );
//            Log.d("w status"," "+productWishlist.get(position));
            if(SalePrice<Productprice)
            {
                holder.offpercenttv.setVisibility(View.VISIBLE);
                holder.discountedpricetv.setVisibility(View.VISIBLE);
                holder.orignalpricetv.setTextColor(Color.LTGRAY);
                int percentage = ((discoutnamount*100)/Productprice);
                if(productnamee.get(position).length()>=47) {
                    String stringlimit = productnamee.get(position).substring(0, 47);
                    holder.productname.setText(stringlimit + "...");
                }
                else
                {
                    holder.productname.setText(productnamee.get(position)+ "...");
                }

                holder.orignalpricetv.setText("PKR "+formatedproductprice);
                holder.orignalpricetv.setTextSize(10);
                holder.offpercenttv.setText( percentage+"%"+" OFF");
                holder.discountedpricetv.setText("PKR "+formatedsaleprice);
                holder.brandname.setText(prodbrandd.get(position));
                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid.get(position),"1",Dashboard.prefConfig.readID(),"0",productnamee.get(position),Float.toString(Math.round(Float.parseFloat(discountedpricetvv.get(position)))),Productimagee.get(position),prodbrandd.get(position),"","","0");

//                        Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setEnabled(false);
                        addcartstatus = 1;
                    }
                });

                SalePrice = 1;
            }
            else
            {
                if(productnamee.get(position).length()>=47) {
                    String stringlimit = productnamee.get(position).substring(0, 47);
                    holder.productname.setText(stringlimit + "...");
                }
                else
                {
                    holder.productname.setText(productnamee.get(position)+ "...");
                }

                holder.orignalpricetv.setText("PKR "+formatedsaleprice);
                holder.orignalpricetv.setTextColor(Color.BLACK);
                holder.orignalpricetv.setTextSize(20);
                holder.orignalpricetv.setTextColor(Color.BLACK);
                holder.orignalpricetv.setTextSize(20);
                holder.orignalpricetv.setPaintFlags(0);
                holder.offpercenttv.setVisibility(View.GONE);
                holder.discountedpricetv.setVisibility(View.GONE);
                holder.brandname.setText(prodbrandd.get(position));
//                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid.get(position),"1",Dashboard.prefConfig.readID(),"0",productnamee.get(position),Float.toString(Math.round(Float.parseFloat(orignalpricetvv.get(position)))),Productimagee.get(position),prodbrandd.get(position),"","","0");
//                        Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                        holder.addcart.setEnabled(false);
                        addcartstatus = 1;
                    }
                });

            }

//            holder.allcons.addOnItemTouchListener(new RecyclerItemClickListener(Dashboard.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//                @Override
//                public void onItemClick(View view, int position) {
//                    // ...
//                    // Toast.makeText(MainActivity.this, "hello "+position, Toast.LENGTH_SHORT).show();
//`
////                    Intent intent = new Intent(getApplicationContext(),EventDiscriptionActivity.class);
////                    intent.putExtra("ev_tittlee",ev_tittlee[position]);
////                    intent.putExtra("ev_excerptt",ev_excerptt[position]);
////                    intent.putExtra("ev_discriptionn",ev_discriptionn[position]);
////                    intent.putExtra("start_datee",start_datee[position]);
////                    intent.putExtra("end_datee",end_datee[position]);
////                    intent.putExtra("starttimee",starttimee[position]);
////                    intent.putExtra("endtimee",endtimee[position]);
////
////                    intent.putExtra("ev_imagee",ev_imagee[position]);
////                    intent.putExtra("ev_locationn",ev_locationn[position]);
////                    intent.putExtra("ev_addresss",ev_addresss[position]);
//////
////                    startActivity(intent);
//                        Intent intent = new Intent(getApplicationContext(),ProductDetailActivity.class);
//                        intent.putExtra("title",productnamee[position]);
//                        intent.putExtra("discription",proddiscriptionn[position]);
//                        startActivity(intent);
////                    onAddProduct();
//
//
//
//                }
//
//                @Override
//                public void onItemLongClick(View view, int position) {
//                    // ...
//                }
//            }));

            holder.allcons.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


//                    for(int j = 0 ;j<prodimage2d.length;j++ ) {
//                        for(int i = 0 ; i <prodimage2d[j].length;i++) {
//                            Toast.makeText(mContext, "" + prodimage2d[j][i], Toast.LENGTH_SHORT).show();
//                        }
//                        }
//
                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra("title",productnamee.get(position));
                    intent.putExtra("discription",proddiscriptionn.get(position));
                    intent.putExtra("price",orignalpricetvv.get(position));
                    intent.putExtra("saleprice",discountedpricetvv.get(position));
                    intent.putExtra("productid",Integer.toString(pid.get(position)));
                    intent.putExtra("prodbrand",prodbrandd.get(position));
                    intent.putExtra("prodwishlist",productWishlistt.get(position));
                    intent.putExtra("addcartstatus" ,addcartstatus);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
//                    Toast.makeText(mContext, "product id ="+pid[position], Toast.LENGTH_SHORT).show();
                }
            });

            holder.wishlisticon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    if(Dashboard.prefConfig.readLoginStatus()) {

                        if (!db.getproductforwishlist(pid.get(position))) {
                            db.addRecordwishlist(pid.get(position).toString(),Dashboard.prefConfig.readID());
                            Toast.makeText(getActivity(), "Please wait adding item to wishlist.", Toast.LENGTH_SHORT).show();
                            Retrofit retrofit = new Retrofit.Builder()
                                    .baseUrl(ROOT_URL) //Setting the Root URL
                                    .addConverterFactory(GsonConverterFactory.create())
                                    .build();
                            AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
                            Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata, String.valueOf(pid.get(position)), Dashboard.prefConfig.readID());
                            //   Toast.makeText(mContext, " id = "+pid.get(position), Toast.LENGTH_SHORT).show();
                            call.enqueue(new Callback<generalstatusmessagePOJO>() {
                                @Override
                                public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                                    if (response.isSuccessful()) {
                                        int status = response.body().getStatus();
                                        String message = response.body().getMessage().toString();
                                        if (status == 1) {
                                            holder.wishlisticon.setBackgroundResource(R.drawable.heartfilled);
                                            productWishlistt.set(position, 1);
//                                View parentLayout = v.findViewById(android.R.id.content);
//                                Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
//                                View sbView = snackbar.getView();
//                                sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                                snackbar.show();

                                        } else {
                                            holder.wishlisticon.setBackgroundResource(R.drawable.heartgrey);
                                        }
                                    } else {
                                        Toast.makeText(getActivity(), "" + response.message(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                                }
                            });
                        } else {
                            Toast.makeText(getActivity(), "Item already added to wishlist.", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(mContext, "Kindly login first to make wishlist", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }


        @Override
        public int getItemCount() {
            return Productimagee.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            ImageView Productimage,addcart,wishlisticon;
            TextView productname,orignalpricetv,offpercenttv,discountedpricetv,brandname;
            ConstraintLayout allcons;
            com.iarcuschin.simpleratingbar.SimpleRatingBar ratingBaritem;



            public ViewHolder(View itemView) {
                super(itemView);
                Productimage = itemView.findViewById(R.id.imageView2);
                productname = itemView.findViewById(R.id.productname);
                orignalpricetv = itemView.findViewById(R.id.orignalpricetv);
                offpercenttv = itemView.findViewById(R.id.offpercenttv);
                discountedpricetv = itemView.findViewById(R.id.discountedpricetv);
                addcart = itemView.findViewById(R.id.addcart);
                allcons = itemView.findViewById(R.id.allcons);
                wishlisticon = itemView.findViewById(R.id.wishlisticon);
                brandname = itemView.findViewById(R.id.brandname);


            }
        }

    }



    public  class RecyclerViewAdapter4 extends RecyclerView.Adapter<RecyclerViewAdapter4.ViewHolder> {

        private static final String TAG = "RecyclerViewAdapter";

        //vars

        private ArrayList<Integer> mImageUrlsee = new ArrayList<>();
        String[] Productimagee ;
        String[] productnamee ;
        String[] orignalpricetvv ;
        String[] offpercenttvv ;
        String[]discountedpricetvv;
        String[]proddiscriptionn;
        String[][] prodimage2dd;
        int [] pid ;
        String[] prodbrandd;



        public    Context mContext;


        public RecyclerViewAdapter4(String[] Productimageee,String[] productnameee,
                                    String[] orignalpricetvvv,String[] offpercenttvvv,
                                    String[] discountedpricetvvv,String[] proddiscription, int[] prodid,String[] prodbrand) {

            Productimagee = Productimageee;
            productnamee = productnameee;
            orignalpricetvv = orignalpricetvvv;
            offpercenttvv = offpercenttvvv;
            discountedpricetvv = discountedpricetvvv;
            proddiscriptionn = proddiscription;
            pid = prodid;
            prodbrandd = prodbrand;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custommonthlytopitem, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Log.d(TAG, "onBindViewHolder: called.");

//            holder.name.setText(mNames.get(position));
            Glide.with(mContext).load(Productimagee[position])
                    .into(holder.Productimage);

//            holder.goalname.setText(mNames.get(position).toString());
//
//            holder.status.setText(mStatus.get(position).toString());


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
//            Date date = null;
//            String showdate = "";
//            try {
//                date = simpleDateFormat.parse(start_datee[position]);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            showdate = simpleDateFormat1.format(date);
//            holder.Productimage.setBackgroundResource(mImageUrls.get(1));
            Productprice = Math.round(Float.parseFloat(orignalpricetvv[position]));
            SalePrice = Math.round(Float.parseFloat(offpercenttvv[position])) ;
            discoutnamount = Productprice-SalePrice;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            final String formatedproductprice = formatter.format(Productprice);
            final String formatedsaleprice = formatter.format(SalePrice);

            Log.e("Discount data",Productprice+"\n"+SalePrice+"\n"+discoutnamount);
            if(SalePrice!=0)
            {
                holder.offpercenttv.setVisibility(View.VISIBLE);
                holder.discountedpricetv.setVisibility(View.VISIBLE);
                holder.orignalpricetv.setTextColor(Color.LTGRAY);
                int percentage = ((discoutnamount*100)/Productprice);
                holder.productname.setText(productnamee[position]);
                holder.orignalpricetv.setText("RS "+formatedproductprice);
                holder.offpercenttv.setText( percentage+"%"+" OFF");
                holder.discountedpricetv.setText("RS "+formatedsaleprice);
                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid[position],"1",Dashboard.prefConfig.readID(),"0",productnamee[position],Float.toString(Math.round(Float.parseFloat(offpercenttvv[position]))),Productimagee[position],"","","","0");
//                        Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                    }
                });

                SalePrice = 1;
            }
            else
            {
                holder.productname.setText(productnamee[position]);
                holder.orignalpricetv.setText("RS "+formatedproductprice);
                holder.orignalpricetv.setTextColor(Color.BLACK);
                holder.orignalpricetv.setTextSize(16);
                holder.offpercenttv.setVisibility(View.GONE);
                holder.discountedpricetv.setVisibility(View.GONE);
//                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid[position],"1",Dashboard.prefConfig.readID(),"0",productnamee[position],Float.toString(Math.round(Float.parseFloat(offpercenttvv[position]))),Productimagee[position],"","","","0");
//                        Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                    }
                });

            }

//            holder.allcons.addOnItemTouchListener(new RecyclerItemClickListener(Dashboard.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {a
//                @Override
//                public void onItemClick(View view, int position) {
//                    // ...
//                    // Toast.makeText(MainActivity.this, "hello "+position, Toast.LENGTH_SHORT).show();
//`
////                    Intent intent = new Intent(getApplicationContext(),EventDiscriptionActivity.class);
////                    intent.putExtra("ev_tittlee",ev_tittlee[position]);
////                    intent.putExtra("ev_excerptt",ev_excerptt[position]);
////                    intent.putExtra("ev_discriptionn",ev_discriptionn[position]);
////                    intent.putExtra("start_datee",start_datee[position]);
////                    intent.putExtra("end_datee",end_datee[position]);
////                    intent.putExtra("starttimee",starttimee[position]);
////                    intent.putExtra("endtimee",endtimee[position]);
////
////                    intent.putExtra("ev_imagee",ev_imagee[position]);
////                    intent.putExtra("ev_locationn",ev_locationn[position]);
////                    intent.putExtra("ev_addresss",ev_addresss[position]);
//////
////                    startActivity(intent);
//                        Intent intent = new Intent(getApplicationContext(),ProductDetailActivity.class);
//                        intent.putExtra("title",productnamee[position]);
//                        intent.putExtra("discription",proddiscriptionn[position]);
//                        startActivity(intent);
////                    onAddProduct();
//
//
//
//                }
//
//                @Override
//                public void onItemLongClick(View view, int position) {
//                    // ...
//                }
//            }));

            holder.allcons.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


//                    for(int j = 0 ;j<prodimage2d.length;j++ ) {
//                        for(int i = 0 ; i <prodimage2d[j].length;i++) {
//                            Toast.makeText(mContext, "" + prodimage2d[j][i], Toast.LENGTH_SHORT).show();
//                        }
//                        }
//
                    try
                    {
                        Intent intent = new Intent(context, ProductDetailActivity.class);
                        intent.putExtra("title",productnamee[position]);
                        intent.putExtra("discription",proddiscriptionn[position]);
                        intent.putExtra("price",orignalpricetvv[position]);
                        intent.putExtra("productid",Integer.toString(pid[position]));
                        intent.putExtra("prodbrand",prodbrandd[position]);
                        mContext.startActivity(intent);
                    }
                    catch (Exception e)
                    {

                    }

//                    Toast.makeText(mContext, "product id ="+pid[position], Toast.LENGTH_SHORT).show();
                }
            });

            holder.wishlisticon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
                    Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata,String.valueOf(pid[position]),Dashboard.prefConfig.readID());
                   // Toast.makeText(mContext, " id = "+prodid[position], Toast.LENGTH_SHORT).show();
                    call.enqueue(new Callback<generalstatusmessagePOJO>() {
                        @Override
                        public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                            if (response.isSuccessful()) {
                                int status = response.body().getStatus();
                                String message = response.body().getMessage().toString();
                                if (status == 1) {
                                    holder.wishlisticon.setBackgroundResource(R.drawable.wishlistaddedicon);
                                    View parentLayout = v.findViewById(android.R.id.content);
                                    Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
                                    View sbView = snackbar.getView();
                                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
                                    snackbar.show();

                                } else {
                                    holder.wishlisticon.setBackgroundResource(R.drawable.wishlistnotaddedicon);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                        }
                    });

                }
            });
        }


        @Override
        public int getItemCount() {
            return mImageUrls.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            ImageView Productimage,addcart,wishlisticon;
            TextView productname,orignalpricetv,offpercenttv,discountedpricetv;
            ConstraintLayout allcons;
            com.iarcuschin.simpleratingbar.SimpleRatingBar ratingBaritem;



            public ViewHolder(View itemView) {
                super(itemView);
                Productimage = itemView.findViewById(R.id.imageView2);
                productname = itemView.findViewById(R.id.productname);
                orignalpricetv = itemView.findViewById(R.id.orignalpricetv);
                offpercenttv = itemView.findViewById(R.id.offpercenttv);
                discountedpricetv = itemView.findViewById(R.id.discountedpricetv);
                addcart = itemView.findViewById(R.id.addcart);
                allcons = itemView.findViewById(R.id.allcons);
                wishlisticon = itemView.findViewById(R.id.wishlisticon);
            }
        }

    }





    public  class RecyclerViewAdapter5 extends RecyclerView.Adapter<RecyclerViewAdapter5.ViewHolder> {

        private static final String TAG = "RecyclerViewAdapter";

        //vars

        private ArrayList<Integer> mImageUrlsee = new ArrayList<>();
        String[] Productimagee ;
        String[] productnamee ;
        String[] orignalpricetvv ;
        String[] offpercenttvv ;
        String[]discountedpricetvv;
        String[]proddiscriptionn;
        String[][] prodimage2dd;
        int [] pid ;
        String[] prodbrandd;



        public    Context mContext;


        public RecyclerViewAdapter5(String[] Productimageee,String[] productnameee,
                                    String[] orignalpricetvvv,String[] offpercenttvvv,
                                    String[] discountedpricetvvv,String[] proddiscription, int[] prodid,String[] prodbrand) {

            Productimagee = Productimageee;
            productnamee = productnameee;
            orignalpricetvv = orignalpricetvvv;
            offpercenttvv = offpercenttvvv;
            discountedpricetvv = discountedpricetvvv;
            proddiscriptionn = proddiscription;
            pid = prodid;
            prodbrandd = prodbrand;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custommonthlytopitem, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            Log.d(TAG, "onBindViewHolder: called.");

//            holder.name.setText(mNames.get(position));
            Glide.with(mContext).load(Productimagee[position])
                    .into(holder.Productimage);

//            holder.goalname.setText(mNames.get(position).toString());
//
//            holder.status.setText(mStatus.get(position).toString());


            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd MMM yyyy", Locale.US);
//            Date date = null;
//            String showdate = "";
//            try {
//                date = simpleDateFormat.parse(start_datee[position]);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            showdate = simpleDateFormat1.format(date);
//            holder.Productimage.setBackgroundResource(mImageUrls.get(1));
            Productprice = Math.round(Float.parseFloat(orignalpricetvv[position]));
            SalePrice = Math.round(Float.parseFloat(offpercenttvv[position])) ;
            discoutnamount = Productprice-SalePrice;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            final String formatedproductprice = formatter.format(Productprice);
            final String formatedsaleprice = formatter.format(SalePrice);

            Log.e("Discount data",Productprice+"\n"+SalePrice+"\n"+discoutnamount);
            if(SalePrice!=0)
            {
                holder.offpercenttv.setVisibility(View.VISIBLE);
                holder.discountedpricetv.setVisibility(View.VISIBLE);
                holder.orignalpricetv.setTextColor(Color.LTGRAY);
                int percentage = ((discoutnamount*100)/Productprice);
                holder.productname.setText(productnamee[position]);
                holder.orignalpricetv.setText("RS "+formatedproductprice);
                holder.offpercenttv.setText( percentage+"%"+" OFF");
                holder.discountedpricetv.setText("RS "+formatedsaleprice);
                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid[position],"1",Dashboard.prefConfig.readID(),"0",productnamee[position],Float.toString(Math.round(Float.parseFloat(offpercenttvv[position]))),Productimagee[position],"","","","0");
//                        Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                    }
                });

                SalePrice = 1;
            }
            else
            {
                holder.productname.setText(productnamee[position]);
                holder.orignalpricetv.setText("RS "+formatedproductprice);
                holder.orignalpricetv.setTextColor(Color.BLACK);
                holder.orignalpricetv.setTextSize(16);
                holder.offpercenttv.setVisibility(View.GONE);
                holder.discountedpricetv.setVisibility(View.GONE);
//                holder.orignalpricetv.setPaintFlags(holder.orignalpricetv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.addcart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onAddProduct(pid[position],"1",Dashboard.prefConfig.readID(),"0",productnamee[position],Float.toString(Math.round(Float.parseFloat(offpercenttvv[position]))),Productimagee[position],"","","","0");

//                        Toast.makeText(mContext, "Added to cart!", Toast.LENGTH_SHORT).show();
                        holder.addcart.setBackgroundResource(R.drawable.added_to_cart);
                    }
                });

            }


//            holder.allcons.addOnItemTouchListener(new RecyclerItemClickListener(Dashboard.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
//                @Override
//                public void onItemClick(View view, int position) {
//                    // ...
//                    // Toast.makeText(MainActivity.this, "hello "+position, Toast.LENGTH_SHORT).show();
//`
////                    Intent intent = new Intent(getApplicationContext(),EventDiscriptionActivity.class);
////                    intent.putExtra("ev_tittlee",ev_tittlee[position]);
////                    intent.putExtra("ev_excerptt",ev_excerptt[position]);
////                    intent.putExtra("ev_discriptionn",ev_discriptionn[position]);
////                    intent.putExtra("start_datee",start_datee[position]);
////                    intent.putExtra("end_datee",end_datee[position]);
////                    intent.putExtra("starttimee",starttimee[position]);
////                    intent.putExtra("endtimee",endtimee[position]);
////
////                    intent.putExtra("ev_imagee",ev_imagee[position]);
////                    intent.putExtra("ev_locationn",ev_locationn[position]);
////                    intent.putExtra("ev_addresss",ev_addresss[position]);
//////
////                    startActivity(intent);
//                        Intent intent = new Intent(getApplicationContext(),ProductDetailActivity.class);
//                        intent.putExtra("title",productnamee[position]);
//                        intent.putExtra("discription",proddiscriptionn[position]);
//                        startActivity(intent);
////                    onAddProduct();
//
//
//
//                }
//
//                @Override
//                public void onItemLongClick(View view, int position) {
//                    // ...
//                }
//            }));

            holder.allcons.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


//                    for(int j = 0 ;j<prodimage2d.length;j++ ) {
//                        for(int i = 0 ; i <prodimage2d[j].length;i++) {
//                            Toast.makeText(mContext, "" + prodimage2d[j][i], Toast.LENGTH_SHORT).show();
//                        }
//                        }
//
                    try {
                        Intent intent = new Intent(context, ProductDetailActivity.class);
                        intent.putExtra("title",productnamee[position]);
                        intent.putExtra("discription",proddiscriptionn[position]);
                        intent.putExtra("price",orignalpricetvv[position]);
                        intent.putExtra("productid",Integer.toString(pid[position])  );
                        intent.putExtra("prodbrand",prodbrandd[position]);
                        mContext.startActivity(intent);
                    }
                    catch (Exception e)
                    {

                    }

//                    Toast.makeText(mContext, "product id ="+pid[position], Toast.LENGTH_SHORT).show();
                }
            });
            holder.wishlisticon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
                    Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata,String.valueOf(pid[position]),Dashboard.prefConfig.readID());
                 //   Toast.makeText(mContext, " id = "+prodid[position], Toast.LENGTH_SHORT).show();
                    call.enqueue(new Callback<generalstatusmessagePOJO>() {
                        @Override
                        public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                            if (response.isSuccessful()) {
                                int status = response.body().getStatus();
                                String message = response.body().getMessage().toString();
                                if (status == 1) {
                                    holder.wishlisticon.setBackgroundResource(R.drawable.wishlistaddedicon);
                                    View parentLayout = v.findViewById(android.R.id.content);
                                    Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
                                    View sbView = snackbar.getView();
                                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
                                    snackbar.show();

                                } else {
                                    holder.wishlisticon.setBackgroundResource(R.drawable.wishlistnotaddedicon);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                        }
                    });

                }
            });
        }


        @Override
        public int getItemCount() {
            return mImageUrls.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            ImageView Productimage,addcart,wishlisticon;
            TextView productname,orignalpricetv,offpercenttv,discountedpricetv;
            ConstraintLayout allcons;
            com.iarcuschin.simpleratingbar.SimpleRatingBar ratingBaritem;



            public ViewHolder(View itemView) {
                super(itemView);
                Productimage = itemView.findViewById(R.id.imageView2);
                productname = itemView.findViewById(R.id.productname);
                orignalpricetv = itemView.findViewById(R.id.orignalpricetv);
                offpercenttv = itemView.findViewById(R.id.offpercenttv);
                discountedpricetv = itemView.findViewById(R.id.discountedpricetv);
                addcart = itemView.findViewById(R.id.addcart);
                allcons = itemView.findViewById(R.id.allcons);
                wishlisticon = itemView.findViewById(R.id.wishlisticon);
            }
        }

    }



    public static class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
        public interface OnItemClickListener {
            void onItemClick(View view, int position);

            void onItemLongClick(View view, int position);
        }

        private OnItemClickListener mListener;

        private GestureDetector mGestureDetector;

        public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, OnItemClickListener listener) {
            mListener = listener;

            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View childView = recyclerView.findChildViewUnder(e.getX(), e.getY());

                    if (childView != null && mListener != null) {
                        mListener.onItemLongClick(childView, recyclerView.getChildAdapterPosition(childView));
                    }
                }
            });
        }
        @Override
        public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());

            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
            }

            return false;
        }
        @Override
        public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }



    }
}

package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface AddtowishlistAPI {
    @FormUrlEncoded
    @POST("wishlist/addProductToWishList")
    Call<generalstatusmessagePOJO> getDetails(@Header("data") String data, @Field("id") String id, @Field("customerid") String customerid);
}

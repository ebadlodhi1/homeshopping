package com.homeshopping.admin.homeshoping10.POJO.MonthlyTopSellerPOJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MonthlyTopsellerExample {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("topsellers")
    @Expose
    private List<Topseller> topsellers = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Topseller> getTopsellers() {
        return topsellers;
    }

    public void setTopsellers(List<Topseller> topsellers) {
        this.topsellers = topsellers;
    }
}

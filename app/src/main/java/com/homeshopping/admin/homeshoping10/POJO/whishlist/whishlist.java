package com.homeshopping.admin.homeshoping10.POJO.whishlist;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class whishlist {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("wishlist")
    @Expose
    private List<whishlistdetails> wishlist = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<whishlistdetails> getWishlist() {
        return wishlist;
    }

    public void setWishlist(List<whishlistdetails> wishlist) {
        this.wishlist = wishlist;
    }
}

package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.PlaceOrderPOJO.PlaceOrderExample;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface UpdatequantitiAPI {
    @PUT("cart/updateCart")
    Call<generalstatusmessagePOJO> getDetails(@Header("data") String data, @Body RequestBody body);
}

package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Fragment.DiscriptionFragment;
import com.homeshopping.admin.homeshoping10.R;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;

public class ContactusActivity extends AppCompatActivity {
    private WebView wv1;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Contactus Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        wv1=(WebView)findViewById(R.id.webview);
        wv1.setWebViewClient(new MyBrowser());
        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptCanOpenWindowsAutomatically(true );
        wv1.getSettings().setDefaultTextEncodingName("utf-8");
        wv1.getSettings().setJavaScriptEnabled(true);
        WebSettings mWebSettings = wv1.getSettings();
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        wv1.setVerticalScrollBarEnabled(true);
//                        wv1.requestFocus();
        mWebSettings.setBuiltInZoomControls(true);

        wv1.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        wv1.setScrollbarFadingEnabled(false);
        wv1.setWebViewClient(new MyBrowser());
        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);

        wv1.getSettings().setAppCacheMaxSize(5 * 1024 * 1024); // 5MB
//                    wv1.getSettings().setAppCachePath(getContext().getCacheDir().getAbsolutePath());
        wv1.getSettings().setAllowFileAccess(true);
        wv1.getSettings().setAppCacheEnabled(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT); // load online by default
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        wv1.loadUrl("https://homeshopping.pk/pages/App-contact-us.html");
    }
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
    @Override
    public void onBackPressed () {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(),Dashboard.class);
        startActivity(intent);
        finish();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);

    }

}

package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.NetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Class.NotLoginDataNetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Class.PrefConfig;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Interfaces.AddcartAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.LoginApi;
import com.homeshopping.admin.homeshoping10.POJO.LoginPOJO;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;
import com.homeshopping.admin.homeshoping10.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;


public class LoginActivity extends AppCompatActivity {

    TextView donthavaccount,forgetpassword;
    EditText Email,password;
    Button Login;
    String useremail , userpass;
    Boolean flag = true;
    public static PrefConfig prefConfig;
    Context context;
    ProgressBar progressBar10;
    Bundle bundle;
    String comingcart = "0";
    ImageView passshow;
    Boolean flagpass = true;
    private DatabaseHelper db;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("Sign In");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Login Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        progressBar10 = findViewById(R.id.progressBar10);
        progressBar10.setVisibility(View.GONE);
        context=this;
        db = new DatabaseHelper(context);
        prefConfig =  new PrefConfig(this);
         bundle = getIntent().getExtras();
        passshow = findViewById(R.id.imageView56);
        Email = findViewById(R.id.full_emaillogin);
        password = findViewById(R.id.full_emaillpasslogin);
        forgetpassword = findViewById(R.id.textView21);
        forgetpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ForgetPasswordActivity.class);
                startActivity(intent);
            }
        });
        passshow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag == true)
                {
                    passshow.setBackgroundResource(R.drawable.eye_off);
                    flag = false;
                    password.setTransformationMethod(new PasswordTransformationMethod());
                }
            else
                {
                    passshow.setBackgroundResource(R.drawable.eye);
                    flag = true;
                    password.setTransformationMethod(null);
                }
            }
        });
        donthavaccount = findViewById(R.id.textView22);
        Spannable word = new SpannableString(" Signup");
        word.setSpan(new ForegroundColorSpan(Color.rgb(171,208,55)), 0, word.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        donthavaccount.append(word);

        donthavaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),SignupActivity.class);
                startActivity(intent);
                finish();
            }
        });


        Login = findViewById(R.id.button7);
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                useremail = Email.getText().toString();
                userpass = password.getText().toString();
                flag = true;
                if(useremail.isEmpty())
                {

                    Email.setError("Please enter email");
                    Email.requestFocus();
                    flag = false;
                }
                else if(!Patterns.EMAIL_ADDRESS.matcher(useremail).matches())
                {
                    Email.setError("Invalid email");
                    Email.requestFocus();
                    flag=false;

                }
                if(userpass.isEmpty())
                {

                    password.setError("Please enter password");
                    password.requestFocus();
                    flag=false;
                }
                if(flag)
                {
                    progressBar10.setVisibility(View.VISIBLE);
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    LoginApi api = retrofit.create(LoginApi.class);
                    Call<LoginPOJO> call = api.getDetails(MainActivity.Tokendata,useremail,userpass);
                    call.enqueue(new Callback<LoginPOJO>() {
                        @Override
                        public void onResponse(Call<LoginPOJO> call, Response<LoginPOJO> response) {
                            if (response.isSuccessful()) {
                                int success = response.body().getStatus();
                                String message = response.body().getMessage().toString();

//                            Toast.makeText(LoginActivity.this, ""+success, Toast.LENGTH_SHORT).show();
                                if (success == 0) {
                                    Toast.makeText(LoginActivity.this, "" + message,
                                            Toast.LENGTH_SHORT).show();
                                    prefConfig.writeLoginStatus(false);
                                    progressBar10.setVisibility(View.GONE);

                                } else if (success == 1) {


                                    String CustomerID = response.body().getCustomerId().toString();
                                    String FirstName = response.body().getCustomerFirstName().toString();
                                    String LastName = response.body().getCustomerLastName().toString();
                                    String Email = response.body().getCustomerEmail().toString();
                                    String pno = response.body().getCustomerPhone().toString();
                                    String city = response.body().getCustomerCity().toString();
                                    Toast.makeText(LoginActivity.this, "" + message + "\n" + Email,
                                            Toast.LENGTH_SHORT).show();
                                    Dashboard.loginbefore.setVisibility(View.GONE);
                                    Dashboard.loginafter.setVisibility(View.VISIBLE);

//                                    Dashboard.username.setText("Hello " + FirstName);
//                                    Dashboard.email.setText("" + Email);
                                    prefConfig.writeLoginStatus(true);
                                    prefConfig.writeName(CustomerID, FirstName, LastName, Email, pno,city);
                                  //  Toast.makeText(LoginActivity.this, "aaaa   " + CustomerID, Toast.LENGTH_LONG).show();
                                    try {
//                                        context.unregisterReceiver(new NetworkStateChecker());
//                                        context.unregisterReceiver(new NotLoginDataNetworkStateChecker());
//
////                    Toast.makeText(c, "Throughing", Toast.LENGTH_SHORT).show();
//                                        context.registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//                                        context.registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//

                                        try {
                                            comingcart  = bundle.getString("comingcart", "0");
                                        }
                                        catch (Exception e)
                                        {

                                        }

                                        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                                        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

                                        if (activeNetwork != null) {
//            Toast.makeText(context, "this is active network", Toast.LENGTH_SHORT).show();
                                            //if connected to wifi or mobile data plan
                                            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI || activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
//                Toast.makeText(context, "this is active network inside", Toast.LENGTH_SHORT).show();
                                                //getting all the unsynced names
//                                                Cursor cursor = db.getUnsyncedNames();
//                                                Toast.makeText(getApplicationContext(), "cursur count "+cursor.getCount(), Toast.LENGTH_SHORT).show();
//
//                                                if (cursor != null) {
//                                        Toast.makeText(context, "inside cursur "+cursor.getCount(), Toast.LENGTH_SHORT).show();
//
//                                                    if (cursor.moveToFirst()) {
//                                                        do {
//                                                            //calling the method to save the unsynced name to MySQL
//                                                            saveName(
//                                                                    cursor.getInt(3),
//                                                                    cursor.getInt(1),
//                                                                    cursor.getInt(2),
//                                                                    cursor.getInt(4),
//                                                                    cursor.getInt(5)
//
//                                                            );
//                                                        } while (cursor.moveToNext());
////                        Toast.makeText(context, "Data came", Toast.LENGTH_SHORT).show();
//                                                    }
//                                                }
//
//
//                                                else{
//                    Toast.makeText(context, "Database is Empty", Toast.LENGTH_SHORT).show();
//                                                }


                                                Cursor cursor1 = db.getnotlogincart();
//                                                Toast.makeText(context, "cursur count 2  "+cursor1.getCount(), Toast.LENGTH_SHORT).show();
                                                if (cursor1.getCount()>0) {
//                                                    Toast.makeText(context, "inside cursur2 "+cursor1.getCount(), Toast.LENGTH_SHORT).show();

                                                    if (cursor1.moveToFirst()) {
                                                        do {
                                                            //calling the method to save the unsynced name to MySQL
                                                            saveName(
                                                                    Integer.parseInt(Dashboard.prefConfig.readID()),
                                                                    cursor1.getInt(1),
                                                                    cursor1.getInt(2),
                                                                    cursor1.getInt(4),
                                                                    cursor1.getInt(5),
                                                                    cursor1.getString(10),
                                                                    cursor1.getString(11),
                                                                    cursor1.getString(12)

                                                            );
                                                        } while (cursor1.moveToNext());
//                                              Toast.makeText(context, "Data came", Toast.LENGTH_SHORT).show();
                                                    }
                                                }


                                                else{
                                                    progressBar10.setVisibility(View.GONE);

                                                    if (comingcart.equals("0")) {
                                                        progressBar10.setVisibility(View.GONE);
                                                        Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                                                        startActivity(intent);
                                                        finish();
                                                    } else {
                                                        progressBar10.setVisibility(View.GONE);
                                                        Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                                                        startActivity(intent);
                                                        finish();
                                                    }
//                                                    Toast.makeText(context, "Database is Empty", Toast.LENGTH_SHORT).show();
                                                }


                                            }
                                        }




                                    } catch (Exception e) {
                                        Log.e("error", "" + e);
                                    }

                                }
                            }
                            else
                            {
                                Toast.makeText(LoginActivity.this, ""+response.message(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginPOJO> call, Throwable t) {
                            Toast.makeText(LoginActivity.this, "Server not responding.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

    }

    private  void saveName(final int  userid,final int  productid,final int  productqty,final int  Combinationid,final int  status,final String feildID,String feildName,String bankId)
    {
//                Toast.makeText(context, "save name   \nuser id "+Integer.toString(userid)+"\npid "+Integer.toString(productid)+"\n p qty"+Integer.toString(productqty)+"\n Combination id"+Integer.toString(Combinationid)+"\n status"+Integer.toString(status), Toast.LENGTH_SHORT).show();
        Log.d("Data aya","save name   \nuser id "+Integer.toString(userid)+"\npid "+Integer.toString(productid)+"\n p qty"+Integer.toString(productqty)+"\n Combinationid id"+Integer.toString(Combinationid)+"\n status"+Integer.toString(status)+"\n bankId"+bankId);
        Retrofit retrofitaddcart = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        AddcartAPI addcartAPI = retrofitaddcart.create(AddcartAPI.class);
        Call<generalstatusmessagePOJO> call = addcartAPI.getDetails(MainActivity.Tokendata,Integer.toString(productid),Integer.toString(productqty),Integer.toString(userid),Integer.toString(Combinationid),feildID,feildName,bankId);
        call.enqueue(new Callback<generalstatusmessagePOJO>() {
            @Override
            public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                if (response.isSuccessful()) {
                    int status = response.body().getStatus();
                    String message = response.body().getMessage().toString();
//                    Toast.makeText(context, "body "+response.body().toString(), Toast.LENGTH_SHORT).show();
                    Log.d("Response body",""+response.body().toString());
                    if (status == 1) {

                        boolean res = db.updateStatus();

                        if (res == true) {
                            db.updateUserIdStatus(userid);
//                            db.deletesync();
                            progressBar10.setVisibility(View.GONE);

                            if (comingcart.equals("0")) {
                                progressBar10.setVisibility(View.GONE);
                                Intent intent = new Intent(getApplicationContext(), Dashboard.class);
                                startActivity(intent);
                                finish();
                            } else {
                                progressBar10.setVisibility(View.GONE);
                                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        } else {
                            Log.d("err", "update name status error");
                        }

                    } else {
                        Log.d("server", "no insert to server");
                    }

                }
                else
                {
//                            Toast.makeText(, "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {
                Log.d("net","network error");
            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }




}

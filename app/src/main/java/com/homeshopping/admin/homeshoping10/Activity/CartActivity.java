package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Class.NetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Class.NotLoginDataNetworkStateChecker;
import com.homeshopping.admin.homeshoping10.Database.DatabaseHelper;
import com.homeshopping.admin.homeshoping10.Interfaces.AddtowishlistAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.CouponApi;
import com.homeshopping.admin.homeshoping10.Interfaces.GetcityAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.UpdatequantitiAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.getcartAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.removecartitemAPI;
import com.homeshopping.admin.homeshoping10.Interfaces.shipaddressAPI;
import com.homeshopping.admin.homeshoping10.POJO.Cart.Cart;
import com.homeshopping.admin.homeshoping10.POJO.Cart.CartDetail;
import com.homeshopping.admin.homeshoping10.POJO.Cart.CouponExample;
import com.homeshopping.admin.homeshoping10.POJO.City.city;
import com.homeshopping.admin.homeshoping10.POJO.City.cityDetails;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;
import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddress;
import com.homeshopping.admin.homeshoping10.POJO.shippingadddress.shipaddresslist;
import com.homeshopping.admin.homeshoping10.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import androidx.constraintlayout.widget.ConstraintLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.support.v4.app.Fragment.instantiate;
import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class CartActivity extends AppCompatActivity {
ListView cartlistview;
TextView carttotalamount,carttotalitems,cartitemstop;
ProgressBar progressBar7;
int totalamount , quant = 0;
    String[] qty = {"1","2","3","4","5","6","7","8","9","10"};
    List<CartDetail> cart = new ArrayList<>();
    public static List<CartDetail> cartforreciept = new ArrayList<>();

    ArrayList<String> cartId  = new ArrayList<>();
    ArrayList<String> productId  = new ArrayList<>();
    ArrayList<String> quantitycartId  = new ArrayList<>();
    ArrayList<String> quantitycartquantity  = new ArrayList<>();

    ArrayList<String> productImage  = new ArrayList<>();
    ArrayList<String> productTitle  = new ArrayList<>();
    ArrayList<String> productAvailability  = new ArrayList<>();
    ArrayList<String> quantity  = new ArrayList<>();
    ArrayList<String> productPrice  = new ArrayList<>();
    ArrayList<String> variationName  = new ArrayList<>();
    ArrayList<String> variationOptionName  = new ArrayList<>();
    ArrayList<String> variationPrice  = new ArrayList<>();
    ArrayList<String> variationFunction  = new ArrayList<>();
    ArrayList<String> fieldName  = new ArrayList<>();
    ArrayList<String> fieldOption  = new ArrayList<>();
    ArrayList<String> OutOfStock  = new ArrayList<>();
    ArrayList<String> BrandName  = new ArrayList<>();
    DatabaseHelper db;
    Button Checkout,Couponapply;
    TextView Couponmsg,sutotaltv,discounttv;
    EditText coupon_code;
    JSONArray cartItems = new JSONArray();
    ConstraintLayout CouponConstraint,discountapplyconstraint;
    public static  String cityidd =  "";
    public static  String citynamee =  "";
    List<cityDetails> city = new ArrayList<>();
    public static ArrayList<String> cityname,cityid;
    public static String finalvoucher = "";
    public static Integer finalvoucherprice = 0;

    JSONObject mainobj = new JSONObject();
    JSONArray carts ;
    ConstraintLayout cartfilledlayout,cartemptylayout,couponcons;
    Button continueshop;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("My Cart");
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Cart Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());
        couponcons = findViewById(R.id.constraintLayout105);
        couponcons.setVisibility(View.GONE);
        cartfilledlayout = findViewById(R.id.cartfilledlayout);

        cartemptylayout = findViewById(R.id.cartemptylayout);

        cartfilledlayout.setVisibility(View.VISIBLE);
        cartemptylayout.setVisibility(View.GONE);
        cartlistview = findViewById(R.id.cartlistview);
        carttotalamount =findViewById(R.id.carttotalamount);
        carttotalitems = findViewById(R.id.carttotalitems);
        cartitemstop = findViewById(R.id.textView110);
        progressBar7 = findViewById(R.id.progressBar7);
        Couponapply  = findViewById(R.id.button19);
        Couponmsg = findViewById(R.id.textView80);
        coupon_code = findViewById(R.id.coupon_code);
        CouponConstraint = findViewById(R.id.constraintLayout105);
        discountapplyconstraint = findViewById(R.id.discountapplyconstraint);
        discountapplyconstraint.setVisibility(View.GONE);
        sutotaltv = findViewById(R.id.sutotaltv);
        discounttv = findViewById(R.id.discounttv);
        Checkout = findViewById(R.id.button9);
        continueshop = findViewById(R.id.button27);
        db = new DatabaseHelper(this);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GetcityAPI api = retrofit.create(GetcityAPI.class);
        Call<com.homeshopping.admin.homeshoping10.POJO.City.city> cityCall = api.getDetails(MainActivity.Tokendata,"Pakistan");
        cityCall.enqueue(new Callback<com.homeshopping.admin.homeshoping10.POJO.City.city>() {
            @Override
            public void onResponse(Call<city> call, Response<city> response) {
                if (response.isSuccessful()) {
                    int status = response.body().getStatus();
                    if (status == 1) {
                        city = response.body().getDetail();
                        cityname = new ArrayList<>(city.size());
                        cityid = new ArrayList<>(city.size());
                        for (int i = 0; i < city.size(); i++) {
                            cityname.add(city.get(i).getCityName().toString());
                            cityid.add(city.get(i).getCityId().toString());
                        }

                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(), ""+response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<city> call, Throwable t) {

            }
        });

        try {
            //  Toast.makeText(getContext(), "Throughing", Toast.LENGTH_SHORT).show();
            unregisterReceiver(new NetworkStateChecker());
            unregisterReceiver(new NotLoginDataNetworkStateChecker());

            registerReceiver(new NetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            registerReceiver(new NotLoginDataNetworkStateChecker(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        }
        catch (Exception e)
        {
            Log.e("error",""+e);
        }



if(prefConfig.readLoginStatus())
{

    getcartAPI api2  = retrofit.create(getcartAPI.class);
    Call<Cart> call = api2.getDetails(MainActivity.Tokendata, prefConfig.readID());
    call.enqueue(new Callback<Cart>() {
        @Override
        public void onResponse(Call<Cart> call, Response<Cart> response) {
            if (response.isSuccessful()) {
                int status = response.body().getStatus();
                if (status == 1) {
                    cart = response.body().getCartDetail();
                    cartforreciept = response.body().getCartDetail();
                    if (cart.size() > 0) {
                        CouponConstraint.setVisibility(View.VISIBLE);
                        progressBar7.setVisibility(View.GONE);
                        for (int i = 0; i < cart.size(); i++) {
                            cartId.add(cart.get(i).getCartId().toString());
                            productId.add(cart.get(i).getProductId().toString());
                            productImage.add(cart.get(i).getProductImage());
                            productTitle.add(cart.get(i).getProductTitle());
                            productAvailability.add(cart.get(i).getProductAvailability());
                            quantity.add(cart.get(i).getQuantity().toString());
                            productPrice.add(cart.get(i).getProductPrice());
                            BrandName.add(cart.get(i).getBrandName());
                            variationPrice.add(cart.get(i).getVariationPrice());

                            int amountfloat = Math.round(Float.parseFloat(productPrice.get(i))+Float.parseFloat(variationPrice.get(i)));
                            quant = quant + Integer.parseInt(quantity.get(i));
                            int totalqtyamount = amountfloat * Integer.parseInt(quantity.get(i));
                            totalamount = (int) (totalamount + totalqtyamount);

                            JSONObject cart = new JSONObject();
                            try {
                                cart.put("productId", productId.get(i));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            cartItems.put(cart);
                        }
                        getcartAdapter2 getcartAdapter2 = new getcartAdapter2(getApplicationContext(), cart);
                        cartlistview.setAdapter(getcartAdapter2);
//
//                    Toast.makeText(CartActivity.this, ""+quant, Toast.LENGTH_SHORT).show();
                        DecimalFormat formatter = new DecimalFormat("#,###,###");
                        String pricee = formatter.format(totalamount);
                        carttotalamount.setText("RS " + pricee);
                        carttotalitems.setText("Items " + quant);
                        if(quant>1)
                        {
                            cartitemstop.setText("My Cart ("+quant+" Items)");
                        }
                        else
                        {
                            cartitemstop.setText("My Cart ("+quant+" Item)");
                        }
                        Dashboard.cart_count = quant;
                        invalidateOptionsMenu();
                    } else {
                        CouponConstraint.setVisibility(View.GONE);

                    }

                } else {
//                    Toast.makeText(CartActivity.this, "Cart is empty", Toast.LENGTH_SHORT).show();
                    cartfilledlayout.setVisibility(View.GONE);
                    cartemptylayout.setVisibility(View.VISIBLE);
                    progressBar7.setVisibility(View.GONE);
                    Checkout.setVisibility(View.GONE);
                    continueshop.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                            startActivity(intent);
                            finish();
                        }
                    });

                }
            }
        }
        @Override
        public void onFailure(Call<Cart> call, Throwable t) {

        }
    });

}
else
{
    progressBar7.setVisibility(View.GONE);
    Cursor  cursor = db.getnotlogincartforDisplay();
//    Toast.makeText(this, "c count "+cursor.getCount(), Toast.LENGTH_SHORT).show();
    if (cursor.getCount() != 0) {
        Checkout.setVisibility(View.VISIBLE);
//        Toast.makeText(getApplicationContext(), "inside cursur "+cursor.getCount(), Toast.LENGTH_SHORT).show();
      productId.clear();
      productImage.clear();
      productTitle.clear();
      quantity.clear();
      productPrice.clear();
        if (cursor.moveToFirst()) {
            do {
                //calling the method to save the unsynced name to MySQL
//                saveName(
//                        cursor.getInt(1),
//                        cursor.getInt(2),
//                        cursor.getInt(3),
//                        cursor.getInt(4),
//                        cursor.getInt(5)
//
//                );

                productId.add(Integer.toString(cursor.getInt(1)));
                productImage.add(cursor.getString(8));
                productTitle.add(cursor.getString(6));
                quantity.add(Integer.toString(cursor.getInt(2)));
                productPrice.add(cursor.getString(7));
                BrandName.add(cursor.getString(9));
//                Toast.makeText(this, " this my "+productTitle, Toast.LENGTH_SHORT).show();
                float amountfloat = Math.round(Float.parseFloat(productPrice.get(cursor.getPosition())));
                int qty = Integer.parseInt(quantity.get(cursor.getPosition()));
                int amntint = (int)amountfloat*qty;
                totalamount = (int) (totalamount+amntint);


//                String editedprice = productPrice.get(cursor.getPosition()).replace(',',' ');
//                Toast.makeText(this, "ep "+ productPrice.get(cursor.getPosition()), Toast.LENGTH_SHORT).show();


            } while (cursor.moveToNext());
            quant =  0 ;
            quant = cursor.getCount();

//            Toast.makeText(getApplicationContext(), "Data came", Toast.LENGTH_SHORT).show();

            getcartAdapter2 getcartAdapter2 = new getcartAdapter2(getApplicationContext(),cart);
            cartlistview.setAdapter(getcartAdapter2);
//
//            Toast.makeText(CartActivity.this, ""+quant, Toast.LENGTH_SHORT).show();


            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String pricee = formatter.format(totalamount);
            carttotalamount.setText("RS "+pricee);
            carttotalitems.setText("Items "+quant);
            if(quant>1)
            {
                cartitemstop.setText("My Cart ("+quant+" Items)");
            }
            else
            {
                cartitemstop.setText("My Cart ("+quant+" Item)");
            }

            Cursor cursor1 = db.getcountnotlogin();
            if(cursor1.moveToFirst()) {

                Dashboard.cart_count = cursor1.getInt(0);
                invalidateOptionsMenu();
            }

        }
    }


    else{
//        Toast.makeText(getApplicationContext(), "Database is Empty", Toast.LENGTH_SHORT).show();
        Checkout.setVisibility(View.GONE);
        cartfilledlayout.setVisibility(View.GONE);
        cartemptylayout.setVisibility(View.VISIBLE);
        progressBar7.setVisibility(View.GONE);
        Checkout.setVisibility(View.GONE);
        continueshop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                startActivity(intent);
                finish();
            }
        });
    }

}


Checkout.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(prefConfig.readLoginStatus()) {
            progressBar7.setVisibility(View.VISIBLE);
            quantitycartId = cartId;
            quantitycartquantity = quantity;
            carts = new JSONArray();
            for (int i = 0;i<quantity.size();i++)
            {

                try {
                    JSONObject   cartjsonobj = new JSONObject();
                    cartjsonobj.put("cartId",quantitycartId.get(i));
                    cartjsonobj.put("quantity",Integer.toString((Integer.parseInt(quantitycartquantity.get(i)))));
                    carts.put(cartjsonobj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            try {
                mainobj.put("userId", prefConfig.readID());
                mainobj.put("carts",carts);
                Log.d("updatecartjson",""+mainobj);

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ROOT_URL) //Setting the Root URL
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                UpdatequantitiAPI updatequantitiAPI = retrofit.create(UpdatequantitiAPI.class);
                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), mainobj.toString());
                Call<generalstatusmessagePOJO> generalstatusmessagePOJOCall = updatequantitiAPI.getDetails(MainActivity.Tokendata,body);
                generalstatusmessagePOJOCall.enqueue(new Callback<generalstatusmessagePOJO>() {
                    @Override
                    public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                        if (response.isSuccessful())
                        {
                            int status = response.body().getStatus();
                            if(status == 1)
                            {
                                progressBar7.setVisibility(View.GONE);
                                getcartAPI api2  = retrofit.create(getcartAPI.class);
                                Call<Cart> call2 = api2.getDetails(MainActivity.Tokendata, prefConfig.readID());
                                call2.enqueue(new Callback<Cart>() {
                                    @Override
                                    public void onResponse(Call<Cart> call, Response<Cart> response) {
                                        if (response.isSuccessful()) {
                                            int status = response.body().getStatus();
                                            if (status == 1) {
                                                cart.clear();
                                                cartforreciept.clear();
                                                cart = response.body().getCartDetail();
                                                cartforreciept = response.body().getCartDetail();
                                                Intent intent = new Intent(getApplicationContext(), CheckOutActivity.class);
                                                startActivity(intent);

                                            } else {
//                                                Toast.makeText(CartActivity.this, "Cart is empty", Toast.LENGTH_SHORT).show();
                                                setContentView(R.layout.emptycartlayout);
                                                progressBar7.setVisibility(View.GONE);
                                                Checkout.setVisibility(View.GONE);

                                            }
                                        }
                                    }
                                    @Override
                                    public void onFailure(Call<Cart> call, Throwable t) {

                                    }
                                });

                            }
                            else
                            {
//                                Toast.makeText(CartActivity.this, "bb"+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        else
                        {
//                            Toast.makeText(CartActivity.this, "aa"+response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

//            Intent intent = new Intent(getApplicationContext(), CheckOutActivity.class);
//            startActivity(intent);

        }
        else
        {
            Toast.makeText(CartActivity.this, "Kindly login First to Proceed", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
            intent.putExtra("comingcart","1");
            startActivity(intent);
            finish();

        }


    }
});
        Couponapply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject mainobj = new JSONObject();
                String str = carttotalamount.getText().toString();
                StringBuffer sb = new StringBuffer();
                for(int i=0; i<str.length(); i++)
                {
                    if(Character.isDigit(str.charAt(i)))
                        sb.append(str.charAt(i));
                }
                String a =  sb.toString();
//               Toast.makeText(CartActivity.this, ""+a, Toast.LENGTH_SHORT).show();
                int currenttotalamount = Integer.parseInt(a);
                try {
                    mainobj.put("couponCode",""+coupon_code.getText().toString());
                    mainobj.put("cartTotal" ,currenttotalamount);
                    mainobj.put("cartItems",cartItems);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("mainobj ",""+mainobj);
                if(coupon_code.getText().toString().equals(""))
                {
                    Couponmsg.setVisibility(View.VISIBLE);
                    Couponmsg.setText("Invalid Coupon");
                    Couponmsg.setTextColor(getResources().getColor( R.color.Red));
                }
                else
                {

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    CouponApi couponApi = retrofit.create(CouponApi.class);
                    RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), mainobj.toString());

                    Call<CouponExample> couponExampleCall = couponApi.getDetails(MainActivity.Tokendata,body);
                    couponExampleCall.enqueue(new Callback<CouponExample>() {
                        @Override
                        public void onResponse(Call<CouponExample> call, Response<CouponExample> response) {
                            if (response.isSuccessful())
                            {
                            int status = Integer.parseInt(response.body().getStatus());
                            if(status == 0)
                            {
                                Couponmsg.setText(""+response.body().getMessage());
                                Couponmsg.setTextColor(getResources().getColor( R.color.Red));
                                Couponapply.setEnabled(true);
                            }
                            else if(status == 1)
                            {
                                Couponapply.setEnabled(false);
                                CartActivity.finalvoucher = coupon_code.getText().toString();
                                Couponmsg.setText("Coupon Applied");
                                Couponmsg.setTextColor(getResources().getColor( R.color.Blue));
                                float amountfloat = Math.round(Float.parseFloat(response.body().getDiscount()));
                                int discount = (int)amountfloat;
                                discountapplyconstraint.setVisibility(View.VISIBLE);
                                String str = carttotalamount.getText().toString();
                                StringBuffer sb = new StringBuffer();
                                for(int i=0; i<str.length(); i++)
                                {
                                    if(Character.isDigit(str.charAt(i)))
                                        sb.append(str.charAt(i));
                                }
                                String a =  sb.toString();
//               Toast.makeText(CartActivity.this, ""+a, Toast.LENGTH_SHORT).show();
                                int currenttotalamount = Integer.parseInt(a);

                                DecimalFormat formatterr = new DecimalFormat("#,###,###");
                                String priceee = formatterr.format(currenttotalamount);
                                sutotaltv.setText("RS "+priceee);


                                if(discount>=currenttotalamount)
                                {
                                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                                    String pricee = formatter.format(currenttotalamount);
                                    discounttv.setText("RS -"+pricee);
                                    finalvoucherprice = currenttotalamount;
                                }
                                else
                                {
                                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                                    String pricee = formatter.format(discount);
                                    discounttv.setText("RS -"+pricee);
                                    finalvoucherprice = discount;
                                }



                                totalamount = currenttotalamount-discount;
                                DecimalFormat formatter = new DecimalFormat("#,###,###");
                                String pricee = formatter.format(totalamount);
                                if(totalamount<0)
                                {
                                    carttotalamount.setText("RS 0" );
                                }
                                else
                                {
                                    carttotalamount.setText("RS "+pricee);
                                }



                            }
                            }
                            else
                            {
//                                Toast.makeText(getApplicationContext(), ""+response.message(), Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<CouponExample> call, Throwable t) {

                        }
                    });
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(prefConfig.readLoginStatus()) {
            quantitycartId = cartId;
            quantitycartquantity = quantity;


            carts = new JSONArray();
            for (int i = 0; i < quantity.size(); i++) {

                try {
                    JSONObject cartjsonobj = new JSONObject();
                    cartjsonobj.put("cartId", quantitycartId.get(i));
                    cartjsonobj.put("quantity", Integer.toString((Integer.parseInt(quantitycartquantity.get(i)))));
                    carts.put(cartjsonobj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            try {
                mainobj.put("userId", prefConfig.readID());
                mainobj.put("carts", carts);
                Log.d("updatecartjson", "" + mainobj);

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(ROOT_URL) //Setting the Root URL
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                UpdatequantitiAPI updatequantitiAPI = retrofit.create(UpdatequantitiAPI.class);
                RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), mainobj.toString());
                Call<generalstatusmessagePOJO> generalstatusmessagePOJOCall = updatequantitiAPI.getDetails(MainActivity.Tokendata, body);
                generalstatusmessagePOJOCall.enqueue(new Callback<generalstatusmessagePOJO>() {
                    @Override
                    public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                        if (response.isSuccessful()) {
                            int status = response.body().getStatus();
                            if (status == 1) {

//                                Toast.makeText(CartActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
//                                Toast.makeText(CartActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
//                            Toast.makeText(CartActivity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
//        Intent intent = new Intent(getApplicationContext(),Dashboard.class);
//        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

//        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
//        startActivity(intent);
//finish();
    }


    public class getcartAdapter2 extends ArrayAdapter<String> {



        public getcartAdapter2(Context context, List<CartDetail> list ) {
            super(context, R.layout.whishlistcustomitemlayout, productId);




        }

        public View getView(final int position, View view, ViewGroup parent) {
            ViewHolder2 vh;
            View v = view;
            if(v == null) {
                vh = new ViewHolder2();
                LayoutInflater li = LayoutInflater.from(getApplicationContext());
                v = li.inflate(R.layout.whishlistcustomitemlayout,null);
//            Example app = items.get(position);
//            Log.d("position",Integer.toString(position));
//            if(app != null) {

                vh.wlbrand = (TextView) v.findViewById(R.id.wlbrand);
                vh. wlproductname = (TextView) v.findViewById(R.id.wlproductname);
                vh. wlprice = (TextView) v.findViewById(R.id.wlprice);
                vh. qty = (TextView) v.findViewById(R.id.qty);
                vh.pavailability = v.findViewById(R.id.pavailability);
                vh.pvariation = v.findViewById(R.id.pvariation);


                vh.cartmovetowl = v.findViewById(R.id.cartmovetowl);
                vh.wishlistheart = v.findViewById(R.id.imageView35);
                vh.wishlisttext = v.findViewById(R.id.textView116);

                vh.cartremove = v.findViewById(R.id.cartremove);
                vh. wishlistlayout = v.findViewById(R.id.wishlistlayout);
                vh. cartlayout =v.findViewById(R.id.cartlayout);
                vh.cartqtyadd=v.findViewById(R.id.cartqtyadd);
                vh.cartqtysub=v.findViewById(R.id.cartqtysub);

                vh. cartlayout.setVisibility(View.VISIBLE);
                vh. wishlistlayout.setVisibility(View.GONE);

                vh.wlimage = v.findViewById(R.id.wlimage);

                v.setTag(vh);
            }
            else{
                vh = (ViewHolder2) view.getTag();
            }



// give a timezone reference for formatting (see comment at the bottom)


//            vh.wlbrand.setText(brandName.get(position));
//            ArrayAdapter aa = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,qty);
////            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
////            //Setting the ArrayAdapter data on the Spinner
////            vh.spinnercartqty.setAdapter(aa);
            vh.wlbrand.setVisibility(View.VISIBLE);
            vh.wishlistheart.setBackgroundResource(R.drawable.heart);
            vh.wishlisttext.setText("Add to Wishlist");
       if(prefConfig.readLoginStatus())
       {
           vh.wlproductname.setText(productTitle.get(position));
           DecimalFormat formatter = new DecimalFormat("#,###,###");
           String pricee = formatter.format(Math.round(Float.parseFloat(productPrice.get(position)))+Math.round(Float.parseFloat(cart.get(position).getVariationPrice())));
           vh.wlprice.setText("Rs "+pricee);
           vh.wlbrand.setText(""+BrandName.get(position));
           vh.qty.setText(quantity.get(position));
           vh.pavailability.setText(cart.get(position).getProductAvailability());
           vh.pvariation.setText(cart.get(position).getVariationName()+"\n"+cart.get(position).getVariationOptionName()+"\n"+cart.get(position).getFieldName()+"\n"+cart.get(position).getFieldOption());

           Boolean res2 = db.getproductforwishlist(cart.get(position).getProductId());

           if(res2 == true)
           {
               vh.wishlistheart.setBackgroundResource(R.drawable.heartfilled);
               vh.wishlisttext.setText("Added to Wishlist");
           }
           vh.cartmovetowl.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {

                   if (! db.getproductforwishlist(cart.get(position).getProductId())) {
                       db.addRecordwishlist(String.valueOf(cart.get(position).getProductId()), prefConfig.readID());
                       Toast.makeText(getApplicationContext(), "Please wait adding item to wishlist.", Toast.LENGTH_SHORT).show();
                       Retrofit retrofit = new Retrofit.Builder()
                               .baseUrl(ROOT_URL) //Setting the Root URL
                               .addConverterFactory(GsonConverterFactory.create())
                               .build();
                       AddtowishlistAPI addtowishlistAPI = retrofit.create(AddtowishlistAPI.class);
                       Call<generalstatusmessagePOJO> call = addtowishlistAPI.getDetails(MainActivity.Tokendata, String.valueOf(cart.get(position).getProductId()), prefConfig.readID());
                       //   Toast.makeText(mContext, " id = "+pid.get(position), Toast.LENGTH_SHORT).show();
                       call.enqueue(new Callback<generalstatusmessagePOJO>() {
                           @Override
                           public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                               if (response.isSuccessful()) {
                                   int status = response.body().getStatus();
                                   String message = response.body().getMessage().toString();
                                   if (status == 1) {
                                       vh.wishlistheart.setBackgroundResource(R.drawable.heartfilled);
                                       vh.wishlisttext.setText("Added to Wishlist");
//                                View parentLayout = v.findViewById(android.R.id.content);
//                                Snackbar snackbar = Snackbar.make(parentLayout, "Added to wishlist", Snackbar.LENGTH_LONG);
//                                View sbView = snackbar.getView();
//                                sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                                snackbar.show();

                                   }
                               } else {
                                   Toast.makeText(getApplicationContext(), "" + response.message(), Toast.LENGTH_SHORT).show();
                               }
                           }

                           @Override
                           public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                           }
                       });

                   }
                   else {
                       Toast.makeText(CartActivity.this, "Item already added to wishlist.", Toast.LENGTH_SHORT).show();
                   }
               }
           });


//            DecimalFormat formatter = new DecimalFormat("#,###,###");
//            formatedproductprice = formatter.format(Math.round(Float.parseFloat(itemprodprice[position])));
//            vh.itemamount.setText("Rs "+formatedproductprice);
           Glide.with(getApplicationContext())
                   .load(productImage.get(position))// image url
                   // any placeholder to load at start
                   .into( vh.wlimage);
           vh.cartremove.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   progressBar7.setVisibility(View.VISIBLE);
                   View parentLayout = findViewById(android.R.id.content);
                   Snackbar snackbar = Snackbar.make(parentLayout, "Remove from cart", Snackbar.LENGTH_LONG);
                   View sbView = snackbar.getView();
                   sbView.setBackgroundColor(Color.rgb(161, 201, 19));
                   snackbar.show();

                   quantitycartId = cartId;
                   quantitycartquantity = quantity;
//                   Toast.makeText(CartActivity.this, "cart size = "+quantitycartquantity.size(), Toast.LENGTH_SHORT).show();
                    if (quantitycartquantity.size()==1)
                    {
                        cartfilledlayout.setVisibility(View.GONE);
                        cartemptylayout.setVisibility(View.VISIBLE);
                        progressBar7.setVisibility(View.GONE);
                        Checkout.setVisibility(View.GONE);
                        continueshop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                        Dashboard.cart_count = 0;
                        invalidateOptionsMenu();
                    }


                   carts = new JSONArray();
                   for (int i = 0;i<quantity.size();i++)
                   {

                       try {
                           JSONObject   cartjsonobj = new JSONObject();
                           cartjsonobj.put("cartId",quantitycartId.get(i));
                           cartjsonobj.put("quantity",Integer.toString((Integer.parseInt(quantitycartquantity.get(i)))));
                           carts.put(cartjsonobj);
                       } catch (JSONException e) {
                           e.printStackTrace();
                       }
                   }

                   try {
//                       Toast.makeText(CartActivity.this, ""+productId.get(position), Toast.LENGTH_SHORT).show();
                       db.deletecartproduct(productId.get(position));
                       mainobj.put("userId", prefConfig.readID());
                       mainobj.put("carts",carts);
                       Log.d("updatecartjson",""+mainobj);

                       Retrofit retrofit = new Retrofit.Builder()
                               .baseUrl(ROOT_URL) //Setting the Root URL
                               .addConverterFactory(GsonConverterFactory.create())
                               .build();
                       UpdatequantitiAPI updatequantitiAPI = retrofit.create(UpdatequantitiAPI.class);
                       RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), mainobj.toString());
                       Log.d("Body",""+body);
                       Call<generalstatusmessagePOJO> generalstatusmessagePOJOCall = updatequantitiAPI.getDetails(MainActivity.Tokendata,body);
                       generalstatusmessagePOJOCall.enqueue(new Callback<generalstatusmessagePOJO>() {
                           @Override
                           public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                               if (response.isSuccessful())
                               {
                                   int status = response.body().getStatus();
                                   if(status == 1)
                                   {
                                       progressBar7.setVisibility(View.GONE);

                                       Retrofit retrofit = new Retrofit.Builder()
                                               .baseUrl(ROOT_URL) //Setting the Root URL
                                               .addConverterFactory(GsonConverterFactory.create())
                                               .build();
                                       removecartitemAPI api = retrofit.create(removecartitemAPI.class);
                                       Call<Cart> calll = api.getDetails(MainActivity.Tokendata,cartId.get(position), prefConfig.readID());
                                       calll.enqueue(new Callback<Cart>() {
                                           @Override
                                           public void onResponse(Call<Cart> call, Response<Cart> response) {
                                               int status = response.body().getStatus();
                                               if (status == 1) {
                                                   cartforreciept = response.body().getCartDetail();
                                                   cart = response.body().getCartDetail();
                                                   cartId.clear();
                                                   productId.clear();
                                                   productImage.clear();
                                                   productTitle.clear();
                                                   productAvailability.clear();
                                                   quantity.clear();
                                                   productPrice.clear();
                                                   BrandName.clear();
                                                   progressBar7.setVisibility(View.GONE);
                                                   totalamount = 0;
                                                   quant = 0;
                                                   for(int i = 0 ;i<cart.size();i++)
                                                   {
                                                       cartId.add(cart.get(i).getCartId().toString());
                                                       productId.add(cart.get(i).getProductId().toString());
                                                       productImage.add(cart.get(i).getProductImage());
                                                       productTitle.add(cart.get(i).getProductTitle());
                                                       productAvailability.add(cart.get(i).getProductAvailability());
                                                       quantity.add(cart.get(i).getQuantity().toString());
                                                       productPrice.add(cart.get(i).getProductPrice());
                                                       BrandName.add(cart.get(i).getBrandName());
                                                       float amountfloat = Math.round(Float.parseFloat(productPrice.get(i)));

                                                       totalamount = (int) (totalamount+(amountfloat*Integer.parseInt(quantity.get(i))));
                                                       quant = quant+Integer.parseInt(quantity.get(i));
                                                   }

                                                   if(cart.size()==0)
                                                   {
                                                       progressBar7.setVisibility(View.GONE);
                                                       cartlistview.setAdapter(null);
//                                                       Toast.makeText(CartActivity.this, "cart is empty", Toast.LENGTH_SHORT).show();
                                                       Checkout.setVisibility(View.GONE);
                                                       notifyDataSetChanged();
                                                       notifyDataSetInvalidated();
                                                       cartfilledlayout.setVisibility(View.GONE);
                                                       cartemptylayout.setVisibility(View.VISIBLE);
                                                       progressBar7.setVisibility(View.GONE);
                                                       Checkout.setVisibility(View.GONE);
                                                       continueshop.setOnClickListener(new View.OnClickListener() {
                                                           @Override
                                                           public void onClick(View v) {
                                                               Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                                                               startActivity(intent);
                                                               finish();
                                                           }
                                                       });
                                                   }
                                                   else

                                                   {

                                                       cartlistview.setAdapter(null);
                                                       getcartAdapter2 getcartAdapter2 = new getcartAdapter2(getApplicationContext(),cart);
                                                       cartlistview.setAdapter(getcartAdapter2);
                                                       notifyDataSetChanged();
                                                       notifyDataSetInvalidated();}

//
//                                                  Toast.makeText(CartActivity.this, ""+quant, Toast.LENGTH_SHORT).show();

                                                   DecimalFormat formatter = new DecimalFormat("#,###,###");
                                                   String pricee = formatter.format(totalamount);
                                                   carttotalamount.setText("RS "+pricee);
                                                   carttotalitems.setText("Items "+quant);
                                                   if(quant>1)
                                                   {
                                                       cartitemstop.setText("My Cart ("+quant+" Items)");
                                                   }
                                                   else
                                                   {
                                                       cartitemstop.setText("My Cart ("+quant+" Item)");
                                                   }
                                                   Dashboard.cart_count = quant;
                                                   invalidateOptionsMenu();
                                               }
                                               else
                                               {
//                     ASS         Toast.makeText(CartActivity.this, "Network not responding", Toast.LENGTH_SHORT).show();
                                                    progressBar7.setVisibility(View.GONE);
                                                   quant = 0;
                                                   cartlistview.setAdapter(null);
                                                   carttotalamount.setText("RS 0"  );
                                                   carttotalitems.setText("Items 0" );
                                                   cartitemstop.setText("My Cart (0 Item)");
                                                   Dashboard.cart_count = quant;
                                                   invalidateOptionsMenu();
                                                   cartfilledlayout.setVisibility(View.GONE);
                                                   cartemptylayout.setVisibility(View.VISIBLE);
                                                   progressBar7.setVisibility(View.GONE);
                                                   Checkout.setVisibility(View.GONE);
                                                   continueshop.setOnClickListener(new View.OnClickListener() {
                                                       @Override
                                                       public void onClick(View v) {
                                                           Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                                                           startActivity(intent);
                                                           finish();
                                                       }
                                                   });
                                               }

                                           }

                                           @Override
                                           public void onFailure(Call<Cart> call, Throwable t) {

                                           }
                                       });
                                   }
                                   else
                                   {
//                                       Toast.makeText(CartActivity.this, "bbb"+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                       progressBar7.setVisibility(View.GONE);
                                       quant = 0;
                                       cartfilledlayout.setVisibility(View.GONE);
                                       cartemptylayout.setVisibility(View.VISIBLE);
                                       progressBar7.setVisibility(View.GONE);
                                       Checkout.setVisibility(View.GONE);
                                       continueshop.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                                               startActivity(intent);
                                               finish();
                                           }
                                       });
                                       Dashboard.cart_count = quant;
                                       invalidateOptionsMenu();
                                   }
                               }
                               else
                               {
//                                   Toast.makeText(CartActivity.this, "aaa"+response.message(), Toast.LENGTH_SHORT).show();
                                   cartfilledlayout.setVisibility(View.GONE);
                                   cartemptylayout.setVisibility(View.VISIBLE);
                                   progressBar7.setVisibility(View.GONE);
                                   Checkout.setVisibility(View.GONE);
                                   continueshop.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                                           startActivity(intent);
                                           finish();
                                       }
                                   });
                               }
                           }

                           @Override
                           public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {
                               cartfilledlayout.setVisibility(View.GONE);
                               cartemptylayout.setVisibility(View.VISIBLE);
                               progressBar7.setVisibility(View.GONE);
                               Checkout.setVisibility(View.GONE);
                               continueshop.setOnClickListener(new View.OnClickListener() {
                                   @Override
                                   public void onClick(View v) {
                                       Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                                       startActivity(intent);
                                       finish();
                                   }
                               });
                           }
                       });
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }







//                    Call<whishlist> whishlistremoveCall = api.getDetails(wishlistId.get(position),productId.get(position),"32997");
//                    whishlistremoveCall.enqueue(new Callback<whishlist>() {
//                        @Override
//                        public void onResponse(Call<whishlist> call, Response<whishlist> response) {
////                            Toast.makeText(WhishlistActivity.this, ""+response.body(), Toast.LENGTH_SHORT).show();
//                            wishlist = response.body().getWishlist();
//
//                            wishlistId.clear();
//                            brandName.clear();
//                            productTitle.clear();
//                            productTotalPrice.clear();
//                            productSalePrice.clear();
//                            productImageUrl.clear();
//                            productId.clear();
//                            progressBar.setVisibility(View.GONE);
//                            for(int i = 0 ; i < wishlist.size();i++)
//                            {
//                                wishlistId.add(wishlist.get(i).getWishlistId().toString());
//                                brandName.add( wishlist.get(i).getBrandName());
//                                productTitle.add( wishlist.get(i).getProductTitle());
//                                productTotalPrice.add( wishlist.get(i).getProductTotalPrice());
//                                productSalePrice.add(wishlist.get(i).getProductSalePrice());
//                                productImageUrl.add( wishlist.get(i).getProductImageUrl());
//                                productId.add( wishlist.get(i).getProductId().toString());
//
//                            }
//                            totalitem.setText("My Wishlist ("+brandName.size()+")");
//                            WhishlistActivity.getwishlistAdapter2 getwishlistAdapter2 = new WhishlistActivity.getwishlistAdapter2(getApplicationContext(),wishlist);
//                            whishlistview.setAdapter(getwishlistAdapter2);
//                            progressBar.setVisibility(View.GONE);
////            catproductgrid.setAdapter(dataImageAdapter);
//
//                            whishlistview.deferNotifyDataSetChanged();
//                        }
//
//                        @Override
//                        public void onFailure(Call<whishlist> call, Throwable t) {
//
//                        }
//                    });
               }
           });

//            vh.wlmovetocart.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    View parentLayout = findViewById(android.R.id.content);
//                    Snackbar snackbar = Snackbar.make(parentLayout, "Added to cart", Snackbar.LENGTH_LONG);
//                    View sbView = snackbar.getView();
//                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                    snackbar.show();
//                    onAddProduct(Integer.parseInt(productId.get(position)),"1",Dashboard.prefConfig.readID(),"0");
//                }
//            });

       vh.cartqtyadd.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {



                int currentqty = Integer.parseInt(vh.qty.getText().toString());
                currentqty = currentqty+1;
                vh.qty.setText(""+currentqty);

               quantitycartId = cartId;
               quantitycartquantity = quantity;
               quantitycartquantity.set(position,vh.qty.getText().toString());

               carts = new JSONArray();
               for (int i = 0;i<quantity.size();i++)
               {

                   try {
                       JSONObject   cartjsonobj = new JSONObject();
                       cartjsonobj.put("cartId",quantitycartId.get(i));
                       cartjsonobj.put("quantity",Integer.toString((Integer.parseInt(quantitycartquantity.get(i)))));
                       carts.put(cartjsonobj);
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
               }

               try {
                   mainobj.put("userId", prefConfig.readID());
                   mainobj.put("carts",carts);
                   Log.d("updatecartjson",""+mainobj);
               } catch (JSONException e) {
                   e.printStackTrace();
               }

                Dashboard.cart_count = Dashboard.cart_count+1;
                invalidateOptionsMenu();
               String str = carttotalamount.getText().toString();
               StringBuffer sb = new StringBuffer();
               for(int i=0; i<str.length(); i++)
               {
                   if(Character.isDigit(str.charAt(i)))
                       sb.append(str.charAt(i));
               }
               String a =  sb.toString();
//               Toast.makeText(CartActivity.this, ""+a, Toast.LENGTH_SHORT).show();
                int currenttotalamount = Integer.parseInt(a);
//                int currentitemtotal = Integer.parseInt(carttotalitems.getText().toString());
                currenttotalamount = currenttotalamount + Math.round(Float.parseFloat(productPrice.get(position)));
               DecimalFormat formatter = new DecimalFormat("#,###,###");
               String pricee = formatter.format(currenttotalamount);
                carttotalamount.setText("RS "+pricee);
               carttotalitems.setText(" items "+Dashboard.cart_count);

               if(Dashboard.cart_count>1)
               {
                   cartitemstop.setText("My Cart ("+Dashboard.cart_count+" Items)");
               }
               else
               {
                   cartitemstop.setText("My Cart ("+Dashboard.cart_count+" Item)");
               }
           }
       });
       vh.cartqtysub.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               quantitycartId = cartId;
               quantitycartquantity = quantity;

               int currentqty = Integer.parseInt(vh.qty.getText().toString());
               if(currentqty>1) {
                   currentqty = currentqty - 1;
                   vh.qty.setText("" + currentqty);
                   quantitycartquantity.set(position,vh.qty.getText().toString());


                   carts = new JSONArray();
                   for (int i = 0;i<quantity.size();i++)
                   {

                       try {
                           JSONObject   cartjsonobj = new JSONObject();
                           cartjsonobj.put("cartId",quantitycartId.get(i));
                           cartjsonobj.put("quantity",Integer.toString((Integer.parseInt(quantitycartquantity.get(i)))));
                           carts.put(cartjsonobj);
                       } catch (JSONException e) {
                           e.printStackTrace();
                       }
                   }

                   try {
                       mainobj.put("userId", prefConfig.readID());
                       mainobj.put("carts",carts);
                       Log.d("updatecartjson",""+mainobj);
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }

                   Dashboard.cart_count = Dashboard.cart_count - 1;
                   invalidateOptionsMenu();
                   String str = carttotalamount.getText().toString();
                   StringBuffer sb = new StringBuffer();
                   for(int i=0; i<str.length(); i++)
                   {
                       if(Character.isDigit(str.charAt(i)))
                           sb.append(str.charAt(i));
                   }
                   String a =  sb.toString();
                   int currenttotalamount = Integer.parseInt(a);
//                   int currentitemtotal = Integer.parseInt(carttotalitems.getText().toString());
                   currenttotalamount = currenttotalamount - Math.round(Float.parseFloat(productPrice.get(position)));
                   DecimalFormat formatter = new DecimalFormat("#,###,###");
                   String pricee = formatter.format(currenttotalamount);
                   carttotalamount.setText("RS "+pricee);
                   carttotalitems.setText(" items "+Dashboard.cart_count);
                   if(Dashboard.cart_count>1)
                   {
                       cartitemstop.setText("My Cart ("+Dashboard.cart_count+" Items)");
                   }
                   else
                   {
                       cartitemstop.setText("My Cart ("+Dashboard.cart_count+" Item)");
                   }

               }
               }
       });

       }
       else
       {
           vh.wlproductname.setText(productTitle.get(position));

           DecimalFormat formatter = new DecimalFormat("#,###,###");
           String pricee = formatter.format(Math.round(Float.parseFloat(productPrice.get(position))));
           vh.wlprice.setText("Rs "+pricee);
           vh.qty.setText(quantity.get(position));
           vh.wlbrand.setText(""+BrandName.get(position));

//            DecimalFormat formatter = new DecimalFormat("#,###,###");
//            formatedproductprice = formatter.format(Math.round(Float.parseFloat(itemprodprice[position])));
//            vh.itemamount.setText("Rs "+formatedproductprice);
           Glide.with(getApplicationContext())
                   .load(productImage.get(position))// image url
                   // any placeholder to load at start
                   .into( vh.wlimage);
           vh.cartremove.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   progressBar7.setVisibility(View.VISIBLE);
                   cartlistview.setAdapter(null);
                   View parentLayout = findViewById(android.R.id.content);
                   Snackbar snackbar = Snackbar.make(parentLayout, "Remove from cart", Snackbar.LENGTH_LONG);
                   View sbView = snackbar.getView();
                   sbView.setBackgroundColor(Color.rgb(161, 201, 19));
                   snackbar.show();
//                   Toast.makeText(CartActivity.this, ""+productId.get(position), Toast.LENGTH_SHORT).show();
                   db.deletecartproduct(productId.get(position));
                   progressBar7.setVisibility(View.GONE);
                   Cursor  cursor = db.getnotlogincartforDisplay();
//                   Toast.makeText(CartActivity.this, "count "+cursor.getCount(), Toast.LENGTH_SHORT).show();
                   if (cursor.getCount()!=0)
                    {
//                       Toast.makeText(getApplicationContext(), "inside cursur " + cursor.getCount(), Toast.LENGTH_SHORT).show();
                       productId.clear();
                       productImage.clear();
                       productTitle.clear();
                       quantity.clear();
                       productPrice.clear();
                       if (cursor.moveToFirst()) {
                           do {
                               //calling the method to save the unsynced name to MySQL
//                saveName(
//                        cursor.getInt(1),
//                        cursor.getInt(2),
//                        cursor.getInt(3),
//                        cursor.getInt(4),
//                        cursor.getInt(5)
//
//                );

                               productId.add(Integer.toString(cursor.getInt(1)));
                               productImage.add(cursor.getString(8));
                               productTitle.add(cursor.getString(6));
                               quantity.add(Integer.toString(cursor.getInt(2)));
                               productPrice.add(cursor.getString(7));
//                Toast.makeText(this, " this my "+productTitle, Toast.LENGTH_SHORT).show();
//                float amountfloat = Math.round(Float.parseFloat(productPrice.get(cursor.getPosition())));

//                totalamount = (int) (totalamount+Integer.parseInt(productPrice.get(cursor.getPosition()));

                           } while (cursor.moveToNext());
                           quant = 0;
                           quant = cursor.getCount();

//                           Toast.makeText(getApplicationContext(), "Data came", Toast.LENGTH_SHORT).show();

                           getcartAdapter2 getcartAdapter2 = new getcartAdapter2(getApplicationContext(), cart);
                           cartlistview.setAdapter(null);
                           cartlistview.setAdapter(getcartAdapter2);
//
//                           Toast.makeText(CartActivity.this, "" + quant, Toast.LENGTH_SHORT).show();
                           totalamount = 0 ;
                            for (int  i = 0 ; i <productPrice.size();i++)
                            {
//                                Toast.makeText(CartActivity.this, "prod price = "+productPrice.get(i), Toast.LENGTH_SHORT).show();
                                Float floatprice = Float.parseFloat(productPrice.get(i));

                                totalamount = totalamount+Math.round(floatprice);
                            }
                           DecimalFormat formatter = new DecimalFormat("#,###,###");
                           String pricee = formatter.format(totalamount);
                           carttotalamount.setText("RS "+pricee);
                           carttotalitems.setText("Items " + quant);
                           if(quant>1)
                           {
                               cartitemstop.setText("My Cart ("+quant+" Items)");
                           }
                           else
                           {
                               cartitemstop.setText("My Cart ("+quant+" Item)");
                           }
                           Dashboard.cart_count = quant;
                           invalidateOptionsMenu();
                       }
                   }
                   else
                   {
                       quant = 0;
                       cartlistview.setAdapter(null);
                       carttotalamount.setText("RS 0"  );
                       carttotalitems.setText("Items 0" );
                       cartitemstop.setText("My Cart (0 Item)");
                       Dashboard.cart_count = quant;
                       invalidateOptionsMenu();
                       cartfilledlayout.setVisibility(View.GONE);
                       cartemptylayout.setVisibility(View.VISIBLE);
                       progressBar7.setVisibility(View.GONE);
                       Checkout.setVisibility(View.GONE);
                       continueshop.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                               Intent intent = new Intent(getApplicationContext(),Dashboard.class);
                               startActivity(intent);
                               finish();
                           }
                       });
                   }

//                    Retrofit retrofit = new Retrofit.Builder()
//                            .baseUrl(ROOT_URL) //Setting the Root URL
//                            .addConverterFactory(GsonConverterFactory.create())
//                            .build();
//                    removewishlistAPI api = retrofit.create(removewishlistAPI.class);
//                    Call<whishlist> whishlistremoveCall = api.getDetails(wishlistId.get(position),productId.get(position),"32997");
//                    whishlistremoveCall.enqueue(new Callback<whishlist>() {
//                        @Override
//                        public void onResponse(Call<whishlist> call, Response<whishlist> response) {
////                            Toast.makeText(WhishlistActivity.this, ""+response.body(), Toast.LENGTH_SHORT).show();
//                            wishlist = response.body().getWishlist();
//
//                            wishlistId.clear();
//                            brandName.clear();
//                            productTitle.clear();
//                            productTotalPrice.clear();
//                            productSalePrice.clear();
//                            productImageUrl.clear();
//                            productId.clear();
//                            progressBar.setVisibility(View.GONE);
//                            for(int i = 0 ; i < wishlist.size();i++)
//                            {
//                                wishlistId.add(wishlist.get(i).getWishlistId().toString());
//                                brandName.add( wishlist.get(i).getBrandName());
//                                productTitle.add( wishlist.get(i).getProductTitle());
//                                productTotalPrice.add( wishlist.get(i).getProductTotalPrice());
//                                productSalePrice.add(wishlist.get(i).getProductSalePrice());
//                                productImageUrl.add( wishlist.get(i).getProductImageUrl());
//                                productId.add( wishlist.get(i).getProductId().toString());
//
//                            }
//                            totalitem.setText("My Wishlist ("+brandName.size()+")");
//                            WhishlistActivity.getwishlistAdapter2 getwishlistAdapter2 = new WhishlistActivity.getwishlistAdapter2(getApplicationContext(),wishlist);
//                            whishlistview.setAdapter(getwishlistAdapter2);
//                            progressBar.setVisibility(View.GONE);
////            catproductgrid.setAdapter(dataImageAdapter);
//
//                            whishlistview.deferNotifyDataSetChanged();
//                        }
//
//                        @Override
//                        public void onFailure(Call<whishlist> call, Throwable t) {
//
//                        }
//                    });
               }
           });
           vh.cartmovetowl.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Toast.makeText(CartActivity.this, "Kindly login first to make wishlist.", Toast.LENGTH_SHORT).show();
               }
           });
           vh.cartqtyadd.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   int currentqty = Integer.parseInt(vh.qty.getText().toString());
                   currentqty = currentqty+1;
                   db.updateQuantity(currentqty,Integer.parseInt(productId.get(position)));
                   vh.qty.setText(""+currentqty);
                   Dashboard.cart_count = Dashboard.cart_count+1;
                   invalidateOptionsMenu();
                   String str = carttotalamount.getText().toString();
                   StringBuffer sb = new StringBuffer();
                   for(int i=0; i<str.length(); i++)
                   {
                       if(Character.isDigit(str.charAt(i)))
                           sb.append(str.charAt(i));
                   }
                   String a =  sb.toString();
//               Toast.makeText(CartActivity.this, ""+a, Toast.LENGTH_SHORT).show();
                   int currenttotalamount = Integer.parseInt(a);
//                int currentitemtotal = Integer.parseInt(carttotalitems.getText().toString());
                   currenttotalamount = currenttotalamount + Math.round(Float.parseFloat(productPrice.get(position)));
                   DecimalFormat formatter = new DecimalFormat("#,###,###");
                   String pricee = formatter.format(currenttotalamount);
                   carttotalamount.setText("RS "+pricee);
                   carttotalitems.setText(" items "+Dashboard.cart_count);
                   if(Dashboard.cart_count>1)
                   {
                       cartitemstop.setText("My Cart ("+Dashboard.cart_count+" Items)");
                   }
                   else
                   {
                       cartitemstop.setText("My Cart ("+Dashboard.cart_count+" Item)");
                   }

               }
           });
           vh.cartqtysub.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {


                   int currentqty = Integer.parseInt(vh.qty.getText().toString());
                   if(currentqty>1) {
                       currentqty = currentqty - 1;
                       db.updateQuantity(currentqty,Integer.parseInt(productId.get(position)));
                       vh.qty.setText("" + currentqty);

                       Dashboard.cart_count = Dashboard.cart_count - 1;
                       invalidateOptionsMenu();
                       String str = carttotalamount.getText().toString();
                       StringBuffer sb = new StringBuffer();
                       for(int i=0; i<str.length(); i++)
                       {
                           if(Character.isDigit(str.charAt(i)))
                               sb.append(str.charAt(i));
                       }
                       String a =  sb.toString();
                       int currenttotalamount = Integer.parseInt(a);
//                   int currentitemtotal = Integer.parseInt(carttotalitems.getText().toString());
                       currenttotalamount = currenttotalamount - Math.round(Float.parseFloat(productPrice.get(position)));
                       DecimalFormat formatter = new DecimalFormat("#,###,###");
                       String pricee = formatter.format(currenttotalamount);
                       carttotalamount.setText("RS "+pricee);
                       carttotalitems.setText(" items "+Dashboard.cart_count);
                       if(Dashboard.cart_count>1)
                       {
                           cartitemstop.setText("My Cart ("+Dashboard.cart_count+" Items)");
                       }
                       else
                       {
                           cartitemstop.setText("My Cart ("+Dashboard.cart_count+" Item)");
                       }

                   }
               }
           });



//            vh.wlmovetocart.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    View parentLayout = findViewById(android.R.id.content);
//                    Snackbar snackbar = Snackbar.make(parentLayout, "Added to cart", Snackbar.LENGTH_LONG);
//                    View sbView = snackbar.getView();
//                    sbView.setBackgroundColor(Color.rgb(161, 201, 19));
//                    snackbar.show();
//                    onAddProduct(Integer.parseInt(productId.get(position)),"1",Dashboard.prefConfig.readID(),"0");
//                }
//            });
       }
            return v;

        };
    }
    static class ViewHolder2
    {
        // ImageView imageView;
        TextView wlbrand,wlproductname,wlprice,qty,pvariation,pavailability,wishlisttext;
        ImageView wlimage;
        androidx.constraintlayout.widget.ConstraintLayout cartmovetowl,cartremove;
        androidx.constraintlayout.widget.ConstraintLayout wishlistlayout,cartlayout;
       Spinner spinnercartqty;
       ImageView cartqtyadd,cartqtysub,wishlistheart;




    }


}

package com.homeshopping.admin.homeshoping10.Activity;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.homeshopping.admin.homeshoping10.Class.AnalyticsApplication;
import com.homeshopping.admin.homeshoping10.Class.Converter;
import com.homeshopping.admin.homeshoping10.Interfaces.UpdateUserDetailApi;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;
import com.homeshopping.admin.homeshoping10.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.homeshopping.admin.homeshoping10.Activity.Dashboard.prefConfig;
import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;

public class ProfileActivity extends AppCompatActivity {
EditText proffname,proflastname,profpno;
Button profsave;
ProgressBar progressBar13;
Boolean flag =true;
    private Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle("My Profile");
        proffname = findViewById(R.id.proffname);
        proflastname = findViewById(R.id.proflastname);
        profpno = findViewById(R.id.profpno);
        profsave = findViewById(R.id.profsave);
        progressBar13 = findViewById(R.id.progressBar13);
        progressBar13.setVisibility(View.GONE);

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //String name = this.getClass().getSimpleName();
        mTracker.setScreenName("Profile Activity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Homeshopping App User")
//                .setAction("User id")
//                .setLabel(""+prefConfig.readID())
//                .setValue(1)
                .build());

        proffname.setText(prefConfig.readName());
        proflastname.setText(prefConfig.readlastname());
        profpno.setText(prefConfig.readpno());


        profsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar13.setVisibility(View.VISIBLE);
                String  firstame = proffname.getText().toString().trim();
                String  lastname = proflastname.getText().toString().trim();
                String  phonenumber = profpno.getText().toString().trim();
                flag = true;
                if(firstame.isEmpty())
                {
                    progressBar13.setVisibility(View.GONE);
                    proffname.setError("Enter firstname");
                    proffname.setBackgroundResource(R.drawable.costraintborderred);
                    proffname.requestFocus();
                    flag = false;
                }
                else  if(lastname.isEmpty())
                {
                    progressBar13.setVisibility(View.GONE);
                    proflastname.setError("Enter last name");
                    proflastname.setBackgroundResource(R.drawable.costraintborderred);
                    proflastname.requestFocus();
                    flag = false;
                }
                else  if(phonenumber.isEmpty())
                {
                    progressBar13.setVisibility(View.GONE);
                    profpno.setError("Enter phone number");
                    profpno.setBackgroundResource(R.drawable.costraintborderred);
                    profpno.requestFocus();
                    flag = false;
                }
                if(flag)
                {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                    UpdateUserDetailApi updateUserDetailApi = retrofit.create(UpdateUserDetailApi.class);
                    Call<generalstatusmessagePOJO> call = updateUserDetailApi.getDetails(MainActivity.Tokendata, prefConfig.readID(),firstame,lastname, prefConfig.reademail(),phonenumber);
                    call.enqueue(new Callback<generalstatusmessagePOJO>() {
                        @Override
                        public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                            if (response.isSuccessful())
                            {
                                int status = response.body().getStatus();
                                if(status == 1) {
                                    progressBar13.setVisibility(View.GONE);
                                    Toast.makeText(ProfileActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    prefConfig.writeName(prefConfig.readID(),firstame,lastname, prefConfig.reademail(),phonenumber, prefConfig.readcity());

                                }
                                else
                                {
                                    progressBar13.setVisibility(View.GONE);
                                    Toast.makeText(ProfileActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                        }
                    });
                }


            }
        });
    }
        @Override
        public void onBackPressed () {
            super.onBackPressed();
            finish();
        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.normalmenu, menu);
        MenuItem menuItem = menu.findItem(R.id.refresh2);
        menuItem.setIcon(Converter.convertLayoutToImage(getApplicationContext(),Dashboard.cart_count,R.drawable.ttcart));
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.search: {
                Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.refresh2: {
                refresh();
                break;
            }
            case android.R.id.home:{
                onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }



    public void refresh()
    {

        Intent intent = new Intent(getApplicationContext(),CartActivity.class);
        startActivity(intent);
        finish();

    }


    public void showDialog1()
    {
       AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.personalinfoeditcustomlayout, null);
        dialogBuilder.setView(dialogView);
        View view = null;
        dialogBuilder.setTitle("Edit Personal Information ");
        dialogBuilder.setCancelable(true);
        final  AlertDialog b = dialogBuilder.create();

        b.show();

    }


    public void showDialog2()
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.passwordresetcustomlayout, null);
        dialogBuilder.setView(dialogView);
        View view = null;
        dialogBuilder.setTitle("Change Password ");
        dialogBuilder.setCancelable(true);
        final  AlertDialog b = dialogBuilder.create();

        b.show();

    }
    }


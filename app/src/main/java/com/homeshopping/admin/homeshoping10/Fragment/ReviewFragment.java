package com.homeshopping.admin.homeshoping10.Fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.homeshopping.admin.homeshoping10.Activity.MainActivity;
import com.homeshopping.admin.homeshoping10.Activity.ProductDetailActivity;
import com.homeshopping.admin.homeshoping10.Interfaces.ReviewApi;
import com.homeshopping.admin.homeshoping10.Interfaces.ReviewPostApi;
import com.homeshopping.admin.homeshoping10.POJO.generalstatusmessagePOJO;
import com.homeshopping.admin.homeshoping10.POJO.ReviewRatingPOJO.Detail;
import com.homeshopping.admin.homeshoping10.POJO.ReviewRatingPOJO.ReviewExample;
import com.homeshopping.admin.homeshoping10.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.homeshopping.admin.homeshoping10.Fragment.ScrollingSearchExampleFragment.ROOT_URL;


public class ReviewFragment extends Fragment {

    public ReviewFragment() {
        // Required empty public constructor
    }
    ListView reviewlistview;
    TextView noreview;
    Button writeareview;

    String[] reviewtitle ,username,review;
    int[] reviewrating;
    long[] reviewdate;
    List<Detail> reviewdetail = new ArrayList<>();
    String[] qty = {"1","2","3","4","5"};
    String Stringreviewtitle = "";
    String Stringreview = "";
    String Stringreviewname = "";
    Boolean flag = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_review, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        reviewlistview = view.findViewById(R.id.reviewlistview);
        writeareview = view.findViewById(R.id.writeareview);
        noreview = view.findViewById(R.id.textView26);

        writeareview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(ROOT_URL) //Setting the Root URL
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ReviewApi reviewApi = retrofit2.create(ReviewApi.class);
        Call<ReviewExample> reviewExampleCall = reviewApi.getDetails(MainActivity.Tokendata,ProductDetailActivity.pid);
        reviewExampleCall.enqueue(new Callback<ReviewExample>() {
            @Override
            public void onResponse(Call<ReviewExample> call, Response<ReviewExample> response) {
                int status = response.body().getStatus();
                if(status == 1)
                {
                    noreview.setVisibility(View.GONE);
                         reviewdetail = response.body().getDetails();
                    reviewtitle = new String[reviewdetail.size()];
                    reviewrating= new int[reviewdetail.size()];
                    username= new String[reviewdetail.size()];
                    review= new String[reviewdetail.size()];
                    reviewdate= new long[reviewdetail.size()];
                    for (int i = 0 ; i <reviewdetail.size();i++)
                    {
                        reviewtitle[i] = reviewdetail.get(i).getReviewTitle();
                        reviewrating[i] = reviewdetail.get(i).getReviewRating();
                        username[i] = reviewdetail.get(i).getReviewFrom();
                        review[i] = reviewdetail.get(i).getReviewText();
                        reviewdate[i] = reviewdetail.get(i).getReviewDate();
                    }
                    MyListAdapter adapter = new MyListAdapter(getActivity(), reviewtitle, reviewrating, username, review,reviewdate);
                    reviewlistview.setAdapter(adapter);
                }
                else
                {
                    reviewlistview.setVisibility(View.GONE);
                    noreview.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ReviewExample> call, Throwable t) {

            }
        });

//        ListAdapter listadp = reviewlistview.getAdapter();
//        if (listadp != null) {
//            int totalHeight = 0;
//            for (int i = 0; i < listadp.getCount(); i++) {
//                View listItem = listadp.getView(i, null, reviewlistview);
//                listItem.measure(0, 0);
//                totalHeight += listItem.getMeasuredHeight();
//            }
//            ViewGroup.LayoutParams params = reviewlistview.getLayoutParams();
//            params.height = totalHeight + (reviewlistview.getDividerHeight() * (listadp.getCount() - 1));
//            reviewlistview.setLayoutParams(params);
//            reviewlistview.requestLayout();
//        }
    }

    public class MyListAdapter extends ArrayAdapter<String> {

        private final Activity context;
        private final String[] reviewtitlee;
        private final int[] reviewratingg;
        private final String[] userinfoo;
        private final String[] revieww;
        private final long[] reviewdate;
        public MyListAdapter(Activity context, String[] reviewtitle
                ,int[] reviewrating, String[] userinfo,String[] review ,long[] reviewdate ) {
            super(context, R.layout.reviewcustomitem, reviewtitle);


            this.context=context;
            this.reviewtitlee=reviewtitle;
            this.reviewratingg=reviewrating;
            this.userinfoo=userinfo;
            this.revieww=review;
            this.reviewdate = reviewdate;

        }

        public View getView(int position,View view,ViewGroup parent) {
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.reviewcustomitem, null,true);

            TextView reviewtitle = (TextView) rowView.findViewById(R.id.reviewtitle);
            TextView userinfo = (TextView) rowView.findViewById(R.id.userinfo);
            TextView review = (TextView) rowView.findViewById(R.id.review);
            ImageView ratingimage = (ImageView) rowView.findViewById(R.id.ratingimage);


            reviewtitle.setText(reviewtitlee[position]);
            userinfo.setText("Posted by "+userinfoo[position]+" on "+reviewdate[position]);
            review.setText(revieww[position]);
            int rate = reviewratingg[position];
            if(rate == 0 )
            {
                ratingimage.setBackgroundResource(R.drawable.star0);
            }
            else if(rate == 1 )
            {
                ratingimage.setBackgroundResource(R.drawable.star1);
            }
            else if(rate == 2 )
            {
                ratingimage.setBackgroundResource(R.drawable.star2);
            }
            else if(rate == 3 )
            {
                ratingimage.setBackgroundResource(R.drawable.star3);
            }
            else if(rate == 4 )
            {
                ratingimage.setBackgroundResource(R.drawable.star4);
            }
            else if(rate == 5 )
            {
                ratingimage.setBackgroundResource(R.drawable.star5);
            }

            return rowView;

        };
    }

    public void showDialog()
    {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.givereviewcustomlayout, null);
        dialogBuilder.setView(dialogView);
        View view = null;
        dialogBuilder.setTitle("Write a Review ");
        dialogBuilder.setCancelable(true);
        final  AlertDialog b = dialogBuilder.create();
        Button Sendareview;
        final TextView editTexttitle,editTextreview,editTextreviewname;
        editTexttitle = dialogView.findViewById(R.id.editTexttitle);
        editTextreview = dialogView.findViewById(R.id.editTextreview);
        editTextreviewname = dialogView.findViewById(R.id.editTextreviewname);
        Sendareview = dialogView.findViewById(R.id.button11);
        final Spinner qtyespinner = dialogView.findViewById(R.id.ratingspinner);

        Sendareview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Stringreviewtitle = editTexttitle.getText().toString();
                Stringreview = editTextreview.getText().toString();
                Stringreviewname = editTextreviewname.getText().toString();
                flag = true;
                if(Stringreviewtitle.isEmpty())
                {
                    editTexttitle.setError("Kindly give Review tittle");
                    editTexttitle.requestFocus();
                    flag = false;
                }

                if(Stringreview.isEmpty())
                {
                    editTextreview.setError("Kindly give Review");
                    editTextreview.requestFocus();
                    flag = false;
                }

                if(flag) {

                    Retrofit retrofit2 = new Retrofit.Builder()
                            .baseUrl(ROOT_URL) //Setting the Root URL
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    ReviewPostApi reviewPostApi = retrofit2.create(ReviewPostApi.class);
                    Call<generalstatusmessagePOJO> stringCall = reviewPostApi.getDetails(MainActivity.Tokendata,ProductDetailActivity.pid,editTextreviewname.getText().toString(),qtyespinner.getSelectedItem().toString()
                            ,Stringreview,Stringreviewtitle);
                    stringCall.enqueue(new Callback<generalstatusmessagePOJO>() {
                        @Override
                        public void onResponse(Call<generalstatusmessagePOJO> call, Response<generalstatusmessagePOJO> response) {
                            int status = response.body().getStatus();
                            if(status ==1)
                            {
                                Snackbar snackbar = Snackbar.make(getView(), ""+response.body().getMessage(), Snackbar.LENGTH_LONG);
                                View sbView = snackbar.getView();
                                sbView.setBackgroundColor(Color.rgb(161, 201, 19));
                                snackbar.show();
                                b.dismiss();
                            }
                            else
                            {
                                Snackbar snackbar = Snackbar.make(getView(), "Review not Submited", Snackbar.LENGTH_LONG);
                                View sbView = snackbar.getView();
                                sbView.setBackgroundColor(Color.rgb(161, 201, 19));
                                snackbar.show();
                                b.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<generalstatusmessagePOJO> call, Throwable t) {

                        }
                    });
                }

            }
        });

        ArrayAdapter aa = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_item,qty);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        qtyespinner.setAdapter(aa);


        b.show();
    }


}
package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.CategoryProductPOJO.ProductExample;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface CategoryFilterProduct {
    @POST("products/getProductbyFilters")
    Call<ProductExample> getDetails(@Header("data") String data, @Body RequestBody body );
}

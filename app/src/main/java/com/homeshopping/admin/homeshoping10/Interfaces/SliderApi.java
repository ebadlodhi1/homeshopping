package com.homeshopping.admin.homeshoping10.Interfaces;

import com.homeshopping.admin.homeshoping10.POJO.SliderPOJO.SliderExample;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface SliderApi {
    @GET("home/getSlider")
    Call<SliderExample> getDetails(@Header("data") String data, @Query("device") String device);

}
